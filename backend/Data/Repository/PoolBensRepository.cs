﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;
using System.Data;

namespace Data.Repository
{
    public class PoolBensRepository : BaseRepository, IPoolBensRepository
    {
        public Domain.Entities._2BTrust.PoolBens InsertPoolBens(Domain.Entities._2BTrust.PoolBens con)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO [financ].[PoolBens] ");
                    strInsert.Append("       ([IdContrato] ");
                    strInsert.Append("       ,[Nome] ");
                    strInsert.Append("       ,[DtInicio] ");
                    strInsert.Append("       ,[DtVigencia] ");
                    strInsert.Append("       ,[IdStatus] ");
                    strInsert.Append("       ,[IdUsuarioInclusao] ");
                    strInsert.Append("       ,[DtInclusao], iUsarLogoApp, PathLocationIIS, PathLocation, PathLocationIISLogoMaior, PathLocationIISLogoMenor) ");
                    strInsert.Append(" VALUES ");
                    strInsert.Append("       (@IdContrato ");
                    strInsert.Append("       ,@Nome ");
                    strInsert.Append("       ,@DtInicio ");
                    strInsert.Append("       ,@DtVigencia ");
                    strInsert.Append("       ,@IdStatus ");
                    strInsert.Append("       ,@IdUsuarioInclusao ");
                    strInsert.Append("       ,getDate() , @iUsarLogoApp, @PathLocationIIS, @PathLocation, @PathLocationIISLogoMaior, @PathLocationIISLogoMenor) SELECT IDENT_CURRENT('[financ].[PoolBens]') AS [IDENT_CURRENT]");

                    var idPoolBens = _db.Query<int>(strInsert.ToString(),
                        new
                        {
                            IdContrato = con.IdContrato,
                            Nome = con.Nome,
                            DtInicio = con.DtInicio,
                            DtVigencia = con.DtVigencia,
                            IdStatus = con.IdStatus,
                            IdUsuarioInclusao = con.IdUsuarioInclusao,
                            iUsarLogoApp = con.iUsarLogoApp,
                            PathLocationIIS= con.PathLocationIIS,
                            PathLocation = con.PathLocation,
                            PathLocationIISLogoMaior = con.PathLocationIISLogoMaior,
                            PathLocationIISLogoMenor = con.PathLocationIISLogoMenor

                        }).FirstOrDefault();

                    con.Id = idPoolBens;

                    return con;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "InsertPoolBens");

                return null;
            }
        }

        public Domain.Entities._2BTrust.PoolBens UpdatePoolBens(Domain.Entities._2BTrust.PoolBens con)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" UPDATE [financ].[PoolBens] ");
                    strUpdate.Append("    SET [IdContrato] = @IdContrato ");
                    strUpdate.Append("      ,[Nome] = @Nome ");
                    strUpdate.Append("      ,[Cotas] = @Cotas ");
                    strUpdate.Append("      ,[DtInicio] = @DtInicio ");
                    strUpdate.Append("      ,[DtVigencia] = @DtVigencia ");
                    strUpdate.Append("      ,[IdStatus] = @IdStatus ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate() ");

                    strUpdate.Append("      ,[iUsarLogoApp] = @iUsarLogoApp ");
                    strUpdate.Append("      ,[PathLocationIIS] = @PathLocationIIS  ");
                    strUpdate.Append("      ,[PathLocation] =@PathLocation  ");
                    strUpdate.Append("      ,[PathLocationIISLogoMaior] =@PathLocationIISLogoMaior  ");
                    strUpdate.Append("      ,[PathLocationIISLogoMenor] =@PathLocationIISLogoMenor ");
                    strUpdate.Append(" WHERE  Id = " + con.Id);

                    _db.Execute(strUpdate.ToString(),
                      new
                      {
                          IdContrato = con.IdContrato,
                          Nome = con.Nome,
                          Cotas = con.Cotas,
                          DtInicio = con.DtInicio,
                          DtVigencia = con.DtVigencia,
                          IdStatus = con.IdStatus,
                          IdUsuarioAlteracao = con.IdUsuarioAlteracao,

                          iUsarLogoApp = con.iUsarLogoApp,
                          PathLocationIIS = con.PathLocationIIS,
                          PathLocation = con.PathLocation,
                          PathLocationIISLogoMaior = con.PathLocationIISLogoMaior,
                          PathLocationIISLogoMenor = con.PathLocationIISLogoMenor,
                          Id = con.Id
                      });



                    return con;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "UpdatePoolBens");

                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.PoolBens> GetListPoolBens()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetList = new StringBuilder();

                    strGetList.Append(" SELECT P.[Id] ");
                    strGetList.Append("       ,P.[IdContrato] ");
                    strGetList.Append("       ,P.[Nome] ");
                    strGetList.Append("       ,P.[Cotas] ");
                    strGetList.Append("       ,P.[DtInicio] ");
                    strGetList.Append("       ,P.[DtVigencia] ");
                    strGetList.Append("       ,P.[IdStatus] ");
                    strGetList.Append("       ,P.[IdUsuarioInclusao] ");
                    strGetList.Append("       ,P.[DtInclusao] ");
                    strGetList.Append("       ,P.[IdUsuarioAlteracao] ");
                    strGetList.Append("       ,P.[DtAlteracao] ");
                    strGetList.Append("       ,P.[PathLocation] ");
                    strGetList.Append("       ,P.[PathLocationIIS] ");
                    strGetList.Append("       ,P.[PathLocationIISLogoMenor]");
                    strGetList.Append("       ,P.[PathLocationIISLogoMaior]");
                    strGetList.Append("       ,P.[iUsarLogoApp]");
                    strGetList.Append("       ,C.[Id] ");
                    strGetList.Append("       ,C.[Codigo] ");
                    strGetList.Append("       ,C.[Descricao] ");
                    strGetList.Append("       ,C.[DtInicio] ");
                    strGetList.Append("       ,C.[DtVigencia] ");
                    strGetList.Append("       ,C.[IdStatus] ");
                    strGetList.Append("       ,C.[IdUsuarioInclusao] ");
                    strGetList.Append("       ,C.[DtInclusao] ");
                    strGetList.Append("       ,C.[IdUsuarioAlteracao] ");
                    strGetList.Append("       ,C.[DtAlteracao] ");
                    strGetList.Append(" FROM [financ].[PoolBens] P Inner join [financ].[Contratos] C on C.Id = P.IdContrato  order by Nome");

                    return _db.Query<PoolBens, Contratos, PoolBens>(strGetList.ToString(), (P, C)
                        =>
                        {
                            P._PoolsBensRentabilidadePerformance = GetListPoolsBensRentabilidadePerformance(P.Id).ToList();
                            P._Contrato = C;
                            return P;

                        }, splitOn: "Id").AsEnumerable();
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "GetListPoolBens");

                return null;
            }
        }

        public Domain.Entities._2BTrust.PoolBens GetPoolBensById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetList = new StringBuilder();

                    strGetList.Append(" SELECT [Id] ");
                    strGetList.Append("     ,[IdContrato] ");
                    strGetList.Append("     ,[Nome] "); ;
                    strGetList.Append("     ,[Cotas] ");
                    strGetList.Append("     ,[DtInicio] ");
                    strGetList.Append("     ,[DtVigencia] ");
                    strGetList.Append("     ,[IdStatus] ");
                    strGetList.Append("     ,[IdUsuarioInclusao] ");
                    strGetList.Append("     ,[DtInclusao] ");
                    strGetList.Append("     ,[IdUsuarioAlteracao] ");
                    strGetList.Append("     ,[DtAlteracao] ");
                    strGetList.Append("      ,[PathLocation] ");
                    strGetList.Append("      ,[PathLocationIIS] ");
                    strGetList.Append("      ,[PathLocationIISLogoMenor]");
                    strGetList.Append("      ,[PathLocationIISLogoMaior]");
                    strGetList.Append("      ,[iUsarLogoApp]");
                    strGetList.Append(" FROM [financ].[PoolBens] where Id = " + id);

                    return _db.Query<PoolBens>(strGetList.ToString(), new { id = id }).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "GetPoolBensById");

                return null;
            }
        }


        public IEnumerable<PoolBens> GetListPoolParaInvestimento()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {


                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" select   ");
                    strGetList.Append("  PB.[Id]");
                    strGetList.Append(" ,PB.[IdContrato]");
                    strGetList.Append(" ,PB.[Nome]");
                    strGetList.Append(" ,PB.[Cotas]");
                    strGetList.Append(" ,PB.[DtInicio]");
                    strGetList.Append(" ,PB.[DtVigencia]");
                    strGetList.Append(" ,PB.[IdStatus]");
                    strGetList.Append(" ,PB.[IdUsuarioInclusao]");
                    strGetList.Append(" ,PB.[DtInclusao]");
                    strGetList.Append(" ,PB.[IdUsuarioAlteracao]");
                    strGetList.Append(" ,PB.[DtAlteracao]"); 
                    strGetList.Append("      ,[PathLocation] ");
                    strGetList.Append("      ,[PathLocationIIS] ");
                    strGetList.Append("      ,[PathLocationIISLogoMenor]");
                    strGetList.Append("      ,[PathLocationIISLogoMaior]");
                    strGetList.Append("      ,[iUsarLogoApp]");
                    strGetList.Append(" ,(PB.[Cotas] - ISNULL((select SUM(Cota) from [financ].[PoolBensPessoas] where IdStatus = 1  and  [IdPoolBem] = PB.Id ),0)) as CotasDisponiveis");
                    strGetList.Append(" ,B.[Id]");
                    strGetList.Append(" ,B.[IdPool]");
                    strGetList.Append(" ,B.[Nome]");
                    strGetList.Append(" ,B.[performance]");
                    strGetList.Append(" , (B.[CapitalInvestido] + (select ISNULL(SUM(Valor),0) from dbo.BensGastos  where IdBem = B.id and FlagAtivo = 1))  as CapitalInvestido ");
                    strGetList.Append(" ,B.[IdStatus]");
                    strGetList.Append(" ,B.[IdUsuarioInclusao]");
                    strGetList.Append(" ,B.[DtInclusao]");
                    strGetList.Append(" ,B.[IdUsuarioAlteracao]");
                    strGetList.Append(" ,B.[DtAlteracao]");
                    strGetList.Append(" 	from [financ].[PoolBens] PB");
                    strGetList.Append(" 		inner join [financ].[Bens] B on B.IdPool = Pb.Id ");
                    strGetList.Append(" 	where PB.IdStatus = 1 and B.IdStatus =1 order by PB.Nome");

                    var lookup = new Dictionary<int, PoolBens>();
                    _db.Query<PoolBens, Bens, PoolBens>(strGetList.ToString(),
                        (PB, B) =>
                        {
                            PoolBens activeParent;
                            if (!lookup.TryGetValue(PB.Id, out activeParent))
                            {
                                activeParent = PB;
                                lookup.Add(activeParent.Id, activeParent);
                            }

                            //TODO: if you need to check for duplicates or null do so here
                            if (activeParent._Bens == null) activeParent._Bens = new List<Bens>();
                            activeParent._Bens.Add(B);

                            activeParent._PoolsBensRentabilidadePerformance = GetListPoolsBensRentabilidadePerformance(activeParent.Id).ToList();
                            return activeParent;

                        }, splitOn: "Id");


                    foreach (var item in lookup.Values)
                    {
                        foreach (var item2 in item._Bens)
                        {
                            item2._Imagens = GetListImagens(item2.Id);
                        }
                    }

                    return lookup.Values;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "GetListPoolParaInvestimento");

                return null;
            }
        }



        public PoolBens GetDetailsPoolParaInvestimentoById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {


                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" select   ");
                    strGetList.Append("  PB.[Id]");
                    strGetList.Append(" ,PB.[IdContrato]");
                    strGetList.Append(" ,PB.[Nome]");
                    strGetList.Append(" ,PB.[Cotas]");
                    strGetList.Append(" ,(PB.[Cotas] - ISNULL((select SUM(Cota) from [financ].[PoolBensPessoas] where IdStatus = 1  and  [IdPoolBem] = PB.Id ),0)) as CotasDisponiveis");
                    strGetList.Append(" ,PB.[DtInicio]");
                    strGetList.Append(" ,PB.[DtVigencia]");
                    strGetList.Append(" ,PB.[IdStatus]");
                    strGetList.Append(" ,PB.[IdUsuarioInclusao]");
                    strGetList.Append(" ,PB.[DtInclusao]");
                    strGetList.Append(" ,PB.[IdUsuarioAlteracao]");
                    strGetList.Append(" ,PB.[DtAlteracao]");

                    strGetList.Append("      ,[PathLocation] ");
                    strGetList.Append("      ,[PathLocationIIS] ");
                    strGetList.Append("      ,[PathLocationIISLogoMenor]");
                    strGetList.Append("      ,[PathLocationIISLogoMaior]");
                    strGetList.Append("      ,[iUsarLogoApp]");
                    strGetList.Append(" ,B.[Id]");
                    strGetList.Append(" ,B.[IdPool]");
                    strGetList.Append(" ,B.[Nome]");
                    strGetList.Append(" ,B.[Descritivo]");
                    strGetList.Append(" ,B.[performance]");
                    strGetList.Append(" ,(B.[CapitalInvestido] + (select ISNULL(SUM(Valor),0) from dbo.BensGastos  where IdBem = B.id and FlagAtivo = 1))  as CapitalInvestido");
                    strGetList.Append(" ,B.[IdStatus]");
                    strGetList.Append(" ,B.[IdUsuarioInclusao]");
                    strGetList.Append(" ,B.[DtInclusao]");
                    strGetList.Append(" ,B.[IdUsuarioAlteracao]");
                    strGetList.Append(" ,B.[DtAlteracao]");
                    strGetList.Append(" ,R.[Id] ");
                    strGetList.Append(" ,R.[IdBem] ");
                    strGetList.Append(" ,R.[DtInicio]");
                    strGetList.Append(" ,R.[DtFim]");
                    strGetList.Append(" ,R.[LucroLiquido]");
                    strGetList.Append(" ,R.[CapitalInvestido]");
                    strGetList.Append(" ,R.[Valor]");
                    strGetList.Append(" ,R.[IdUsuarioInclusao]");
                    strGetList.Append(" ,R.[DtInclusao]");
                    strGetList.Append(" ,R.[IdUsuarioAlteracao]");
                    strGetList.Append(" ,R.[DtAlteracao]");
                    strGetList.Append(" 	from [financ].[PoolBens] PB");
                    strGetList.Append(" 		inner join [financ].[Bens] B on B.IdPool = Pb.Id and B.IdStatus = 1");
                    strGetList.Append(" 		left join [financ].[Rentabilidade] R on R.IdBem = B.Id ");
                    strGetList.Append(" 	where PB.Id =" + id + " order by PB.Nome ");

                    var lookup = new Dictionary<int, PoolBens>();
                    _db.Query<PoolBens, Bens, Rentabilidade, PoolBens>(strGetList.ToString(),
                        (PB, B, R) =>
                        {
                            PoolBens activeParent;
                            if (!lookup.TryGetValue(PB.Id, out activeParent))
                            {
                                activeParent = PB;
                                lookup.Add(activeParent.Id, activeParent);
                            }

                            //TODO: if you need to check for duplicates or null do so here
                            if (activeParent._Bens == null) activeParent._Bens = new List<Bens>();
                            B._Rentabilidade = R;
                            activeParent._Bens.Add(B);

                            return activeParent;

                        }, splitOn: "Id");

                    foreach (var item in lookup.Values)
                    {
                        foreach (var item2 in item._Bens)
                        {
                            item2._Imagens = GetListImagens(item2.Id);
                        }
                    }


                    return lookup.Values.FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "GetDetailsPoolParaInvestimentoById");

                return null;
            }
        }

        private IEnumerable<BensImagens> GetListImagens(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetImanges = new StringBuilder();
                    strGetImanges.Append(" SELECT [Id] ");
                    strGetImanges.Append("      ,[IdBem] ");
                    strGetImanges.Append("      ,[PathLocation] ");
                    strGetImanges.Append("      ,[PathLocationIIS] ");
                    strGetImanges.Append("      ,[PathLocationIISLogoMenor]");
                    strGetImanges.Append("      ,[PathLocationIISLogoMaior]");
                    strGetImanges.Append("      ,[FlagAtivo] ");
                    strGetImanges.Append("      ,[IdUsuarioInclusao] ");
                    strGetImanges.Append("      ,[DtInclusao] ");
                    strGetImanges.Append("      ,[IdUsuarioAlteracao] ");
                    strGetImanges.Append("      ,[DtAlteracao] ");
                    strGetImanges.Append("  FROM [financ].[BensImagem] where IdBem = @idBem and FlagAtivo =1");

                    return _db.Query<BensImagens>(strGetImanges.ToString(), new { idBem = idBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetListImagens");
                return null;
            }
        }

        private IEnumerable<BensImagens> GetListImagens_db(int idBem, IDbConnection _db)
        {
            try
            {

                StringBuilder strGetImanges = new StringBuilder();
                strGetImanges.Append(" SELECT [Id] ");
                strGetImanges.Append("      ,[IdBem] ");
                strGetImanges.Append("      ,[PathLocation] ");
                strGetImanges.Append("      ,[PathLocationIIS] ");
                strGetImanges.Append("      ,[FlagAtivo] ");
                strGetImanges.Append("      ,[IdUsuarioInclusao] ");
                strGetImanges.Append("      ,[DtInclusao] ");
                strGetImanges.Append("      ,[IdUsuarioAlteracao] ");
                strGetImanges.Append("      ,[DtAlteracao] ");
                strGetImanges.Append("  FROM [financ].[BensImagem] where IdBem = @idBem and FlagAtivo =1");

                return _db.Query<BensImagens>(strGetImanges.ToString(), new { idBem = idBem }).AsEnumerable();

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetListImagens");
                return null;
            }
        }


        public IEnumerable<PoolBensPessoas> GetListMeusPoolInvestidos(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string strGetPoolBensPessoa = "select  [IdPoolBem],[IdPessoa],SUM([Cota]) as Cota,[IdStatus] from [financ].[PoolBensPessoas] where IdStatus = 1 and IdPessoa = " + idPessoa + " group by   [IdPoolBem],[IdPessoa] ,[IdStatus] ";

                    var listaPoolsBensPessoas = _db.Query<PoolBensPessoas>(strGetPoolBensPessoa).AsEnumerable();


                    foreach (var pbp in listaPoolsBensPessoas)
                    {


                        StringBuilder strGetList = new StringBuilder();
                        strGetList.Append(" select   ");
                        strGetList.Append("  PB.[Id]");
                        strGetList.Append(" ,PB.[IdContrato]");
                        strGetList.Append(" ,PB.[Nome]");
                        strGetList.Append(" ,PB.[Cotas]");
                        strGetList.Append(" ,PB.[DtInicio]");
                        strGetList.Append(" ,PB.[DtVigencia]");
                        strGetList.Append(" ,PB.[IdStatus]");
                        strGetList.Append(" ,PB.[IdUsuarioInclusao]");
                        strGetList.Append(" ,PB.[DtInclusao]");
                        strGetList.Append(" ,PB.[IdUsuarioAlteracao]");
                        strGetList.Append(" ,PB.[DtAlteracao]"); 
                        strGetList.Append(" ,PB.[PathLocation] ");
                        strGetList.Append(" ,PB.[PathLocationIIS] ");
                        strGetList.Append(" ,PB.[PathLocationIISLogoMenor]");
                        strGetList.Append(" ,PB.[PathLocationIISLogoMaior]");
                        strGetList.Append(" ,B.[Id]");
                        strGetList.Append(" ,B.[IdPool]");
                        strGetList.Append(" ,B.[Nome]");
                        strGetList.Append(" ,B.[performance]");
                        strGetList.Append(" ,(B.[CapitalInvestido] + (select ISNULL(SUM(Valor),0) from dbo.BensGastos  where IdBem = B.id and FlagAtivo = 1))  as CapitalInvestido");
                        strGetList.Append(" ,B.[IdStatus]");
                        strGetList.Append(" ,B.[IdUsuarioInclusao]");
                        strGetList.Append(" ,B.[DtInclusao]");
                        strGetList.Append(" ,B.[IdUsuarioAlteracao]");
                        strGetList.Append(" ,B.[DtAlteracao]");
                        strGetList.Append(" 	from [financ].[PoolBens] PB");
                        strGetList.Append(" 		inner join [financ].[Bens] B on B.IdPool = Pb.Id and B.IdStatus =1");
                        strGetList.Append(" 	where PB.IdStatus = 1 and PB.[Id] = " + pbp.IdPoolBem + " order by PB.Nome");

                        var lookup = new Dictionary<int, PoolBens>();
                        _db.Query<PoolBens, Bens, PoolBens>(strGetList.ToString(),
                            (PB, B) =>
                            {
                                PoolBens activeParent;
                                if (!lookup.TryGetValue(PB.Id, out activeParent))
                                {
                                    activeParent = PB;
                                    lookup.Add(activeParent.Id, activeParent);
                                }

                                //TODO: if you need to check for duplicates or null do so here
                                if (activeParent._Bens == null) activeParent._Bens = new List<Bens>();
                                activeParent._Bens.Add(B);

                                return activeParent;

                            }, splitOn: "Id");


                        foreach (var item in lookup.Values)
                        {
                            foreach (var item2 in item._Bens)
                            {
                                item2._Imagens = GetListImagens_db(item2.Id, _db);
                            }
                        }

                        pbp._PoolBens = lookup.Values.FirstOrDefault();



                    }

                    return listaPoolsBensPessoas;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "GetListPoolParaInvestimento");

                return null;
            }
        }


        public IEnumerable<PoolsBensRentabilidadePerformance> GetListPoolsBensRentabilidadePerformance(int idPoolBem)
        {

            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetImanges = new StringBuilder();
                    strGetImanges.Append(" SELECT [Id] ");
                    strGetImanges.Append("     ,[IdPoolBens] ");
                    strGetImanges.Append("     ,[iAno] ");
                    strGetImanges.Append("     ,[iMes] ");
                    strGetImanges.Append("     ,[mRentabilidade] ");
                    strGetImanges.Append("     ,[mPerformance] ");
                    strGetImanges.Append("     ,[flgAtivo] ");
                    strGetImanges.Append("     ,[dtInclusao] ");
                    strGetImanges.Append("     ,[idUsuarioinclusao] ");
                    strGetImanges.Append(" FROM  [PoolsBensRentabilidadePerformance] where IdPoolBens = @idPoolBem order by iAno,iMes desc ");



                    return _db.Query<PoolsBensRentabilidadePerformance>(strGetImanges.ToString(), new { idPoolBem = idPoolBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetListPoolsBensRentabilidadePerformance");
                return null;
            }
        }

        public PoolsBensRentabilidadePerformance InsertPoolsBensRentabilidadePerformance(PoolsBensRentabilidadePerformance con)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO  PoolsBensRentabilidadePerformance  ");
                    strInsert.Append("       ([IdPoolBens] ");
                    strInsert.Append("      ,[iAno] ");
                    strInsert.Append("      ,[iMes] ");
                    strInsert.Append("      ,[mRentabilidade] ");
                    strInsert.Append("      ,[mPerformance] ");
                    strInsert.Append("      ,[flgAtivo] ");
                    strInsert.Append("      ,[dtInclusao] ");
                    strInsert.Append("      ,[idUsuarioinclusao]) ");
                    strInsert.Append(" VALUES ");
                    strInsert.Append("      (@IdPoolBens ");
                    strInsert.Append("      ,@iAno ");
                    strInsert.Append("      ,@iMes ");
                    strInsert.Append("      ,@mRentabilidade ");
                    strInsert.Append("      ,@mPerformance ");
                    strInsert.Append("      ,@flgAtivo ");
                    strInsert.Append("      ,getDate()  ");
                    strInsert.Append("      ,@idUsuarioinclusao ) SELECT IDENT_CURRENT('PoolsBensRentabilidadePerformance') AS [IDENT_CURRENT]");

                    var idPoolsBensRentabilidadePerformance = _db.Query<int>(strInsert.ToString(),
                        new
                        {
                            IdPoolBens = con.IdPoolBens,
                            iAno = con.iAno,
                            iMes = con.iMes,
                            mRentabilidade = con.mRentabilidade,
                            mPerformance = con.mPerformance,
                            flgAtivo = con.flgAtivo,
                            idUsuarioinclusao = con.idUsuarioinclusao

                        }).FirstOrDefault();

                    con.Id = idPoolsBensRentabilidadePerformance;

                    return con;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "InsertPoolsBensRentabilidadePerformance");

                return null;
            }
        }

        public bool DesativarAtivarPoolsBensRentabilidadePerformance(int id, bool flgAtivo)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" UPDATE   PoolsBensRentabilidadePerformance SET");
                    strInsert.Append("   [flgAtivo] = @flgAtivo   ");
                    strInsert.Append(" where ");
                    strInsert.Append("  Id = @Id");

                    var idPoolsBensRentabilidadePerformance = _db.Execute(strInsert.ToString(),
                        new
                        {
                            Id = id,
                            flgAtivo = flgAtivo

                        });

                  

                    return true;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensRepository", "DesativarAtivarPoolsBensRentabilidadePerformance");


                return false;
            }
        }
    }
}
