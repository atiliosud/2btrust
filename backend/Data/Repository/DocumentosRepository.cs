﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Data.Repository
{
    public class DocumentosRepository : BaseRepository, IDocumentosRepository
    {

        public IEnumerable<Documentos> GetListDocumentos()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("     ,[Codigo] ");
                    strGet.Append("     ,[Nome] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append(" FROM [crm].[Documentos] order by [Nome]");

                    return _db.Query<Documentos>(strGet.ToString()).AsEnumerable();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "DocumentosRepository", "GetListDocumentos");

                return null;
            }

        }
    }
}
