﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Domain.Entities._2BTrust;
using Domain.Enuns;
using Domain.Entities;

namespace Data.Repository
{
    public class PessoasRepository : BaseRepository, IPessoasRepository
    {
        public int iContador = 0;
        public DateTime? dtInsertAprova = null;
        public Pessoas GetPessoaById(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetPessoa = new StringBuilder();
                    strGetPessoa.Append(" SELECT [Id] ");
                    strGetPessoa.Append("      ,[Nome]");
                    strGetPessoa.Append("      ,[FlagResidenteAmericado]");
                    strGetPessoa.Append("      ,[DtNascimento]");
                    strGetPessoa.Append("      ,[IdStatus]");
                    strGetPessoa.Append("      ,[IdUsuarioInclusao]");
                    strGetPessoa.Append("      ,[DtInclusao]");
                    strGetPessoa.Append("      ,[IdUsuarioAlteracao]");

                    strGetPessoa.Append("      ,[TelefoneComercial]");
                    strGetPessoa.Append("      ,[TelefoneCelular]");
                    strGetPessoa.Append("      ,[ValorInicial]");
                    strGetPessoa.Append("      ,[fCondicao1]");
                    strGetPessoa.Append("      ,[fCondicao2]");
                    strGetPessoa.Append("      ,[fCondicao3]");
                    strGetPessoa.Append("      ,[fCondicao4]");
                    strGetPessoa.Append("      ,[fCondicao5]");
                    strGetPessoa.Append("      ,[fCondicao6]");
                    strGetPessoa.Append("      ,[fConfirmoDados]");

                    strGetPessoa.Append("      ,[DtAlteracao]");
                    strGetPessoa.Append("  FROM [crm].[Pessoas] where Id = @IdPessoa");


                    return _db.Query<Pessoas>(strGetPessoa.ToString(),
                        new { IdPessoa = idPessoa }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetPessoaById");

                return null;
            }
        }

        public Pessoas InsertPessoa(Pessoas pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [crm].[Pessoas] ");
                    strInsert.Append(" ([Nome] ");
                    strInsert.Append(" ,[FlagResidenteAmericado] ");
                    strInsert.Append(" ,[DtNascimento] ");
                    strInsert.Append(" ,[IdStatus] ");
                    strInsert.Append(" ,[IdUsuarioInclusao] ");
                    strInsert.Append(" ,[DtInclusao] ");
                    strInsert.Append(" ,[TelefoneComercial]");
                    strInsert.Append(" ,[TelefoneCelular]");
                    strInsert.Append(" ,[ValorInicial]");
                    strInsert.Append(" ,[fCondicao1]");
                    strInsert.Append(" ,[fCondicao2]");
                    strInsert.Append(" ,[fCondicao3]");
                    strInsert.Append(" ,[fCondicao4]");
                    strInsert.Append(" ,[fCondicao5]");
                    strInsert.Append(" ,[fCondicao6]");
                    strInsert.Append(" ,[fConfirmoDados]");
                    strInsert.Append(" ) ");
                    strInsert.Append("      VALUES ");
                    strInsert.Append(" (@Nome");
                    strInsert.Append(" ,@FlagResidenteAmericado");
                    strInsert.Append(" ,@DtNascimento");
                    strInsert.Append(" ,@IdStatus");
                    strInsert.Append(" ,@IdUsuarioInclusao");
                    strInsert.Append(" ,getDate()");

                    strInsert.Append(" ,@TelefoneComercial");
                    strInsert.Append(" ,@TelefoneCelular");
                    strInsert.Append(" ,@ValorInicial");
                    strInsert.Append(" ,@fCondicao1");
                    strInsert.Append(" ,@fCondicao2");
                    strInsert.Append(" ,@fCondicao3");
                    strInsert.Append(" ,@fCondicao4");
                    strInsert.Append(" ,@fCondicao5");
                    strInsert.Append(" ,@fCondicao6");
                    strInsert.Append(" ,@fConfirmoDados");
                    strInsert.Append(" )  SELECT IDENT_CURRENT('[crm].[Pessoas]') AS [IDENT_CURRENT]");

                    var idPessoa = _db.Query<int>(strInsert.ToString(), new
                    {
                        Nome = pes.Nome,
                        FlagResidenteAmericado = pes.FlagResidenteAmericado,
                        DtNascimento = pes.DtNascimento,
                        IdStatus = pes.IdStatus,
                        IdUsuarioInclusao = pes.IdUsuarioInclusao,
                        TelefoneComercial = pes.TelefoneComercial,
                        TelefoneCelular = pes.TelefoneCelular,
                        ValorInicial = pes.ValorInicial,
                        fCondicao1 = pes.fCondicao1,
                        fCondicao2 = pes.fCondicao2,
                        fCondicao3 = pes.fCondicao3,
                        fCondicao4 = pes.fCondicao4,
                        fCondicao5 = pes.fCondicao5,
                        fCondicao6 = pes.fCondicao6,
                        fConfirmoDados = pes.fConfirmoDados

                    }).FirstOrDefault();

                    //criar uma carteira
                    StringBuilder strInsCarteira = new StringBuilder();
                    strInsCarteira.Append(" INSERT INTO [dbo].[Carteira] ");
                    strInsCarteira.Append("  ([IdPessoa] ");
                    strInsCarteira.Append("  ,[Saldo] ");
                    strInsCarteira.Append("  ,[DtInclusao] ");
                    strInsCarteira.Append("  ,[IdUsuarioInclusao]) ");
                    strInsCarteira.Append("    VALUES ");
                    strInsCarteira.Append("   (@IdPessoa ");
                    strInsCarteira.Append("   ,0 ");
                    strInsCarteira.Append("   ,getDate() ");
                    strInsCarteira.Append("   ,3)");

                    _db.Execute(strInsCarteira.ToString(), new { IdPessoa = idPessoa });



                    return GetPessoaById(idPessoa);
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "InsertPessoa");

                return null;
            }
        }

        public Pessoas UpdatePessoa(Pessoas pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [crm].[Pessoas] ");
                    strUpdate.Append(" SET [Nome] = @Nome");
                    strUpdate.Append("  ,[DtNascimento] = @DtNascimento");
                    strUpdate.Append("  ,[IdStatus] = @IdStatus");
                    strUpdate.Append("  ,[IdUsuarioAlteracao] =@IdUsuarioAlteracao");
                    strUpdate.Append("  ,[DtAlteracao] = getDate() ");
                    strUpdate.Append("  ,[TelefoneComercial] = @TelefoneComercial");
                    strUpdate.Append("  ,[TelefoneCelular] = @TelefoneCelular");
                    strUpdate.Append(" WHERE Id = @IdPessoa");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        Nome = pes.Nome,
                        DtNascimento = pes.DtNascimento,
                        IdStatus = pes.IdStatus,
                        IdUsuarioAlteracao = pes.IdUsuarioAlteracao,
                        IdPessoa = pes.Id,
                        TelefoneComercial = pes.TelefoneComercial,
                        TelefoneCelular = pes.TelefoneCelular,

                    });

                    return pes;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "UpdatePessoa");

                return null;
            }
        }

        public IEnumerable<PessoasGenricList> GetListReduzidoPessoas()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGEt = new StringBuilder();


                    strGEt.Append(" select P.id, P.Nome, P.DtInclusao as DataCadastro, P.FlagResidenteAmericado, ");
                    strGEt.Append(" U.Email, UG.IdGrupo ");
                    strGEt.Append(" from crm.Pessoas P ");
                    strGEt.Append(" inner join[user].Usuarios U on U.IdPessoa = P.Id ");
                    strGEt.Append(" inner join[user].UsuariosGrupos UG on UG.IdUsuario = U.Id ");

                    return _db.Query<PessoasGenricList>(strGEt.ToString()).AsEnumerable();

                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool LiberacaoViaEmail(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [crm].[Pessoas] ");
                    strUpdate.Append("   SET  [IdStatus] = @IdStatus");
                    strUpdate.Append("      ,[DtAlteracao] = getDate() ");
                    strUpdate.Append(" WHERE Id = @IdPessoa");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        IdStatus = 1,
                        IdPessoa = idPessoa
                    });

                    return true;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "LiberacaoViaEmail");

                return false;
            }
        }


        public Pessoas GetPessoasById(int idPessoa)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT P.[Id] ");
                    strGet.Append("      ,P.[Nome] ");
                    strGet.Append("      ,P.[FlagResidenteAmericado] ");
                    strGet.Append("      ,P.[DtNascimento] ");
                    strGet.Append("      ,P.[IdStatus] ");
                    strGet.Append("      ,P.[IdUsuarioInclusao] ");
                    strGet.Append("      ,P.[DtInclusao] ");
                    strGet.Append("      ,P.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,P.[DtAlteracao] ");
                    strGet.Append("      ,P.[TelefoneComercial]");
                    strGet.Append("      ,P.[TelefoneCelular]");
                    strGet.Append("      ,P.[ValorInicial]");
                    strGet.Append("      ,P.[fCondicao1]");
                    strGet.Append("      ,P.[fCondicao2]");
                    strGet.Append("      ,P.[fCondicao3]");
                    strGet.Append("      ,P.[fCondicao4]");
                    strGet.Append("      ,P.[fCondicao5]");
                    strGet.Append("      ,P.[fCondicao6]");
                    strGet.Append("      ,P.[fConfirmoDados]");
                    strGet.Append("      ,U.[Id] ");
                    strGet.Append("      ,U.[Nome] ");
                    strGet.Append("      ,U.[Email] ");
                    strGet.Append("      ,U.[Admin] ");
                    strGet.Append("      ,U.[Bloqueado] ");
                    strGet.Append("      ,U.[Saltkey] ");
                    strGet.Append("      ,U.[IdStatus] ");
                    strGet.Append("      ,U.[IdUsuarioInclusao] ");
                    strGet.Append("      ,U.[DtInclusao] ");
                    strGet.Append("      ,U.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,U.[DtAlteracao] ");
                    strGet.Append("      ,U.[IdPessoa]");
                    strGet.Append("      ,PF.[Id]");
                    strGet.Append("      ,PF.[IdPessoa]");
                    strGet.Append("      ,PF.[CPF]");
                    strGet.Append("      ,PF.[TaxId]");
                    strGet.Append("      ,PF.[ITIN]");
                    strGet.Append("      ,PF.[SocialNumber]");
                    strGet.Append("      ,PF.[Apelido]");
                    strGet.Append("      ,PF.[IdUsuarioInclusao]");
                    strGet.Append("      ,PF.[DtInclusao]");
                    strGet.Append("      ,PF.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PF.[DtAlteracao]");
                    strGet.Append("      ,PJ.[Id]");
                    strGet.Append("      ,PJ.[IdPessoa]");
                    strGet.Append("      ,PJ.[Fantasia]");
                    strGet.Append("      ,PJ.[CNPJ]");
                    strGet.Append("      ,PJ.[IdUsuarioInclusao]");
                    strGet.Append("      ,PJ.[DtInclusao]");
                    strGet.Append("      ,PJ.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PJ.[DtAlteracao] ");
                    strGet.Append("      ,L.[Id]  ");
                    strGet.Append("      ,L.[IdUsuario] ");
                    strGet.Append("      ,L.[Login] ");
                    strGet.Append("      ,L.[Senha] ");
                    strGet.Append("      ,L.[Bloqueado] ");
                    strGet.Append("      ,L.[Tentativas] ");
                    strGet.Append("      ,L.[IdUsuarioInclusao] ");
                    strGet.Append("      ,L.[DtInclusao] ");
                    strGet.Append("      ,L.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,L.[DtAlteracao] ");


                    strGet.Append("          from [crm].[Pessoas] P ");
                    strGet.Append("      Inner join [user].[Usuarios] U on P.Id = U.IdPessoa ");
                    strGet.Append("      Inner join [user].[Logins] L on L.IdUsuario = U.Id ");
                    strGet.Append("      left JOin [crm].[PessoasFisica] PF on PF.IdPessoa = P.Id ");
                    strGet.Append("      left JOin [crm].[PessoasJuridica] PJ  on PJ.IdPessoa = P.Id  where P.Id =  " + idPessoa);

                    var res = _db.Query<Pessoas, Usuarios, PessoasFisicas, PessoasJuridica, Logins, Pessoas>(strGet.ToString(),
                        (P, U, PF, PJ, L) =>
                        {
                            P._Usuario = U;
                            P._Usuario._Login = L;
                            P._PessoaFisica = PF;
                            P._PessoaJuridica = PJ;

                            P._Usuario._Login = L;
                            return P;

                        }, splitOn: "Id").FirstOrDefault();

                    return res;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetPessoasById");

                return null;
            }
        }

        public Carteira GetCarteiraByIdPessoa(int idPessoa)
        {

            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Saldo] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("  FROM [dbo].[Carteira] where IdPessoa = @idPessoa");

                    var objCarteira = _db.Query<Carteira>(strGet.ToString(), new { idPessoa = idPessoa }).FirstOrDefault();

                    decimal saldoAtual = 0;
                    if (objCarteira != null)
                    {
                        saldoAtual = objCarteira.Saldo;

                        objCarteira._Comprovantes = GetListComprovanteDepositoByIdCarteira(objCarteira.Id);

                        foreach (var item in objCarteira._Comprovantes)
                        {
                            if (item.FlagComputado == false && item.FlagNegado == false && item.FlagPendente == false)
                            {
                                //atualizar saldo em carteira

                                var valorDeposito = item.Valor + objCarteira.Saldo;
                                objCarteira.Saldo = valorDeposito;

                                var objCarteiraI = UpdateCarteira(objCarteira);
                                if (objCarteiraI != null)
                                {
                                    saldoAtual = valorDeposito;
                                    //atualizar comprovante
                                    item.FlagComputado = true;
                                    UpdateComprovanteDeposito(item);
                                }
                            }
                        }
                    }

                    objCarteira.Saldo = saldoAtual;
                    return objCarteira;


                }

            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PessoasRepository", "GetCarteiraByIdPessoa");

                return null;
            }

        }


        public Carteira GetCarteiraByIdCarteira(int idCarteira, int icontador = 0)
        {

            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Saldo] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("  FROM [dbo].[Carteira] where Id = @idCarteira");

                    var objCarteira = _db.Query<Carteira>(strGet.ToString(), new { idCarteira = idCarteira }).FirstOrDefault();

                    decimal saldoAtual = 0;
                    if (objCarteira != null)
                    {
                        saldoAtual = objCarteira.Saldo;

                        objCarteira._Comprovantes = GetListComprovanteDepositoByIdCarteira(objCarteira.Id);

                        foreach (var item in objCarteira._Comprovantes)
                        {
                            if (item.FlagComputado == false && item.FlagNegado == false && item.FlagPendente == false)
                            {
                                //atualizar saldo em carteira

                                var valorDeposito = item.Valor + objCarteira.Saldo;
                                objCarteira.Saldo = valorDeposito;

                                var objCarteiraI = UpdateCarteira(objCarteira);
                                if (objCarteiraI != null)
                                {
                                    saldoAtual = valorDeposito;
                                    //atualizar comprovante
                                    item.FlagComputado = true;
                                    UpdateComprovanteDepositoUnico(item);


                                    if (iContador == 1)
                                    {
                                        try
                                        {
                                            using (_db = new SqlConnection(urlConectionPrincipal))
                                            {
                                                var idPessoa = _db.Query<int>("select IdPessoa from [Carteira] where Id = @idCarteira", new { idCarteira = item.IdCarteira }).FirstOrDefault();

                                                var getLogCarteira = GetLastLogCarteira(idPessoa);
                                                if (getLogCarteira == null)
                                                {
                                                    CarteiraLog objNew = new CarteiraLog()
                                                    {
                                                        Amount = item.Valor,
                                                        AvaliableBalance = (item.IdStatus == EnumStatusCarteira.Aprovado) ? item.Valor : 0,
                                                        Data = dtInsertAprova,
                                                        Descricao = (item.IdStatus == EnumStatusCarteira.Aprovado) ? "Deposit Approved" : "Deposit Refused",
                                                        FlagAtivo = true,
                                                        IdPessoa = idPessoa,
                                                        Tipo = (item.IdStatus == EnumStatusCarteira.Aprovado) ? "deposito aprovado" : "deposito recusado",
                                                    };

                                                    InserLogCarteira(objNew);
                                                }
                                                else
                                                {
                                                    getLogCarteira.Descricao = (item.IdStatus == EnumStatusCarteira.Aprovado) ? "Deposit Approved" : "Deposit Refused";
                                                    getLogCarteira.Tipo = (item.IdStatus == EnumStatusCarteira.Aprovado) ? "deposito aprovado" : "deposito recusado";
                                                    getLogCarteira.Amount = item.Valor;
                                                    getLogCarteira.AvaliableBalance = (item.IdStatus == EnumStatusCarteira.Aprovado) ? item.Valor + getLogCarteira.AvaliableBalance : item.Valor;
                                                    getLogCarteira.Data = dtInsertAprova;

                                                    InserLogCarteira(getLogCarteira);

                                                }
                                            }
                                        }
                                        catch (Exception ex) { }
                                    }



                                }
                            }
                        }
                    }

                    objCarteira.Saldo = saldoAtual;




                    return objCarteira;


                }

            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PessoasRepository", "GetCarteiraByIdPessoa");

                return null;
            }

        }


        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoByIdCarteira(int idCarteira)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdCarteira] ");
                    strGet.Append("      ,[Valor] ");
                    strGet.Append("      ,[ComprovantePathLocation] ");
                    strGet.Append("      ,[ComprovantePathLocationIIS] ");
                    strGet.Append("      ,[FlagPendente] ");
                    strGet.Append("      ,[FlagNegado] ");
                    strGet.Append("      ,[FlagComputado] ");
                    strGet.Append("      ,[IdStatus] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("      ,[SwiftCode] ");
                    strGet.Append("      ,[FlagAtivoSwiftCode] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("  FROM [dbo].[ComprovanteDeposito] where IdCarteira = @idCarteira and  [IdStatus] != 2");

                    return _db.Query<ComprovanteDeposito>(strGet.ToString(), new { idCarteira = idCarteira }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetListComprovanteDepositoByIdCarteira");

                return null;
            }
        }

        public ComprovanteDeposito GetComprovanteDepositoById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT P.Nome as NomeCliente, CD.[Id] ");
                    strGet.Append("      ,CD.[IdCarteira] ");
                    strGet.Append("      ,CD.[Valor] ");
                    strGet.Append("      ,CD.[ComprovantePathLocation] ");
                    strGet.Append("      ,CD.[ComprovantePathLocationIIS] ");
                    strGet.Append("      ,CD.[FlagPendente] ");
                    strGet.Append("      ,CD.[FlagNegado] ");
                    strGet.Append("      ,CD.[FlagComputado] ");
                    strGet.Append("      ,CD.[IdStatus] ");
                    strGet.Append("      ,CD.[IdUsuarioInclusao] ");
                    strGet.Append("      ,CD.[DtInclusao] ");
                    strGet.Append("      ,CD.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,CD.[DtAlteracao] ");
                    strGet.Append("      ,CD.[SwiftCode] ");
                    strGet.Append("      ,CD.[FlagAtivoSwiftCode] ");
                    strGet.Append("  FROM [dbo].[ComprovanteDeposito] CD inner join [dbo].[Carteira] C on C.Id = CD.IdCarteira inner join [crm].[Pessoas] P on P.Id = C.IdPessoa");
                    strGet.Append("   where CD.Id = @Id");

                    return _db.Query<ComprovanteDeposito>(strGet.ToString(), new { Id = id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetListComprovanteDepositoByIdCarteira");

                return null;
            }
        }

        public ComprovanteDeposito InsertComprovanteDeposito(ComprovanteDeposito comp)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [dbo].[ComprovanteDeposito] ");
                    strInsert.Append("          ([IdCarteira] ");
                    strInsert.Append("          ,[Valor] ");
                    strInsert.Append("          ,[ComprovantePathLocation] ");
                    strInsert.Append("          ,[ComprovantePathLocationIIS] ");
                    strInsert.Append("          ,[FlagPendente] ");
                    strInsert.Append("          ,[FlagNegado] ");
                    strInsert.Append("          ,[FlagComputado] ");
                    strInsert.Append("          ,[IdStatus] ");
                    strInsert.Append("          ,[IdUsuarioInclusao] ");
                    strInsert.Append("          ,[DtInclusaoAdm] ");
                    strInsert.Append("          ,[SwiftCode] ");
                    strInsert.Append("          ,[FlagAtivoSwiftCode] ");
                    strInsert.Append("          ,[DtInclusao]) ");
                    strInsert.Append("    VALUES ");
                    strInsert.Append("          (@IdCarteira ");
                    strInsert.Append("          ,@Valor ");
                    strInsert.Append("          ,@ComprovantePathLocation ");
                    strInsert.Append("          ,@ComprovantePathLocationIIS ");
                    strInsert.Append("          ,@FlagPendente ");
                    strInsert.Append("          ,@FlagNegado ");
                    strInsert.Append("          ,@FlagComputado ");
                    strInsert.Append("          ,@IdStatus ");
                    strInsert.Append("          ,@IdUsuarioInclusao ");
                    strInsert.Append("          ,@DtInclusaoAdm ");
                    strInsert.Append("          ,@SwiftCode ");
                    strInsert.Append("          ,@FlagAtivoSwiftCode ");
                    strInsert.Append("          ,@DtInclusao) ");

                    DateTime? dtAdm = (comp.DtInclusaoAdm == null) ? (DateTime?)null : DateTime.Now;
                    DateTime dtNow = (comp.DtInclusaoAdm == null) ? DateTime.Now : comp.DtInclusaoAdm.Value;


                    string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
                    string numbers = "1234567890";

                    string characters = numbers;

                    characters = alphabets + small_alphabets + numbers;

                    int length = 6;
                    string otp = string.Empty;
                    for (int i = 0; i < length; i++)
                    {
                        string character = string.Empty;
                        do
                        {
                            int index = new Random().Next(0, characters.Length);
                            character = characters.ToCharArray()[index].ToString();
                        } while (otp.IndexOf(character) != -1);
                        otp += character;
                    }
                    string swiftCode = otp;


                    _db.Execute(strInsert.ToString(), new
                    {
                        SwiftCode = swiftCode,
                        FlagAtivoSwiftCode = true,
                        IdCarteira = comp.IdCarteira,
                        Valor = comp.Valor,
                        ComprovantePathLocation = comp.ComprovantePathLocation,
                        ComprovantePathLocationIIS = comp.ComprovantePathLocationIIS,
                        FlagPendente = comp.FlagPendente,
                        FlagNegado = comp.FlagNegado,
                        FlagComputado = comp.FlagComputado,
                        IdStatus = (int)comp.IdStatus,
                        IdUsuarioInclusao = comp.IdUsuarioInclusao,
                        DtInclusaoAdm = dtAdm,
                        DtInclusao = dtNow
                    });


                    comp.SwiftCode = swiftCode;

                    try
                    {
                        var idPessoa = _db.Query<int>("select IdPessoa from [Carteira] where Id = @idCarteira", new { idCarteira = comp.IdCarteira }).FirstOrDefault();

                        var getLogCarteira = GetLastLogCarteira(idPessoa);
                        if (getLogCarteira == null)
                        {
                            CarteiraLog objNew = new CarteiraLog()
                            {
                                Amount = comp.Valor,
                                AvaliableBalance = 0,
                                Data = (comp.DtInclusaoAdm == null) ? DateTime.Now : comp.DtInclusaoAdm.Value,
                                Descricao = "Deposit Requested ",
                                FlagAtivo = true,
                                IdPessoa = idPessoa,
                                Tipo = "deposito"
                            };

                            InserLogCarteira(objNew);
                        }
                        else
                        {
                            getLogCarteira.Descricao = "Deposit Requested";
                            getLogCarteira.Tipo = "deposito";
                            getLogCarteira.Amount = comp.Valor;
                            getLogCarteira.Data = (comp.DtInclusaoAdm == null) ? DateTime.Now : comp.DtInclusaoAdm.Value;

                            InserLogCarteira(getLogCarteira);

                        }
                    }
                    catch (Exception ex) { InsertLogError(ex, "PessoasRepository", "InsertComprovanteDeposito1");  }


                    string strGetNome = "select Nome from crm.Pessoas P inner join dbo.Carteira C on C.IdPessoa = P.Id  where C.Id = @idCarteira";
                    comp.NomeCliente = _db.Query<string>(strGetNome, new { idCarteira = comp.IdCarteira }).FirstOrDefault();

                    string getMail = " select Email from [user].Usuarios U inner join  crm.Pessoas P on P.Id = U.idPessoa  inner join dbo.Carteira C on C.IdPessoa = P.Id   where C.Id =  @idCarteira";
                    comp.EmailCliente = _db.Query<string>(getMail, new { idCarteira = comp.IdCarteira }).FirstOrDefault();

                    return comp;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "InsertComprovanteDeposito");

                return null;
            }
        }


        public ComprovanteDeposito UpdateComprovanteDeposito(ComprovanteDeposito comp)
        {
            try
            {
                iContador++;
                if (dtInsertAprova == null) dtInsertAprova = comp.DtAprovaReprova;
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strupdate = new StringBuilder();
                    strupdate.Append(" UPDATE [dbo].[ComprovanteDeposito] ");
                    strupdate.Append("  SET [IdCarteira] = @IdCarteira ");
                    strupdate.Append("     ,[Valor] = @Valor ");
                    strupdate.Append("     ,[ComprovantePathLocation] = @ComprovantePathLocation ");
                    strupdate.Append("     ,[ComprovantePathLocationIIS] = @ComprovantePathLocationIIS ");
                    strupdate.Append("     ,[FlagPendente] = @FlagPendente ");
                    strupdate.Append("     ,[FlagNegado] = @FlagNegado ");
                    strupdate.Append("     ,[FlagComputado] = @FlagComputado ");
                    strupdate.Append("     ,[IdStatus] = @IdStatus ");
                    strupdate.Append("     ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strupdate.Append("     ,[DtAlteracao] = getDate() ");
                    strupdate.Append("     ,[DtAprovaReprova] =@DtAprovaReprova");
                    strupdate.Append(" WHERE Id = @id");


                    _db.Execute(strupdate.ToString(), new
                    {
                        id = comp.Id,
                        IdCarteira = comp.IdCarteira,
                        Valor = comp.Valor,
                        ComprovantePathLocation = comp.ComprovantePathLocation,
                        ComprovantePathLocationIIS = comp.ComprovantePathLocationIIS,
                        FlagPendente = comp.FlagPendente,
                        FlagNegado = comp.FlagNegado,
                        FlagComputado = comp.FlagComputado,
                        IdStatus = comp.IdStatus,
                        IdUsuarioAlteracao = comp.IdUsuarioAlteracao,
                        DtAprovaReprova = (comp.IdStatus == EnumStatusCarteira.Aprovado || comp.IdStatus == EnumStatusCarteira.Recusado) ? comp.DtAprovaReprova : (DateTime?)null
                    });


                }
                if (comp.IdStatus == EnumStatusCarteira.Aprovado)
                {
                    var get = GetCarteiraByIdCarteira(comp.IdCarteira, iContador);
                }


                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    if (comp.IdStatus == EnumStatusCarteira.Recusado)

                        try
                        {
                            var idPessoa = _db.Query<int>("select IdPessoa from [Carteira] where Id = @idCarteira", new { idCarteira = comp.IdCarteira }).FirstOrDefault();

                            var getLogCarteira = GetLastLogCarteira(idPessoa);
                            if (getLogCarteira == null)
                            {
                                CarteiraLog objNew = new CarteiraLog()
                                {
                                    Amount = comp.Valor,
                                    AvaliableBalance = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? comp.Valor : 0,
                                    Data = comp.DtAprovaReprova,
                                    Descricao = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "Deposit Approved" : "Deposit Refused",
                                    FlagAtivo = true,
                                    IdPessoa = idPessoa,
                                    Tipo = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "deposito aprovado" : "deposito recusado",
                                };

                                InserLogCarteira(objNew);
                            }
                            else
                            {
                                getLogCarteira.Descricao = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "Deposit Approved" : "Deposit Refused";
                                getLogCarteira.Tipo = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "deposito aprovado" : "deposito recusado";
                                getLogCarteira.Amount = comp.Valor;
                                getLogCarteira.AvaliableBalance = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? comp.Valor + getLogCarteira.AvaliableBalance : comp.Valor;
                                getLogCarteira.Data = comp.DtAprovaReprova;

                                InserLogCarteira(getLogCarteira);

                            }
                        }
                        catch (Exception ex) { }

                }

                return comp;

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "InsertComprovanteDeposito");

                return null;
            }
        }


        public ComprovanteDeposito UpdateComprovanteDepositoUnico(ComprovanteDeposito comp)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strupdate = new StringBuilder();
                    strupdate.Append(" UPDATE [dbo].[ComprovanteDeposito] ");
                    strupdate.Append("  SET [IdCarteira] = @IdCarteira ");
                    strupdate.Append("     ,[Valor] = @Valor ");
                    strupdate.Append("     ,[ComprovantePathLocation] = @ComprovantePathLocation ");
                    strupdate.Append("     ,[ComprovantePathLocationIIS] = @ComprovantePathLocationIIS ");
                    strupdate.Append("     ,[FlagPendente] = @FlagPendente ");
                    strupdate.Append("     ,[FlagNegado] = @FlagNegado ");
                    strupdate.Append("     ,[FlagComputado] = @FlagComputado ");
                    strupdate.Append("     ,[IdStatus] = @IdStatus ");
                    strupdate.Append("     ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strupdate.Append("     ,[DtAlteracao] = getDate() ");
                    strupdate.Append("     ,[DtAprovaReprova] =@DtAprovaReprova");
                    strupdate.Append(" WHERE Id = @id");

                    if (dtInsertAprova == null) dtInsertAprova = DateTime.Now;
                    _db.Execute(strupdate.ToString(), new
                    {
                        id = comp.Id,
                        IdCarteira = comp.IdCarteira,
                        Valor = comp.Valor,
                        ComprovantePathLocation = comp.ComprovantePathLocation,
                        ComprovantePathLocationIIS = comp.ComprovantePathLocationIIS,
                        FlagPendente = comp.FlagPendente,
                        FlagNegado = comp.FlagNegado,
                        FlagComputado = comp.FlagComputado,
                        IdStatus = comp.IdStatus,
                        IdUsuarioAlteracao = comp.IdUsuarioAlteracao,
                        DtAprovaReprova = (comp.IdStatus == EnumStatusCarteira.Aprovado || comp.IdStatus == EnumStatusCarteira.Recusado) ? dtInsertAprova : (DateTime?)null
                    });


                }
                return comp;

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "UpdateComprovanteDepositoUnico");

                return null;
            }
        }

        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoPendentes()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT P.Nome as NomeCliente, CD.[Id] ");
                    strGet.Append("      ,CD.[IdCarteira] ");
                    strGet.Append("      ,CD.[Valor] ");
                    strGet.Append("      ,CD.[ComprovantePathLocation] ");
                    strGet.Append("      ,CD.[ComprovantePathLocationIIS] ");
                    strGet.Append("      ,CD.[FlagPendente] ");
                    strGet.Append("      ,CD.[FlagNegado] ");
                    strGet.Append("      ,CD.[FlagComputado] ");
                    strGet.Append("      ,CD.[IdStatus] ");
                    strGet.Append("      ,CD.[IdUsuarioInclusao] ");
                    strGet.Append("      ,CD.[DtInclusao] ");
                    strGet.Append("      ,CD.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,CD.[DtAlteracao] ");
                    strGet.Append("      ,CD.[SwiftCode] ");
                    strGet.Append("      ,CD.[FlagAtivoSwiftCode] ");
                    strGet.Append("  FROM [dbo].[ComprovanteDeposito] CD inner join [dbo].[Carteira] C on C.Id = CD.IdCarteira inner join [crm].[Pessoas] P on P.Id = C.IdPessoa");
                    strGet.Append("  where CD.FlagPendente = 1 and CD.[IdStatus] != 2 order by CD.[DtInclusao] ");

                    return _db.Query<ComprovanteDeposito>(strGet.ToString()).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetListComprovanteDepositoByIdCarteira");

                return null;
            }
        }





        public IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaByIdCarteira(int idCarteira)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdCarteira] ");
                    strGet.Append("      ,[Valor] ");
                    strGet.Append("      ,[ComprovantePathLocation] ");
                    strGet.Append("      ,[ComprovantePathLocationIIS] ");
                    strGet.Append("      ,[FlagPendente] ");
                    strGet.Append("      ,[FlagNegado] ");
                    strGet.Append("      ,[FlagComputado] ");
                    strGet.Append("      ,[IdStatus] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("      ,[SwiftCode] ");
                    strGet.Append("      ,[FlagAtivoSwiftCode] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("  FROM [dbo].[ComprovanteRetirada] where IdCarteira = @idCarteira and  [IdStatus] != 2");

                    return _db.Query<ComprovanteRetirada>(strGet.ToString(), new { idCarteira = idCarteira }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetListComprovanteRetiradaByIdCarteira");

                return null;
            }
        }

        public ComprovanteRetirada GetComprovanteRetiradaById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT P.Nome as NomeCliente, CD.[Id] ");
                    strGet.Append("      ,CD.[IdCarteira] ");
                    strGet.Append("      ,CD.[Valor] ");
                    strGet.Append("      ,CD.[ComprovantePathLocation] ");
                    strGet.Append("      ,CD.[ComprovantePathLocationIIS] ");
                    strGet.Append("      ,CD.[FlagPendente] ");
                    strGet.Append("      ,CD.[FlagNegado] ");
                    strGet.Append("      ,CD.[FlagComputado] ");
                    strGet.Append("      ,CD.[IdStatus] ");
                    strGet.Append("      ,CD.[IdUsuarioInclusao] ");
                    strGet.Append("      ,CD.[DtInclusao] ");
                    strGet.Append("      ,CD.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,CD.[DtAlteracao] ");
                    strGet.Append("      ,CD.[SwiftCode] ");
                    strGet.Append("      ,CD.[FlagAtivoSwiftCode] ");
                    strGet.Append("  FROM [dbo].[ComprovanteRetirada] CD inner join [dbo].[Carteira] C on C.Id = CD.IdCarteira inner join [crm].[Pessoas] P on P.Id = C.IdPessoa");
                    strGet.Append("   where CD.Id = @Id");

                    return _db.Query<ComprovanteRetirada>(strGet.ToString(), new { Id = id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetListComprovanteRetiradaByIdCarteira");

                return null;
            }
        }

        public ComprovanteRetirada InsertComprovanteRetirada(ComprovanteRetirada comp)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [dbo].[ComprovanteRetirada] ");
                    strInsert.Append("          ([IdCarteira] ");
                    strInsert.Append("          ,[Valor] ");
                    strInsert.Append("          ,[ComprovantePathLocation] ");
                    strInsert.Append("          ,[ComprovantePathLocationIIS] ");
                    strInsert.Append("          ,[FlagPendente] ");
                    strInsert.Append("          ,[FlagNegado] ");
                    strInsert.Append("          ,[FlagComputado] ");
                    strInsert.Append("          ,[IdStatus] ");
                    strInsert.Append("          ,[IdUsuarioInclusao] ");
                    strInsert.Append("          ,[DtInclusaoAdm] ");
                    strInsert.Append("          ,[SwiftCode] ");
                    strInsert.Append("          ,[FlagAtivoSwiftCode] ");
                    strInsert.Append("          ,[DtInclusao]) ");
                    strInsert.Append("    VALUES ");
                    strInsert.Append("          (@IdCarteira ");
                    strInsert.Append("          ,@Valor ");
                    strInsert.Append("          ,@ComprovantePathLocation ");
                    strInsert.Append("          ,@ComprovantePathLocationIIS ");
                    strInsert.Append("          ,@FlagPendente ");
                    strInsert.Append("          ,@FlagNegado ");
                    strInsert.Append("          ,@FlagComputado ");
                    strInsert.Append("          ,@IdStatus ");
                    strInsert.Append("          ,@IdUsuarioInclusao ");
                    strInsert.Append("          ,@DtInclusaoAdm ");
                    strInsert.Append("          ,@SwiftCode ");
                    strInsert.Append("          ,@FlagAtivoSwiftCode ");
                    strInsert.Append("          ,@DtInclusao) ");

                    DateTime? dtAdm = (comp.DtInclusaoAdm == null) ? (DateTime?)null : DateTime.Now;
                    DateTime dtNow = (comp.DtInclusaoAdm == null) ? DateTime.Now : comp.DtInclusaoAdm.Value;


                    string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
                    string numbers = "1234567890";

                    string characters = numbers;

                    characters = alphabets + small_alphabets + numbers;

                    int length = 6;
                    string otp = string.Empty;
                    for (int i = 0; i < length; i++)
                    {
                        string character = string.Empty;
                        do
                        {
                            int index = new Random().Next(0, characters.Length);
                            character = characters.ToCharArray()[index].ToString();
                        } while (otp.IndexOf(character) != -1);
                        otp += character;
                    }
                    string swiftCode = otp;


                    _db.Execute(strInsert.ToString(), new
                    {
                        SwiftCode = swiftCode,
                        FlagAtivoSwiftCode = true,
                        IdCarteira = comp.IdCarteira,
                        Valor = comp.Valor,
                        ComprovantePathLocation = comp.ComprovantePathLocation,
                        ComprovantePathLocationIIS = comp.ComprovantePathLocationIIS,
                        FlagPendente = comp.FlagPendente,
                        FlagNegado = comp.FlagNegado,
                        FlagComputado = comp.FlagComputado,
                        IdStatus = (int)comp.IdStatus,
                        IdUsuarioInclusao = comp.IdUsuarioInclusao,
                        DtInclusaoAdm = dtAdm,
                        DtInclusao = dtNow
                    });


                    comp.SwiftCode = swiftCode;




                    try
                    {
                        var idPessoa = _db.Query<int>("select IdPessoa from [Carteira] where Id = @idCarteira", new { idCarteira = comp.IdCarteira }).FirstOrDefault();

                        var getLogCarteira = GetLastLogCarteira(idPessoa);
                        if (getLogCarteira == null)
                        {
                            CarteiraLog objNew = new CarteiraLog()
                            {
                                Amount = comp.Valor,
                                AvaliableBalance = 0,
                                Data = (comp.DtInclusaoAdm == null) ? DateTime.Now : comp.DtInclusaoAdm,
                                Descricao = "Withdrawal Requested",
                                FlagAtivo = true,
                                IdPessoa = idPessoa,
                                Tipo = "retirada"
                            };

                            InserLogCarteira(objNew);
                        }
                        else
                        {
                            getLogCarteira.Descricao = "Withdrawal Requested";
                            getLogCarteira.Tipo = "retirada";
                            getLogCarteira.Amount = comp.Valor;
                            getLogCarteira.Data = (comp.DtInclusaoAdm == null) ? DateTime.Now : comp.DtInclusaoAdm;

                            InserLogCarteira(getLogCarteira);

                        }
                    }
                    catch { }


                    return comp;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "InsertComprovanteRetirada");

                return null;
            }
        }

        public ComprovanteRetirada UpdateComprovanteRetirada(ComprovanteRetirada comp)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strupdate = new StringBuilder();
                    strupdate.Append(" UPDATE [dbo].[ComprovanteRetirada] ");
                    strupdate.Append("  SET [IdCarteira] = @IdCarteira ");
                    strupdate.Append("     ,[Valor] = @Valor ");
                    strupdate.Append("     ,[ComprovantePathLocation] = @ComprovantePathLocation ");
                    strupdate.Append("     ,[ComprovantePathLocationIIS] = @ComprovantePathLocationIIS ");
                    strupdate.Append("     ,[FlagPendente] = @FlagPendente ");
                    strupdate.Append("     ,[FlagNegado] = @FlagNegado ");
                    strupdate.Append("     ,[FlagComputado] = @FlagComputado ");
                    strupdate.Append("     ,[IdStatus] = @IdStatus ");
                    strupdate.Append("     ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strupdate.Append("     ,[DtAlteracao] = DtAlteracao ");
                    strupdate.Append(" WHERE Id = @id");


                    _db.Execute(strupdate.ToString(), new
                    {
                        id = comp.Id,
                        IdCarteira = comp.IdCarteira,
                        Valor = comp.Valor,
                        ComprovantePathLocation = comp.ComprovantePathLocation,
                        ComprovantePathLocationIIS = comp.ComprovantePathLocationIIS,
                        FlagPendente = comp.FlagPendente,
                        FlagNegado = comp.FlagNegado,
                        FlagComputado = comp.FlagComputado,
                        IdStatus = comp.IdStatus,
                        IdUsuarioAlteracao = comp.IdUsuarioAlteracao,
                        DtAlteracao = comp.DtAlteracao
                    });



                    if (comp.IdStatus == EnumStatusCarteira.Aprovado)
                    {
                        //subtrair da carteira o valor solicitado
                        var carteira = GetCarteiraByIdCarteira(comp.IdCarteira);

                        carteira.Saldo -= comp.Valor;

                        UpdateCarteira(carteira);

                    }


                    if (comp.IdStatus == EnumStatusCarteira.Aprovado || comp.IdStatus == EnumStatusCarteira.Recusado)
                    {

                        try
                        {
                            using (_db = new SqlConnection(urlConectionPrincipal))
                            {
                                var idPessoa = _db.Query<int>("select IdPessoa from [Carteira] where Id = @idCarteira", new { idCarteira = comp.IdCarteira }).FirstOrDefault();

                                var getLogCarteira = GetLastLogCarteira(idPessoa);
                                if (getLogCarteira == null)
                                {
                                    CarteiraLog objNew = new CarteiraLog()
                                    {
                                        Amount = comp.Valor,
                                        AvaliableBalance = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? comp.Valor : 0,
                                        Data = comp.DtAlteracao,
                                        Descricao = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "Withdrawal Completed" : "Withdrawal refused",
                                        FlagAtivo = true,
                                        IdPessoa = idPessoa,
                                        Tipo = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "retirada aprovado" : "retirada recusado",
                                    };

                                    InserLogCarteira(objNew);
                                }
                                else
                                {
                                    getLogCarteira.Descricao = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "Withdrawal Completed" : "Withdrawal refused";
                                    getLogCarteira.Tipo = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? "retirada aprovado" : "retirada recusado";
                                    getLogCarteira.Amount = comp.Valor;
                                    getLogCarteira.AvaliableBalance = (comp.IdStatus == EnumStatusCarteira.Aprovado) ? getLogCarteira.AvaliableBalance - comp.Valor : comp.Valor;
                                    getLogCarteira.Data = comp.DtAlteracao;

                                    InserLogCarteira(getLogCarteira);

                                }
                            }
                        }
                        catch { }



                    }

                    if (comp.IdPessoa == 0)
                    {
                        StringBuilder strGet = new StringBuilder();
                        strGet.Append(" SELECT [Id] ");
                        strGet.Append("      ,[IdPessoa] ");
                        strGet.Append("      ,[Saldo] ");
                        strGet.Append("      ,[DtInclusao] ");
                        strGet.Append("      ,[IdUsuarioInclusao] ");
                        strGet.Append("      ,[DtAlteracao] ");
                        strGet.Append("      ,[IdUsuarioAlteracao] ");
                        strGet.Append("  FROM [dbo].[Carteira] where Id = @idCarteira");

                        var objCarteira = _db.Query<Carteira>(strGet.ToString(), new { idCarteira = comp.IdCarteira }).FirstOrDefault();

                        string getMail = " select Email from[user].Usuarios where IdPessoa = @idPessoa";
                        comp.EmailCliente = _db.Query<string>(getMail, new { idPessoa = objCarteira.IdPessoa }).FirstOrDefault();
                    }
                    else
                    {
                        string getMail = " select Email from[user].Usuarios where IdPessoa = @idPessoa";
                        comp.EmailCliente = _db.Query<string>(getMail, new { idPessoa = comp.IdPessoa }).FirstOrDefault();

                    }



                    return comp;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "InsertComprovanteRetirada");

                return null;
            }
        }

        public IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaPendentes()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT P.Nome as NomeCliente, CD.[Id] ");
                    strGet.Append("      ,CD.[IdCarteira] ");
                    strGet.Append("      ,CD.[Valor] ");
                    strGet.Append("      ,CD.[ComprovantePathLocation] ");
                    strGet.Append("      ,CD.[ComprovantePathLocationIIS] ");
                    strGet.Append("      ,CD.[FlagPendente] ");
                    strGet.Append("      ,CD.[FlagNegado] ");
                    strGet.Append("      ,CD.[FlagComputado] ");
                    strGet.Append("      ,CD.[IdStatus] ");
                    strGet.Append("      ,CD.[IdUsuarioInclusao] ");
                    strGet.Append("      ,CD.[DtInclusao] ");
                    strGet.Append("      ,CD.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,CD.[DtAlteracao] ");
                    strGet.Append("      ,CD.[SwiftCode] ");
                    strGet.Append("      ,CD.[FlagAtivoSwiftCode] ");
                    strGet.Append("  FROM [dbo].[ComprovanteRetirada] CD inner join [dbo].[Carteira] C on C.Id = CD.IdCarteira inner join [crm].[Pessoas] P on P.Id = C.IdPessoa");
                    strGet.Append("  where CD.FlagPendente = 1 and CD.[IdStatus] != 2 order by CD.[DtInclusao] ");

                    return _db.Query<ComprovanteRetirada>(strGet.ToString()).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetListComprovanteRetiradaByIdCarteira");

                return null;
            }
        }




        public Carteira InsertCarteira(Carteira car)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsett = new StringBuilder();
                    strInsett.Append(" INSERT INTO [dbo].[Carteira] ");
                    strInsett.Append("          ( [IdPessoa] ");
                    strInsett.Append("          ,[Saldo] ");
                    strInsett.Append("          ,[DtInclusao] ");
                    strInsett.Append("          ,[IdUsuarioInclusao] ");
                    strInsett.Append("           ) ");
                    strInsett.Append("    VALUES ");
                    strInsett.Append("          (@IdPessoa ");
                    strInsett.Append("          ,@Saldo ");
                    strInsett.Append("          ,getDate() ");
                    strInsett.Append("          ,@IdUsuarioInclusao ");
                    strInsett.Append(")");

                    _db.Execute(strInsett.ToString(), new
                    {
                        IdPessoa = car.IdPessoa,
                        Saldo = car.Saldo,
                        IdUsuarioInclusao = car.IdUsuarioInclusao
                    });

                    return car;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "InsertCarteira");

                return null;
            }
        }

        public Carteira UpdateCarteira(Carteira car)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [dbo].[Carteira] ");
                    strUpdate.Append("   SET [IdPessoa] = @IdPessoa ");
                    strUpdate.Append("      ,[Saldo] = @Saldo ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate() ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append(" WHERE  Id = @id");


                    _db.Execute(strUpdate.ToString(), new
                    {
                        id = car.Id,
                        IdPessoa = car.IdPessoa,
                        Saldo = car.Saldo,
                        IdUsuarioAlteracao = car.IdUsuarioAlteracao

                    });

                    return car;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "UpdateCarteira");

                return null;
            }
        }


        public IEnumerable<PoolBensPessoas> GetListPoolBensByIdPessoa(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT PBP.[Id]");
                    strGet.Append("   ,PBP.[IdPoolBem]");
                    strGet.Append("   ,PBP.[IdPessoa] ");
                    strGet.Append("   ,PBP.[Cota] ");
                    strGet.Append("   ,PBP.[IdStatus] ");
                    strGet.Append("   ,PBP.[IdUsuarioInclusao] ");
                    strGet.Append("   ,PBP.[DtInclusao] ");
                    strGet.Append("   ,PBP.[IdUsuarioAlteracao] ");
                    strGet.Append("   ,PBP.[DtAlteracao] ");
                    strGet.Append("   ,PB.[Id] ");
                    strGet.Append("   ,PB.[IdContrato] ");
                    strGet.Append("   ,PB.[Nome] ");
                    strGet.Append("   ,PB.[PathLocationIIS] ");
                    strGet.Append("   ,PB.[PathLocation] ");
                    strGet.Append("   ,PB.[DtInicio] ");
                    strGet.Append("   ,PB.[DtVigencia] ");
                    strGet.Append("   ,PB.[IdStatus] ");
                    strGet.Append("   ,PB.[IdUsuarioInclusao] ");
                    strGet.Append("   ,PB.[DtInclusao] ");
                    strGet.Append("   ,PB.[IdUsuarioAlteracao] ");
                    strGet.Append("   ,PB.[DtAlteracao] ");
                    strGet.Append("   ,PB.[Cotas] ");
                    strGet.Append("   ,B.[Id] ");
                    strGet.Append("   ,B.[IdPool] ");
                    strGet.Append("   ,B.[Nome] ");
                    strGet.Append("   ,B.[CapitalInvestido] ");
                    strGet.Append("   ,B.[IdStatus] ");
                    strGet.Append("   ,B.[IdUsuarioInclusao] ");
                    strGet.Append("   ,B.[DtInclusao] ");
                    strGet.Append("   ,B.[IdUsuarioAlteracao] ");
                    strGet.Append("   ,B.[DtAlteracao] ");
                    strGet.Append("   ,B.[Descritivo] ");

                    //strGet.Append("               ,");
                    //strGet.Append("   PBR.[Id]      ,PBR.[IdPoolBens]");
                    //strGet.Append("  ,PBR.[iAno]      ,PBR.[iMes]      ,PBR.[mRentabilidade]      ,PBR.[mPerformance]      ,PBR.[flgAtivo]");
                    //strGet.Append("  ,PBR.[dtInclusao]      ,PBR.[idUsuarioinclusao]");

                    strGet.Append("  FROM [financ].[PoolBensPessoas] PBP");
                    strGet.Append("  inner join [financ].[PoolBens] PB on PB.Id = PBP.IdPoolBem");
                    strGet.Append("  inner join [financ].[Bens] B  on B.IdPool = PB.Id");

                 //   strGet.Append("  inner join [PoolsBensRentabilidadePerformance] PBR on PBR.IdPoolBens = PBP.[IdPoolBem]");

                    strGet.Append("  where PBP.[IdStatus]  = 1 and PBP.[IdPessoa]  =" + idPessoa);

                    Dictionary<int, PoolBensPessoas> lookup = new Dictionary<int, PoolBensPessoas>();
                    List<Bens> Bens = new List<Bens>();


                    var query = _db.Query<PoolBensPessoas, PoolBens, Bens,   PoolBensPessoas>(strGet.ToString(),
                          (PBP, PB, B ) =>
                          {
                          
                              PoolBensPessoas controll;
                              if (!lookup.TryGetValue(PBP.Id, out controll))
                              {

                                  lookup.Add(PBP.Id, PBP);
                                  controll = PBP;
                              }

                              if (!Bens.Contains(B))
                              {
                                  Bens.Add(B);
                                  PB._Bens = Bens;
                              //    if (PB._PoolsBensRentabilidadePerformance == null)
                              //        PB._PoolsBensRentabilidadePerformance = new List<PoolsBensRentabilidadePerformance>();
                                //  PB._PoolsBensRentabilidadePerformance.Add(PBR);
                                  PBP._PoolBens = PB;
                              }

                              return controll;

                          }, splitOn: "Id").Distinct();


                    var retorno = query;
                    PoolBensRepository _poolBensRepository = new PoolBensRepository();
                    foreach (var item in retorno)
                    {
                        item._PoolBens._PoolsBensRentabilidadePerformance = new List<PoolsBensRentabilidadePerformance>();
                        var listagem = _poolBensRepository.GetListPoolsBensRentabilidadePerformance(item._PoolBens.Id);
                        if (listagem.Count() > 0)
                            item._PoolBens._PoolsBensRentabilidadePerformance.AddRange(listagem);

                    }

                    return retorno;

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public IEnumerable<PoolBensPessoas> GetListPoolBensByIdPessoaSimples(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT PBP.[Id]");
                    strGet.Append("   ,PBP.[IdPoolBem]");
                    strGet.Append("   ,PBP.[IdPessoa] ");
                    strGet.Append("   ,PBP.[Cota] ");
                    strGet.Append("   ,PBP.[IdStatus] ");
                    strGet.Append("   ,PBP.[IdUsuarioInclusao] ");
                    strGet.Append("   ,PBP.[DtInclusao] ");
                    strGet.Append("   ,PBP.[IdUsuarioAlteracao] ");
                    strGet.Append("   ,PBP.[DtAlteracao] ");
                    strGet.Append("   ,PB.[Id] ");
                    strGet.Append("   ,PB.[IdContrato] ");
                    strGet.Append("   ,PB.[Nome] ");
                    strGet.Append("   ,PB.[PathLocationIIS] ");
                    strGet.Append("   ,PB.[PathLocation] ");
                    strGet.Append("   ,PB.[DtInicio] ");
                    strGet.Append("   ,PB.[DtVigencia] ");
                    strGet.Append("   ,PB.[IdStatus] ");
                    strGet.Append("   ,PB.[IdUsuarioInclusao] ");
                    strGet.Append("   ,PB.[DtInclusao] ");
                    strGet.Append("   ,PB.[IdUsuarioAlteracao] ");
                    strGet.Append("   ,PB.[DtAlteracao] ");
                    strGet.Append("   ,PB.[Cotas] "); 
                    strGet.Append("   ,B.[Id] ");
                    strGet.Append("   ,B.[IdPool] ");
                    strGet.Append("   ,B.[Nome] ");
                    strGet.Append("   ,B.[CapitalInvestido] ");
                    strGet.Append("   ,B.[IdStatus] ");
                    strGet.Append("   ,B.[IdUsuarioInclusao] ");
                    strGet.Append("   ,B.[DtInclusao] ");
                    strGet.Append("   ,B.[IdUsuarioAlteracao] ");
                    strGet.Append("   ,B.[DtAlteracao] ");
                    strGet.Append("   ,B.[Descritivo] ");

                    //strGet.Append("               ,");
                    //strGet.Append("   PBR.[Id]      ,PBR.[IdPoolBens]");
                    //strGet.Append("  ,PBR.[iAno]      ,PBR.[iMes]      ,PBR.[mRentabilidade]      ,PBR.[mPerformance]      ,PBR.[flgAtivo]");
                    //strGet.Append("  ,PBR.[dtInclusao]      ,PBR.[idUsuarioinclusao]");

                    strGet.Append("  FROM [financ].[PoolBensPessoas] PBP");
                    strGet.Append("  inner join [financ].[PoolBens] PB on PB.Id = PBP.IdPoolBem");
                    strGet.Append("  inner join [financ].[Bens] B  on B.IdPool = PB.Id");

                    //   strGet.Append("  inner join [PoolsBensRentabilidadePerformance] PBR on PBR.IdPoolBens = PBP.[IdPoolBem]");

                    strGet.Append("  where PBP.[IdStatus]  = 1 and PBP.[IdPessoa]  =" + idPessoa);

                    Dictionary<int, PoolBensPessoas> lookup = new Dictionary<int, PoolBensPessoas>();
                    List<Bens> Bens = new List<Bens>();


                    var query = _db.Query<PoolBensPessoas, PoolBens, Bens, PoolBensPessoas>(strGet.ToString(),
                          (PBP, PB, B) =>
                          {

                              PoolBensPessoas controll;
                              if (!lookup.TryGetValue(PBP.Id, out controll))
                              {

                                  lookup.Add(PBP.Id, PBP);
                                  controll = PBP;
                              }

                              if (!Bens.Contains(B))
                              {
                                  Bens.Add(B);
                                  PB._Bens = Bens;
                                 
                                  PBP._PoolBens = PB;
                              }

                              return controll;

                          }, splitOn: "Id").Distinct();


                    var retorno = query;
                    //PoolBensRepository _poolBensRepository = new PoolBensRepository();
                    //foreach (var item in retorno)
                    //{
                    //    item._PoolBens._PoolsBensRentabilidadePerformance = new List<PoolsBensRentabilidadePerformance>();
                    //    var listagem = _poolBensRepository.GetListPoolsBensRentabilidadePerformance(item._PoolBens.Id);
                    //    if (listagem.Count() > 0)
                    //        item._PoolBens._PoolsBensRentabilidadePerformance.AddRange(listagem);

                    //}

                    return retorno;

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

     

        public Pessoas AtualizaDadosPessoa(int idPessoa)
        {
            throw new NotImplementedException();
        }




        public Pessoas PutValidaUsuarioInicial(string sUsuario)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT P.[Id] ");
                    strGet.Append("      ,P.[Nome] ");
                    strGet.Append("      ,P.[DtNascimento] ");
                    strGet.Append("      ,P.[IdStatus] ");
                    strGet.Append("      ,P.[IdUsuarioInclusao] ");
                    strGet.Append("      ,P.[DtInclusao] ");
                    strGet.Append("      ,P.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,P.[DtAlteracao] ");
                    strGet.Append("      ,U.[Id] ");
                    strGet.Append("      ,U.[Nome] ");
                    strGet.Append("      ,U.[Email] ");
                    strGet.Append("      ,U.[Admin] ");
                    strGet.Append("      ,U.[Bloqueado] ");
                    strGet.Append("      ,U.[Saltkey] ");
                    strGet.Append("      ,U.[IdStatus] ");
                    strGet.Append("      ,U.[IdUsuarioInclusao] ");
                    strGet.Append("      ,U.[DtInclusao] ");
                    strGet.Append("      ,U.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,U.[DtAlteracao] ");
                    strGet.Append("      ,U.[IdPessoa]");
                    strGet.Append("      ,PF.[Id]");
                    strGet.Append("      ,PF.[IdPessoa]");
                    strGet.Append("      ,PF.[CPF]");
                    strGet.Append("      ,PF.[Apelido]");
                    strGet.Append("      ,PF.[IdUsuarioInclusao]");
                    strGet.Append("      ,PF.[DtInclusao]");
                    strGet.Append("      ,PF.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PF.[DtAlteracao]");
                    strGet.Append("      ,PJ.[Id]");
                    strGet.Append("      ,PJ.[IdPessoa]");
                    strGet.Append("      ,PJ.[Fantasia]");
                    strGet.Append("      ,PJ.[CNPJ]");
                    strGet.Append("      ,PJ.[IdUsuarioInclusao]");
                    strGet.Append("      ,PJ.[DtInclusao]");
                    strGet.Append("      ,PJ.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PJ.[DtAlteracao] ");
                    strGet.Append("      ,L.[Id]  ");
                    strGet.Append("      ,L.[IdUsuario] ");
                    strGet.Append("      ,L.[Login] ");
                    strGet.Append("      ,L.[Senha] ");
                    strGet.Append("      ,L.[Bloqueado] ");
                    strGet.Append("      ,L.[Tentativas] ");
                    strGet.Append("      ,L.[IdUsuarioInclusao] ");
                    strGet.Append("      ,L.[DtInclusao] ");
                    strGet.Append("      ,L.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,L.[DtAlteracao] ");


                    strGet.Append("          from [crm].[Pessoas] P ");
                    strGet.Append("      Inner join [user].[Usuarios] U on P.Id = U.IdPessoa ");
                    strGet.Append("      Inner join [user].[Logins] L on L.IdUsuario = U.Id ");
                    strGet.Append("      left JOin [crm].[PessoasFisica] PF on PF.IdPessoa = P.Id ");
                    strGet.Append("      left JOin [crm].[PessoasJuridica] PJ  on PJ.IdPessoa = P.Id  where L.[Login] =  '" + sUsuario + "'");

                    var res = _db.Query<Pessoas, Usuarios, PessoasFisicas, PessoasJuridica, Logins, Pessoas>(strGet.ToString(),
                        (P, U, PF, PJ, L) =>
                        {
                            P._Usuario = U;
                            P._Usuario._Login = L;
                            P._PessoaFisica = PF;
                            P._PessoaJuridica = PJ;

                            P._Usuario._Login = L;
                            return P;

                        }, splitOn: "Id").FirstOrDefault();

                    return res;

                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Pessoas PutValidaEmailInicial(string sEmail)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT P.[Id] ");
                    strGet.Append("      ,P.[Nome] ");
                    strGet.Append("      ,P.[DtNascimento] ");
                    strGet.Append("      ,P.[IdStatus] ");
                    strGet.Append("      ,P.[IdUsuarioInclusao] ");
                    strGet.Append("      ,P.[DtInclusao] ");
                    strGet.Append("      ,P.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,P.[DtAlteracao] ");
                    strGet.Append("      ,U.[Id] ");
                    strGet.Append("      ,U.[Nome] ");
                    strGet.Append("      ,U.[Email] ");
                    strGet.Append("      ,U.[Admin] ");
                    strGet.Append("      ,U.[Bloqueado] ");
                    strGet.Append("      ,U.[Saltkey] ");
                    strGet.Append("      ,U.[IdStatus] ");
                    strGet.Append("      ,U.[IdUsuarioInclusao] ");
                    strGet.Append("      ,U.[DtInclusao] ");
                    strGet.Append("      ,U.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,U.[DtAlteracao] ");
                    strGet.Append("      ,U.[IdPessoa]");
                    strGet.Append("      ,PF.[Id]");
                    strGet.Append("      ,PF.[IdPessoa]");
                    strGet.Append("      ,PF.[CPF]");
                    strGet.Append("      ,PF.[Apelido]");
                    strGet.Append("      ,PF.[IdUsuarioInclusao]");
                    strGet.Append("      ,PF.[DtInclusao]");
                    strGet.Append("      ,PF.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PF.[DtAlteracao]");
                    strGet.Append("      ,PJ.[Id]");
                    strGet.Append("      ,PJ.[IdPessoa]");
                    strGet.Append("      ,PJ.[Fantasia]");
                    strGet.Append("      ,PJ.[CNPJ]");
                    strGet.Append("      ,PJ.[IdUsuarioInclusao]");
                    strGet.Append("      ,PJ.[DtInclusao]");
                    strGet.Append("      ,PJ.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PJ.[DtAlteracao] ");
                    strGet.Append("      ,L.[Id]  ");
                    strGet.Append("      ,L.[IdUsuario] ");
                    strGet.Append("      ,L.[Login] ");
                    strGet.Append("      ,L.[Senha] ");
                    strGet.Append("      ,L.[Bloqueado] ");
                    strGet.Append("      ,L.[Tentativas] ");
                    strGet.Append("      ,L.[IdUsuarioInclusao] ");
                    strGet.Append("      ,L.[DtInclusao] ");
                    strGet.Append("      ,L.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,L.[DtAlteracao] ");


                    strGet.Append("          from [crm].[Pessoas] P ");
                    strGet.Append("      Inner join [user].[Usuarios] U on P.Id = U.IdPessoa ");
                    strGet.Append("      Inner join [user].[Logins] L on L.IdUsuario = U.Id ");
                    strGet.Append("      left JOin [crm].[PessoasFisica] PF on PF.IdPessoa = P.Id ");
                    strGet.Append("      left JOin [crm].[PessoasJuridica] PJ  on PJ.IdPessoa = P.Id  where U.[Email] =  '" + sEmail + "'");

                    var res = _db.Query<Pessoas, Usuarios, PessoasFisicas, PessoasJuridica, Logins, Pessoas>(strGet.ToString(),
                        (P, U, PF, PJ, L) =>
                        {
                            P._Usuario = U;
                            P._Usuario._Login = L;
                            P._PessoaFisica = PF;
                            P._PessoaJuridica = PJ;

                            P._Usuario._Login = L;
                            return P;

                        }, splitOn: "Id").FirstOrDefault();

                    return res;


                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public IEnumerable<Pessoas> GetListPessoasIndex(bool fAll)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT P.[Id] ");
                    strGet.Append("      ,P.[Nome] ");
                    strGet.Append("      ,P.[FlagResidenteAmericado] ");
                    strGet.Append("      ,P.[DtNascimento] ");
                    strGet.Append("      ,P.[IdStatus] ");
                    strGet.Append("      ,P.[IdUsuarioInclusao] ");
                    strGet.Append("      ,P.[DtInclusao] ");
                    strGet.Append("      ,P.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,P.[DtAlteracao] ");
                    strGet.Append("      ,P.[TelefoneComercial]");
                    strGet.Append("      ,P.[TelefoneCelular]");
                    strGet.Append("      ,P.[ValorInicial]");
                    strGet.Append("      ,P.[fCondicao1]");
                    strGet.Append("      ,P.[fCondicao2]");
                    strGet.Append("      ,P.[fCondicao3]");
                    strGet.Append("      ,P.[fCondicao4]");
                    strGet.Append("      ,P.[fCondicao5]");
                    strGet.Append("      ,P.[fCondicao6]");
                    strGet.Append("      ,P.[fConfirmoDados]");
                    strGet.Append("      ,U.[Id] ");
                    strGet.Append("      ,U.[Nome] ");
                    strGet.Append("      ,U.[Email] ");
                    strGet.Append("      ,U.[Admin] ");
                    strGet.Append("      ,U.[Bloqueado] ");
                    strGet.Append("      ,U.[Saltkey] ");
                    strGet.Append("      ,U.[IdStatus] ");
                    strGet.Append("      ,U.[IdUsuarioInclusao] ");
                    strGet.Append("      ,U.[DtInclusao] ");
                    strGet.Append("      ,U.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,U.[DtAlteracao] ");
                    strGet.Append("      ,U.[IdPessoa]");
                    strGet.Append("      ,U.[CadastroValido]");
                    strGet.Append("      ,PF.[Id]");
                    strGet.Append("      ,PF.[IdPessoa]");
                    strGet.Append("      ,PF.[CPF]");
                    strGet.Append("      ,PF.[Apelido]");
                    strGet.Append("      ,PF.[IdUsuarioInclusao]");
                    strGet.Append("      ,PF.[DtInclusao]");
                    strGet.Append("      ,PF.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PF.[DtAlteracao]");
                    strGet.Append("      ,PJ.[Id]");
                    strGet.Append("      ,PJ.[IdPessoa]");
                    strGet.Append("      ,PJ.[Fantasia]");
                    strGet.Append("      ,PJ.[CNPJ]");
                    strGet.Append("      ,PJ.[IdUsuarioInclusao]");
                    strGet.Append("      ,PJ.[DtInclusao]");
                    strGet.Append("      ,PJ.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PJ.[DtAlteracao] ");
                    strGet.Append("      ,L.[Id]  ");
                    strGet.Append("      ,L.[IdUsuario] ");
                    strGet.Append("      ,L.[Login] ");
                    strGet.Append("      ,L.[Senha] ");
                    strGet.Append("      ,L.[Bloqueado] ");
                    strGet.Append("      ,L.[Tentativas] ");
                    strGet.Append("      ,L.[IdUsuarioInclusao] ");
                    strGet.Append("      ,L.[DtInclusao] ");
                    strGet.Append("      ,L.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,L.[DtAlteracao] ");


                    strGet.Append("          from [crm].[Pessoas] P ");
                    strGet.Append("      Inner join [user].[Usuarios] U on P.Id = U.IdPessoa ");
                    strGet.Append("      Inner join [user].[UsuariosGrupos] UG on U.Id = UG.[IdUsuario] ");
                    strGet.Append("      Inner join [user].[Logins] L on L.IdUsuario = U.Id ");
                    strGet.Append("      left join [crm].[PessoasFisica] PF on PF.IdPessoa = P.Id ");
                    strGet.Append("      left join [crm].[PessoasJuridica] PJ  on PJ.IdPessoa = P.Id");

                    if (!fAll)
                    {
                        strGet.Append(" where  UG.IdGrupo not in ( " + (int)EnumPerfil.Administrador + "," + (int)EnumPerfil.Administrador_Super + ")");
                    }

                    var res = _db.Query<Pessoas, Usuarios, PessoasFisicas, PessoasJuridica, Logins, Pessoas>(strGet.ToString(),
                        (P, U, PF, PJ, L) =>
                        {
                            P._Usuario = U;
                            P._Usuario._Login = L;
                            P._PessoaFisica = PF;
                            P._PessoaJuridica = PJ;

                            P._Usuario._Login = L;
                            return P;

                        }, splitOn: "Id").AsEnumerable();

                    return res;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetPessoasById");

                return null;
            }
        }


        public IEnumerable<Pessoas> GetListPessoas(bool fAll)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT P.[Id] ");
                    strGet.Append("      ,P.[Nome] ");
                    strGet.Append("      ,P.[FlagResidenteAmericado] ");
                    strGet.Append("      ,P.[DtNascimento] ");
                    strGet.Append("      ,P.[IdStatus] ");
                    strGet.Append("      ,P.[IdUsuarioInclusao] ");
                    strGet.Append("      ,P.[DtInclusao] ");
                    strGet.Append("      ,P.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,P.[DtAlteracao] ");
                    strGet.Append("      ,P.[TelefoneComercial]");
                    strGet.Append("      ,P.[TelefoneCelular]");
                    strGet.Append("      ,P.[ValorInicial]");
                    strGet.Append("      ,P.[fCondicao1]");
                    strGet.Append("      ,P.[fCondicao2]");
                    strGet.Append("      ,P.[fCondicao3]");
                    strGet.Append("      ,P.[fCondicao4]");
                    strGet.Append("      ,P.[fCondicao5]");
                    strGet.Append("      ,P.[fCondicao6]");
                    strGet.Append("      ,P.[fConfirmoDados]");
                    strGet.Append("      ,U.[Id] ");
                    strGet.Append("      ,U.[Nome] ");
                    strGet.Append("      ,U.[Email] ");
                    strGet.Append("      ,U.[Admin] ");
                    strGet.Append("      ,U.[Bloqueado] ");
                    strGet.Append("      ,U.[Saltkey] ");
                    strGet.Append("      ,U.[IdStatus] ");
                    strGet.Append("      ,U.[IdUsuarioInclusao] ");
                    strGet.Append("      ,U.[DtInclusao] ");
                    strGet.Append("      ,U.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,U.[DtAlteracao] ");
                    strGet.Append("      ,U.[IdPessoa]");
                    strGet.Append("      ,PF.[Id]");
                    strGet.Append("      ,PF.[IdPessoa]");
                    strGet.Append("      ,PF.[CPF]");
                    strGet.Append("      ,PF.[Apelido]");
                    strGet.Append("      ,PF.[IdUsuarioInclusao]");
                    strGet.Append("      ,PF.[DtInclusao]");
                    strGet.Append("      ,PF.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PF.[DtAlteracao]");
                    strGet.Append("      ,PJ.[Id]");
                    strGet.Append("      ,PJ.[IdPessoa]");
                    strGet.Append("      ,PJ.[Fantasia]");
                    strGet.Append("      ,PJ.[CNPJ]");
                    strGet.Append("      ,PJ.[IdUsuarioInclusao]");
                    strGet.Append("      ,PJ.[DtInclusao]");
                    strGet.Append("      ,PJ.[IdUsuarioAlteracao]");
                    strGet.Append("      ,PJ.[DtAlteracao] ");
                    strGet.Append("      ,L.[Id]  ");
                    strGet.Append("      ,L.[IdUsuario] ");
                    strGet.Append("      ,L.[Login] ");
                    strGet.Append("      ,L.[Senha] ");
                    strGet.Append("      ,L.[Bloqueado] ");
                    strGet.Append("      ,L.[Tentativas] ");
                    strGet.Append("      ,L.[IdUsuarioInclusao] ");
                    strGet.Append("      ,L.[DtInclusao] ");
                    strGet.Append("      ,L.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,L.[DtAlteracao] ");


                    strGet.Append("          from [crm].[Pessoas] P ");
                    strGet.Append("      Inner join [user].[Usuarios] U on P.Id = U.IdPessoa ");
                    strGet.Append("      Inner join [user].[UsuariosGrupos] UG on U.Id = UG.[IdUsuario] ");
                    strGet.Append("      Inner join [user].[Logins] L on L.IdUsuario = U.Id ");
                    strGet.Append("      left join [crm].[PessoasFisica] PF on PF.IdPessoa = P.Id ");
                    strGet.Append("      left join [crm].[PessoasJuridica] PJ  on PJ.IdPessoa = P.Id");

                    if (!fAll)
                    {
                        strGet.Append(" where P.[IdStatus] = 1 and UG.IdGrupo not in ( " + (int)EnumPerfil.Administrador + "," + (int)EnumPerfil.Administrador_Super + ")");
                    }

                    var res = _db.Query<Pessoas, Usuarios, PessoasFisicas, PessoasJuridica, Logins, Pessoas>(strGet.ToString(),
                        (P, U, PF, PJ, L) =>
                        {
                            P._Usuario = U;
                            P._Usuario._Login = L;
                            P._PessoaFisica = PF;
                            P._PessoaJuridica = PJ;

                            P._Usuario._Login = L;
                            return P;

                        }, splitOn: "Id").AsEnumerable();

                    return res;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasRepository", "GetPessoasById");

                return null;
            }
        }

        public IEnumerable<HistoricoCarteira> GetHistoricoByIdPessoa(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    List<HistoricoCarteira> RetornoList = new List<HistoricoCarteira>();


                    StringBuilder str1 = new StringBuilder();
                    str1.Append(" select CD.Id as Id,  '$ '+Replace(Replace(Replace(CONVERT(varchar, CAST( CD.Valor AS money), 1),'.',';'),',','.'),';',',') as Valores, CD.DtInclusao as Data, ");
                    str1.Append(" '<td class=\"fa fa-money\" style=\"color:#607d8b\"> Avaliable</td>'   as Status   ");
                    str1.Append("  from [dbo].[ComprovanteDeposito] CD ");
                    str1.Append(" inner join [dbo].[Carteira] C on C.Id = CD.IdCarteira  ");
                    str1.Append("  where C.idPessoa =@idPessoa  order by CD.DtInclusao  ");


                    StringBuilder str2 = new StringBuilder();
                    str2.Append(" select CD.Id as Id,  '$ '+Replace(Replace(Replace(CONVERT(varchar, CAST( CD.Valor AS money), 1),'.',';'),',','.'),';',',') as Valores, ISNULL(CD.DtAlteracao, CD.DtInclusao) as Data,   ");
                    str2.Append(" CASE When CD.IdStatus = 5 then  '<td class=\"fa fa-envelope-square\" style=\"color:#8b6d00\"> wating</td>' When CD.IdStatus = 8 then '<td class=\"fa fa-envelope-open\" style=\"color:blue\"> Processing</td>' else '<td class=\"fa fa-envelope-open\" style=\"color:#35357e\"> Pending</td>' end as Status  ");
                    str2.Append("  from [dbo].[ComprovanteDeposito] CD  ");
                    str2.Append(" inner join [dbo].[Carteira] C on C.Id = CD.IdCarteira   ");
                    str2.Append("  where C.idPessoa =@idPessoa  order by CD.DtAlteracao   ");

                    StringBuilder str3 = new StringBuilder();
                    str3.Append(" select CD.Id as Id,  '$ '+Replace(Replace(Replace(CONVERT(varchar, CAST( CD.Valor AS money), 1),'.',';'),',','.'),';',',') as Valores,  CD.DtAprovaReprova as Data,   ");
                    str3.Append("CASE When CD.IdStatus = 6 then '<td class=\"fa fa-check\" style=\"color:green\"> Approved</td>' When CD.IdStatus = 7 then '<td class=\"fa fa-times\" style=\"color:red\"> Disapproved</td>' else '<td class=\"fa fa-envelope-open\" style=\"color:#35357e\"> Processing</td>' end as Status  ");
                    str3.Append(" from [dbo].[ComprovanteDeposito] CD  ");
                    str3.Append("inner join [dbo].[Carteira] C on C.Id = CD.IdCarteira   ");
                    str3.Append(" where C.idPessoa =@idPessoa and CD.DtAprovaReprova is not null  order by CD.DtInclusao   ");

                    StringBuilder str4 = new StringBuilder();
                    str4.Append(" select PBP.Id as Id, 'Acquisition '  ");
                    // str4.Append(" + Convert(varchar,PBP.Cota) + ' Units of : US$ '+Replace(Replace(Replace(CONVERT(varchar, CAST( (PBP.Cota*(sum(B.CapitalInvestido)/PB.Cotas)) AS money), 1),'.',';'),',','.'),';',',') as Valores,  ");
                    str4.Append(" + Convert(varchar,PBP.Cota) + ' Units of '+ PB.Nome as Valores,  ");
                    str4.Append(" PBP.DtInclusao as Data, CASE ");
                    str4.Append("  When PBP.IdStatus = 1 then  '<td class=\"fa fa-paper-plane\" style=\"color:#0288d1\"> Invested</td>'");
                    str4.Append("  When PBP.IdStatus = 9 then  '<td class=\"fa fa-paper-plane\" style=\"color:blue\"> Sale Requested</td>'");
                    str4.Append("  When PBP.IdStatus = 10 then  '<td class=\"fa fa-paper-plane\" style=\"color:green\"> Sale Approved</td>'");
                    str4.Append("  When PBP.IdStatus = 11 then  '<td class=\"fa fa-times\" style=\"color:red\"> Disapproved Sale</td>'");
                    str4.Append("  When PBP.IdStatus = 12 then  '<td class=\"fa fa-paper-plane\" style=\"color:#0288d1\"> Purchase Order</td>'");
                    str4.Append("  When PBP.IdStatus = 13 then  '<td class=\"fa fa-paper-plane\" style=\"color:green\"> Approved purchase</td>'");
                    str4.Append("  When PBP.IdStatus = 14 then  '<td class=\"fa fa-times\" style=\"color:red\"> Purchase declined</td>' else '' end ");
                    str4.Append(" as  Status  ");
                    str4.Append(" from   ");
                    str4.Append("[financ].[PoolBensPessoas]  PBP   ");
                    str4.Append("inner join [financ].[PoolBens] PB on PB.Id = PBP.IdPoolBem  ");
                    str4.Append("inner join [financ].[Bens] B on PB.Id = B.IdPool  ");
                    str4.Append("where IdPessoa=@idPessoa ");
                    str4.Append("group by  PBP.Id, PBP.Cota,PB.Cotas,PBP.DtInclusao, PB.Nome, PBP.IdStatus  ");

                    StringBuilder str5 = new StringBuilder();
                    str5.Append(" select Id, '$ '+ CAST(Valor as varchar) as Valores, DtLancamento as Data,  ");
                    str5.Append("  '<td class=\"fa fa-check\" style=\"color:green\"> Profit</td>' as Status ");
                    str5.Append("  from financ.Lancamentos where idPessoa = @idPessoa");



                    RetornoList.AddRange(_db.Query<HistoricoCarteira>(str1.ToString(), new { idPessoa = idPessoa }).AsEnumerable());
                    RetornoList.AddRange(_db.Query<HistoricoCarteira>(str2.ToString(), new { idPessoa = idPessoa }).AsEnumerable());
                    RetornoList.AddRange(_db.Query<HistoricoCarteira>(str3.ToString(), new { idPessoa = idPessoa }).AsEnumerable());
                    RetornoList.AddRange(_db.Query<HistoricoCarteira>(str4.ToString(), new { idPessoa = idPessoa }).AsEnumerable());
                    RetornoList.AddRange(_db.Query<HistoricoCarteira>(str5.ToString(), new { idPessoa = idPessoa }).AsEnumerable());

                    var lista = RetornoList.OrderByDescending(c => c.Data);


                    return lista;

                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public InformacoesAdminHome GetInformacoesAdminHome()
        {
            try
            {
                InformacoesAdminHome objRetorno = new InformacoesAdminHome();
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    // soma de saldos disponiveis
                    StringBuilder strGetSaldoDisponivel = new StringBuilder();
                    strGetSaldoDisponivel.Append(" select sum(Saldo) from Carteira C ");
                    strGetSaldoDisponivel.Append("inner join crm.Pessoas P on P.Id = C.IdPessoa where P.IdStatus = 1");

                    objRetorno.SaldoTotalClienteEmCarteira = _db.Query<decimal>(strGetSaldoDisponivel.ToString()).FirstOrDefault();


                    //saldo total disponivel + saldo em Investimentos
                    StringBuilder strGetValoresInvestidos = new StringBuilder();
                    strGetValoresInvestidos.Append(" select  SUM(PBP.Cota) * 100 ");
                    strGetValoresInvestidos.Append(" from financ.PoolBensPessoas PBP   inner join crm.Pessoas P on  ");
                    strGetValoresInvestidos.Append("  P.Id = PBP.IdPessoa and P.IdStatus = 1 where PBP.idStatus = 1    group by  PBP.IdPoolBem   ");

                    objRetorno.SaldoTotalClienteEmInvestimentos = _db.Query<decimal>(strGetValoresInvestidos.ToString()).FirstOrDefault();
                    objRetorno.SaldoTotalClienteEmInvestimentos_e_Carteira = objRetorno.SaldoTotalClienteEmInvestimentos + objRetorno.SaldoTotalClienteEmCarteira;


                    return objRetorno;



                }

            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PessoasRepository", "GetInformacoesAdminHome");

                return null;
            }
        }

        public IEnumerable<PessoaInvestimentoPoolsValores> GetListHomeInvestidores()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" select P.id as idPessoa, P.Nome NomeCliente, C.Saldo SaldoCarteira, PB.Nome NomePool, SUM(PBP.Cota) as Cotas, (SUM(PBP.Cota) * 100)  as ValorInvestido,");
                    strGet.Append("  (select ISNULL(SUM(Valor),0) from financ.Lancamentos where IdPessoa = P.Id)  as DividendYeld ");
                    strGet.Append("   from");
                    strGet.Append(" financ.PoolBensPessoas PBP");
                    strGet.Append(" inner");
                    strGet.Append("   join crm.Pessoas P on P.Id = PBP.IdPessoa");
                    strGet.Append(" inner");
                    strGet.Append("   join dbo.Carteira C on C.IdPessoa = P.Id");
                    strGet.Append(" inner");
                    strGet.Append("   join financ.PoolBens PB on PB.Id = PBP.IdPoolBem and PBP.IdStatus = 1");
                    strGet.Append(" group by P.Nome, C.Saldo, PB.Nome, PBP.IdPoolBem, P.Id order by NomeCliente");

                    return _db.Query<PessoaInvestimentoPoolsValores>(strGet.ToString()).AsEnumerable();

                }
            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PessoasRepository", "GetInformacoesAdminHome");

                return null;
            }
        }

        public IEnumerable<PainelRentabilidadeMes> GetRentabilidadeMesBens(int idPool, int iMes, int iAno)
        {

            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" select  ");
                    strGet.Append(" B.DividendYeld, ");
                    strGet.Append(" B1.CapitalInvestido, ");
                    strGet.Append(" ISNULL((select SUM(Valor) from dbo.BensGastos B2 where B2.IdBem = B1.Id), 0) as Investimentos, ");
                    strGet.Append("  ");
                    strGet.Append(" B1.CapitalInvestido + ");
                    strGet.Append(" ISNULL((select SUM(Valor) from dbo.BensGastos B2 where B2.IdBem = B1.Id),0) as CapitalTotal, ");
                    strGet.Append("  ");
                    strGet.Append(" PB.Cotas ");
                    strGet.Append("  ");
                    strGet.Append(" from[dbo].[financ.BensBaseCalculoHistorico] ");
                    strGet.Append(" B ");
                    strGet.Append(" inner join financ.Bens B1 on B1.Id = B.IdBem ");
                    strGet.Append(" inner join financ.PoolBens PB on PB.Id = B1.IdPool ");
                    strGet.Append(" where B1.IdPool = @idPool and B1.IdStatus = 1 and B.iAno = @iano and B.iMes = @imes");

                    return _db.Query<PainelRentabilidadeMes>(strGet.ToString(), new { idPool = idPool, iAno = iAno, iMes = iMes }).AsEnumerable();

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public IEnumerable<PainelRentabilidadeMes> GetRentabilidadeMesBens(int idPool = 0)
        {

            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    if (idPool == 0)
                    {
                        StringBuilder strGets = new StringBuilder();
                        strGets.Append("  select  B1.Id as idBem, PB.Id as idPool,  B.DividendYeld, ((B1.CapitalInvestido) + (select ISNULL(SUM(Valor),0)  ");
                        strGets.Append("  from dbo.BensGastos B2 where B2.IdBem = B1.Id)) as CapitalTotalInvestido, PB.Cotas,B.iMes,B.iAno, ISNULL(B.percentRentabilidade,0) as percentRentabilidade  ");
                        strGets.Append("  ");
                        strGets.Append("  from[dbo].[financ.BensBaseCalculoHistorico] B inner join financ.Bens B1 on B1.Id = B.IdBem ");
                        strGets.Append("  ");
                        strGets.Append(" inner join financ.PoolBens PB on PB.Id = B1.IdPool ");
                        strGets.Append("  ");
                        strGets.Append(" where B1.IdStatus = 1 ");
                        strGets.Append("  ");
                        strGets.Append(" group by B.DividendYeld , B1.Id, PB.Id, B1.CapitalInvestido, PB.Cotas, B.iMes, B.iAno, B.percentRentabilidade ");

                        return _db.Query<PainelRentabilidadeMes>(strGets.ToString()).AsEnumerable();
                    }
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append("  select  B1.Id as idBem, PB.Id as idPool,  B.DividendYeld, ((B1.CapitalInvestido) + (select ISNULL(SUM(Valor),0)  ");
                    strGet.Append("  from dbo.BensGastos B2 where B2.IdBem = B1.Id)) as CapitalTotalInvestido, PB.Cotas,B.iMes,B.iAno, ISNULL(B.percentRentabilidade,0) as percentRentabilidade ");
                    strGet.Append("  ");
                    strGet.Append("  from[dbo].[financ.BensBaseCalculoHistorico] B inner join financ.Bens B1 on B1.Id = B.IdBem ");
                    strGet.Append("  ");
                    strGet.Append(" inner join financ.PoolBens PB on PB.Id = B1.IdPool ");
                    strGet.Append("  ");
                    strGet.Append(" where B1.IdStatus = 1 and PB.Id = @idPool ");
                    strGet.Append("  ");
                    strGet.Append(" group by B.DividendYeld , B1.Id, PB.Id, B1.CapitalInvestido, PB.Cotas, B.iMes, B.iAno, B.percentRentabilidade");

                    return _db.Query<PainelRentabilidadeMes>(strGet.ToString(), new { idPool = idPool }).AsEnumerable();

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public ValidToken createTokenAcessMobile(int idUsuario, string Device)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    Guid g;
                    // Create and display the value of two GUIDs.
                    g = Guid.NewGuid();

                    ValidToken retorno = new ValidToken()
                    {
                        Device = Device,
                        dtAcesso = DateTime.Now,
                        FlagAtivo = true,
                        IdUsuario = idUsuario,
                        HashValid = g.ToString().Replace("-", "")
                    };

                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO [mobile.ValidToken] ");
                    strInsert.Append("  ([IdUsuario] ");
                    strInsert.Append("  ,[Device] ");
                    strInsert.Append("  ,[HashValid] ");
                    strInsert.Append("  ,[dtAcesso] ");
                    strInsert.Append("  ,[FlagAtivo]) ");
                    strInsert.Append("   VALUES ");
                    strInsert.Append("   (@IdUsuario");
                    strInsert.Append("   ,@Device");
                    strInsert.Append("   ,@HashValid");
                    strInsert.Append("   ,getDate()");
                    strInsert.Append("   ,1)  SELECT IDENT_CURRENT('[mobile.ValidToken]') AS [IDENT_CURRENT]");


                    var idValidToken = _db.Query<int>(strInsert.ToString(), new
                    {
                        IdUsuario = retorno.IdUsuario,
                        Device = retorno.Device,
                        HashValid = retorno.HashValid,

                    }).FirstOrDefault();

                    retorno.Id = idValidToken;

                    return retorno;

                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool validaTokenAcessMobile(int idUsuario, string sHash)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string get = "select * from  [mobile.ValidToken] where IdUsuario  = @idUsuario and [HashValid] = @sHash";

                    var ret = _db.Query<ValidToken>(get, new { idUsuario = idUsuario, sHash = sHash }).FirstOrDefault();

                    var timer = int.Parse(System.Configuration.ConfigurationManager.AppSettings["timerHashMobile"]);

                    if (ret != null)
                    {

                        if (ret.dtAcesso.AddMinutes(timer) > DateTime.Now)
                        {
                            return true;
                        }
                        else
                        {
                            //token invalido
                            string upd = "Update   [mobile.ValidToken] set FlagAtivo = 0 where Id = @id";
                            _db.Execute(upd, new { id = ret.Id });

                            return false;

                        }

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }


        }

        public IEnumerable<CarteiraLog> GetCarteiraLogByIdPessoa(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append("SELECT [Id] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Data] ");
                    strGet.Append("      ,[Descricao] ");
                    strGet.Append("      ,[Tipo] ");
                    strGet.Append("      ,[Amount] ");
                    strGet.Append("      ,[AvaliableBalance] ");
                    strGet.Append("      ,[FlagAtivo], ");



                    strGet.Append("  case  Tipo when 'deposito' then 1 ");
                    strGet.Append("  when 'solicitação compra cota' then 1 ");
                    strGet.Append("  when 'solicitação venda cota' then 1 ");
                    strGet.Append("  when 'retirada' then 1 else 0 end as fRequest, ");
                    strGet.Append("   ");
                    strGet.Append("  case  Tipo when 'deposito aprovado' then 1 ");
                    strGet.Append("  when 'dividendo' then 1 ");
                    strGet.Append("  when 'compra cota aprovado' then 1 ");
                    strGet.Append("  when 'compra cota aprovado' then 1 ");
                    strGet.Append("  when 'retirada aprovado' then 1 else 0 end as fAproved, ");
                    strGet.Append("   ");
                    strGet.Append("    ");
                    strGet.Append("  case  Tipo when 'deposito recusado' then 1 ");
                    strGet.Append("  when 'compra cota recusado' then 1 ");
                    strGet.Append("  when 'venda cota recusado' then 1 ");
                    strGet.Append("  when 'retirada reprovado'then 1 else 0 end as fReproved ");


                    strGet.Append("  FROM [CarteiraLog] where IdPessoa=@IdPessoa  order by Id desc");

                    return _db.Query<CarteiraLog>(strGet.ToString(), new { IdPessoa = idPessoa }).AsEnumerable();

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "GetCarteiraLogByIdPessoa");
                return null;
            }
        }

        public Pessoas PutValidaCPFInicial(string sCPF)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" select * from crm.Pessoas P inner join crm.PessoasFisica PF on PF.IdPessoa = P.Id where CPF=@CPF");

                    return _db.Query<Pessoas>(strGet.ToString(), new { CPF = sCPF }).FirstOrDefault();

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "PutValidaCPFInicial");
                return null;
            }
        }

        public Pessoas PutValidaTAXInicial(string sTAX)
        {

            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append("  select * from crm.Pessoas P inner join crm.PessoasFisica PF on PF.IdPessoa = P.Id where TaxId=@TaxId");

                    return _db.Query<Pessoas>(strGet.ToString(), new { TaxId = sTAX }).FirstOrDefault();

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "PutValidaTAXInicial");
                return null;
            }
        }

        public Pessoas PutValidaSocialInicial(string sSocial)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append("  select * from crm.Pessoas P inner join crm.PessoasFisica PF on PF.IdPessoa = P.Id where SocialNumber=@SocialNumber");

                    return _db.Query<Pessoas>(strGet.ToString(), new { SocialNumber = sSocial }).FirstOrDefault();

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "PutValidaTAXInicial");
                return null;
            }
        }




        public RecoveryPassword ReceveryPasswordByEmail(string sEmail)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetIdUsuario = new StringBuilder();

                    strGetIdUsuario.Append(" select Id from [user].Usuarios where Email = @Email");

                    int idUsuario = _db.Query<int>(strGetIdUsuario.ToString(), new { Email = sEmail }).FirstOrDefault();

                    string token = DateTime.Now.ToString(idUsuario + "-yyyyMMdd-HHmmss");
                    RecoveryPassword obj = new RecoveryPassword()
                    {
                        Id = 0,
                        dtSolicitacao = DateTime.Now,
                        FlagValido = true,
                        IdUsuario = idUsuario,
                        Token = token
                    };
                    if (idUsuario > 0)
                    {


                        StringBuilder strInsert = new StringBuilder();
                        strInsert.Append(" INSERT INTO  [dbo].[RecoveryPassword] ");
                        strInsert.Append("       ([Token] ");
                        strInsert.Append("       ,[IdUsuario] ");
                        strInsert.Append("       ,[dtSolicitacao] ");
                        strInsert.Append("       ,[FlagValido]) ");
                        strInsert.Append(" VALUES ");
                        strInsert.Append("       (@Token ");
                        strInsert.Append("       ,@IdUsuario ");
                        strInsert.Append("       ,@dtSolicitacao ");
                        strInsert.Append("       ,@FlagValido) SELECT IDENT_CURRENT('[dbo].[RecoveryPassword]') AS [IDENT_CURRENT]");


                        var idInsertToken = _db.Query<int>(strInsert.ToString(), new
                        {
                            Token = obj.Token,
                            IdUsuario = obj.IdUsuario,
                            dtSolicitacao = obj.dtSolicitacao,
                            FlagValido = obj.FlagValido
                        }).FirstOrDefault();

                        obj.Id = idInsertToken;
                    }
                    return obj;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "ReceveryPasswordByEmail");
                return null;
            }
        }

        public bool RecoveryPasswordValidaToken(string sToken)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string sValida = "select FlagValido from [dbo].[RecoveryPassword] where Token = @Token ";

                    return _db.Query<bool>(sValida, new { Token = sToken }).FirstOrDefault();


                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "RecoveryPasswordValidaToken");
                return false;
            }
        }


        public bool ReceveryPasswordUpdate(int idUsuario, string password, string sToken)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    idUsuario = _db.Query<int>("select IdUsuario from dbo.RecoveryPassword where Token = @Token", new { Token = sToken }).FirstOrDefault();
                    if (idUsuario == 0)
                        return false;

                    string updateSenha = "UPDATE [user].Logins set Senha = @password where IdUsuario = @idUsuario ";
                    string updateToken = "UPDATE [dbo].[RecoveryPassword] set FlagValido = 0 where Token = @Token ";



                    _db.Query<bool>(updateSenha, new { IdUsuario = idUsuario, password = password }).FirstOrDefault();
                    _db.Query<bool>(updateToken, new { Token = sToken }).FirstOrDefault();

                    return true;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "ReceveryPasswordUpdate");
                return false;
            }
        }



        public bool InsertLogEmailApp(LogEmailApp log)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [LogEmailAPP] ");
                    strInsert.Append("   ([IdPessoa] ");
                    strInsert.Append("   ,[Titulo] ");
                    strInsert.Append("   ,[Mensagem] ");
                    strInsert.Append("   ,[DataEnvio] ");
                    strInsert.Append("   ,[FlagAtivo]) ");
                    strInsert.Append("  VALUES ");
                    strInsert.Append("  (@IdPessoa ");
                    strInsert.Append("  ,@Titulo ");
                    strInsert.Append("  ,@Mensagem ");
                    strInsert.Append("  ,@DataEnvio ");
                    strInsert.Append("  ,1)");

                    _db.Execute(strInsert.ToString(), new
                    {
                        IdPessoa = log.IdPessoa,
                        Titulo = log.Titulo,
                        Mensagem = log.Mensagem,
                        DataEnvio = log.DataEnvio
                    });


                    return true;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "InsertLogEmailApp");
                return false;
            }
        }

        public bool DeleteLogEmailApp(int idLog)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string update = "update [dbo].[LogEmailAPP] set [FlagAtivo] = 0 where Id = @Id ";
                    _db.Execute(update, new { Id = idLog });
                    return true;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "DeleteLogEmailApp");
                return false;
            }
        }

        public IEnumerable<LogEmailApp> GetListLogEmailApp(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string gets = "select * from [dbo].[LogEmailAPP]  where IdPessoa = @IdPessoa order by DataEnvio asc ";
                    var lista = _db.Query<LogEmailApp>(gets, new { IdPessoa = idPessoa }).AsEnumerable();

                    return lista;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "GetListLogEmailApp");
                return null;
            }
        }

        public HelpInterno InsertHelpInterno(HelpInterno help)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string gets = @"INSERT INTO  [HelpInterno]
                                               ([IdPessoa]
                                               ,[Conteudo]
                                               ,[Titulo]
                                               ,[DtInclusao])
                                         VALUES
                                               (@IdPessoa 
                                               ,@Conteudo 
                                               ,@Titulo 
                                               ,getDate()) SELECT SCOPE_IDENTITY()  ";
                    var idInserted = _db.Query<int>(gets, new
                    {
                        IdPessoa = help.IdPessoa,
                        Conteudo = help.Conteudo,
                        Titulo = help.Titulo
                    }).FirstOrDefault();
                    help.Id = idInserted;

                    return help;
                }
            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PessoaRepository", "InsertHelpInterno");
                return null;
            }
        }

        public IEnumerable<HelpInterno> GetListHelpinterno()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string gets = @"select      [Id]
                                               ,[IdPessoa]
                                               ,[Conteudo]
                                               ,[Titulo]
                                               ,[DtInclusao] from  [HelpInterno] order by DtInclusao asc ";
                    var lista = _db.Query<HelpInterno>(gets).AsEnumerable();

                    return lista;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "GetListHelpinterno");
                return null;
            }
        }

        public HelpInterno GetHelpinternoById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string gets = @"select      [Id]
                                               ,[IdPessoa]
                                               ,[Conteudo]
                                               ,[Titulo]
                                               ,[DtInclusao] from  [HelpInterno] where Id = @id";
                    var lista = _db.Query<HelpInterno>(gets, new { id = id }).FirstOrDefault();

                    return lista;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoaRepository", "GetHelpinternoById");
                return null;
            }
        }

    }
}
