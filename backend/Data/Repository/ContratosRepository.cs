﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;


namespace Data.Repository
{
    public class ContratosRepository : BaseRepository, IContratosRepository
    {
        public Domain.Entities._2BTrust.Contratos InsertContratos(Domain.Entities._2BTrust.Contratos con)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [financ].[Contratos] ");
                    strInsert.Append(" ([Codigo] ");
                    strInsert.Append(" ,[Descricao] ");
                    strInsert.Append(" ,[DtInicio] ");
                    strInsert.Append(" ,[DtVigencia] ");
                    strInsert.Append(" ,[IdStatus] ");
                    strInsert.Append(" ,[IdUsuarioInclusao] ");
                    strInsert.Append(" ,[DtInclusao]) ");
                    strInsert.Append("   VALUES ");
                    strInsert.Append("  (@Codigo ");
                    strInsert.Append("  ,@Descricao ");
                    strInsert.Append("  ,@DtInicio ");
                    strInsert.Append("  ,@DtVigencia ");
                    strInsert.Append("  ,@IdStatus ");
                    strInsert.Append("  ,@IdUsuarioInclusao ");
                    strInsert.Append("  , getDate())  SELECT IDENT_CURRENT('[financ].[Contratos]') AS [IDENT_CURRENT]");

                    var idContrato = _db.Query<int>(strInsert.ToString(), new
                    {
                        Codigo = con.Codigo,
                        Descricao = con.Descricao,
                        DtInicio = con.DtInicio,
                        DtVigencia = con.DtVigencia,
                        IdStatus = con.IdStatus,
                        IdUsuarioInclusao = con.IdUsuarioInclusao

                    }).FirstOrDefault();

                    con.Id = idContrato;

                    return con;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "ContratosRepository", "InsertContratos");
                return null;
            }
        }

        public Domain.Entities._2BTrust.Contratos UpdateContratos(Domain.Entities._2BTrust.Contratos con)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [financ].[Contratos] ");
                    strUpdate.Append("   SET [Codigo] = @Codigo ");
                    strUpdate.Append("      ,[Descricao] = @Descricao  ");
                    strUpdate.Append("      ,[DtInicio] = @DtInicio ");
                    strUpdate.Append("      ,[DtVigencia] = @DtVigencia ");
                    strUpdate.Append("      ,[IdStatus] = @IdStatus ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate()");
                    strUpdate.Append(" WHERE  Id = " + con.Id);


                    _db.Execute(strUpdate.ToString(), new
                    {
                        Codigo = con.Codigo,
                        Descricao = con.Descricao,
                        DtInicio = con.DtInicio,
                        DtVigencia = con.DtVigencia,
                        IdStatus = con.IdStatus,
                        IdUsuarioAlteracao = con.IdUsuarioAlteracao
                    });


                    return con;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "ContratosRepository", "UpdateContratos");
                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.Contratos> GetListContratos()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT [Id] ");
                    strGetList.Append("      ,[Codigo] ");
                    strGetList.Append("      ,[Descricao] ");
                    strGetList.Append("      ,[DtInicio] ");
                    strGetList.Append("      ,[DtVigencia] ");
                    strGetList.Append("      ,[IdStatus] ");
                    strGetList.Append("      ,[IdUsuarioInclusao] ");
                    strGetList.Append("      ,[DtInclusao] ");
                    strGetList.Append("      ,[IdUsuarioAlteracao] ");
                    strGetList.Append("      ,[DtAlteracao] ");
                    strGetList.Append("  FROM [financ].[Contratos] order by Descricao");

                    return _db.Query<Contratos>(strGetList.ToString()).AsEnumerable();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "ContratosRepository", "GetListContratos");
                return null;
            }
        }

        public Domain.Entities._2BTrust.Contratos GetContratosById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT [Id] ");
                    strGetList.Append("      ,[Codigo] ");
                    strGetList.Append("      ,[Descricao] ");
                    strGetList.Append("      ,[DtInicio] ");
                    strGetList.Append("      ,[DtVigencia] ");
                    strGetList.Append("      ,[IdStatus] ");
                    strGetList.Append("      ,[IdUsuarioInclusao] ");
                    strGetList.Append("      ,[DtInclusao] ");
                    strGetList.Append("      ,[IdUsuarioAlteracao] ");
                    strGetList.Append("      ,[DtAlteracao] ");
                    strGetList.Append("  FROM [financ].[Contratos] where Id = @id");

                    return _db.Query<Contratos>(strGetList.ToString(), new { id = id }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "ContratosRepository", "GetContratosById");
                return null;
            }
        }
    }
}
