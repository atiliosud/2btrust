﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Data.Repository
{
    public class EstadosRepository : BaseRepository, IEstadosRepository
    {

        public IEnumerable<Estados> GetListEstados()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("  ,[Sigla] ");
                    strGet.Append("  ,[Nome] ");
                    strGet.Append("  ,[CodigoIBGE] ");
                    strGet.Append("  ,[IdUsuarioInclusao] ");
                    strGet.Append("  ,[DtInclusao] ");
                    strGet.Append("  ,[IdUsuarioAlteracao] ");
                    strGet.Append("  ,[DtAlteracao] ");
                    strGet.Append("  FROM [crm].[Estados] ");

                    return _db.Query<Estados>(strGet.ToString()).AsEnumerable();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "EstadosRepository", "GetListEstados");

                return null;
            }
        }

    }
}
