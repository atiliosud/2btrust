﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;



namespace Data.Repository
{
    public class AcoesRepository : BaseRepository, IAcoesRepository
    {


        public IEnumerable<Acoes> GetListAcoes()
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("     ,[IdControle] ");
                    strGet.Append("     ,[Codigo] ");
                    strGet.Append("     ,[Nome] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append("     ,[FlgAtivo] ");
                    strGet.Append(" FROM [user].[Acoes] order by Nome  ");

                    return _db.Query<Acoes>(strGet.ToString()).AsEnumerable();


                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "AcoesRepository", "GetListAcoes");
                return null;
            }
        }

        public Domain.Entities._2BTrust.Acoes InsertAcoes(Acoes acoes)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [user].[Acoes] ");
                    strInsert.Append("    ([IdControle] ");
                    strInsert.Append("    ,[Codigo] ");
                    strInsert.Append("    ,[Nome] ");
                    strInsert.Append("    ,[IdUsuarioInclusao] ");
                    strInsert.Append("    ,[DtInclusao] ");
                    strInsert.Append("    ,[FlgAtivo]) ");
                    strInsert.Append("   VALUES ");
                    strInsert.Append("     (@IdControle ");
                    strInsert.Append("     ,@Codigo ");
                    strInsert.Append("     ,@Nome ");
                    strInsert.Append("     ,@IdUsuarioInclusao ");
                    strInsert.Append("     ,getDate() ");
                    strInsert.Append("     ,@FlagAtivo)  SELECT IDENT_CURRENT('[user].[Acoes]') AS [IDENT_CURRENT]");


                    var idacoes = _db.Query<int>(strInsert.ToString(),
                        new
                        {
                            IdControle = acoes.IdControle,
                            Codigo = acoes.Codigo,
                            Nome = acoes.Nome,
                            IdUsuarioInclusao = acoes.IdUsuarioInclusao,
                            FlagAtivo = acoes.FlgAtivo
                        }).FirstOrDefault();

                    acoes.Id = idacoes;

                    return acoes;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "AcoesRepository", "InsertAcoes");
                return null;
            }
        }

        public Acoes GetAcoesById(int idAcoes)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("     ,[IdControle] ");
                    strGet.Append("     ,[Codigo] ");
                    strGet.Append("     ,[Nome] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append("     ,[FlgAtivo] ");
                    strGet.Append(" FROM [user].[Acoes] Id = @Id  ");

                    return _db.Query<Acoes>(strGet.ToString(), new { Id = idAcoes }).FirstOrDefault();


                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "AcoesRepository", "GetAcoesById");
                return null;
            }
        }

        public Acoes UpdateAcoes(Acoes acoes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [user].[Acoes] ");
                    strUpdate.Append("   SET [IdControle] = @IdControle ");
                    strUpdate.Append("      ,[Codigo] =@Codigo ");
                    strUpdate.Append("      ,[Nome] = @Nome ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate() ");
                    strUpdate.Append("      ,[FlgAtivo] = @FlgAtivo ");
                    strUpdate.Append(" WHERE Id= @Id");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        IdControle = acoes.IdControle,
                        Codigo = acoes.Codigo,
                        Nome = acoes.Nome,
                        IdUsuarioAlteracao = acoes.IdUsuarioAlteracao,
                        Id = acoes.Id,
                        FlgAtivo = acoes.FlgAtivo
                    });


                    return acoes;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "AcoesRepository", "UpdateAcoes");
                return null;
            }
        }
         
    }
}
