﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Threading.Tasks;
using Domain.Entities._2BTrust;
using Domain.Entities;

namespace Data.Repository
{
    public class LancamentosRepository : BaseRepository, ILancamentosRepository
    {
        public Domain.Entities._2BTrust.Lancamentos InsertLancamentos(Domain.Entities._2BTrust.Lancamentos con)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {


                    ///////////verificar existencia de um lançamento para a mesma referencia

                    var strGetExistencia = "select * from [financ].[Lancamentos]  where DtLancamento = @dtLancamento and  IdPessoa=@idPessoa and IdPoolBemPessoa = @IdPoolBemPessoa";

                    var lancamentoAntrior = _db.Query<Lancamentos>(strGetExistencia, new
                    {
                        dtLancamento = con.DtLancamento,
                        idPessoa = con.IdPessoa,
                        IdPoolBemPessoa = con.IdPoolBemPessoa

                    }).FirstOrDefault();


                    if (lancamentoAntrior != null)
                    {

                        /// atualizar lançamento
                        StringBuilder strUpdate = new StringBuilder();
                        strUpdate.Append(" UPDATE [financ].[Lancamentos] ");
                        strUpdate.Append("   SET [IdTpLancamento] = @IdTpLancamento ");
                        strUpdate.Append("  ,[IdPoolBem] = @IdPoolBem ");
                        strUpdate.Append("  ,[IdPoolBemPessoa] = @IdPoolBemPessoa ");
                        strUpdate.Append("  ,[DtLancamento] = @DtLancamento ");
                        strUpdate.Append("  ,[Valor] = @Valor ");
                        strUpdate.Append("  ,[IdPessoa] = @IdPessoa ");
                        strUpdate.Append("  ,[Obs] = @Obs ");
                        strUpdate.Append("  ,[Credito] = @Credito ");
                        strUpdate.Append("  ,[NumParticao] = @NumParticao ");
                        strUpdate.Append("  ,[IdUsuarioAlteracao] =@IdUsuarioAlteracao ");
                        strUpdate.Append("  ,[DtAlteracao] = getDate() ");
                        strUpdate.Append(" WHERE Id = @id");


                        _db.Execute(strUpdate.ToString(),
                            new
                            {
                                IdTpLancamento = con.IdTpLancamento,
                                IdPoolBem = con.IdPoolBem,
                                IdPoolBemPessoa = con.IdPoolBemPessoa,
                                DtLancamento = con.DtLancamento,
                                Valor = con.Valor,
                                IdPessoa = con.IdPessoa,
                                Obs = con.Obs,
                                Credito = con.Credito,
                                NumParticao = con.NumParticao,
                                IdUsuarioAlteracao = con.IdUsuarioAlteracao,
                                id = lancamentoAntrior.Id 
                            });



                        // atualizar carteira com novo valor

                        //remover valor anterior
                        string strUpdateCarteira = "update dbo.Carteira set Saldo = Saldo - @valor where IdPessoa =@idPessoa";
                        _db.Execute(strUpdateCarteira.ToString(), new { valor = lancamentoAntrior.Valor, idPessoa = con.IdPessoa });


                        //adicionar o valor novo
                        strUpdateCarteira = "update dbo.Carteira set Saldo = Saldo + @valor where IdPessoa =@idPessoa";
                        _db.Execute(strUpdateCarteira.ToString(), new { valor = con.Valor, idPessoa = con.IdPessoa });



                        // atualizar o extrato
                       // string descricaoAnterior = lancamentoAntrior.DtLancamento.ToString("MM/yyyy") + " - " + lancamentoAntrior.Obs;
                        string descricaoAnterior =   lancamentoAntrior.Obs;
                        var historicoAnterior = GetCarteiraLogByDescIdPessoa((int)lancamentoAntrior.IdPessoa, descricaoAnterior);

                        ///historicoAnterior.Descricao = con.DtLancamento.ToString("MM/yyyy") + " - " + con.Obs;
                        historicoAnterior.Descricao =  con.Obs;
                        historicoAnterior.Tipo = "dividendo";
                        historicoAnterior.Amount = con.Valor;
                        historicoAnterior.AvaliableBalance = (historicoAnterior.AvaliableBalance - (lancamentoAntrior.Valor)) + (con.Valor);
                      

                        UpdateLogCarteira(historicoAnterior);

                        return con;



                    }
                    else
                    {
                        StringBuilder strInsert = new StringBuilder();

                        strInsert.Append(" INSERT INTO [financ].[Lancamentos] ");
                        strInsert.Append("        ([IdTpLancamento] ");
                        strInsert.Append("        ,[IdPoolBem] ");
                        strInsert.Append("        ,[IdPoolBemPessoa] ");
                        strInsert.Append("        ,[DtLancamento] ");
                        strInsert.Append("        ,[Valor] ");
                        strInsert.Append("        ,[IdPessoa] ");
                        strInsert.Append("        ,[Obs] ");
                        strInsert.Append("        ,[Credito] ");
                        strInsert.Append("        ,[NumParticao] ");
                        strInsert.Append("        ,[IdUsuarioInclusao] ");
                        strInsert.Append("        ,[DtInclusao] ) ");
                        strInsert.Append("  VALUES ");
                        strInsert.Append("        (@IdTpLancamento ");
                        strInsert.Append("        ,@IdPoolBem ");
                        strInsert.Append("        ,@IdPoolBemPessoa ");
                        strInsert.Append("        ,@DtLancamento ");
                        strInsert.Append("        ,@Valor ");
                        strInsert.Append("        ,@IdPessoa ");
                        strInsert.Append("        ,@Obs ");
                        strInsert.Append("        ,@Credito ");
                        strInsert.Append("        ,@NumParticao ");
                        strInsert.Append("        ,@IdUsuarioInclusao ");
                        strInsert.Append("        ,@dtCadastro)  SELECT IDENT_CURRENT('[financ].[Lancamentos]') AS [IDENT_CURRENT]");




                        var idLancamento = _db.Query<int>(strInsert.ToString(),
                            new
                            {
                                IdTpLancamento = con.IdTpLancamento,
                                IdPoolBem = con.IdPoolBem,
                                IdPoolBemPessoa = con.IdPoolBemPessoa,
                                DtLancamento = con.DtLancamento,
                                Valor = con.Valor,
                                IdPessoa = con.IdPessoa,
                                Obs = con.Obs,
                                Credito = con.Credito,
                                NumParticao = con.NumParticao,
                                IdUsuarioInclusao = con.IdUsuarioInclusao,
                                dtCadastro = con.DtLancamento

                            }).FirstOrDefault();

                        con.Id = idLancamento;


                        string strUpdateCarteira = "update dbo.Carteira set Saldo = Saldo + @valor where IdPessoa =@idPessoa";

                        _db.Execute(strUpdateCarteira.ToString(), new { valor = con.Valor, idPessoa = con.IdPessoa });

                         


                        try
                        {
                            string nomePool = _db.Query<string>("select Nome   FROM [financ].[PoolBens] where Id= @id", new { id = con.IdPoolBem }).FirstOrDefault();

                            var getLogCarteira = GetLastLogCarteira((int)con.IdPessoa);

                         ///   getLogCarteira.Descricao = con.DtLancamento.ToString("MM/yyyy") + " - " + con.Obs;
                            getLogCarteira.Descricao =   con.Obs;
                            getLogCarteira.Tipo = "dividendo";
                            getLogCarteira.Amount = con.Valor;
                            getLogCarteira.AvaliableBalance = getLogCarteira.AvaliableBalance + (con.Valor);
                            getLogCarteira.Data = con.DtLancamento;

                            InserLogCarteira(getLogCarteira);


                        }
                        catch { }



                    }


                    PessoasRepository _pessoaRepository = new PessoasRepository();
                    LogEmailApp app = new LogEmailApp()
                    {
                        DataEnvio = DateTime.Now,
                        FlagAtivo = true,
                        IdPessoa = (int)con.IdPessoa,
                        Titulo = "[Dividend] 2bTrust - Investments",
                        Mensagem = con.Obs +" ("+ con.NumParticao + " Units) of "+ con.Valor.ToString("c"),

                    };

                    _pessoaRepository.InsertLogEmailApp(app);


                    return con;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LancamentosRepository", "InsertLancamentos");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Lancamentos UpdateLancamentos(Domain.Entities._2BTrust.Lancamentos con)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" UPDATE [financ].[Lancamentos] ");
                    strUpdate.Append("   SET [IdTpLancamento] = @IdTpLancamento ");
                    strUpdate.Append("  ,[IdPoolBem] = @IdPoolBem ");
                    strUpdate.Append("  ,[IdPoolBemPessoa] = @IdPoolBemPessoa ");
                    strUpdate.Append("  ,[DtLancamento] = @DtLancamento ");
                    strUpdate.Append("  ,[Valor] = @Valor ");
                    strUpdate.Append("  ,[IdPessoa] = @IdPessoa ");
                    strUpdate.Append("  ,[Obs] = @Obs ");
                    strUpdate.Append("  ,[Credito] = @Credito ");
                    strUpdate.Append("  ,[NumParticao] = @NumParticao ");
                    strUpdate.Append("  ,[IdUsuarioAlteracao] =@IdUsuarioAlteracao ");
                    strUpdate.Append("  ,[DtAlteracao] = getDate() ");
                    strUpdate.Append(" WHERE Id = @id");


                    _db.Execute(strUpdate.ToString(),
                        new
                        {
                            IdTpLancamento = con.IdTpLancamento,
                            IdPoolBem = con.IdPoolBem,
                            IdPoolBemPessoa = con.IdPoolBemPessoa,
                            DtLancamento = con.DtLancamento,
                            Valor = con.Valor,
                            IdPessoa = con.IdPessoa,
                            Obs = con.Obs,
                            Credito = con.Credito,
                            NumParticao = con.NumParticao,
                            IdUsuarioInclusao = con.IdUsuarioInclusao,
                            id = con.Id

                        });

                    return con;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LancamentosRepository", "UpdateLancamentos");

                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetListLancamentos()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT [IdTpLancamento] ");
                    strGetList.Append("      ,[IdPoolBem] ");
                    strGetList.Append("      ,[IdPoolBemPessoa] ");
                    strGetList.Append("      ,[DtLancamento] ");
                    strGetList.Append("      ,[Valor] ");
                    strGetList.Append("      ,[IdPessoa] ");
                    strGetList.Append("      ,[Obs] ");
                    strGetList.Append("      ,[Credito] ");
                    strGetList.Append("      ,[NumParticao] ");
                    strGetList.Append("      ,[IdUsuarioInclusao] ");
                    strGetList.Append("      ,[DtInclusao] ");
                    strGetList.Append("      ,[IdUsuarioAlteracao] ");
                    strGetList.Append("      ,[DtAlteracao] ");
                    strGetList.Append("      ,[Id] ");
                    strGetList.Append("  FROM [financ].[Lancamentos] order by dtLancamento");

                    return _db.Query<Lancamentos>(strGetList.ToString()).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LancamentosRepository", "GetListLancamentos");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Lancamentos GetLancamentosById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT [IdTpLancamento] ");
                    strGetList.Append("      ,[IdPoolBem] ");
                    strGetList.Append("      ,[IdPoolBemPessoa] ");
                    strGetList.Append("      ,[DtLancamento] ");
                    strGetList.Append("      ,[Valor] ");
                    strGetList.Append("      ,[IdPessoa] ");
                    strGetList.Append("      ,[Obs] ");
                    strGetList.Append("      ,[Credito] ");
                    strGetList.Append("      ,[NumParticao] ");
                    strGetList.Append("      ,[IdUsuarioInclusao] ");
                    strGetList.Append("      ,[DtInclusao] ");
                    strGetList.Append("      ,[IdUsuarioAlteracao] ");
                    strGetList.Append("      ,[DtAlteracao] ");
                    strGetList.Append("      ,[Id] ");
                    strGetList.Append("  FROM [financ].[Lancamentos] where Id = @Id");

                    return _db.Query<Lancamentos>(strGetList.ToString(), new { Id = id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LancamentosRepository", "GetLancamentosById");

                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetLancamentosByIdPessoa(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT [IdTpLancamento] ");
                    strGetList.Append("      ,[IdPoolBem] ");
                    strGetList.Append("      ,[IdPoolBemPessoa] ");
                    strGetList.Append("      ,[DtLancamento] ");
                    strGetList.Append("      ,[Valor] ");
                    strGetList.Append("      ,[IdPessoa] ");
                    strGetList.Append("      ,[Obs] ");
                    strGetList.Append("      ,[Credito] ");
                    strGetList.Append("      ,[NumParticao] ");
                    strGetList.Append("      ,[IdUsuarioInclusao] ");
                    strGetList.Append("      ,[DtInclusao] ");
                    strGetList.Append("      ,[IdUsuarioAlteracao] ");
                    strGetList.Append("      ,[DtAlteracao] ");
                    strGetList.Append("      ,[Id] ");
                    strGetList.Append("  FROM [financ].[Lancamentos] where IdPessoa = @IdPessoa");

                    return _db.Query<Lancamentos>(strGetList.ToString(), new { IdPessoa = idPessoa }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LancamentosRepository", "GetLancamentosByIdPessoa");

                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetLancamentosByIdPoolBem(int idPoolBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT [IdTpLancamento] ");
                    strGetList.Append("      ,[IdPoolBem] ");
                    strGetList.Append("      ,[IdPoolBemPessoa] ");
                    strGetList.Append("      ,[DtLancamento] ");
                    strGetList.Append("      ,[Valor] ");
                    strGetList.Append("      ,[IdPessoa] ");
                    strGetList.Append("      ,[Obs] ");
                    strGetList.Append("      ,[Credito] ");
                    strGetList.Append("      ,[NumParticao] ");
                    strGetList.Append("      ,[IdUsuarioInclusao] ");
                    strGetList.Append("      ,[DtInclusao] ");
                    strGetList.Append("      ,[IdUsuarioAlteracao] ");
                    strGetList.Append("      ,[DtAlteracao] ");
                    strGetList.Append("      ,[Id] ");
                    strGetList.Append("  FROM [financ].[Lancamentos] where IdPoolBem = @IdPoolBem");

                    return _db.Query<Lancamentos>(strGetList.ToString(), new { IdPoolBem = idPoolBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LancamentosRepository", "GetLancamentosByIdPoolBem");

                return null;
            }
        }

        public IEnumerable<PessoaPoolsInvestidos> GetPessoasPoolsInvestido()
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" select PB.Id, PB.IdPoolBem, PB.IdPessoa, P.Nome as NomePool, PE.Nome as NomeCliente, PB.Cota as Cotas , ");
                    strGet.Append(" (select top(1) DtLancamento from financ.Lancamentos where IdPessoa = PB.idPessoa order by DtLancamento desc) as UltimoLancamento ");
                    strGet.Append(" from ");
                    strGet.Append("  [financ].[PoolBensPessoas] PB ");
                    strGet.Append("  inner join ");
                    strGet.Append("  financ.PoolBens P on P.id = PB.idPoolBem ");
                    strGet.Append("  inner join crm.Pessoas PE on PE.id = PB.IdPessoa ");
                    strGet.Append("  where PB.IdStatus = 1 and PB.Cota > 0 order by  PE.Nome");

                    return _db.Query<PessoaPoolsInvestidos>(strGet.ToString()).AsEnumerable();

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public PessoaPoolsInvestidos GetPessoasPoolsInvestido(int idPoolBemPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" select PB.Id, PB.IdPoolBem, PB.IdPessoa, P.Nome as NomePool, PE.Nome as NomeCliente, PB.Cota as Cotas , ");
                    strGet.Append(" (select top(1) DtLancamento from financ.Lancamentos where IdPessoa = PB.idPessoa order by DtLancamento desc) as UltimoLancamento ");
                    strGet.Append(" from ");
                    strGet.Append("  [financ].[PoolBensPessoas] PB ");
                    strGet.Append("  inner join ");
                    strGet.Append("  financ.PoolBens P on P.id = PB.idPoolBem ");
                    strGet.Append("  inner join crm.Pessoas PE on PE.id = PB.IdPessoa ");
                    strGet.Append("  where PB.Id = @id");

                    var objRetorno = _db.Query<PessoaPoolsInvestidos>(strGet.ToString(), new { id = idPoolBemPessoa }).FirstOrDefault();



                    StringBuilder strGetLancamentos = new StringBuilder();
                    strGetLancamentos.Append(" SELECT [IdTpLancamento] ");
                    strGetLancamentos.Append("     ,[IdPoolBem] ");
                    strGetLancamentos.Append("     ,[IdPoolBemPessoa] ");
                    strGetLancamentos.Append("     ,[DtLancamento] ");
                    strGetLancamentos.Append("     ,[Valor] ");
                    strGetLancamentos.Append("     ,[IdPessoa] ");
                    strGetLancamentos.Append("     ,[Obs] ");
                    strGetLancamentos.Append("     ,[Credito] ");
                    strGetLancamentos.Append("     ,[NumParticao] ");
                    strGetLancamentos.Append("     ,[IdUsuarioInclusao] ");
                    strGetLancamentos.Append("     ,[DtInclusao] ");
                    strGetLancamentos.Append("     ,[IdUsuarioAlteracao] ");
                    strGetLancamentos.Append("     ,[DtAlteracao] ");
                    strGetLancamentos.Append("     ,[Id] ");
                    strGetLancamentos.Append(" FROM [financ].[Lancamentos] where IdPoolBemPessoa = @id");


                    var listaMovimentacao = _db.Query<Lancamentos>(strGetLancamentos.ToString(), new { id = idPoolBemPessoa }).AsEnumerable();

                    objRetorno._Lancamentos = listaMovimentacao;


                    return objRetorno;
                }


            }
            catch (Exception ex)
            {

                throw;
            }
        }


    }
}
