﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;
namespace Data.Repository
{
    public class ControlesRepository : BaseRepository, IControlesRepository
    {
        public IEnumerable<Controles> GetControlesByIdGrupo(int idGrupo)
        {
            try
            {
                Dictionary<int, Controles> lookup = new Dictionary<int, Controles>();
                List<Acoes> Acoes = new List<Acoes>();
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" select  ");
                    strGet.Append(" C.Id, C.IdControlePrincipal, C.IdArea, C.Codigo, C.Nome, C.[IdUsuarioInclusao], C.DtInclusao, C.IdUsuarioAlteracao, C.DtAlteracao, C.UrlController, C.iconLI, C.idJqueryPai, C.idJqueryFilho, C.Controller, ");
                    strGet.Append(" A.Id, A.Codigo, A.Nome, A.[IdUsuarioInclusao], A.DtInclusao, A.IdUsuarioAlteracao, A.DtAlteracao,  ");
                    strGet.Append(" GA.IdGrupo, GA.IdAcao, GA.IdUsuarioInclusao, GA.DtInclusao ");
                    strGet.Append("  ");
                    strGet.Append(" from [user].[Controles] C ");
                    strGet.Append(" inner join [user].[Acoes] A on A.idControle = C.Id ");
                    strGet.Append(" inner join [user].[GruposAcoes] GA on  GA.IdAcao  =  A.Id ");
                    strGet.Append(" where GA.IdGrupo = " + idGrupo + " order by C.iOrdem");

                    var query = _db.Query<Controles, Acoes, GruposAcoes, Controles>(strGet.ToString(),
                        (C, A, GA) =>
                        {
                            Controles controll;
                            if (!lookup.TryGetValue(C.Id, out controll))
                            {

                                lookup.Add(C.Id, C);
                                controll = C;
                            }

                            A._UnicoGrupoAcoes = GA;
                            Acoes.Add(A);
                            controll._Acoes = Acoes;

                            return controll;

                        }, splitOn: "Id,Id,IdGrupo").Distinct();


                    return query;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "ControlesRepository", "GetControlesByIdGrupo");
                return null;
            }
        }


        public IEnumerable<Controles> GetListControles()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT C.[Id] ");
                    strGetList.Append("     ,C.[IdControlePrincipal] ");
                    strGetList.Append("     ,C.[IdArea] ");
                    strGetList.Append("     ,C.[Codigo] ");
                    strGetList.Append("     ,C.[Nome] ");
                    strGetList.Append("     ,C.[IdUsuarioInclusao] ");
                    strGetList.Append("     ,C.[DtInclusao] ");
                    strGetList.Append("     ,C.[IdUsuarioAlteracao] ");
                    strGetList.Append("     ,C.[DtAlteracao] ");
                    strGetList.Append("     ,C.[UrlController] ");
                    strGetList.Append("     ,C.[iconLI] ");
                    strGetList.Append("     ,C.[idJqueryPai] ");
                    strGetList.Append("     ,C.[idJqueryFilho] ");
                    strGetList.Append("     ,C.[Controller] ");
                    strGetList.Append("     ,C.[iOrdem] ");
                    strGetList.Append("     ,C.[FlagAtivo] ");
                    strGetList.Append("     ,A.[Id] ");
                    strGetList.Append("     ,A.[Codigo] ");
                    strGetList.Append("     ,A.[Nome] ");
                    strGetList.Append("     ,A.[IdUsuarioInclusao] ");
                    strGetList.Append("     ,A.[DtInclusao] ");
                    strGetList.Append("     ,A.[IdUsuarioAlteracao] ");
                    strGetList.Append("     ,A.[DtAlteracao] ");
                    strGetList.Append("     ,A.[FlgAtivo] ");
                    strGetList.Append(" FROM [user].[Controles] C ");
                    strGetList.Append(" inner join [user].[Areas] A on A.Id = C.IdArea");
                    strGetList.Append(" order by C.Nome ");


                    return _db.Query<Controles, Areas, Controles>(strGetList.ToString(), (C, A) =>
                        {
                            C._Area = A;
                            return C;
                        }, splitOn: "Id").AsEnumerable();



                }

            }
            catch (Exception ex)
            {

                InsertLogError(ex, "ControlesRepository", "GetListControles");
                return null;
            }
        }

        public Controles InsertControles(Controles Controles)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {



                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [user].[Controles] ");
                    strInsert.Append("        ([IdControlePrincipal] ");
                    strInsert.Append("        ,[IdArea] ");
                    strInsert.Append("        ,[Codigo] ");
                    strInsert.Append("        ,[Nome] ");
                    strInsert.Append("        ,[IdUsuarioInclusao] ");
                    strInsert.Append("        ,[DtInclusao] "); 
                    strInsert.Append("        ,[UrlController] ");
                    strInsert.Append("        ,[iconLI] ");
                    strInsert.Append("        ,[idJqueryPai] ");
                    strInsert.Append("        ,[idJqueryFilho] ");
                    strInsert.Append("        ,[Controller] ");
                    strInsert.Append("        ,[iOrdem] ");
                    strInsert.Append("        ,[FlagAtivo]) ");
                    strInsert.Append("  VALUES ");
                    strInsert.Append("        (@IdControlePrincipal ");
                    strInsert.Append("        ,@IdArea ");
                    strInsert.Append("        ,@Codigo ");
                    strInsert.Append("        ,@Nome ");
                    strInsert.Append("        ,@IdUsuarioInclusao ");
                    strInsert.Append("        ,getDate() ");
                    strInsert.Append("        ,@UrlController ");
                    strInsert.Append("        ,@iconLI ");
                    strInsert.Append("        ,@idJqueryPai ");
                    strInsert.Append("        ,@idJqueryFilho ");
                    strInsert.Append("        ,@Controller ");
                    strInsert.Append("        ,@iOrdem ");
                    strInsert.Append("        ,1)  SELECT IDENT_CURRENT('[user].[Controles]') AS [IDENT_CURRENT]");


                    var idControle = _db.Query<int>(strInsert.ToString(),
                        new
                        {
                            IdControlePrincipal = Controles.IdControlePrincipal,
                            IdArea = Controles.IdArea,
                            Codigo = Controles.Codigo,
                            Nome = Controles.Nome,
                            IdUsuarioInclusao = Controles.IdUsuarioInclusao,
                            UrlController = Controles.UrlController,
                            iconLI = Controles.iconLI,
                            idJqueryPai = Controles.idJqueryPai,
                            idJqueryFilho = Controles.idJqueryFilho,
                            Controller = Controles.Controller,
                            iOrdem = Controles.iOrdem

                        }).FirstOrDefault();

                    Controles.Id = idControle;

                    return Controles;

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "ControlesRepository", "InsertControles");
                return null;
            }
        }

        public Controles GetControlesById(int idAcoes)
        {
            try
            {

                Dictionary<int, Controles> lookup = new Dictionary<int, Controles>();
                List<Acoes> Acoes = new List<Acoes>();
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" select  ");
                    strGet.Append(" C.Id, C.IdControlePrincipal, C.IdArea, C.Codigo, C.Nome, C.[IdUsuarioInclusao], C.DtInclusao, C.IdUsuarioAlteracao, C.DtAlteracao, C.UrlController, C.iconLI, C.idJqueryPai, C.idJqueryFilho, C.Controller, C.FlagAtivo, ");
                    strGet.Append(" A.Id, A.Codigo, A.Nome +' - '+G.Nome as Nome, A.[IdUsuarioInclusao], A.DtInclusao, A.IdUsuarioAlteracao, A.DtAlteracao, A.FlgAtivo,  ");
                    strGet.Append(" GA.IdGrupo, GA.IdAcao, GA.IdUsuarioInclusao, GA.DtInclusao ");
                    strGet.Append("  ");
                    strGet.Append(" from [user].[Controles] C ");
                    strGet.Append(" inner join [user].[Acoes] A on A.idControle = C.Id ");
                    strGet.Append(" inner join [user].[GruposAcoes] GA on  GA.IdAcao  =  A.Id ");
                    strGet.Append(" inner join [user].[Grupos] G on  G.Id  =  GA.IdGrupo ");
                    strGet.Append(" where C.Id = " + idAcoes );

                    var query = _db.Query<Controles, Acoes, GruposAcoes, Controles>(strGet.ToString(),
                        (C, A, GA) =>
                        {
                            Controles controll;
                            if (!lookup.TryGetValue(C.Id, out controll))
                            {

                                lookup.Add(C.Id, C);
                                controll = C;
                            }

                            A._UnicoGrupoAcoes = GA;
                            Acoes.Add(A);
                            controll._Acoes = Acoes;

                            return controll;

                        }, splitOn: "Id,Id,IdGrupo").Distinct();


                    return query.FirstOrDefault();

                } 

            }
            catch (Exception ex)
            {

                InsertLogError(ex, "ControlesRepository", "GetControlesById");
                return null;
            }
        }

        public Controles UpdateControles(Controles Controles)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" UPDATE [user].[Controles] ");
                    strUpdate.Append("   SET [IdControlePrincipal] = @IdControlePrincipal ");
                    strUpdate.Append("    ,[IdArea] = @IdArea ");
                    strUpdate.Append("    ,[Codigo] = @Codigo ");
                    strUpdate.Append("    ,[Nome] = @Nome ");
                    strUpdate.Append("    ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("    ,[DtAlteracao] =getDate() ");
                    strUpdate.Append("    ,[UrlController] =@UrlController ");
                    strUpdate.Append("    ,[iconLI] = @iconLI ");
                    strUpdate.Append("    ,[idJqueryPai] = @idJqueryPai ");
                    strUpdate.Append("    ,[idJqueryFilho] = @idJqueryFilho ");
                    strUpdate.Append("    ,[Controller] = @Controller ");
                    strUpdate.Append("    ,[iOrdem] = @iOrdem ");
                    strUpdate.Append("    ,[FlagAtivo] = @FlagAtivo");
                    strUpdate.Append(" WHERE   Id = @Id");


                    _db.Execute(strUpdate.ToString(),
                        new
                        {
                            IdControlePrincipal = Controles.IdControlePrincipal,
                            IdArea = Controles.IdArea,
                            Codigo = Controles.Codigo,
                            Nome = Controles.Nome,
                            IdUsuarioAlteracao = Controles.IdUsuarioAlteracao,
                            UrlController = Controles.UrlController,
                            iconLI = Controles.iconLI,
                            idJqueryPai = Controles.idJqueryPai,
                            idJqueryFilho = Controles.idJqueryFilho,
                            Controller = Controles.Controller,
                            iOrdem = Controles.iOrdem,
                            Id = Controles.Id,
                            FlagAtivo = Controles.FlagAtivo

                        });


                    return Controles;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "ControlesRepository", "UpdateControles");
                return null;
            }
        }


        public IEnumerable<Controles> GetListControlesPai()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetList = new StringBuilder();
                    strGetList.Append(" SELECT C.[Id] ");
                    strGetList.Append("     ,C.[IdControlePrincipal] ");
                    strGetList.Append("     ,C.[IdArea] ");
                    strGetList.Append("     ,C.[Codigo] ");
                    strGetList.Append("     ,C.[Nome] +' - '+A.[Nome] as Nome");
                    strGetList.Append("     ,C.[IdUsuarioInclusao] ");
                    strGetList.Append("     ,C.[DtInclusao] ");
                    strGetList.Append("     ,C.[IdUsuarioAlteracao] ");
                    strGetList.Append("     ,C.[DtAlteracao] ");
                    strGetList.Append("     ,C.[UrlController] ");
                    strGetList.Append("     ,C.[iconLI] ");
                    strGetList.Append("     ,C.[idJqueryPai] ");
                    strGetList.Append("     ,C.[idJqueryFilho] ");
                    strGetList.Append("     ,C.[Controller] ");
                    strGetList.Append("     ,C.[iOrdem] ");
                    strGetList.Append("     ,C.[FlagAtivo] "); 
                    strGetList.Append(" FROM [user].[Controles] C ");
                    strGetList.Append(" inner join [user].[Areas] A on A.Id = C.IdArea where IdControlePrincipal is null");
                    strGetList.Append(" order by C.Nome ");


                    return _db.Query<Controles>(strGetList.ToString()).AsEnumerable();



                }

            }
            catch (Exception ex)
            {

                InsertLogError(ex, "ControlesRepository", "GetListControlesPai");
                return null;
            }
        }
    }
}
