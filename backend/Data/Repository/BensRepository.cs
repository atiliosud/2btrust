﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;

namespace Data.Repository
{
    public class BensRepository : BaseRepository, IBensRepository
    {
        public Domain.Entities._2BTrust.Bens InsertBens(Domain.Entities._2BTrust.Bens bem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {


                    decimal valueCapital = Convert.ToDecimal(bem.CapitalInvestido);
                    decimal valueCustos = Convert.ToDecimal(bem.Custos);

                    decimal valueRent = Convert.ToDecimal(bem.Rent);
                    decimal valueHoa = Convert.ToDecimal(bem.Hoa);
                    decimal valuePropertyTaxProvision = Convert.ToDecimal(bem.PropertyTaxProvision);
                    decimal valuePropertyManagement = Convert.ToDecimal(bem.PropertyManagement);
                    decimal valueAdministrationFee = Convert.ToDecimal(bem.AdministrationFee);
                    decimal valueDividendYeld = Convert.ToDecimal(bem.DividendYeld);
                    decimal valueOutros = Convert.ToDecimal((string.IsNullOrEmpty(bem.OutrasDespesas) ? "0" : bem.OutrasDespesas));

                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append("  INSERT INTO [financ].[Bens] ");
                    strInsert.Append("  ([IdPool] ");
                    strInsert.Append("  ,[Nome] ");
                    strInsert.Append("  ,[Descritivo] ");
                    strInsert.Append("  ,[CapitalInvestido] ");
                    strInsert.Append("  ,[performance] ");
                    strInsert.Append("  ,[iMes] ");
                    strInsert.Append("  ,[iAno] ");
                    strInsert.Append("  ,[Rent] ");
                    strInsert.Append("  ,[Hoa] ");
                    strInsert.Append("  ,[PropertyTaxProvision] ");
                    strInsert.Append("  ,[PropertyManagement] ");
                    strInsert.Append("  ,[AdministrationFee] ");
                    strInsert.Append("  ,[DividendYeld] ");

                    strInsert.Append("  ,[OutrasDespesas] ");
                    strInsert.Append("  ,[ObservacaoOutras] ");
                    strInsert.Append("  ,[percentRentabilidade] ");

                    strInsert.Append("  ,[TipoBem] ");

                    strInsert.Append("  ,[IdStatus] ");
                    strInsert.Append("  ,[IdUsuarioInclusao] ");
                    strInsert.Append("  ,[DtInclusao] ) ");
                    strInsert.Append("      VALUES ");
                    strInsert.Append("   (@IdPool ");
                    strInsert.Append("   ,@Nome ");
                    strInsert.Append("   ,@Descritivo ");
                    strInsert.Append("   ,@CapitalInvestido ");
                    strInsert.Append("   ,@performance ");
                    strInsert.Append("   ,@iMes ");
                    strInsert.Append("   ,@iAno ");
                    strInsert.Append("   ,@Rent  ");
                    strInsert.Append("   ,@Hoa  ");
                    strInsert.Append("   ,@PropertyTaxProvision  ");
                    strInsert.Append("   ,@PropertyManagement  ");
                    strInsert.Append("   ,@AdministrationFee  ");
                    strInsert.Append("   ,@DividendYeld  ");

                    strInsert.Append("   ,@OutrasDespesas  ");
                    strInsert.Append("   ,@ObservacaoOutras  ");
                    strInsert.Append("   ,@percentRentabilidade  ");

                    strInsert.Append("   ,@TipoBem  ");

                    strInsert.Append("   ,@IdStatus ");
                    strInsert.Append("   ,@IdUsuarioInclusao ");
                    strInsert.Append("   ,getDate())  SELECT IDENT_CURRENT('[financ].[Bens]') AS [IDENT_CURRENT]");


                    var idBem = _db.Query<int>(strInsert.ToString(), new
                    {
                        IdPool = bem.IdPool,
                        Descritivo = bem.Descritivo,
                        Nome = bem.Nome,
                        CapitalInvestido = valueCapital,
                        performance = bem.performance,
                        iMes = bem.iMes,
                        iAno = bem.iAno,
                        Rent = valueRent,
                        Hoa = valueHoa,
                        PropertyTaxProvision = valuePropertyTaxProvision,
                        PropertyManagement = valuePropertyManagement,
                        AdministrationFee = valueAdministrationFee,
                        DividendYeld = valueDividendYeld,
                        ObservacaoOutras = bem.ObservacaoOutras,
                        percentRentabilidade = bem.percentRentabilidade,
                        TipoBem = bem.TipoBem,
                        OutrasDespesas = valueOutros,
                        IdStatus = bem.IdStatus,
                        IdUsuarioInclusao = bem.IdUsuarioInclusao
                    }).FirstOrDefault();

                    bem.Id = idBem;

                    if (bem.TipoBem == Domain.Enuns.EnumTipoBem.Ação && bem._BensAcao != null)
                    {
                        bem._BensAcao.IdBem = idBem;
                        BensAcoesRepository bensAcoesRepository = new BensAcoesRepository();
                        bensAcoesRepository.InsertBensAcoes(bem._BensAcao);
                    }

                    if (bem._Imagens != null)
                    {
                        if (bem._Imagens.Count() > 0)
                        {
                            foreach (var item in bem._Imagens)
                            {
                                item.PathLocation = item.PathLocation.Replace("Bem_0", "Bem_" + bem.Id.ToString());
                                item.PathLocationIIS = item.PathLocationIIS.Replace("Bem_0", "Bem_" + bem.Id.ToString());
                                item.IdBem = idBem;
                                InsertImagens(item);
                            }
                        }
                    }


                    if (bem._BensGastos != null)
                    {
                        if (bem._BensGastos.Count() > 0)
                        {
                            foreach (var item in bem._BensGastos)
                            {
                                item.PathLocation = item.PathLocation.Replace("Bem_0", "Bem_" + bem.Id.ToString());
                                item.PathLocationIIS = item.PathLocationIIS.Replace("Bem_0", "Bem_" + bem.Id.ToString());
                                item.IdBem = idBem;

                                InsertBensGasto(item);
                            }
                        }
                    }
                    //gerar historico
                    BensBaseCalculoHistorico objHistorico = new BensBaseCalculoHistorico()
                    {
                        IdBem = bem.Id,
                        iMes = bem.iMes,
                        iAno = bem.iAno,
                        Rent = valueRent.ToString(),
                        Hoa = valueHoa.ToString(),
                        PropertyTaxProvision = valuePropertyTaxProvision.ToString(),
                        PropertyManagement = valuePropertyManagement.ToString(),
                        AdministrationFee = valueAdministrationFee.ToString(),
                        DividendYeld = valueDividendYeld.ToString(),
                        IdUsuarioInclusao = bem.IdUsuarioInclusao,
                        ObservacaoOutras = bem.ObservacaoOutras,
                        OutrasDespesas = bem.OutrasDespesas,
                        percentRentabilidade = bem.percentRentabilidade,
                        performance = bem.performance,
                    };

                    InsertBensBaseCalculoHistorico(objHistorico);



                    return bem;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "InsertBens");
                return null;
            }
        }

        public Domain.Entities._2BTrust.Bens UpdateBens(Domain.Entities._2BTrust.Bens bem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    decimal valueCapital = Convert.ToDecimal(bem.CapitalInvestido);
                    decimal valueCustos = Convert.ToDecimal(bem.Custos);

                    decimal valueRent = Convert.ToDecimal(bem.Rent);
                    decimal valueHoa = Convert.ToDecimal(bem.Hoa);
                    decimal valuePropertyTaxProvision = Convert.ToDecimal(bem.PropertyTaxProvision);
                    decimal valuePropertyManagement = Convert.ToDecimal(bem.PropertyManagement);
                    decimal valueAdministrationFee = Convert.ToDecimal(bem.AdministrationFee);
                    decimal valueDividendYeld = Convert.ToDecimal(bem.DividendYeld);

                    decimal valueOutros = Convert.ToDecimal((string.IsNullOrEmpty(bem.OutrasDespesas) ? "0" : bem.OutrasDespesas));

                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" UPDATE [financ].[Bens] ");
                    strUpdate.Append("    SET [IdPool] = @IdPool ");
                    strUpdate.Append("       ,[Nome] = @Nome ");
                    strUpdate.Append("       ,[Descritivo] = @Descritivo ");
                    strUpdate.Append("       ,[CapitalInvestido] = @CapitalInvestido ");
                    strUpdate.Append("       ,[performance] = @performance ");
                    strUpdate.Append("       ,[iMes] = @iMes ");
                    strUpdate.Append("       ,[iAno] = @iAno ");
                    strUpdate.Append("       ,[Hoa] = @Hoa ");
                    strUpdate.Append("       ,[Rent] = @Rent ");
                    strUpdate.Append("       ,[PropertyTaxProvision] = @PropertyTaxProvision ");
                    strUpdate.Append("       ,[PropertyManagement] = @PropertyManagement ");
                    strUpdate.Append("       ,[AdministrationFee] = @AdministrationFee ");
                    strUpdate.Append("       ,[DividendYeld] = @DividendYeld ");

                    strUpdate.Append("       ,[ObservacaoOutras] = @ObservacaoOutras ");
                    strUpdate.Append("       ,[OutrasDespesas] = @OutrasDespesas ");
                    strUpdate.Append("       ,[percentRentabilidade] = @percentRentabilidade ");

                    strUpdate.Append("       ,[TipoBem] = @TipoBem ");

                    strUpdate.Append("       ,[IdStatus] = @IdStatus ");
                    strUpdate.Append("       ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("       ,[DtAlteracao] = getDate() ");
                    strUpdate.Append("  WHERE Id = @id ");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        IdPool = bem.IdPool,
                        Nome = bem.Nome,
                        Descritivo = bem.Descritivo,
                        CapitalInvestido = valueCapital,
                        performance = bem.performance,
                        iMes = bem.iMes,
                        iAno = bem.iAno,
                        Rent = valueRent,
                        Hoa = valueHoa,
                        PropertyTaxProvision = valuePropertyTaxProvision,
                        PropertyManagement = valuePropertyManagement,
                        AdministrationFee = valueAdministrationFee,
                        DividendYeld = valueDividendYeld,
                        ObservacaoOutras = bem.ObservacaoOutras,
                        percentRentabilidade = bem.percentRentabilidade,
                        TipoBem = bem.TipoBem,
                        OutrasDespesas = valueOutros,
                        IdStatus = bem.IdStatus,
                        IdUsuarioAlteracao = bem.IdUsuarioAlteracao,
                        id = bem.Id
                    });

                    if (bem.TipoBem == Domain.Enuns.EnumTipoBem.Ação && bem._BensAcao != null)
                    {
                        BensAcoesRepository bensAcoesRepository = new BensAcoesRepository();
                        if (bem._BensAcao.Id == 0)
                            bensAcoesRepository.InsertBensAcoes(bem._BensAcao);
                        else
                            bensAcoesRepository.UpdateBensAcoes(bem._BensAcao);
                    }

                    if (bem._Imagens.Count() > 0)
                    {
                        foreach (var item in bem._Imagens)
                        {
                            if (item.Id == 0)
                                InsertImagens(item);
                            else
                                UpdateImagens(item);
                        }
                    }


                    if (bem._BensGastos.Count() > 0)
                    {
                        foreach (var item in bem._BensGastos)
                        {
                            if (item.Id < 0)
                            {
                                item.IdUsuarioInclusao = bem.IdUsuarioAlteracao;
                                item.IdBem = bem.Id;
                                InsertBensGasto(item);
                            }
                            else
                            {
                                item.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                                UpdateBensGasto(item);
                            }
                        }
                    }


                    //verificar historico
                    var objHistorico = GetBensBaseCalculoHistoricoByIdBem(bem.Id, bem.iAno, bem.iMes);
                    if (objHistorico == null)
                    {
                        //gerar historico
                        BensBaseCalculoHistorico objHistoricoI = new BensBaseCalculoHistorico()
                        {
                            IdBem = bem.Id,
                            iMes = bem.iMes,
                            iAno = bem.iAno,
                            Rent = valueRent.ToString(),
                            Hoa = valueHoa.ToString(),
                            PropertyTaxProvision = valuePropertyTaxProvision.ToString(),
                            PropertyManagement = valuePropertyManagement.ToString(),
                            AdministrationFee = valueAdministrationFee.ToString(),
                            DividendYeld = valueDividendYeld.ToString(),
                            IdUsuarioInclusao = bem.IdUsuarioInclusao,
                            ObservacaoOutras = bem.ObservacaoOutras,
                            OutrasDespesas = bem.OutrasDespesas,
                            percentRentabilidade = bem.percentRentabilidade,
                            performance = bem.performance,
                        };

                        InsertBensBaseCalculoHistorico(objHistoricoI);
                    }
                    else
                    {
                        objHistorico.iMes = bem.iMes;
                        objHistorico.iAno = bem.iAno;
                        objHistorico.Rent = valueRent.ToString();
                        objHistorico.Hoa = valueHoa.ToString();
                        objHistorico.PropertyTaxProvision = valuePropertyTaxProvision.ToString();
                        objHistorico.PropertyManagement = valuePropertyManagement.ToString();
                        objHistorico.AdministrationFee = valueAdministrationFee.ToString();
                        objHistorico.DividendYeld = valueDividendYeld.ToString();
                        objHistorico.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;

                        objHistorico.ObservacaoOutras = bem.ObservacaoOutras;
                        objHistorico.OutrasDespesas = bem.OutrasDespesas;
                        objHistorico.percentRentabilidade = bem.percentRentabilidade;
                        objHistorico.performance = bem.performance;

                        UpdateBensBaseCalculoHistorico(objHistorico);
                    }


                    return bem;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "UpdateBens");
                throw ex;
            }
        }

        public Domain.Entities._2BTrust.Bens GetBensById(int id)
        {
            try
            {
                Bens retorno = new Bens();
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("     ,[IdPool] ");
                    strGet.Append("     ,[Nome] ");
                    strGet.Append("     ,[Descritivo] ");
                    strGet.Append("     ,[CapitalInvestido] ");
                    strGet.Append("     ,[performance] ");
                    strGet.Append("     ,[Custos] ");
                    strGet.Append("     ,[DtReferenciaCusto] ");
                    strGet.Append("     ,[iMes] ");
                    strGet.Append("     ,[iAno] ");
                    strGet.Append("     ,[Rent] ");
                    strGet.Append("     ,[Hoa] ");
                    strGet.Append("     ,[PropertyTaxProvision] ");
                    strGet.Append("     ,[PropertyManagement] ");
                    strGet.Append("     ,[AdministrationFee] ");
                    strGet.Append("     ,[DividendYeld] ");
                    strGet.Append("     ,ISNULL([OutrasDespesas],'0') as OutrasDespesas ");
                    strGet.Append("     ,[ObservacaoOutras] ");
                    strGet.Append("     ,[percentRentabilidade] ");
                    strGet.Append("     ,[TipoBem] ");
                    strGet.Append("     ,[IdStatus] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append(" FROM [financ].[Bens] where Id =@id");

                    retorno = _db.Query<Bens>(strGet.ToString(), new { id = id }).FirstOrDefault();

                }

                BensAcoesRepository bensAcoesRepository = new BensAcoesRepository();
                retorno._BensAcoes = bensAcoesRepository.GetListBensAcoesByIdBem(id);
                retorno._Imagens = GetListImagens(retorno.Id);
                retorno._BensGastos = GetListBensGastosByIdBem(retorno.Id);

                retorno._BensBaseCalculoHistorico = GetListBensBaseCalculoHistoricoByIdBem(retorno.Id);

                return retorno;
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetBensById");
                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.Bens> GetListBens()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT B.[Id] ");
                    strGet.Append("       ,B.[IdPool] ");
                    strGet.Append("       ,B.[Nome] ");
                    strGet.Append("       ,B.[Descritivo] ");
                    strGet.Append("       ,B.[CapitalInvestido] ");
                    strGet.Append("       ,B.[performance] ");
                    strGet.Append("       ,B.[DtReferenciaCusto] ");
                    strGet.Append("       ,B.[iAno] ");
                    strGet.Append("       ,B.[iMes] ");
                    strGet.Append("       ,(select ISNULL(SUM(Valor),0) from dbo.BensGastos  where IdBem = B.Id and FlagAtivo = 1) as  Custos  ");
                    strGet.Append("       ,B.[Rent] ");
                    strGet.Append("       ,B.[Hoa] ");
                    strGet.Append("       ,B.[PropertyTaxProvision] ");
                    strGet.Append("       ,B.[PropertyManagement] ");
                    strGet.Append("       ,B.[AdministrationFee] ");
                    strGet.Append("       ,B.[DividendYeld] ");
                    strGet.Append("       ,B.[OutrasDespesas] ");
                    strGet.Append("       ,B.[ObservacaoOutras] ");
                    strGet.Append("       ,B.[percentRentabilidade] ");
                    strGet.Append("       ,B.[TipoBem] ");
                    strGet.Append("       ,B.[IdStatus] ");
                    strGet.Append("       ,B.[IdUsuarioInclusao] ");
                    strGet.Append("       ,B.[DtInclusao] ");
                    strGet.Append("       ,B.[IdUsuarioAlteracao] ");
                    strGet.Append("       ,B.[DtAlteracao] ");
                    strGet.Append("       ,P.[Id] ");
                    strGet.Append("       ,P.[IdContrato] ");
                    strGet.Append("       ,P.[Nome] ");
                    strGet.Append("       ,P.[DtInicio] ");
                    strGet.Append("       ,P.[DtVigencia] ");
                    strGet.Append("       ,P.[IdStatus] ");
                    strGet.Append("       ,P.[IdUsuarioInclusao] ");
                    strGet.Append("       ,P.[DtInclusao] ");
                    strGet.Append("       ,P.[IdUsuarioAlteracao] ");
                    strGet.Append("       ,P.[DtAlteracao] ");
                    strGet.Append("       ,C.[Id] ");
                    strGet.Append("       ,C.[Codigo] ");
                    strGet.Append("       ,C.[Descricao] ");
                    strGet.Append("       ,C.[DtInicio] ");
                    strGet.Append("       ,C.[DtVigencia] ");
                    strGet.Append("       ,C.[IdStatus] ");
                    strGet.Append("       ,C.[IdUsuarioInclusao] ");
                    strGet.Append("       ,C.[DtInclusao] ");
                    strGet.Append("       ,C.[IdUsuarioAlteracao] ");
                    strGet.Append("       ,C.[DtAlteracao] ");
                    strGet.Append("       ,A.[Id] ");
                    strGet.Append("       ,A.[CodigoAcao] ");
                    strGet.Append("       ,A.[CotacaoAtual] ");
                    strGet.Append("       ,A.[QtdeCotas] ");
                    strGet.Append(" FROM  [financ].[Bens] B " +
                                        "inner join  [financ].[PoolBens] P ON B.IdPool = P.Id " +
                                        "Inner join [financ].[Contratos] C on C.Id = P.IdContrato   " +
                                        "Left join [financ].[BensAcoes] A on B.Id = A.IdBem   " +
                                        "order by B.Nome");

                    var bem = _db.Query<Bens, PoolBens, Contratos, BensAcoes, Bens>(strGet.ToString(), (B, P, C, A)
                        =>
                    {
                        P._Contrato = C;
                        B._Pool = P;
                        B._BensAcao = A;
                        return B;
                    }
                    , splitOn: "Id"
                    ).AsEnumerable();

                    return bem;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetListBens");
                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.Bens> GetListBensIdPool(int idPool)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT B.[Id] ");
                    strGet.Append("       ,B.[IdPool] ");
                    strGet.Append("       ,B.[Nome] ");
                    strGet.Append("       ,B.[Descritivo] ");
                    strGet.Append("       ,B.[CapitalInvestido] ");
                    strGet.Append("       ,B.[performance] ");
                    strGet.Append("       ,B.[DtReferenciaCusto] ");
                    strGet.Append("       ,B.[iMes] ");
                    strGet.Append("       ,B.[iAno] ");
                    strGet.Append("       ,B.[Custos] ");
                    strGet.Append("       ,B.[Rent] ");
                    strGet.Append("       ,B.[Hoa] ");
                    strGet.Append("       ,B.[PropertyTaxProvision] ");
                    strGet.Append("       ,B.[PropertyManagement] ");
                    strGet.Append("       ,B.[AdministrationFee] ");
                    strGet.Append("       ,B.[DividendYeld] ");
                    strGet.Append("       ,B.[OutrasDespesas] ");
                    strGet.Append("       ,B.[ObservacaoOutras] ");
                    strGet.Append("       ,B.[percentRentabilidade] ");
                    strGet.Append("       ,B.[TipoBem] ");
                    strGet.Append("       ,B.[IdStatus] ");
                    strGet.Append("       ,B.[IdUsuarioInclusao] ");
                    strGet.Append("       ,B.[DtInclusao] ");
                    strGet.Append("       ,B.[IdUsuarioAlteracao] ");
                    strGet.Append("       ,B.[DtAlteracao] ");
                    strGet.Append("       ,P.[Id] ");
                    strGet.Append("       ,P.[IdContrato] ");
                    strGet.Append("       ,P.[Nome] ");
                    strGet.Append("       ,P.[DtInicio] ");
                    strGet.Append("       ,P.[DtVigencia] ");
                    strGet.Append("       ,P.[IdStatus] ");
                    strGet.Append("       ,P.[IdUsuarioInclusao] ");
                    strGet.Append("       ,P.[DtInclusao] ");
                    strGet.Append("       ,P.[IdUsuarioAlteracao] ");
                    strGet.Append("       ,P.[DtAlteracao] ");
                    strGet.Append("       ,C.[Id] ");
                    strGet.Append("       ,C.[Codigo] ");
                    strGet.Append("       ,C.[Descricao] ");
                    strGet.Append("       ,C.[DtInicio] ");
                    strGet.Append("       ,C.[DtVigencia] ");
                    strGet.Append("       ,C.[IdStatus] ");
                    strGet.Append("       ,C.[IdUsuarioInclusao] ");
                    strGet.Append("       ,C.[DtInclusao] ");
                    strGet.Append("       ,C.[IdUsuarioAlteracao] ");
                    strGet.Append("       ,C.[DtAlteracao] ");
                    strGet.Append(" FROM  [financ].[Bens] B inner join  [financ].[PoolBens] P ON B.IdPool = P.Id Inner join [financ].[Contratos] C on C.Id = P.IdContrato where B.[IdPool] = " + idPool + " and  B.[IdStatus] = 1 order by B.Nome");

                    var bem = _db.Query<Bens, PoolBens, Contratos, Bens>(strGet.ToString(), (B, P, C)
                        =>
                    {
                        P._Contrato = C;
                        B._Pool = P;
                        return B;
                    }
                        , splitOn: "Id"
                        ).AsEnumerable();
                    foreach (var item in bem)
                    {
                        item._BensGastos = GetListBensGastosByIdBem(item.Id);
                    }
                    return bem;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetListBens");
                return null;
            }
        }



        //------------------------------IMAGENS----------------------------------------//

        public IEnumerable<BensImagens> GetListImagens(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetImanges = new StringBuilder();
                    strGetImanges.Append(" SELECT [Id] ");
                    strGetImanges.Append("      ,[IdBem] ");
                    strGetImanges.Append("      ,[PathLocation] ");
                    strGetImanges.Append("      ,[PathLocationIIS] ");
                    strGetImanges.Append("      ,[FlagAtivo] ");
                    strGetImanges.Append("      ,[IdUsuarioInclusao] ");
                    strGetImanges.Append("      ,[DtInclusao] ");
                    strGetImanges.Append("      ,[IdUsuarioAlteracao] ");
                    strGetImanges.Append("      ,[DtAlteracao] ");
                    strGetImanges.Append("  FROM [financ].[BensImagem] where IdBem = @idBem and FlagAtivo =1");

                    return _db.Query<BensImagens>(strGetImanges.ToString(), new { idBem = idBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetListImagens");
                return null;
            }
        }

        public BensImagens InsertImagens(BensImagens bemImage)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO [financ].[BensImagem] ");
                    strInsert.Append("        ( [IdBem] ");
                    strInsert.Append("        ,[PathLocation] ");
                    strInsert.Append("        ,[PathLocationIIS] ");
                    strInsert.Append("        ,[FlagAtivo] ");
                    strInsert.Append("        ,[IdUsuarioInclusao] ");
                    strInsert.Append("        ,[DtInclusao] ) ");
                    strInsert.Append("  VALUES ");
                    strInsert.Append("        (@IdBem ");
                    strInsert.Append("        ,@PathLocation ");
                    strInsert.Append("        ,@PathLocationIIS ");
                    strInsert.Append("        ,@FlagAtivo ");
                    strInsert.Append("        ,@IdUsuarioInclusao ");
                    strInsert.Append("        ,getDate() )  SELECT IDENT_CURRENT('[financ].[BensImagem]') AS [IDENT_CURRENT]");

                    var idbemImagem = _db.Query<int>(strInsert.ToString(), new
                    {
                        IdBem = bemImage.IdBem,
                        PathLocation = bemImage.PathLocation,
                        PathLocationIIS = bemImage.PathLocationIIS,
                        FlagAtivo = bemImage.FlagAtivo,
                        IdUsuarioInclusao = bemImage.IdUsuarioInclusao
                    }).FirstOrDefault();

                    bemImage.Id = idbemImagem;

                    return bemImage;


                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "InsertImagens");
                return null;
            }
        }

        public BensImagens UpdateImagens(BensImagens bemImage)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [financ].[BensImagem] ");
                    strUpdate.Append("   SET [IdBem] = @IdBem ");
                    strUpdate.Append("      ,[PathLocation] = @PathLocation ");
                    strUpdate.Append("      ,[PathLocationIIS] = @PathLocationIIS ");
                    strUpdate.Append("      ,[FlagAtivo] = @FlagAtivo ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] =  getDate()");
                    strUpdate.Append(" WHERE Id = @id");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        id = bemImage.Id,
                        IdBem = bemImage.IdBem,
                        PathLocation = bemImage.PathLocation,
                        PathLocationIIS = bemImage.PathLocationIIS,
                        FlagAtivo = bemImage.FlagAtivo,
                        IdUsuarioAlteracao = bemImage.IdUsuarioAlteracao
                    });


                    return bemImage;


                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "UpdateImagens");
                return null;
            }

        }


        //------------------------------BASE CALCULOS------------------------------------//

        public IEnumerable<BensBaseCalculoHistorico> GetListBensBaseCalculoHistoricoByIdBem(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();


                    strGet.Append("  SELECT [Id] ");
                    strGet.Append("     ,[IdBem] ");
                    strGet.Append("     ,[iMes] ");
                    strGet.Append("     ,[iAno] ");
                    strGet.Append("     ,[Rent] ");
                    strGet.Append("     ,[Hoa] ");
                    strGet.Append("     ,[PropertyTaxProvision] ");
                    strGet.Append("     ,[PropertyManagement] ");
                    strGet.Append("     ,[AdministrationFee] ");
                    strGet.Append("     ,[DividendYeld] ");
                    strGet.Append("     ,[OutrasDespesas] ");
                    strGet.Append("     ,[ObservacaoOutras] ");
                    strGet.Append("     ,[percentRentabilidade] ");
                    strGet.Append("     ,[performance] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append(" FROM [dbo].[financ.BensBaseCalculoHistorico]  where IdBem = @IdBem ");


                    return _db.Query<BensBaseCalculoHistorico>(strGet.ToString(), new { IdBem = idBem }).AsEnumerable();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetBensBaseCalculoHistoricoByIdBem");
                return null;
            }
        }
        public BensBaseCalculoHistorico GetBensBaseCalculoHistoricoByIdBem(int idBem, int iAno, int iMes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();


                    strGet.Append("  SELECT [Id] ");
                    strGet.Append("     ,[IdBem] ");
                    strGet.Append("     ,[iMes] ");
                    strGet.Append("     ,[iAno] ");
                    strGet.Append("     ,[Rent] ");
                    strGet.Append("     ,[Hoa] ");
                    strGet.Append("     ,[PropertyTaxProvision] ");
                    strGet.Append("     ,[PropertyManagement] ");
                    strGet.Append("     ,[AdministrationFee] ");
                    strGet.Append("     ,[DividendYeld] ");
                    strGet.Append("     ,[OutrasDespesas] ");
                    strGet.Append("     ,[ObservacaoOutras] ");
                    strGet.Append("     ,[percentRentabilidade] ");
                    strGet.Append("     ,[performance] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append(" FROM [dbo].[financ.BensBaseCalculoHistorico]  where IdBem = @IdBem and iMes = @iMes and iAno = @iAno");


                    return _db.Query<BensBaseCalculoHistorico>(strGet.ToString(), new { IdBem = idBem, iMes = iMes, iAno = iAno }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetBensBaseCalculoHistoricoByIdBem");
                return null;
            }
        }

        public BensBaseCalculoHistorico InsertBensBaseCalculoHistorico(BensBaseCalculoHistorico dto)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();

                    decimal valueRent = Convert.ToDecimal(dto.Rent);
                    decimal valueHoa = Convert.ToDecimal(dto.Hoa);
                    decimal valuePropertyTaxProvision = Convert.ToDecimal(dto.PropertyTaxProvision);
                    decimal valuePropertyManagement = Convert.ToDecimal(dto.PropertyManagement);
                    decimal valueAdministrationFee = Convert.ToDecimal(dto.AdministrationFee);
                    decimal valueDividendYeld = Convert.ToDecimal(dto.DividendYeld);

                    decimal valueOutros = Convert.ToDecimal((string.IsNullOrEmpty(dto.OutrasDespesas) ? "0" : dto.OutrasDespesas));

                    strInsert.Append(" INSERT INTO [dbo].[financ.BensBaseCalculoHistorico] ");
                    strInsert.Append("   ([IdBem] ");
                    strInsert.Append("   ,[iMes] ");
                    strInsert.Append("   ,[iAno] ");
                    strInsert.Append("   ,[Rent] ");
                    strInsert.Append("   ,[Hoa] ");
                    strInsert.Append("   ,[PropertyTaxProvision] ");
                    strInsert.Append("   ,[PropertyManagement] ");
                    strInsert.Append("   ,[AdministrationFee] ");
                    strInsert.Append("   ,[DividendYeld] ");
                    strInsert.Append("   ,[OutrasDespesas] ");
                    strInsert.Append("   ,[ObservacaoOutras] ");
                    strInsert.Append("   ,[percentRentabilidade] ");
                    strInsert.Append("   ,[performance] ");
                    strInsert.Append("   ,[IdUsuarioInclusao] ");
                    strInsert.Append("   ,[DtInclusao]) ");
                    strInsert.Append(" VALUES ");
                    strInsert.Append("   (@IdBem ");
                    strInsert.Append("   ,@iMes ");
                    strInsert.Append("   ,@iAno ");
                    strInsert.Append("   ,@Rent ");
                    strInsert.Append("   ,@Hoa ");
                    strInsert.Append("   ,@PropertyTaxProvision ");
                    strInsert.Append("   ,@PropertyManagement ");
                    strInsert.Append("   ,@AdministrationFee ");
                    strInsert.Append("   ,@DividendYeld ");
                    strInsert.Append("   ,@OutrasDespesas ");
                    strInsert.Append("   ,@ObservacaoOutras ");
                    strInsert.Append("   ,@percentRentabilidade ");
                    strInsert.Append("   ,@performance ");
                    strInsert.Append("   ,@IdUsuarioInclusao ");
                    strInsert.Append("   ,getDate())  SELECT IDENT_CURRENT('[dbo].[financ.BensBaseCalculoHistorico]') AS [IDENT_CURRENT]");

                    var idretorno = _db.Query<int>(strInsert.ToString(), new
                    {
                        IdBem = dto.IdBem,
                        iMes = dto.iMes,
                        iAno = dto.iAno,
                        Rent = valueRent,
                        Hoa = valueHoa,
                        PropertyTaxProvision = valuePropertyTaxProvision,
                        PropertyManagement = valuePropertyManagement,
                        AdministrationFee = valueAdministrationFee,
                        DividendYeld = valueDividendYeld,
                        IdUsuarioInclusao = dto.IdUsuarioInclusao,
                        ObservacaoOutras = dto.ObservacaoOutras,
                        percentRentabilidade = dto.percentRentabilidade,
                        performance = dto.performance,
                        OutrasDespesas = valueOutros,

                    }).FirstOrDefault();
                    dto.Id = idretorno;

                    return dto;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "InsertBensBaseCalculoHistoricoByIdBem");

                return null;
            }
        }

        public BensBaseCalculoHistorico UpdateBensBaseCalculoHistorico(BensBaseCalculoHistorico dto)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {


                    decimal valueRent = Convert.ToDecimal(dto.Rent);
                    decimal valueHoa = Convert.ToDecimal(dto.Hoa);
                    decimal valuePropertyTaxProvision = Convert.ToDecimal(dto.PropertyTaxProvision);
                    decimal valuePropertyManagement = Convert.ToDecimal(dto.PropertyManagement);
                    decimal valueAdministrationFee = Convert.ToDecimal(dto.AdministrationFee);
                    decimal valueDividendYeld = Convert.ToDecimal(dto.DividendYeld);

                    decimal valueOutros = Convert.ToDecimal((string.IsNullOrEmpty(dto.OutrasDespesas) ? "0" : dto.OutrasDespesas));

                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" UPDATE [dbo].[financ.BensBaseCalculoHistorico] ");
                    strUpdate.Append("  SET [IdBem] = @IdBem ");
                    strUpdate.Append("     ,[iMes] = @iMes ");
                    strUpdate.Append("     ,[iAno] = @iAno ");
                    strUpdate.Append("     ,[Rent] = @Rent ");
                    strUpdate.Append("     ,[Hoa] = @Hoa ");
                    strUpdate.Append("     ,[PropertyTaxProvision] = @PropertyTaxProvision ");
                    strUpdate.Append("     ,[PropertyManagement] =@PropertyManagement ");
                    strUpdate.Append("     ,[AdministrationFee] = @AdministrationFee ");
                    strUpdate.Append("     ,[DividendYeld] =@DividendYeld ");
                    strUpdate.Append("     ,[OutrasDespesas] =@OutrasDespesas ");
                    strUpdate.Append("     ,[ObservacaoOutras] =@ObservacaoOutras ");
                    strUpdate.Append("     ,[percentRentabilidade] =@percentRentabilidade ");
                    strUpdate.Append("     ,[performance] =@performance ");
                    strUpdate.Append("     ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("     ,[DtAlteracao] =getDate() ");
                    strUpdate.Append(" WHERE Id = @Id");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        IdBem = dto.IdBem,
                        Id = dto.Id,
                        iMes = dto.iMes,
                        iAno = dto.iAno,
                        Rent = valueRent,
                        Hoa = valueHoa,
                        PropertyTaxProvision = valuePropertyTaxProvision,
                        PropertyManagement = valuePropertyManagement,
                        AdministrationFee = valueAdministrationFee,
                        DividendYeld = valueDividendYeld,
                        ObservacaoOutras = dto.ObservacaoOutras,
                        OutrasDespesas = valueOutros,
                        percentRentabilidade = dto.percentRentabilidade,
                        performance = dto.performance,
                        IdUsuarioAlteracao = dto.IdUsuarioAlteracao
                    });


                    return dto;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "UpdateBensBaseCalculoHistoricoByIdBem");
                return null;
            }
        }




        //---------------------------------------GASTOS---------------------------------//
        public IEnumerable<BensGastos> GetListBensGastosByIdBem(int idBem)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("     ,[IdBem] ");
                    strGet.Append("     ,[PathLocation] ");
                    strGet.Append("     ,[PathLocationIIS] ");
                    strGet.Append("     ,[Descricao] ");
                    strGet.Append("     ,[Valor] ");
                    strGet.Append("     ,[DtOcorrencia] ");
                    strGet.Append("     ,[FlagAtivo] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append(" from [dbo].[BensGastos] where FlagAtivo = 1 and IdBem = @idbem order by DtOcorrencia desc");

                    return _db.Query<BensGastos>(strGet.ToString(), new { idBem = idBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetListBensGastosByIdBem");
                return null;
            }
        }
        public BensGastos InsertBensGasto(BensGastos bemG)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    decimal valor = Convert.ToDecimal(bemG.Valor);

                    strInsert.Append(" INSERT INTO [dbo].[BensGastos] ");
                    strInsert.Append("   ([IdBem] ");
                    strInsert.Append("   ,[PathLocation] ");
                    strInsert.Append("   ,[PathLocationIIS] ");
                    strInsert.Append("   ,[Descricao] ");
                    strInsert.Append("   ,[Valor] ");
                    strInsert.Append("   ,[FlagAtivo] ");
                    strInsert.Append("   ,[IdUsuarioInclusao] ");
                    strInsert.Append("   ,[DtInclusao] ");
                    strInsert.Append("   ,[DtOcorrencia]) ");
                    strInsert.Append("  VALUES ");
                    strInsert.Append("    (@IdBem ");
                    strInsert.Append("    ,@PathLocation ");
                    strInsert.Append("    ,@PathLocationIIS ");
                    strInsert.Append("    ,@Descricao ");
                    strInsert.Append("    ,@Valor ");
                    strInsert.Append("    ,@FlagAtivo ");
                    strInsert.Append("    ,@IdUsuarioInclusao ");
                    strInsert.Append("    , getDate()");
                    strInsert.Append("    ,@DtOcorrencia) SELECT IDENT_CURRENT('[BensGastos]') AS [IDENT_CURRENT]");


                    var idbensgastos = _db.Query<int>(strInsert.ToString(), new
                    {
                        IdBem = bemG.IdBem,
                        PathLocation = bemG.PathLocation,
                        PathLocationIIS = bemG.PathLocationIIS,
                        Descricao = bemG.Descricao,
                        Valor = valor,
                        FlagAtivo = bemG.FlagAtivo,
                        IdUsuarioInclusao = bemG.IdUsuarioInclusao,
                        DtOcorrencia = bemG.DtOcorrencia
                    }).FirstOrDefault();

                    bemG.Id = idbensgastos;

                    return bemG;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "InsertBensGasto");
                return null;
            }
        }
        public BensGastos UpdateBensGasto(BensGastos bemG)
        {

            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    decimal valor = Convert.ToDecimal(bemG.Valor);
                    StringBuilder strUp = new StringBuilder();

                    strUp.Append(" UPDATE [dbo].[BensGastos] ");
                    strUp.Append("  SET [IdBem] = @IdBem ");
                    strUp.Append("     ,[PathLocation] = @PathLocation ");
                    strUp.Append("     ,[PathLocationIIS] = @PathLocationIIS ");
                    strUp.Append("     ,[Descricao] = @Descricao ");
                    strUp.Append("     ,[Valor] = @Valor ");
                    strUp.Append("     ,[FlagAtivo] =@FlagAtivo ");
                    strUp.Append("     ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUp.Append("     ,[DtAlteracao] = getDate() ");
                    strUp.Append("     ,[DtOcorrencia] = @DtOcorrencia ");
                    strUp.Append(" WHERE   Id = @id  ");

                    _db.Execute(strUp.ToString(), new
                    {
                        Id = bemG.Id,
                        IdBem = bemG.IdBem,
                        PathLocation = bemG.PathLocation,
                        PathLocationIIS = bemG.PathLocationIIS,
                        Descricao = bemG.Descricao,
                        Valor = valor,
                        FlagAtivo = bemG.FlagAtivo,
                        IdUsuarioAlteracao = bemG.IdUsuarioAlteracao,
                        DtOcorrencia = bemG.DtOcorrencia
                    });


                    return bemG;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "UpdateBensGasto");
                return null;
            }
        }
        public BensGastos GetBensGastoById(int intIdBemGastos)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("     ,[IdBem] ");
                    strGet.Append("     ,[PathLocation] ");
                    strGet.Append("     ,[PathLocationIIS] ");
                    strGet.Append("     ,[Descricao] ");
                    strGet.Append("     ,[Valor] ");
                    strGet.Append("     ,[DtOcorrencia] ");
                    strGet.Append("     ,[FlagAtivo] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append(" from [dbo].[BensGastos] where  Id = @intIdBemGastos ");

                    return _db.Query<BensGastos>(strGet.ToString(), new { intIdBemGastos = intIdBemGastos }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensRepository", "GetBensGastoById");
                return null;
            }

        }




    }
}
