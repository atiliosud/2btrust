﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using Domain.Enuns;

namespace Data.Repository
{
    public class PoolBensPessoasRepository : BaseRepository, IPoolBensPessoasRepository
    {



        public PoolBensPessoas InsertPoolBensPessoas(PoolBensPessoas con)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strINsert = new StringBuilder();

                    strINsert.Append(" INSERT INTO [financ].[PoolBensPessoas] ");
                    strINsert.Append(" ([IdPoolBem] ");
                    strINsert.Append(" ,[IdPessoa]  ");
                    strINsert.Append(" ,[Cota]      ");
                    strINsert.Append(" ,[IdStatus]  ");
                    strINsert.Append(" ,[IdUsuarioInclusao] ");
                    strINsert.Append(" ,[DtInclusaoAdm] ");
                    strINsert.Append(" ,[DtInclusao] ) ");
                    strINsert.Append("   VALUES ");
                    strINsert.Append("  (@IdPoolBem ");
                    strINsert.Append("  ,@IdPessoa ");
                    strINsert.Append("  ,@Cota ");
                    strINsert.Append("  ,@IdStatus ");
                    strINsert.Append("  ,@IdUsuarioInclusao ");
                    strINsert.Append("  ,@DtInclusaoAdm ");
                    strINsert.Append("  ,@DtInclusao)   SELECT IDENT_CURRENT('[financ].[PoolBensPessoas]') AS [IDENT_CURRENT]");


                    DateTime? dtAdm = (con.DtInclusaoAdm == null) ? (DateTime?)null : DateTime.Now;
                    DateTime dtNow = (con.DtInclusaoAdm == null) ? DateTime.Now : con.DtInclusaoAdm.Value;


                    var idPoolsBensPessoa = _db.Execute(strINsert.ToString(), new
                    {
                        IdPoolBem = con.IdPoolBem,
                        IdPessoa = con.IdPessoa,
                        Cota = con.Cota,
                        IdStatus = con.IdStatus,
                        IdUsuarioInclusao = con.IdUsuarioInclusao,

                        DtInclusaoAdm = dtAdm,
                        DtInclusao = dtNow

                    });


                    if (con.IdStatus == 12)///venda
                    {
                        try
                        {

                            string nomePool = _db.Query<string>("select Nome   FROM [financ].[PoolBens] where Id= @id", new { id = con.IdPoolBem }).FirstOrDefault();
                            var getLogCarteira = GetLastLogCarteira(con.IdPessoa);
                            if (getLogCarteira == null)
                            {
                                CarteiraLog objNew = new CarteiraLog()
                                {
                                    Amount = con.Cota * 100,
                                    AvaliableBalance = 0,
                                    Data = (con.DtInclusaoAdm == null)?DateTime.Now :  con.DtInclusaoAdm  ,
                                    Descricao = "Buy Order - " + (int)con.Cota + " Units " + nomePool + "  ",
                                    FlagAtivo = true,
                                    IdPessoa = con.IdPessoa,
                                    Tipo = "solicitação compra cota",
                                };

                                InserLogCarteira(objNew);
                            }
                            else
                            {
                                getLogCarteira.Descricao = "Buy Order - " + (int)con.Cota + " Units " + nomePool + "  ";
                                getLogCarteira.Tipo = "solicitação compra cota";
                                getLogCarteira.Amount = con.Cota * 100;
                                getLogCarteira.AvaliableBalance = getLogCarteira.AvaliableBalance;
                                getLogCarteira.Data = (con.DtInclusaoAdm == null) ? DateTime.Now : con.DtInclusaoAdm;

                                InserLogCarteira(getLogCarteira);

                            }
                        }
                        catch { }
                    }
                    else if (con.IdStatus == 9)
                    {

                        try
                        {

                            string nomePool = _db.Query<string>("select Nome   FROM [financ].[PoolBens] where Id= @id", new { id = con.IdPoolBem }).FirstOrDefault();
                            var getLogCarteira = GetLastLogCarteira(con.IdPessoa);
                            if (getLogCarteira == null)
                            {
                                CarteiraLog objNew = new CarteiraLog()
                                {
                                    Amount = con.Cota * 100,
                                    AvaliableBalance = 0,
                                    Data = (con.DtInclusaoAdm == null) ? DateTime.Now : con.DtInclusaoAdm,
                                    Descricao = "Sell Order - " + (int)con.Cota + " Units " + nomePool + " ",
                                    FlagAtivo = true,
                                    IdPessoa = con.IdPessoa,
                                    Tipo = "solicitação venda cota",
                                };

                                InserLogCarteira(objNew);
                            }
                            else
                            {
                                getLogCarteira.Descricao = "Sell Order - " + (int)con.Cota + " Units " + nomePool + " ";
                                getLogCarteira.Tipo = "solicitação venda cota";
                                getLogCarteira.Amount = con.Cota * 100;
                                getLogCarteira.AvaliableBalance = getLogCarteira.AvaliableBalance;
                                getLogCarteira.Data = (con.DtInclusaoAdm == null) ? DateTime.Now : con.DtInclusaoAdm;

                                InserLogCarteira(getLogCarteira);

                            }
                        }
                        catch { }
                    }
                    return con;

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensPessoasRepository", "InsertPoolBensPessoas");

                return null;
            }
        }

        public PoolBensPessoas UpdatePoolBensPessoas(PoolBensPessoas con)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [financ].[PoolBensPessoas] ");
                    strUpdate.Append("   SET [IdPoolBem] = @IdPoolBem ");
                    strUpdate.Append("      ,[IdPessoa] = @IdPessoa ");
                    strUpdate.Append("      ,[Cota] = @Cota ");
                    strUpdate.Append("      ,[IdStatus] = @IdStatus ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = @DtAlteracao ");
                    strUpdate.Append(" WHERE Id = @id ");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        IdPoolBem = con.IdPoolBem,
                        IdPessoa = con.IdPessoa,
                        Cota = con.Cota,
                        IdStatus = con.IdStatus,
                        IdUsuarioAlteracao = con.IdUsuarioAlteracao,
                        Id = con.Id,
                        DtAlteracao =con.DtAlteracao
                    });



                    if (con.IdStatus == (int)EnumInvestimento.VendaAprovada)
                    {

                        StringBuilder strGetPBP = new StringBuilder();
                        strGetPBP.Append(" SELECT [Id] ");
                        strGetPBP.Append("    ,[IdPoolBem] ");
                        strGetPBP.Append("    ,[IdPessoa] ");
                        strGetPBP.Append("    ,[Cota] ");
                        strGetPBP.Append("    ,[IdStatus] ");
                        strGetPBP.Append("    ,[IdUsuarioInclusao] ");
                        strGetPBP.Append("    ,[DtInclusao] ");
                        strGetPBP.Append("    ,[IdUsuarioAlteracao] ");
                        strGetPBP.Append("    ,[DtAlteracao] ");
                        strGetPBP.Append("    ,[DtInclusaoAdm] ");
                        strGetPBP.Append(" FROM [financ].[PoolBensPessoas] ");
                        strGetPBP.Append(" where IdPoolBem = @idPool and IdPessoa = @idPessoa and IdStatus = 1 order by  [Cota] asc");


                        var lista = _db.Query<PoolBensPessoas>(strGetPBP.ToString(), new { idPessoa = con.IdPessoa, idPool = con.IdPoolBem }).AsEnumerable();
                        int iRestante = (int)con.Cota;
                        foreach (var item in lista)
                        {
                            if (iRestante == 0) break;

                            if (item.Cota < iRestante)
                            {
                                //    <

                                var iChegaZero = item.Cota - iRestante;
                                iChegaZero = iRestante - (iChegaZero * -1);

                                //valor a ser descontado desse investimento
                                item.Cota = item.Cota - iChegaZero;

                                iRestante = iRestante - (int)iChegaZero;

                                //atualiza PBP
                                StringBuilder strUpdates = new StringBuilder();

                                strUpdates.Append(" UPDATE [financ].[PoolBensPessoas] ");
                                strUpdates.Append("   SET [Cota] = @Cota ");
                                strUpdates.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                                strUpdates.Append("      ,[DtAlteracao] = getDate() ");
                                strUpdates.Append(" WHERE Id = @id ");

                                _db.Execute(strUpdates.ToString(), new
                                {
                                    Cota = item.Cota,
                                    IdStatus = item.IdStatus,
                                    IdUsuarioAlteracao = con.IdUsuarioAlteracao,
                                    Id = item.Id
                                });


                            }
                            else if (item.Cota >= iRestante)
                            {
                                //  >

                                item.Cota = item.Cota - iRestante;

                                StringBuilder strUpdates = new StringBuilder();

                                strUpdates.Append(" UPDATE [financ].[PoolBensPessoas] ");
                                strUpdates.Append("   SET [Cota] = @Cota ");
                                strUpdates.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                                strUpdates.Append("      ,[DtAlteracao] = getDate() ");
                                strUpdates.Append(" WHERE Id = @id ");

                                _db.Execute(strUpdates.ToString(), new
                                {
                                    Cota = item.Cota,
                                    IdStatus = item.IdStatus,
                                    IdUsuarioAlteracao = con.IdUsuarioAlteracao,
                                    Id = item.Id
                                });

                                break;

                            }

                        }


                    }


                    if (con.IdStatus == (int)EnumInvestimento.CompraAprovada)
                    {
                        StringBuilder strINsert = new StringBuilder();

                        strINsert.Append(" INSERT INTO [financ].[PoolBensPessoas] ");
                        strINsert.Append(" ([IdPoolBem] ");
                        strINsert.Append(" ,[IdPessoa]  ");
                        strINsert.Append(" ,[Cota]      ");
                        strINsert.Append(" ,[IdStatus]  ");
                        strINsert.Append(" ,[IdUsuarioInclusao] ");
                        strINsert.Append(" ,[DtInclusaoAdm] ");
                        strINsert.Append(" ,[DtInclusao] ) ");
                        strINsert.Append("   VALUES ");
                        strINsert.Append("  (@IdPoolBem ");
                        strINsert.Append("  ,@IdPessoa ");
                        strINsert.Append("  ,@Cota ");
                        strINsert.Append("  ,@IdStatus ");
                        strINsert.Append("  ,@IdUsuarioInclusao ");
                        strINsert.Append("  ,@DtInclusaoAdm ");
                        strINsert.Append("  ,@DtInclusao)   SELECT IDENT_CURRENT('[financ].[PoolBensPessoas]') AS [IDENT_CURRENT]");


                        DateTime? dtAdm = (con.DtInclusaoAdm == null) ? (DateTime?)null : DateTime.Now;
                        DateTime dtNow = (con.DtInclusaoAdm == null) ? DateTime.Now : con.DtInclusaoAdm.Value;


                        var idPoolsBensPessoa = _db.Execute(strINsert.ToString(), new
                        {
                            IdPoolBem = con.IdPoolBem,
                            IdPessoa = con.IdPessoa,
                            Cota = con.Cota,
                            IdStatus = 1,
                            IdUsuarioInclusao = con.IdUsuarioInclusao,

                            DtInclusaoAdm = dtAdm,
                            DtInclusao = dtNow

                        });
                    }



                    //ajuste log carteira
                    if (con.IdStatus == (int)EnumInvestimento.CompraAprovada || (con.IdStatus == (int)EnumInvestimento.CompraRecusada))
                    {
                        try
                        {
                            string nomePool = _db.Query<string>("select Nome   FROM [financ].[PoolBens] where Id= @id", new { id = con.IdPoolBem }).FirstOrDefault();

                            var getLogCarteira = GetLastLogCarteira(con.IdPessoa);
                            if (getLogCarteira == null)
                            {
                                CarteiraLog objNew = new CarteiraLog()
                                {
                                    Amount = con.Cota * 100,
                                    AvaliableBalance = (con.IdStatus == (int)EnumInvestimento.CompraAprovada) ? con.Cota * 100 : 0,
                                    Data =con.DtAlteracao,
                                    Descricao = (con.IdStatus == (int)EnumInvestimento.CompraAprovada) ? "Buy Order Accepted - " + (int)con.Cota + " Units " + nomePool : "Buy Order Refused -  " + (int)con.Cota + " Units " + nomePool ,
                                    FlagAtivo = true,
                                    IdPessoa = con.IdPessoa,
                                    Tipo = (con.IdStatus == (int)EnumInvestimento.CompraAprovada) ? "compra cota aprovado" : "compra cota recusado",
                                };

                                InserLogCarteira(objNew);
                            }
                            else
                            {
                                getLogCarteira.Descricao = (con.IdStatus == (int)EnumInvestimento.CompraAprovada) ? "Buy Order Accepted - " + (int)con.Cota + " Units " + nomePool : "Buy Order refused " + (int)con.Cota + " Units " + nomePool + " ";
                                getLogCarteira.Tipo = (con.IdStatus == (int)EnumInvestimento.CompraAprovada) ? "compra cota aprovado" : "compra cota recusado";
                                getLogCarteira.Amount = con.Cota * 100;
                                getLogCarteira.AvaliableBalance = (con.IdStatus == (int)EnumInvestimento.CompraAprovada) ? getLogCarteira.AvaliableBalance - (con.Cota * 100) : getLogCarteira.AvaliableBalance;
                                getLogCarteira.Data = con.DtAlteracao;

                                InserLogCarteira(getLogCarteira);

                            }
                        }
                        catch { }
                    }



                    if (con.IdStatus == (int)EnumInvestimento.VendaAprovada || (con.IdStatus == (int)EnumInvestimento.VendaRecusada))
                    {
                        try
                        {
                            string nomePool = _db.Query<string>("select Nome   FROM [financ].[PoolBens] where Id= @id", new { id = con.IdPoolBem }).FirstOrDefault();

                            var getLogCarteira = GetLastLogCarteira(con.IdPessoa);
                            if (getLogCarteira == null)
                            {
                                CarteiraLog objNew = new CarteiraLog()
                                {
                                    Amount = con.Cota * 100,
                                    AvaliableBalance = (con.IdStatus == (int)EnumInvestimento.VendaAprovada) ? con.Cota * 100 : 0,
                                    Data = con.DtAlteracao,
                                    Descricao = (con.IdStatus == (int)EnumInvestimento.VendaAprovada) ? "Sell Order Accept - " + (int)con.Cota + " Units " + nomePool  : "Sell Order Refused - " + (int)con.Cota + " Units " + nomePool + " ",
                                    FlagAtivo = true,
                                    IdPessoa = con.IdPessoa,        
                                    Tipo = (con.IdStatus == (int)EnumInvestimento.VendaAprovada) ? "venda cota aprovado" : "venda cota recusado",
                                };

                                InserLogCarteira(objNew);
                            }
                            else
                            {
                                getLogCarteira.Descricao = (con.IdStatus == (int)EnumInvestimento.VendaAprovada) ? "Sell Order Accept - " + (int)con.Cota + " Units " + nomePool  : "Sell Order Refused - " + (int)con.Cota + " Units " + nomePool + " ";
                                getLogCarteira.Tipo = (con.IdStatus == (int)EnumInvestimento.VendaAprovada) ? "venda cota aprovado" : "venda cota recusado";
                                getLogCarteira.Amount = con.Cota * 100;
                                getLogCarteira.AvaliableBalance = (con.IdStatus == (int)EnumInvestimento.VendaAprovada) ? getLogCarteira.AvaliableBalance + (con.Cota * 100) : getLogCarteira.AvaliableBalance;
                                getLogCarteira.Data = con.DtAlteracao;

                                InserLogCarteira(getLogCarteira);

                            }
                        }
                        catch { }
                    }

                    return con;

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensPessoasRepository", "UpdatePoolBensPessoas");

                return null;
            }
        }

        public IEnumerable<PoolBensPessoas> GetListPoolBensPessoas()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdPoolBem] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Cota] ");
                    strGet.Append("      ,[IdStatus] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("  FROM [financ].[PoolBensPessoas] order by Cota");

                    return _db.Query<PoolBensPessoas>(strGet.ToString()).AsEnumerable();
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensPessoasRepository", "GetListPoolBensPessoas");

                return null;
            }
        }

        public PoolBensPessoas GetPoolBensPessoasById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdPoolBem] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Cota] ");
                    strGet.Append("      ,[IdStatus] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("  FROM [financ].[PoolBensPessoas] where Id =  @Id");

                    return _db.Query<PoolBensPessoas>(strGet.ToString(), new { Id = id }).FirstOrDefault();


                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensPessoasRepository", "GetPoolBensPessoasById");

                return null;
            }
        }

        public IEnumerable<PoolBensPessoas> GetPoolBensPessoasByIdPessoa(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdPoolBem] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Cota] ");
                    strGet.Append("      ,[IdStatus] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("  FROM [financ].[PoolBensPessoas] where IdPessoa =  @IdPessoa");

                    return _db.Query<PoolBensPessoas>(strGet.ToString(), new { IdPessoa = idPessoa }).AsEnumerable();

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensPessoasRepository", "GetPoolBensPessoasByIdPessoa");

                return null;
            }
        }

        public IEnumerable<PoolBensPessoas> GetPoolBensPessoasByIdPoolBem(int idPoolBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("      ,[IdPoolBem] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Cota] ");
                    strGet.Append("      ,[IdStatus] ");
                    strGet.Append("      ,[IdUsuarioInclusao] ");
                    strGet.Append("      ,[DtInclusao] ");
                    strGet.Append("      ,[IdUsuarioAlteracao] ");
                    strGet.Append("      ,[DtAlteracao] ");
                    strGet.Append("  FROM [financ].[PoolBensPessoas] where IdPoolBem =  @idPoolBem");

                    return _db.Query<PoolBensPessoas>(strGet.ToString(), new { idPoolBem = idPoolBem }).AsEnumerable();

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PoolBensPessoasRepository", "GetPoolBensPessoasByIdPoolBem");

                return null;
            }
        }

        public IEnumerable<Pessoas> GetListValidaVendaCota()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" select P.Id, P.Nome, PBP.Id, PBP.Cota, PBP.DtInclusao , PBP.IdStatus, PB.Id, PB.Nome ");
                    strGet.Append(" from financ.PoolBensPessoas PBP ");
                    strGet.Append("  inner ");
                    strGet.Append(" join crm.Pessoas P on P.Id = PBP.IdPessoa ");
                    strGet.Append("  ");
                    strGet.Append(" inner ");
                    strGet.Append(" join financ.PoolBens PB on PB.Id = PBP.IdPoolBem ");
                    strGet.Append(" where PBP.IdStatus in (9, 11) order by PBP.DtInclusao asc");


                    var get = _db.Query<Pessoas, PoolBensPessoas, PoolBens, Pessoas>(strGet.ToString(),
                        (P, PBP, PB) =>
                        {
                            P._ObjMeusPools = PBP;
                            P._ObjMeusPools._PoolBens = PB;

                            return P;
                        }, splitOn: "Id").AsEnumerable();


                    return get;
                }
            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PoolBensPessoasRepository", "GetListValidaVendaCota");

                return null;
            }
        }

        public IEnumerable<Pessoas> GetListValidaRetirada()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    return null;

                }
            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PoolBensPessoasRepository", "GetListValidaRetirada");

                return null;
            }
        }

        public IEnumerable<Pessoas> GetListValidaCompraCota()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" select P.Id, P.Nome, PBP.Id, PBP.Cota, PBP.DtInclusao , PBP.IdStatus, PB.Id, PB.Nome ");
                    strGet.Append(" from financ.PoolBensPessoas PBP ");
                    strGet.Append("  inner ");
                    strGet.Append(" join crm.Pessoas P on P.Id = PBP.IdPessoa ");
                    strGet.Append("  ");
                    strGet.Append(" inner ");
                    strGet.Append(" join financ.PoolBens PB on PB.Id = PBP.IdPoolBem ");
                    strGet.Append(" where PBP.IdStatus in (12, 14) order by PBP.DtInclusao asc");


                    var get = _db.Query<Pessoas, PoolBensPessoas, PoolBens, Pessoas>(strGet.ToString(),
                        (P, PBP, PB) =>
                        {
                            P._ObjMeusPools = PBP;
                            P._ObjMeusPools._PoolBens = PB;

                            return P;
                        }, splitOn: "Id").AsEnumerable();


                    return get;
                }
            }
            catch (Exception ex)
            {

                InsertLogError(ex, "PoolBensPessoasRepository", "GetListValidaCompraCota");

                return null;
            }
        }
    }
}
