﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Data.Repository
{
    public class PessoasEnderecosRepository : BaseRepository, IPessoasEnderecosRepository
    {
        public PessoasEnderecos GetEnderecoById(int idPessoaEndereco)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetEndereco = new StringBuilder();
                    strGetEndereco.Append("  SELECT [Id] ");
                    strGetEndereco.Append("   ,[IdPessoa] ");
                    strGetEndereco.Append("   ,[IdTpEndereco] ");
                    strGetEndereco.Append("   ,[IdCidade] ");
                    strGetEndereco.Append("   ,[Cidade] ");
                    strGetEndereco.Append("   ,[Estado] ");
                    strGetEndereco.Append("   ,[Bairro] ");
                    strGetEndereco.Append("   ,[Logradouro] ");
                    strGetEndereco.Append("   ,[Numero] ");
                    strGetEndereco.Append("   ,[Complemento] ");
                    strGetEndereco.Append("   ,[Pais] ");
                    strGetEndereco.Append("   ,[Cep] ");
                    strGetEndereco.Append("   ,[IdUsuarioInclusao] ");
                    strGetEndereco.Append("   ,[DtInclusao] ");
                    strGetEndereco.Append("   ,[IdUsuarioAlteracao] ");
                    strGetEndereco.Append("   ,[DtAlteracao] ");
                    strGetEndereco.Append("   FROM [crm].[PessoasEnderecos] where Id  = @idPessoaEndereco");

                    return _db.Query<PessoasEnderecos>(strGetEndereco.ToString(), new { idPessoaEndereco = idPessoaEndereco }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasEnderecosRepository", "GetEnderecoById");

                return null;
            }
        }

        public PessoasEnderecos GetEnderecoByIdPessoa(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetEndereco = new StringBuilder();
                    strGetEndereco.Append("  SELECT [Id] ");
                    strGetEndereco.Append("   ,[IdPessoa] ");
                    strGetEndereco.Append("   ,[IdTpEndereco] ");
                    strGetEndereco.Append("   ,[IdCidade] ");
                    strGetEndereco.Append("   ,[Cidade] ");
                    strGetEndereco.Append("   ,[Estado] ");
                    strGetEndereco.Append("   ,[Bairro] ");
                    strGetEndereco.Append("   ,[Logradouro] ");
                    strGetEndereco.Append("   ,[Pais] ");
                    strGetEndereco.Append("   ,[Numero] ");
                    strGetEndereco.Append("   ,[Complemento] ");
                    strGetEndereco.Append("   ,[Cep] ");
                    strGetEndereco.Append("   ,[IdUsuarioInclusao] ");
                    strGetEndereco.Append("   ,[DtInclusao] ");
                    strGetEndereco.Append("   ,[IdUsuarioAlteracao] ");
                    strGetEndereco.Append("   ,[DtAlteracao] ");
                    strGetEndereco.Append("   FROM [crm].[PessoasEnderecos] where IdPessoa  = @IdPessoa");

                    return _db.Query<PessoasEnderecos>(strGetEndereco.ToString(), new { IdPessoa = idPessoa }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasEnderecosRepository", "GetEnderecoByIdPessoa");

                return null;
            }
        }

        public PessoasEnderecos InsertEndereco(PessoasEnderecos pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                

                    StringBuilder strInsertEndereco = new StringBuilder();
                    strInsertEndereco.Append(" INSERT INTO [crm].[PessoasEnderecos] ");
                    strInsertEndereco.Append("         ([IdPessoa] ");
                    strInsertEndereco.Append("          ,[Cidade] ");
                    strInsertEndereco.Append("          ,[Estado] "); 
                    strInsertEndereco.Append("         ,[Bairro] ");
                    strInsertEndereco.Append("         ,[Logradouro] ");
                    strInsertEndereco.Append("         ,[Numero] ");
                    strInsertEndereco.Append("         ,[Complemento] ");
                    strInsertEndereco.Append("         ,[Pais] ");
                    strInsertEndereco.Append("         ,[Cep] ");
                    strInsertEndereco.Append("         ,[IdUsuarioInclusao] ");
                    strInsertEndereco.Append("         ,[DtInclusao] ");
                    strInsertEndereco.Append("         ) ");
                    strInsertEndereco.Append("   VALUES ");
                    strInsertEndereco.Append("         (@IdPessoa "); 
                    strInsertEndereco.Append("         ,@Cidade ");
                    strInsertEndereco.Append("         ,@Estado "); 
                    strInsertEndereco.Append("         ,@Bairro ");
                    strInsertEndereco.Append("         ,@Logradouro  ");
                    strInsertEndereco.Append("         ,@Numero ");
                    strInsertEndereco.Append("         ,@Complemento  ");
                    strInsertEndereco.Append("         ,@Pais  ");
                    strInsertEndereco.Append("         ,@Cep ");
                    strInsertEndereco.Append("         ,@IdUsuarioInclusao  ");
                    strInsertEndereco.Append("         ,getDate() ");
                    strInsertEndereco.Append("         ) SELECT IDENT_CURRENT('[crm].[PessoasEnderecos]') AS [IDENT_CURRENT]");

                    var idPessoaEndereco = _db.Query<int>(strInsertEndereco.ToString(), new
                    {
                        IdPessoa = pes.IdPessoa,
                        Estado = pes.Estado,
                        Cidade = pes.Cidade,
                        Bairro = pes.Bairro,
                        Logradouro = pes.Logradouro,
                        Numero = pes.Numero,
                        Complemento = pes.Complemento,
                        Pais = pes.Pais,
                        Cep = pes.Cep,
                        IdUsuarioInclusao = pes.IdUsuarioInclusao
                    }).FirstOrDefault();

                    return GetEnderecoById(idPessoaEndereco);

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasEnderecoRepository", "InsertEnderecoByIdPessoa");

                return null;
            }
        }

        public PessoasEnderecos UpdateEndereco(PessoasEnderecos pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                   
                    StringBuilder strUpdateEndereco = new StringBuilder();
                    strUpdateEndereco.Append(" UPDATE [crm].[PessoasEnderecos] ");
                    strUpdateEndereco.Append("   SET [IdPessoa] = @IdPessoa ");
                    strUpdateEndereco.Append("  ,[Cidade] = @Cidade  ");
                    strUpdateEndereco.Append("  ,[Estado] = @Estado  ");
                    strUpdateEndereco.Append("  ,[Bairro] = @Bairro ");
                    strUpdateEndereco.Append("  ,[Logradouro] = @Logradouro ");
                    strUpdateEndereco.Append("  ,[Numero] = @Numero ");
                    strUpdateEndereco.Append("  ,[Complemento] =@Complemento ");
                    strUpdateEndereco.Append("  ,[Pais] =@Pais ");
                    strUpdateEndereco.Append("  ,[Cep] =@Cep");
                    strUpdateEndereco.Append("  ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdateEndereco.Append("  ,[DtAlteracao] = getDate() ");
                    strUpdateEndereco.Append(" WHERE Id = @IdPessoaEndereco ");

                    _db.Execute(strUpdateEndereco.ToString(), new
                   {
                       IdPessoa = pes.IdPessoa, 
                       Bairro = pes.Bairro,
                       Cidade = pes.Cidade,
                       Estado = pes.Estado,
                       Logradouro = pes.Logradouro,
                       Numero = pes.Numero,
                       Complemento = pes.Complemento,
                       Pais = pes.Pais,
                       Cep = pes.Cep,
                       IdUsuarioAlteracao = pes.IdUsuarioAlteracao,
                       IdPessoaEndereco = pes.Id
                   });

                    return pes;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasEnderecoRepository", "UpdateEnderecoByIdPessoa");

                return null;
            }
        }


    }
}
