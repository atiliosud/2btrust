﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using Domain.Entities._2BTrust;


namespace Data.Repository
{
    public class BensAcoesRepository : BaseRepository, IBensAcoesRepository
    {
        public Domain.Entities._2BTrust.BensAcoes InsertBensAcoes(Domain.Entities._2BTrust.BensAcoes bemAcao)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                { 

                    StringBuilder strInsertAcoes = new StringBuilder();
                    strInsertAcoes.Append(" INSERT INTO [financ].[BensAcoes] ");
                    strInsertAcoes.Append("         ([IdBem] ");
                    strInsertAcoes.Append("         ,[CodigoAcao] ");
                    strInsertAcoes.Append("         ,[CotacaoAtual] ");
                    strInsertAcoes.Append("         ,[QtdeCotas] ");
                    strInsertAcoes.Append("         ,[IdUsuarioInclusao] ");
                    strInsertAcoes.Append("         ,[DtInclusao] ");
                    strInsertAcoes.Append("         ) ");
                    strInsertAcoes.Append("   VALUES ");
                    strInsertAcoes.Append("         (@IdBem ");
                    strInsertAcoes.Append("         ,@CodigoAcao ");
                    strInsertAcoes.Append("         ,@CotacaoAtual ");
                    strInsertAcoes.Append("         ,@QtdeCotas ");
                    strInsertAcoes.Append("         ,@IdUsuarioInclusao  ");
                    strInsertAcoes.Append("         ,getDate() ");
                    strInsertAcoes.Append("         ) SELECT IDENT_CURRENT('[financ].[BensAcoes]') AS [IDENT_CURRENT]");

                    var idBemAcoes = _db.Query<int>(strInsertAcoes.ToString(), new
                    {
                        IdBem = bemAcao.IdBem,
                        CodigoAcao = bemAcao.CodigoAcao,
                        CotacaoAtual = bemAcao.CotacaoAtual,
                        QtdeCotas = bemAcao.QtdeCotas,
                        IdUsuarioInclusao = bemAcao.IdUsuarioInclusao
                    }).FirstOrDefault();


                    bemAcao.Id = idBemAcoes;

                    return bemAcao;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensAcoesRepository", "InsertBensAcoes");
                return null;
            }
        }

        public Domain.Entities._2BTrust.BensAcoes UpdateBensAcoes(Domain.Entities._2BTrust.BensAcoes bemAcao)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                { 
 
                    StringBuilder strUpdateAcoes = new StringBuilder();
                    strUpdateAcoes.Append(" UPDATE [financ].[BensAcoes] ");
                    strUpdateAcoes.Append("   SET [IdBem] = @IdBem ");
                    strUpdateAcoes.Append("  ,[CodigoAcao] = @CodigoAcao ");
                    strUpdateAcoes.Append("  ,[CotacaoAtual] = @CotacaoAtual ");
                    strUpdateAcoes.Append("  ,[QtdeCotas] = @QtdeCotas ");
                    strUpdateAcoes.Append("  ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdateAcoes.Append("  ,[DtAlteracao] = getDate() ");
                    strUpdateAcoes.Append(" WHERE Id = @IdBemAcoes ");

                    _db.Execute(strUpdateAcoes.ToString(), new
                    {
                        IdBem = bemAcao.IdBem,
                        CodigoAcao = bemAcao.CodigoAcao,
                        CotacaoAtual = bemAcao.CotacaoAtual,
                        QtdeCotas = bemAcao.QtdeCotas,
                        IdUsuarioAlteracao = bemAcao.IdUsuarioAlteracao,
                       IdBemAcoes = bemAcao.Id
                   });

                    return bemAcao;

                

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensAcoesRepository", "UpdateBensAcoes");
                return null;
            }
        }

        public IEnumerable<Domain.Entities._2BTrust.BensAcoes> GetListBensAcoesByIdBem(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetAcoes = new StringBuilder();
                    strGetAcoes.Append("  SELECT  [Id] ");
                    strGetAcoes.Append("         ,[IdBem] ");
                    strGetAcoes.Append("         ,[CodigoAcao] ");
                    strGetAcoes.Append("         ,[CotacaoAtual] ");
                    strGetAcoes.Append("         ,[QtdeCotas] ");
                    strGetAcoes.Append("         ,[IdUsuarioInclusao] ");
                    strGetAcoes.Append("         ,[DtInclusao] ");
                    strGetAcoes.Append("         ,[IdUsuarioAlteracao] ");
                    strGetAcoes.Append("         ,[DtAlteracao] ");
                    strGetAcoes.Append("   FROM [financ].[BensAcoes] where IdBem = @idBem");

                    return _db.Query<BensAcoes>(strGetAcoes.ToString(), new { IdBem = idBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensAcoesRepository", "GetListBensAcoesByIdBem");
                return null;
            }
        }

        public Domain.Entities._2BTrust.BensAcoes GetBensAcoesByIdBem(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetAcoes = new StringBuilder();
                    strGetAcoes.Append("  SELECT  [Id] ");
                    strGetAcoes.Append("         ,[IdBem] ");
                    strGetAcoes.Append("         ,[CodigoAcao] ");
                    strGetAcoes.Append("         ,[CotacaoAtual] ");
                    strGetAcoes.Append("         ,[QtdeCotas] ");
                    strGetAcoes.Append("         ,[IdUsuarioInclusao] ");
                    strGetAcoes.Append("         ,[DtInclusao] ");
                    strGetAcoes.Append("         ,[IdUsuarioAlteracao] ");
                    strGetAcoes.Append("         ,[DtAlteracao] ");
                    strGetAcoes.Append("   FROM [financ].[BensAcoes] where IdBem  = @IdBem");

                    return _db.Query<BensAcoes>(strGetAcoes.ToString(), new { IdBem = idBem }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensAcoesRepository", "GetBensAcoesByIdBem");
                return null;
            }
        }

        public Domain.Entities._2BTrust.BensAcoes GetBensAcoesById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetAcoes = new StringBuilder();
                    strGetAcoes.Append("  SELECT  [Id] ");
                    strGetAcoes.Append("         ,[IdBem] ");
                    strGetAcoes.Append("         ,[CodigoAcao] ");
                    strGetAcoes.Append("         ,[CotacaoAtual] ");
                    strGetAcoes.Append("         ,[QtdeCotas] ");
                    strGetAcoes.Append("         ,[IdUsuarioInclusao] ");
                    strGetAcoes.Append("         ,[DtInclusao] ");
                    strGetAcoes.Append("         ,[IdUsuarioAlteracao] ");
                    strGetAcoes.Append("         ,[DtAlteracao] ");
                    strGetAcoes.Append("   FROM [financ].[BensAcoes] where Id  = @id");

                    return _db.Query<BensAcoes>(strGetAcoes.ToString(), new { id = id }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensAcoesRepository", "GetBensAcoesById");
                return null;
            }
        }
    }
}
