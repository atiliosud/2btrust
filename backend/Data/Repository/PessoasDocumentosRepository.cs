﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;


namespace Data.Repository
{
    public class PessoasDocumentosRepository : BaseRepository, IPessoasDocumentosRepository
    {
        public IEnumerable<PessoasDocumentos> GetListPessoasDocumentosByIdPessoa(int idPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT PD.[Id] ,PD.[IdPessoa] ");
                    strGet.Append("      ,PD.[IdDocumento] ");
                    strGet.Append("      ,PD.[IdOrgaoEmissor] ");
                    strGet.Append("      ,PD.[Conteudo] ");
                    strGet.Append("      ,PD.[Descricao] ");
                    strGet.Append("      ,PD.[FlagAtivo] ");
                    strGet.Append(" 	  ,D.[Id] ");
                    strGet.Append("      ,D.[Codigo] ");
                    strGet.Append("      ,D.[Nome] ");
                    strGet.Append("      ,D.[IdUsuarioInclusao] ");
                    strGet.Append("      ,D.[DtInclusao] ");
                    strGet.Append("      ,D.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,D.[DtAlteracao] ");
                    strGet.Append(" 	  ,OE.[Id] ");
                    strGet.Append("      ,OE.[Codigo] ");
                    strGet.Append("      ,OE.[Nome] ");
                    strGet.Append("      ,OE.[IdEstado] ");
                    strGet.Append("      ,OE.[IdUsuarioInclusao] ");
                    strGet.Append("      ,OE.[DtInclusao] ");
                    strGet.Append("      ,OE.[IdUsuarioAlteracao] ");
                    strGet.Append("      ,OE.[DtAlteracao] ");
                    strGet.Append("  FROM [crm].[PessoasDocumentos]  PD ");
                    strGet.Append("  inner join [crm].[Documentos] D on D.Id = PD.IdDocumento ");
                    strGet.Append("  inner join [crm].[OrgaosEmissores] OE on OE.Id = PD.IdOrgaoEmissor ");
                    strGet.Append("  where  PD.[IdPessoa] = @IdPessoa and PD.FlagAtivo = 1");

                    return _db.Query<PessoasDocumentos, Documentos, OrgaosEmissores, PessoasDocumentos>(strGet.ToString(), (PD, D, OE) =>
                    {
                        PD._Documento = D;
                        PD._OrgaoEmissor = OE;
                        return PD;
                    }, new { IdPessoa = idPessoa }, splitOn: "Id").AsEnumerable();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasDocumentosRepository", "GetListPessoasDocumentosByIdPessoa");
                return null;
            }

        }

        public PessoasDocumentos InsertPessoasDocumentos(PessoasDocumentos pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [crm].[PessoasDocumentos] ");
                    strInsert.Append("    ([IdPessoa] ");
                    strInsert.Append("    ,[IdDocumento] ");
                    strInsert.Append("    ,[IdOrgaoEmissor] ");
                    strInsert.Append("    ,[Conteudo], Descricao, FlagAtivo) ");
                    strInsert.Append("     VALUES ");
                    strInsert.Append("   (@IdPessoa ");
                    strInsert.Append("   ,@IdDocumento ");
                    strInsert.Append("   ,@IdOrgaoEmissor ");
                    strInsert.Append("   ,@Conteudo, @Descricao, 1) SELECT IDENT_CURRENT('[crm].[PessoasDocumentos]') AS [IDENT_CURRENT]");

                    _db.Execute(strInsert.ToString(), new
                    {
                        IdPessoa = pes.IdPessoa,
                        IdDocumento = pes.IdDocumento,
                        IdOrgaoEmissor = pes.IdOrgaoEmissor,
                        Conteudo = pes.Conteudo,
                        Descricao = pes.Descricao
                    });

                    return pes;


                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasDocumentosRepository", "InsertPessoasDocumentos");
                return null;
            }
        }

        public PessoasDocumentos UpdatePessoasDocumentos(PessoasDocumentos pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [crm].[PessoasDocumentos] ");
                    strUpdate.Append("  SET [IdPessoa] = @IdPessoa ");
                    strUpdate.Append("     ,[IdDocumento] = @IdDocumento ");
                    strUpdate.Append("     ,[IdOrgaoEmissor] = @IdOrgaoEmissor ");
                    strUpdate.Append("     ,[Conteudo] = @Conteudo ");
                    strUpdate.Append("     ,[Descricao] = @Descricao ");
                    strUpdate.Append("     ,[FlagAtivo] = 1 ");
                    strUpdate.Append(" WHERE  Id = @id");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        id = pes.Id,
                        IdPessoa = pes.IdPessoa,
                        IdDocumento = pes.IdDocumento,
                        IdOrgaoEmissor = pes.IdOrgaoEmissor,
                        Conteudo = pes.Conteudo,
                        Descricao = pes.Descricao,
                    });

                    return pes;


                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasDocumentosRepository", "UpdatePessoasDocumentos");
                return null;
            }
        }



        public bool FlagAllDelete(int idPessoa)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    _db.Execute("update [crm].[PessoasDocumentos] set [FlagAtivo] =  0 where idPessoa = @idPessoa", new { idPessoa = idPessoa });
                }
                return true;
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasDocumentosRepository", "FlagAllDelete");
                return false;

            }
        }
    }
}