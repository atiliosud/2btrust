﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper; 



namespace Data.Repository
{
    public class PessoasFisicaRepository : BaseRepository, IPessoasFisicaRepository
    {

        public PessoasFisicas GetPessoaFisicaByIdPessoa(int idPessoa)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetPessoaFisica = new StringBuilder();

                    strGetPessoaFisica.Append(" SELECT [IdPessoa] ");
                    strGetPessoaFisica.Append("      ,[CPF]");
                    strGetPessoaFisica.Append("      ,[TaxId]");
                    strGetPessoaFisica.Append("      ,[ITIN]");
                    strGetPessoaFisica.Append("      ,[SocialNumber]");
                    strGetPessoaFisica.Append("      ,[Apelido]");

                    strGetPessoaFisica.Append("      ,[EmploymentStatus]");
                    strGetPessoaFisica.Append("      ,[Employer]");
                    strGetPessoaFisica.Append("      ,[NatureOfBusiness]"); 
                    strGetPessoaFisica.Append("      ,[Ocupation]");

                    strGetPessoaFisica.Append("      ,[IdUsuarioInclusao]");
                    strGetPessoaFisica.Append("      ,[DtInclusao]");
                    strGetPessoaFisica.Append("      ,[IdUsuarioAlteracao]");
                    strGetPessoaFisica.Append("      ,[DtAlteracao]");
                    strGetPessoaFisica.Append("  FROM [crm].[PessoasFisica] where IdPessoa =@IdPessoa");

                    return _db.Query<PessoasFisicas>(strGetPessoaFisica.ToString(), new { IdPessoa = idPessoa }).FirstOrDefault();



                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasFisicaRepository", "GetPessoaFisicaByIdPessoa");
                return null;
            }
        }

        public PessoasFisicas InsertPessoaFisica (PessoasFisicas pes)
        {
            try
            {

                using (var _db = new SqlConnection(urlConectionPrincipal))
                {

                    int iContadpr = _db.Query<int>("select Count(*) from [crm].[PessoasFisica]").FirstOrDefault();
                    if(iContadpr>0)
                    {

                    }

                    StringBuilder strInsertPessoaFisica = new StringBuilder();

                    strInsertPessoaFisica.Append(" INSERT INTO [crm].[PessoasFisica] ");
                    strInsertPessoaFisica.Append("  ([IdPessoa] ");
                    strInsertPessoaFisica.Append("  ,[CPF] ");
                    strInsertPessoaFisica.Append("  ,[TaxId]");
                    strInsertPessoaFisica.Append("  ,[ITIN]");
                    strInsertPessoaFisica.Append("  ,[SocialNumber]");
                    strInsertPessoaFisica.Append("  ,[Apelido] ");


                    strInsertPessoaFisica.Append("      ,[EmploymentStatus]");
                    strInsertPessoaFisica.Append("      ,[Employer]");
                    strInsertPessoaFisica.Append("      ,[NatureOfBusiness]");
                    strInsertPessoaFisica.Append("      ,[Ocupation]");

                    strInsertPessoaFisica.Append("  ,[IdUsuarioInclusao] ");
                    strInsertPessoaFisica.Append("  ,[DtInclusao] ");
                    strInsertPessoaFisica.Append("   ) ");
                    strInsertPessoaFisica.Append("    VALUES ");
                    strInsertPessoaFisica.Append("  (@IdPessoa");
                    strInsertPessoaFisica.Append("  ,@CPF");
                    strInsertPessoaFisica.Append("  ,@TaxId");
                    strInsertPessoaFisica.Append("  ,@ITIN");
                    strInsertPessoaFisica.Append("  ,@SocialNumber");
                    strInsertPessoaFisica.Append("  ,@Apelido");

                    strInsertPessoaFisica.Append("  ,@EmploymentStatus");
                    strInsertPessoaFisica.Append("  ,@Employer");
                    strInsertPessoaFisica.Append("  ,@NatureOfBusiness");
                    strInsertPessoaFisica.Append("  ,@Ocupation");

                    strInsertPessoaFisica.Append("  ,@IdUsuarioInclusao");
                    strInsertPessoaFisica.Append("  ,getDate()");
                    strInsertPessoaFisica.Append(" ) SELECT IDENT_CURRENT('[crm].[PessoasFisica]') AS [IDENT_CURRENT] ");

                    _db.Execute(strInsertPessoaFisica.ToString(), new
                    {
                        IdPessoa = pes.IdPessoa,
                        CPF = pes.CPF,
                        TaxId = pes.TaxId,
                        ITIN = pes.ITIN,
                        SocialNumber = pes.SocialNumber,
                        Apelido = pes.Apelido,


                        EmploymentStatus = pes.EmploymentStatus,
                        Employer = pes.Employer,
                        NatureOfBusiness = pes.NatureOfBusiness,
                        Ocupation = pes.Ocupation,

                        IdUsuarioInclusao = pes.IdUsuarioInclusao
                    });


                }

                return GetPessoaFisicaByIdPessoa(pes.IdPessoa);
            }
            catch (SqlException ex)
            {
                InsertLogError(ex, "PessoasFisicaRepository", "InsertPessoaFisicaByIdPessoa");
                return null;
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasFisicaRepository", "InsertPessoaFisicaByIdPessoa");
                return null;
            }
        }
                                                 
        public PessoasFisicas UpdatePessoaFisica (PessoasFisicas pes)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strUpdatePessoaFisica = new StringBuilder();

                    strUpdatePessoaFisica.Append(" UPDATE [crm].[PessoasFisica] ");
                    strUpdatePessoaFisica.Append("   SET  [CPF] =  @CPF");
                    strUpdatePessoaFisica.Append("      ,[TaxId] =  @TaxId");
                    strUpdatePessoaFisica.Append("      ,[ITIN] =  @ITIN");
                    strUpdatePessoaFisica.Append("      ,[SocialNumber] =  @SocialNumber");

                    strUpdatePessoaFisica.Append(" ,EmploymentStatus =  @EmploymentStatus");
                    strUpdatePessoaFisica.Append(" ,Employer = @Employer");
                    strUpdatePessoaFisica.Append(" ,NatureOfBusiness =  @NatureOfBusiness");
                    strUpdatePessoaFisica.Append(" ,Ocupation = @Ocupation");


                    strUpdatePessoaFisica.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao");
                    strUpdatePessoaFisica.Append("      ,[DtAlteracao] =getDate() ");
                    strUpdatePessoaFisica.Append(" WHERE IdPessoa = @IdPessoa");

                    _db.Execute(strUpdatePessoaFisica.ToString(), new
                    {

                        IdPessoa = pes.IdPessoa,
                        CPF = pes.CPF,
                        TaxId = pes.TaxId,
                        ITIN = pes.ITIN,
                        SocialNumber = pes.SocialNumber,

                        EmploymentStatus = pes.EmploymentStatus,
                        Employer = pes.Employer,
                        NatureOfBusiness = pes.NatureOfBusiness,
                        Ocupation = pes.Ocupation,

                        IdUsuarioAlteracao = pes.IdUsuarioAlteracao
                    });

                    return pes;

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasFisicaRepository", "InsertPessoaFisicaByIdPessoa");
                return null;
            }
        }

    }
}
