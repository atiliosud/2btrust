﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;


namespace Data.Repository
{
    public class LucratividadeRepository : BaseRepository, ILucratividadeRepository
    {
        public Domain.Entities._2BTrust.Lucratividade InsertLucratividade(Domain.Entities._2BTrust.Lucratividade luc)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strINsert = new StringBuilder();


                    decimal valueReceitaBruta = Convert.ToDecimal(luc.ReceitaBruta);

                    decimal valueValor = Convert.ToDecimal(luc.Valor);

                    decimal valueLucroLiquido = Convert.ToDecimal(luc.LucroLiquido); 


                    strINsert.Append(" INSERT INTO [financ].[Lucratividade] ");
                    strINsert.Append("    ([IdBem] ");
                    strINsert.Append("    ,[DtInicio] ");
                    strINsert.Append("    ,[DtFim] ");
                    strINsert.Append("    ,[LucroLiquido] ");
                    strINsert.Append("    ,[ReceitaBruta] ");
                    strINsert.Append("    ,[Valor] ");
                    strINsert.Append("    ,[IdUsuarioInclusao] ");
                    strINsert.Append("    ,[DtInclusao] ) ");
                    strINsert.Append(" VALUES ");
                    strINsert.Append("   (@IdBem ");
                    strINsert.Append("   ,@DtInicio ");
                    strINsert.Append("   ,@DtFim ");
                    strINsert.Append("   ,@LucroLiquido ");
                    strINsert.Append("   ,@ReceitaBruta ");
                    strINsert.Append("   ,@Valor ");
                    strINsert.Append("   ,@IdUsuarioInclusao ");
                    strINsert.Append("   ,getDate()) ");
                    strINsert.Append("  SELECT IDENT_CURRENT('[financ].[Lucratividade]') AS [IDENT_CURRENT]");


                    var idLucratividade = _db.Query<int>(strINsert.ToString(), new
                    {
                        IdBem = luc.IdBem,
                        DtInicio = luc.DtInicio,
                        DtFim = luc.DtFim,
                        LucroLiquido = valueLucroLiquido,
                        ReceitaBruta = valueReceitaBruta,
                        Valor = valueValor,
                        IdUsuarioInclusao = luc.IdUsuarioInclusao

                    }).FirstOrDefault();


                    luc.Id = idLucratividade;
                    return luc;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LucratividadeRepository", "InsertLucratividade");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Lucratividade UpdateLucratividade(Domain.Entities._2BTrust.Lucratividade luc)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    decimal valueReceitaBruta = Convert.ToDecimal(luc.ReceitaBruta);

                    decimal valueValor = Convert.ToDecimal(luc.Valor);

                    decimal valueLucroLiquido = Convert.ToDecimal(luc.LucroLiquido); 


                    strUpdate.Append(" UPDATE [financ].[Lucratividade] ");
                    strUpdate.Append("   SET [IdBem] = @IdBem ");
                    strUpdate.Append("      ,[DtInicio] =  @DtInicio ");
                    strUpdate.Append("      ,[DtFim] =  @DtFim ");
                    strUpdate.Append("      ,[LucroLiquido] =  @LucroLiquido ");
                    strUpdate.Append("      ,[ReceitaBruta] =  @ReceitaBruta ");
                    strUpdate.Append("      ,[Valor] =  @Valor ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] =  @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate() ");
                    strUpdate.Append(" WHERE Id = @id");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        Id = luc.Id,
                        IdBem = luc.IdBem,
                        DtInicio = luc.DtInicio,
                        DtFim = luc.DtFim,
                        LucroLiquido = valueLucroLiquido,
                        ReceitaBruta = valueReceitaBruta,
                        Valor = valueValor,
                        IdUsuarioAlteracao = luc.IdUsuarioAlteracao
                    });

                    return luc;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LucratividadeRepository", "UpdateLucratividade");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Lucratividade GetLucratividadeByIdBem(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder stGet = new StringBuilder();

                    stGet.Append(" Select ");
                    stGet.Append("       [Id] ");
                    stGet.Append("      ,[IdBem] ");
                    stGet.Append("      ,[DtInicio]  ");
                    stGet.Append("      ,[DtFim]  ");
                    stGet.Append("      ,[LucroLiquido]  ");
                    stGet.Append("      ,[ReceitaBruta]  ");
                    stGet.Append("      ,[Valor]  ");
                    stGet.Append("      ,[IdUsuarioAlteracao]  ");
                    stGet.Append("      ,[DtAlteracao]  ");
                    stGet.Append("      ,[IdUsuarioInclusao]  ");
                    stGet.Append("      ,[DtInclusao]  from  [financ].[Lucratividade] ");
                    stGet.Append(" WHERE IdBem = @IdBem");

                    return _db.Query<Lucratividade>(stGet.ToString(), new { IdBem = idBem }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LucratividadeRepository", "GetLucratividadeByIdBem");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Lucratividade GetLucratividadeById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder stGet = new StringBuilder();

                    stGet.Append(" Select ");
                    stGet.Append("       [Id] ");
                    stGet.Append("      ,[IdBem] ");
                    stGet.Append("      ,[DtInicio]  ");
                    stGet.Append("      ,[DtFim]  ");
                    stGet.Append("      ,[LucroLiquido]  ");
                    stGet.Append("      ,[ReceitaBruta]  ");
                    stGet.Append("      ,[Valor]  ");
                    stGet.Append("      ,[IdUsuarioAlteracao]  ");
                    stGet.Append("      ,[DtAlteracao]  ");
                    stGet.Append("      ,[IdUsuarioInclusao]  ");
                    stGet.Append("      ,[DtInclusao]  from  [financ].[Lucratividade] ");
                    stGet.Append(" WHERE Id = @Id");

                    return _db.Query<Lucratividade>(stGet.ToString(), new { Id = id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LucratividadeRepository", "GetLucratividadeByIdBem");

                return null;
            }
        }


        public IEnumerable<Lucratividade> GetListLucratividadeByIdBem(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder stGet = new StringBuilder();

                    stGet.Append(" Select ");
                    stGet.Append("       [Id] ");
                    stGet.Append("      ,[IdBem] ");
                    stGet.Append("      ,[DtInicio]  ");
                    stGet.Append("      ,[DtFim]  ");
                    stGet.Append("      ,[LucroLiquido]  ");
                    stGet.Append("      ,[ReceitaBruta]  ");
                    stGet.Append("      ,[Valor]  ");
                    stGet.Append("      ,[IdUsuarioAlteracao]  ");
                    stGet.Append("      ,[DtAlteracao]  ");
                    stGet.Append("      ,[IdUsuarioInclusao]  ");
                    stGet.Append("      ,[DtInclusao]  from  [financ].[Lucratividade] ");
                    stGet.Append(" WHERE IdBem = @IdBem");

                    return _db.Query<Lucratividade>(stGet.ToString(), new { IdBem = idBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LucratividadeRepository", "GetListLucratividadeByIdBem");

                return null;
            }
        }
    }
}
