﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;


namespace Data.Repository
{
    public class AreasRepository : BaseRepository, IAreasRepository
    {

        public IEnumerable<Areas> GetListAreas()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT [Id] ");
                     strGet.Append("     ,[Codigo] ");
                     strGet.Append("     ,[Nome] ");
                     strGet.Append("     ,[IdUsuarioInclusao] ");
                     strGet.Append("     ,[DtInclusao] ");
                     strGet.Append("     ,[IdUsuarioAlteracao] ");
                     strGet.Append("     ,[DtAlteracao] ");
                     strGet.Append("     ,[FlgAtivo] ");
                     strGet.Append(" FROM [user].[Areas] order by Nome ");

                    return _db.Query<Areas>(strGet.ToString()).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "GetListAreas", "AreasRepository");
                return null;
            }
        }

        public Areas GetAreasById  (int idArea)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("     ,[Codigo] ");
                    strGet.Append("     ,[Nome] ");
                    strGet.Append("     ,[IdUsuarioInclusao] ");
                    strGet.Append("     ,[DtInclusao] ");
                    strGet.Append("     ,[IdUsuarioAlteracao] ");
                    strGet.Append("     ,[DtAlteracao] ");
                    strGet.Append("     ,[FlgAtivo] ");
                    strGet.Append(" FROM [user].[Areas] where Id = @idArea");

                    return _db.Query<Areas>(strGet.ToString(), new { idArea = idArea }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "GetAreasById", "AreasRepository");
                return null;
            }
        }

        public Areas InsertAreas    (Areas area)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO [user].[Areas] ");
                    strInsert.Append("  ([Codigo] ");
                    strInsert.Append("  ,[Nome] ");
                    strInsert.Append("  ,[IdUsuarioInclusao] ");
                    strInsert.Append("  ,[DtInclusao] "); 
                    strInsert.Append("  ,[FlgAtivo]) ");
                    strInsert.Append("    VALUES ");
                    strInsert.Append("  (@Codigo ");
                    strInsert.Append("  ,@Nome ");
                    strInsert.Append("  ,@IdUsuarioInclusao  ");
                    strInsert.Append("  ,getDate()  ");
                    strInsert.Append("  ,1)  SELECT IDENT_CURRENT('[user].[Areas]') AS [IDENT_CURRENT]");


                    var idAreas = _db.Query<int>(strInsert.ToString(),
                        new
                        {
                            Codigo = area.Codigo,
                            Nome = area.Nome,
                            IdUsuarioInclusao = area.IdUsuarioInclusao

                        }).FirstOrDefault();

                    area.Id = idAreas;

                    return area;

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "InsertAreas", "AreasRepository");
                return null;
            }
        }
         
        public Areas UpdateAreas(Areas area)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [user].[Areas] ");
                    strUpdate.Append("   SET [Codigo] =@Codigo ");
                    strUpdate.Append("      ,[Nome] = @Nome ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate() ");
                    strUpdate.Append("      ,[FlgAtivo] = @FlgAtivo ");
                    strUpdate.Append(" WHERE   Id = @Id");

                    _db.Execute(strUpdate.ToString(), new
                    { 
                        Codigo = area.Codigo,
                        Nome = area.Nome,
                        IdUsuarioAlteracao = area.IdUsuarioAlteracao ,
                        FlgAtivo = area.FlgAtivo,
                        Id = area.Id
                    });

                    return area;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UpdateAreas", "AreasRepository");
                return null;
            }
        }
    }
}
