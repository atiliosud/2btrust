﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using Domain.Entities._2BTrust;


namespace Data.Repository
{
    public class BensEnderecoRepository : BaseRepository, IBensEnderecoRepository
    {
        public Domain.Entities._2BTrust.BensEnderecos InsertBensEnderecos(Domain.Entities._2BTrust.BensEnderecos bem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                { 

                    StringBuilder strInsertEndereco = new StringBuilder();
                    strInsertEndereco.Append(" INSERT INTO [financ].[BensEnderecos] ");
                    strInsertEndereco.Append("         ([IdBem] ");
                    strInsertEndereco.Append("         ,[Cidade] ");
                    strInsertEndereco.Append("         ,[Estado] ");
                    strInsertEndereco.Append("         ,[Bairro] ");
                    strInsertEndereco.Append("         ,[Logradouro] ");
                    strInsertEndereco.Append("         ,[Numero] ");
                    strInsertEndereco.Append("         ,[Complemento] ");
                    strInsertEndereco.Append("         ,[Cep] ");
                    strInsertEndereco.Append("         ,[IdUsuarioInclusao] ");
                    strInsertEndereco.Append("         ,[DtInclusao] ");
                    strInsertEndereco.Append("         ) ");
                    strInsertEndereco.Append("   VALUES ");
                    strInsertEndereco.Append("         (@IdBem ");
                    strInsertEndereco.Append("         ,@Cidade ");
                    strInsertEndereco.Append("         ,@Estado ");
                    strInsertEndereco.Append("         ,@Bairro ");
                    strInsertEndereco.Append("         ,@Logradouro  ");
                    strInsertEndereco.Append("         ,@Numero ");
                    strInsertEndereco.Append("         ,@Complemento  ");
                    strInsertEndereco.Append("         ,@Cep ");
                    strInsertEndereco.Append("         ,@IdUsuarioInclusao  ");
                    strInsertEndereco.Append("         ,getDate() ");
                    strInsertEndereco.Append("         ) SELECT IDENT_CURRENT('[financ].[BensEnderecos]') AS [IDENT_CURRENT]");

                    var idBemEndereco = _db.Query<int>(strInsertEndereco.ToString(), new
                    {
                        IdBem = bem.IdBem,
                        Estado = bem.Estado,
                        Cidade = bem.Cidade,
                        Bairro = bem.Bairro,
                        Logradouro = bem.Logradouro,
                        Numero = bem.Numero,
                        Complemento = bem.Complemento,
                        Cep = bem.Cep,
                        IdUsuarioInclusao = bem.IdUsuarioInclusao
                    }).FirstOrDefault();


                    bem.Id = idBemEndereco;

                    return bem;


                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensEnderecoRepository", "InsertBensEnderecos");
                return null;
            }
        }

        public Domain.Entities._2BTrust.BensEnderecos UpdateBensEnderecos(Domain.Entities._2BTrust.BensEnderecos bem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                { 
 
                    StringBuilder strUpdateEndereco = new StringBuilder();
                    strUpdateEndereco.Append(" UPDATE [financ].[BensEnderecos] ");
                    strUpdateEndereco.Append("   SET [IdBem] = @IdBem ");
                    strUpdateEndereco.Append("  ,[Bairro] = @Bairro ");
                    strUpdateEndereco.Append("  ,[Cidade] = @Cidade ");
                    strUpdateEndereco.Append("  ,[Estado] = @Estado ");
                    strUpdateEndereco.Append("  ,[Logradouro] = @Logradouro ");
                    strUpdateEndereco.Append("  ,[Numero] = @Numero ");
                    strUpdateEndereco.Append("  ,[Complemento] =@Complemento  ");
                    strUpdateEndereco.Append("  ,[Cep] =@Cep");
                    strUpdateEndereco.Append("  ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdateEndereco.Append("  ,[DtAlteracao] = getDate() ");
                    strUpdateEndereco.Append(" WHERE Id = @IdBemEndereco ");

                    _db.Execute(strUpdateEndereco.ToString(), new
                   {
                       IdBem = bem.IdBem,
                       Bairro = bem.Bairro,
                       Estado = bem.Estado,
                       Cidade = bem.Cidade,
                       Logradouro = bem.Logradouro,
                       Numero = bem.Numero,
                       Complemento = bem.Complemento,
                       Cep = bem.Cep,
                       IdUsuarioAlteracao = bem.IdUsuarioAlteracao,
                       IdBemEndereco = bem.Id
                   });

                    return bem;

                

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensEnderecoRepository", "UpdateBensEnderecos");
                return null;
            }
        }

        public Domain.Entities._2BTrust.BensEnderecos GetBensEnderecosByIdEndereco(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetEndereco = new StringBuilder();
                    strGetEndereco.Append("  SELECT [Id] ");
                    strGetEndereco.Append("   ,[IdBem] ");
                    strGetEndereco.Append("   ,[IdTpEndereco] ");
                    strGetEndereco.Append("   ,[IdCidade] ");
                    strGetEndereco.Append("   ,[Bairro] ");
                    strGetEndereco.Append("   ,[Cidade] ");
                    strGetEndereco.Append("   ,[Estado] ");
                    strGetEndereco.Append("   ,[Logradouro] ");
                    strGetEndereco.Append("   ,[Numero] ");
                    strGetEndereco.Append("   ,[Complemento] ");
                    strGetEndereco.Append("   ,[Cep] ");
                    strGetEndereco.Append("   ,[IdUsuarioInclusao] ");
                    strGetEndereco.Append("   ,[DtInclusao] ");
                    strGetEndereco.Append("   ,[IdUsuarioAlteracao] ");
                    strGetEndereco.Append("   ,[DtAlteracao] ");
                    strGetEndereco.Append("   FROM [financ].[BensEnderecos] where IdBem  = @IdBem");

                    return _db.Query<BensEnderecos>(strGetEndereco.ToString(), new { IdBem = idBem }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensEnderecoRepository", "GetBensEnderecosByIdEndereco");
                return null;
            }
        }

        public Domain.Entities._2BTrust.BensEnderecos GetBensEnderecosById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetEndereco = new StringBuilder();
                    strGetEndereco.Append("  SELECT [Id] ");
                    strGetEndereco.Append("   ,[IdBem] ");
                    strGetEndereco.Append("   ,[IdTpEndereco] ");
                    strGetEndereco.Append("   ,[IdCidade] ");
                    strGetEndereco.Append("   ,[Bairro] ");
                    strGetEndereco.Append("   ,[Cidade] ");
                    strGetEndereco.Append("   ,[Estado] ");
                    strGetEndereco.Append("   ,[Logradouro] ");
                    strGetEndereco.Append("   ,[Numero] ");
                    strGetEndereco.Append("   ,[Complemento] ");
                    strGetEndereco.Append("   ,[Cep] ");
                    strGetEndereco.Append("   ,[IdUsuarioInclusao] ");
                    strGetEndereco.Append("   ,[DtInclusao] ");
                    strGetEndereco.Append("   ,[IdUsuarioAlteracao] ");
                    strGetEndereco.Append("   ,[DtAlteracao] ");
                    strGetEndereco.Append("   FROM [financ].[BensEnderecos] where id  = @id");

                    return _db.Query<BensEnderecos>(strGetEndereco.ToString(), new { id = id }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensEnderecoRepository", "GetBensEnderecosById");
                return null;
            }
        }
    }
}
