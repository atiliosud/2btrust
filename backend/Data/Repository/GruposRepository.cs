﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Data.Repository
{
    public class GruposRepository : BaseRepository, IGruposRepository
    {
        public Grupos GetGrupoByIdUsuario(int idUsuario)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetGrupos = new StringBuilder();
                    strGetGrupos.Append(" SELECT G.[Id] ");
                    strGetGrupos.Append("      ,G.[Codigo] ");
                    strGetGrupos.Append("      ,G.[Nome] ");
                    strGetGrupos.Append("      ,G.[IdUsuarioInclusao] ");
                    strGetGrupos.Append("      ,G.[DtInclusao] ");
                    strGetGrupos.Append("      ,G.[IdUsuarioAlteracao] ");
                    strGetGrupos.Append("      ,G.[DtAlteracao] ");
                    strGetGrupos.Append("  FROM [user].[Grupos] G ");
                    strGetGrupos.Append("  inner join  [user].[UsuariosGrupos] UG on G.[Id] = UG.[IdGrupo] ");
                    strGetGrupos.Append("  where UG.[IdUsuario] =  " + idUsuario);

                    return _db.Query<Grupos>(strGetGrupos.ToString()).FirstOrDefault();


                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
