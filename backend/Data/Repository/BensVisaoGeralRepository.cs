﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;

namespace Data.Repository
{
    public class BensVisaoGeralRepository : BaseRepository, IBensVisaoGeralRepository
    {
        public BensVisaoGeral GetBensVisaoGeralById(int id)
        {
            try
            {
                BensVisaoGeral retorno = new BensVisaoGeral();
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("       ,[Nome] ");
                    strGet.Append("       ,[Descritivo] ");
                    strGet.Append("       ,[IdStatus] ");
                    strGet.Append("       ,[IdUsuarioInclusao] ");
                    strGet.Append("       ,[DtInclusao] ");
                    strGet.Append("       ,[IdUsuarioAlteracao] ");
                    strGet.Append("       ,[DtAlteracao] ");
                    strGet.Append(" FROM [financ].[BensVisaoGeral] where Id =@id");

                    retorno = _db.Query<BensVisaoGeral>(strGet.ToString(), new { id = id }).FirstOrDefault();
                    retorno._Imagens = GetListImagens(retorno.Id);
                    return retorno;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensVisaoGeralRepository", "GetBensVisaoGeralById");
                return null;
            }
        }

        public IEnumerable<BensVisaoGeral> GetListBensVisaoGeral()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append(" SELECT [Id] ");
                    strGet.Append("       ,[Nome] ");
                    strGet.Append("       ,[Descritivo] ");
                    strGet.Append("       ,[IdStatus] ");
                    strGet.Append("       ,[IdUsuarioInclusao] ");
                    strGet.Append("       ,[DtInclusao] ");
                    strGet.Append("       ,[IdUsuarioAlteracao] ");
                    strGet.Append("       ,[DtAlteracao] ");
                    strGet.Append(" FROM  [financ].[BensVisaoGeral] ");

                    var listBensVisaoGeral = _db.Query<BensVisaoGeral>(strGet.ToString()).AsEnumerable();

                    foreach (BensVisaoGeral item in listBensVisaoGeral)
                    {
                        item._Imagens = GetListImagens(item.Id);
                    }

                    return listBensVisaoGeral;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensVisaoGeralRepository", "GetListBensVisaoGeral");
                return null;
            }
        }

        public IEnumerable<BensVisaoGeralImagens> GetListImagens(int idBemVisaoGeral)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetImanges = new StringBuilder();
                    strGetImanges.Append(" SELECT [Id] ");
                    strGetImanges.Append("      ,[IdBemVisaoGeral] ");
                    strGetImanges.Append("      ,[PathLocation] ");
                    strGetImanges.Append("      ,[PathLocationIIS] ");
                    strGetImanges.Append("      ,[FlagAtivo] ");
                    strGetImanges.Append("      ,[IdUsuarioInclusao] ");
                    strGetImanges.Append("      ,[DtInclusao] ");
                    strGetImanges.Append("      ,[IdUsuarioAlteracao] ");
                    strGetImanges.Append("      ,[DtAlteracao] ");
                    strGetImanges.Append("  FROM [financ].[BensVisaoGeralImagem] where IdBemVisaoGeral = @idBemVisaoGeral and FlagAtivo =1");

                    return _db.Query<BensVisaoGeralImagens>(strGetImanges.ToString(), new { idBemVisaoGeral = idBemVisaoGeral }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensVisaoGeralRepository", "GetListImagens");
                return null;
            }
        }

        public BensVisaoGeral InsertBensVisaoGeral(BensVisaoGeral bemVisaoGeral)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append("  INSERT INTO [financ].[BensVisaoGeral] ");
                    strInsert.Append("   ([Nome] ");
                    strInsert.Append("  ,[Descritivo] ");
                    strInsert.Append("  ,[IdStatus] ");
                    strInsert.Append("  ,[IdUsuarioInclusao] ");
                    strInsert.Append("  ,[DtInclusao] ) ");
                    strInsert.Append("      VALUES ");
                    strInsert.Append("   (@Nome ");
                    strInsert.Append("   ,@Descritivo ");
                    strInsert.Append("   ,@IdStatus ");
                    strInsert.Append("   ,@IdUsuarioInclusao ");
                    strInsert.Append("   ,getDate())  SELECT IDENT_CURRENT('[financ].[BensVisaoGeral]') AS [IDENT_CURRENT]");

                    var idBemVisaoGeral = _db.Query<int>(strInsert.ToString(), new
                    {
                        Nome = bemVisaoGeral.Nome,
                        Descritivo = bemVisaoGeral.Descritivo,
                        IdStatus = bemVisaoGeral.IdStatus,
                        IdUsuarioInclusao = bemVisaoGeral.IdUsuarioInclusao
                    }).FirstOrDefault();

                    bemVisaoGeral.Id = idBemVisaoGeral;

                    if (bemVisaoGeral._Imagens != null)
                    {
                        if (bemVisaoGeral._Imagens.Count() > 0)
                        {
                            foreach (var item in bemVisaoGeral._Imagens)
                            {
                                item.PathLocation = item.PathLocation.Replace("BemVisaoGeralImagem_0", "BemVisaoGeralImagem_" + bemVisaoGeral.Id.ToString());
                                item.PathLocationIIS = item.PathLocationIIS.Replace("BemVisaoGeralImagem_0", "BemVisaoGeralImagem_" + bemVisaoGeral.Id.ToString());
                                item.IdBemVisaoGeral = idBemVisaoGeral;
                                InsertImagens(item);
                            }
                        }
                    }
                    return bemVisaoGeral;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensVisaoGeralRepository", "InsertBensVisaoGeral");
                return null;
            }
        }

        public BensVisaoGeral UpdateBensVisaoGeral(BensVisaoGeral bem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" UPDATE [financ].[BensVisaoGeral] ");
                    strUpdate.Append("    SET [Nome] = @Nome ");
                    strUpdate.Append("       ,[Descritivo] = @Descritivo ");
                    strUpdate.Append("       ,[IdStatus] = @IdStatus ");
                    strUpdate.Append("       ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("       ,[DtAlteracao] = getDate() ");
                    strUpdate.Append("  WHERE Id = @id ");
                    _db.Execute(strUpdate.ToString(), new
                    {
                        Nome = bem.Nome,
                        Descritivo = bem.Descritivo,
                        IdStatus = bem.IdStatus,
                        IdUsuarioAlteracao = bem.IdUsuarioAlteracao,
                        id = bem.Id
                    });

                    if (bem._Imagens.Count() > 0)
                    {
                        foreach (var item in bem._Imagens)
                        {
                            if (item.Id == 0)
                                InsertImagens(item);
                            else
                                UpdateImagens(item);
                        }
                    }
                    return bem;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensVisaoGeralRepository", "UpdateBensVisaoGeral");
                return null;
            }
        }

        public BensVisaoGeralImagens InsertImagens(BensVisaoGeralImagens bemImagem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO [financ].[BensVisaoGeralImagem] ");
                    strInsert.Append("        ( [IdBemVisaoGeral] ");
                    strInsert.Append("        ,[PathLocation] ");
                    strInsert.Append("        ,[PathLocationIIS] ");
                    strInsert.Append("        ,[FlagAtivo] ");
                    strInsert.Append("        ,[IdUsuarioInclusao] ");
                    strInsert.Append("        ,[Descritivo] ");
                    strInsert.Append("        ,[DtInclusao] ) ");
                    strInsert.Append("  VALUES ");
                    strInsert.Append("        (@IdBemVisaoGeral ");
                    strInsert.Append("        ,@PathLocation ");
                    strInsert.Append("        ,@PathLocationIIS ");
                    strInsert.Append("        ,@FlagAtivo ");
                    strInsert.Append("        ,@IdUsuarioInclusao ");
                    strInsert.Append("        ,@Descritivo ");
                    strInsert.Append("        ,getDate() )  SELECT IDENT_CURRENT('[financ].[BensVisaoGeralImagem]') AS [IDENT_CURRENT]");

                    var idbemImagem = _db.Query<int>(strInsert.ToString(), new
                    {
                        IdBemVisaoGeral = bemImagem.IdBemVisaoGeral,
                        PathLocation = bemImagem.PathLocation,
                        PathLocationIIS = bemImagem.PathLocationIIS,
                        FlagAtivo = bemImagem.FlagAtivo,
                        IdUsuarioInclusao = bemImagem.IdUsuarioInclusao,
                        Descritivo = bemImagem.Descritivo,
                    }).FirstOrDefault();

                    bemImagem.Id = idbemImagem;

                    return bemImagem;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensVisaoGeralRepository", "InsertImagens");
                return null;
            }
        }

        public BensVisaoGeralImagens UpdateImagens(BensVisaoGeralImagens bemImagem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE [financ].[BensVisaoGeralImagem] ");
                    strUpdate.Append("   SET [IdBemVisaoGeral] = @IdBemVisaoGeral ");
                    strUpdate.Append("      ,[PathLocation] = @PathLocation ");
                    strUpdate.Append("      ,[PathLocationIIS] = @PathLocationIIS ");
                    strUpdate.Append("      ,[FlagAtivo] = @FlagAtivo ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[Descritivo] = @Descritivo  ");
                    strUpdate.Append("      ,[DtAlteracao] =  getDate()");
                    strUpdate.Append(" WHERE Id = @id");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        id = bemImagem.Id,
                        IdBemVisaoGeral = bemImagem.IdBemVisaoGeral,
                        PathLocation = bemImagem.PathLocation,
                        PathLocationIIS = bemImagem.PathLocationIIS,
                        FlagAtivo = bemImagem.FlagAtivo,
                        IdUsuarioAlteracao = bemImagem.IdUsuarioAlteracao,
                        Descritivo = bemImagem.Descritivo,
                    });

                    return bemImagem;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BensVisaoGeralRepository", "UpdateImagens");
                return null;
            }
        }
    }
}
