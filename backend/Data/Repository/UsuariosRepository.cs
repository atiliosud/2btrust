﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;

namespace Data.Repository
{
    public class UsuariosRepository : BaseRepository, IUsuariosRepository
    {
        public Domain.Entities._2BTrust.Usuarios LoginUsuario(string sLogin, string sSenha)
        {
            throw new NotImplementedException();
        }


        public Domain.Entities._2BTrust.Usuarios GetUsuarioById(int idUsuario)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetUsuario = new StringBuilder();
                    strGetUsuario.Append(" SELECT [Id] ");
                    strGetUsuario.Append("     ,[Nome] ");
                    strGetUsuario.Append("     ,[Email] ");
                    strGetUsuario.Append("     ,[Admin] ");
                    strGetUsuario.Append("     ,[Bloqueado] ");
                    strGetUsuario.Append("     ,[Saltkey] ");
                    strGetUsuario.Append("     ,[IdStatus] ");
                    strGetUsuario.Append("     ,[IdUsuarioInclusao] ");
                    strGetUsuario.Append("     ,[DtInclusao] ");
                    strGetUsuario.Append("     ,[IdUsuarioAlteracao] ");
                    strGetUsuario.Append("     ,[DtAlteracao] ");
                    strGetUsuario.Append("     ,[IdPessoa] "); 
                    strGetUsuario.Append("     ,[CadastroValido] ");
                    strGetUsuario.Append(" FROM [user].[Usuarios] where Id = " + idUsuario);

                    return _db.Query<Usuarios>(strGetUsuario.ToString()).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UsuariosRepository", "GetUsuarioById");

                return null;
            }
        }


        public Usuarios InsertUsuario(Usuarios user)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [user].[Usuarios] ");
                    strInsert.Append("      ([Nome] ");
                    strInsert.Append("      ,[Email] ");
                    strInsert.Append("      ,[Admin] ");
                    strInsert.Append("      ,[Bloqueado] ");
                    strInsert.Append("      ,[Saltkey] ");
                    strInsert.Append("      ,[IdStatus] ");
                    strInsert.Append("      ,[IdUsuarioInclusao] ");
                    strInsert.Append("      ,[DtInclusao] ");
                    strInsert.Append("      ,[IdPessoa]) ");
                    strInsert.Append("   VALUES ");
                    strInsert.Append("     (@Nome");
                    strInsert.Append("     ,@Email");
                    strInsert.Append("     ,@Admin");
                    strInsert.Append("     ,@Bloqueado");
                    strInsert.Append("     ,@Saltkey");
                    strInsert.Append("     ,@IdStatus");
                    strInsert.Append("     ,@IdUsuarioInclusao");
                    strInsert.Append("     ,getDate() ");
                    strInsert.Append("    ,@IdPessoa)   SELECT IDENT_CURRENT('[user].[Usuarios]') AS [IDENT_CURRENT]");



                    var idUsuario = _db.Query<int>(strInsert.ToString(), new
                    {

                        Nome = "",
                        Email = user.Email,
                        Admin = user.Admin,
                        Bloqueado = user.Bloqueado,
                        Saltkey = user.Saltkey,
                        IdStatus = user.IdStatus,
                        IdUsuarioInclusao = user.IdUsuarioInclusao,
                        IdPessoa = user.IdPessoa

                    }).FirstOrDefault();

                    user.Id = idUsuario;
                    return user;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UsuariosRepository", "InsertUsuario");

                return null;
            }
        }

        public int LiberacaoViaEmail(string saltKey)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strGetUsuario = new StringBuilder();
                    strGetUsuario.Append("select [IdPessoa] from [user].[Usuarios] where [Saltkey] = @SaltKey ");
                    var idPessoa = _db.Query<int>(strGetUsuario.ToString(), new { SaltKey = saltKey }).FirstOrDefault();

                    string updateLiberacao = "update [user].[Usuarios]  set [Bloqueado] = 0 where [Saltkey] = @saltKey";
                    _db.Execute(updateLiberacao, new { saltKey = saltKey });

                    return idPessoa;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UsuariosRepository", "LiberacaoViaEmail");

                return 0;
            }
        }


        public Usuarios UpdateUsuario(Usuarios user)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();
                    strUpdate.Append(" UPDATE [user].[Usuarios] ");
                    strUpdate.Append("   SET [Nome] = @Nome ");
                    strUpdate.Append("      ,[Email] = @Email ");
                    strUpdate.Append("      ,[Admin] = @Admin ");
                    strUpdate.Append("      ,[Bloqueado] = @Bloqueado ");
                    strUpdate.Append("      ,[Saltkey] = @Saltkey ");
                    strUpdate.Append("      ,[IdStatus] = @IdStatus ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate()");
                    strUpdate.Append(" WHERE   IdPessoa = @idPessoa");


                    _db.Execute(strUpdate.ToString(), new
                    {

                        Nome = "",
                        Email = user.Email,
                        Admin = user.Admin,
                        Bloqueado = user.Bloqueado,
                        Saltkey = user.Saltkey,
                        IdStatus = user.IdStatus,
                        IdUsuarioAlteracao = user.IdUsuarioAlteracao,
                        idPessoa = user.IdPessoa

                    });


                    return user;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UsuariosRepository", "UpdateUsuario");

                return null;
            }
        }


        public Usuarios GetUsuarioByIdPessoa(int idPessoa)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetUsuario = new StringBuilder();
                    strGetUsuario.Append(" SELECT [Id] ");
                    strGetUsuario.Append("     ,[Nome] ");
                    strGetUsuario.Append("     ,[Email] ");
                    strGetUsuario.Append("     ,[Admin] ");
                    strGetUsuario.Append("     ,[Bloqueado] ");
                    strGetUsuario.Append("     ,[Saltkey] ");
                    strGetUsuario.Append("     ,[IdStatus] ");
                    strGetUsuario.Append("     ,[IdUsuarioInclusao] ");
                    strGetUsuario.Append("     ,[DtInclusao] ");
                    strGetUsuario.Append("     ,[IdUsuarioAlteracao] ");
                    strGetUsuario.Append("     ,[DtAlteracao] ");
                    strGetUsuario.Append("     ,[IdPessoa] ");
                    strGetUsuario.Append(" FROM [user].[Usuarios] where IdPessoa = " + idPessoa);

                    return _db.Query<Usuarios>(strGetUsuario.ToString()).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UsuariosRepository", "GetUsuarioById");

                return null;
            }
        }


        public bool ValidaUsuarioPlataforma(int idUsuario, bool isValid)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strValida = new StringBuilder();
                    if(isValid)
                    strValida.Append("Update [user].[Usuarios] set CadastroValido = @isValid, bloqueado = 0  where Id = @idUsuario");
                    else
                        strValida.Append("Update [user].[Usuarios] set CadastroValido = @isValid, bloqueado = 1 where Id = @idUsuario");

                    _db.Execute(strValida.ToString(), new { isValid = isValid, idUsuario = idUsuario });

                    return true;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UsuariosRepository", "ValidaUsuarioPlataforma");

                return false;
            }
        }


    }
}
