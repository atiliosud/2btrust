﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Data.Repository
{
    public class LoginsRepository : BaseRepository, ILoginsRepository
    {

        public Logins LoginUsuario(string sLogin, string sSenha)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strLogin = new StringBuilder();

                    strLogin.Append(" SELECT [IdUsuario] ");
                    strLogin.Append("      ,[Login]");
                    strLogin.Append("      ,[Senha]");
                    strLogin.Append("      ,[Bloqueado]");
                    strLogin.Append("      ,[Tentativas]");
                    strLogin.Append("      ,[IdUsuarioInclusao]");
                    strLogin.Append("      ,[DtInclusao]");
                    strLogin.Append("      ,[IdUsuarioAlteracao]");
                    strLogin.Append("      ,[DtAlteracao]");
                    strLogin.Append(" FROM [user].[Logins] where Login = '" + sLogin + "' and Senha='" + sSenha + "'");

                    return _db.Query<Logins>(strLogin.ToString()).FirstOrDefault();
                }


            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LoginsRepository", "LoginUsuario");
                return null;
            }
        }

        public Logins Insertlogins(Logins login)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [user].[Logins] ");
                    strInsert.Append("         ([IdUsuario] ");
                    strInsert.Append("         ,[Login] ");
                    strInsert.Append("         ,[Senha] ");
                    strInsert.Append("         ,[Bloqueado] ");
                    strInsert.Append("         ,[Tentativas] ");
                    strInsert.Append("         ,[IdUsuarioInclusao] ");
                    strInsert.Append("         ,[DtInclusao] ) ");
                    strInsert.Append("   VALUES ");
                    strInsert.Append("         (@IdUsuario");
                    strInsert.Append("         ,@Login");
                    strInsert.Append("         ,@Senha");
                    strInsert.Append("         ,@Bloqueado ");
                    strInsert.Append("         ,0 ");
                    strInsert.Append("         ,@IdUsuarioInclusao");
                    strInsert.Append("         ,getDate())");

                    var idLogin = _db.Execute(strInsert.ToString(), new
                    {
                        IdUsuario = login.IdUsuario,
                        Login = login.Login,
                        Senha = login.Senha,
                        Bloqueado = login.Bloqueado,
                        IdUsuarioInclusao = login.IdUsuarioInclusao

                    });

                    return login;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LoginsRepository", "Insertlogins");
                return null;
            }
        }



        public Logins Updatelogins(Logins login)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strupdate = new StringBuilder();
                    strupdate.Append(" UPDATE [user].[Logins] ");
                    strupdate.Append("   SET  ");
                    strupdate.Append("       [Login] = @Login ");
                    strupdate.Append("      ,[Senha] = @Senha ");
                    strupdate.Append("      ,[Bloqueado] = @Bloqueado ");
                    strupdate.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao ");
                    strupdate.Append("      ,[DtAlteracao] = getDate() ");
                    strupdate.Append(" WHERE IdUsuario = " + login.IdUsuario);

                    _db.Execute(strupdate.ToString(), new
                    {
                        IdUsuario = login.IdUsuario,
                        Login = login.Login,
                        Senha = login.Senha,
                        Bloqueado = login.Bloqueado,
                        IdUsuarioAlteracao = login.IdUsuarioAlteracao

                    });

                    return login;
                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LoginsRepository", "Updatelogins");
                return null;
            }
        }

        public Logins LoginUsuarioByID(int idUsuario)
        {
            try
            {

                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strLogin = new StringBuilder();

                    strLogin.Append(" SELECT [IdUsuario] ");
                    strLogin.Append("      ,[Login]");
                    strLogin.Append("      ,[Senha]");
                    strLogin.Append("      ,[Bloqueado]");
                    strLogin.Append("      ,[Tentativas]");
                    strLogin.Append("      ,[IdUsuarioInclusao]");
                    strLogin.Append("      ,[DtInclusao]");
                    strLogin.Append("      ,[IdUsuarioAlteracao]");
                    strLogin.Append("      ,[DtAlteracao]");
                    strLogin.Append(" FROM [user].[Logins] where IdUsuario = @idUsuario");

                    return _db.Query<Logins>(strLogin.ToString(), new { idUsuario = idUsuario }).FirstOrDefault();
                }


            }
            catch (Exception ex)
            {
                InsertLogError(ex, "LoginsRepository", "LoginUsuario");
                return null;
            }
        }
    }
}
