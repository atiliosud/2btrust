﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Data.Repository
{
    public class CidadesRepository : BaseRepository, ICidadesRepository
    {

        public IEnumerable<Cidades> GetListCidadesByIdEstado(int idEstado)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetCidades = new StringBuilder();
                    strGetCidades.Append(" SELECT [Id] ");
                    strGetCidades.Append("      ,[IdEstado] ");
                    strGetCidades.Append("      ,[Nome] ");
                    strGetCidades.Append("      ,[CodigoIBGE] ");
                    strGetCidades.Append("      ,[IdUsuarioInclusao] ");
                    strGetCidades.Append("      ,[DtInclusao] ");
                    strGetCidades.Append("      ,[IdUsuarioAlteracao ");
                    strGetCidades.Append("      ,[DtAlteracao] ");
                    strGetCidades.Append("      ,[FlgAtivo] ");
                    strGetCidades.Append("  FROM [crm].[Cidades] where IdEstado = @IdEstado ");

                    return _db.Query<Cidades>(strGetCidades.ToString(), new { IdEstado = idEstado }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "CidadesRepository", "GetListCidadesByIdEstado");
                return null;
            }
        }

        public Cidades GetListCidadesByIdCidade(int idCidade)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetCidades = new StringBuilder();
                    strGetCidades.Append(" SELECT [Id] ");
                    strGetCidades.Append("      ,[IdEstado] ");
                    strGetCidades.Append("      ,[Nome] ");
                    strGetCidades.Append("      ,[CodigoIBGE] ");
                    strGetCidades.Append("      ,[IdUsuarioInclusao] ");
                    strGetCidades.Append("      ,[DtInclusao] ");
                    strGetCidades.Append("      ,[IdUsuarioAlteracao] ");
                    strGetCidades.Append("      ,[DtAlteracao] ");
                    strGetCidades.Append("      ,[FlgAtivo] ");
                    strGetCidades.Append("  FROM [crm].[Cidades] where Id = @IdCidade ");

                    return _db.Query<Cidades>(strGetCidades.ToString(), new { IdCidade = idCidade }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "CidadesRepository", "GetListCidadesByIdEstado");
                return null;
            }
        }


        public Estados getEstadoByIdCidade(int idCidade)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetCidades = new StringBuilder();
                    strGetCidades.Append(" SELECT  E.[Id] ");
                    strGetCidades.Append("  ,E.[Sigla] ");
                    strGetCidades.Append("  ,E.[Nome] ");
                    strGetCidades.Append("  ,E.[CodigoIBGE] ");
                    strGetCidades.Append("  ,E.[IdUsuarioInclusao] ");
                    strGetCidades.Append("  ,E.[DtInclusao] ");
                    strGetCidades.Append("  ,E.[IdUsuarioAlteracao] ");
                    strGetCidades.Append("  ,E.[DtAlteracao] ");
                    strGetCidades.Append("  FROM [crm].[Cidades] C inner join [crm].[Estados] E on C.IdEstado = E.Id   where C.Id = @IdCidade ");

                    return _db.Query<Estados>(strGetCidades.ToString(), new { IdCidade = idCidade }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "CidadesRepository", "GetListCidadesByIdEstado");
                return null;
            }
        }


        public Cidades InsertCidade(Cidades cid)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();

                    strInsert.Append(" INSERT INTO [crm].[Cidades] ");
                    strInsert.Append("          ([IdEstado] ");
                    strInsert.Append("          ,[Nome] ");
                    strInsert.Append("          ,[CodigoIBGE] ");
                    strInsert.Append("          ,[IdUsuarioInclusao] ");
                    strInsert.Append("          ,[DtInclusao] ");
                    strInsert.Append("          ,[FlgAtivo] ");
                    strInsert.Append("           ) ");
                    strInsert.Append("    VALUES ");
                    strInsert.Append("          (@IdEstado ");
                    strInsert.Append("          ,@Nome ");
                    strInsert.Append("          ,@CodigoIBGE ");
                    strInsert.Append("          ,@IdUsuarioInclusao ");
                    strInsert.Append("          ,getDate() ");
                    strInsert.Append("          ,1");
                    strInsert.Append(" ) SELECT IDENT_CURRENT('[crm].[Cidades]') AS [IDENT_CURRENT]");

                    var idCidade = _db.Query<int>(strInsert.ToString(), new
                    {
                        IdEstado = cid.IdEstado,
                        Nome = cid.Nome,
                        CodigoIBGE = cid.CodigoIBGE,
                        IdUsuarioInclusao = cid.IdUsuarioInclusao

                    }).FirstOrDefault();

                    return GetListCidadesByIdCidade(idCidade);


                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "CidadesRepository", "InsertCidade");
                return null;
            }
        }

    }
}
