﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using Domain.Entities._2BTrust;

namespace Data.Repository
{
    public class GruposAcoesRepository : BaseRepository, IGruposAcoesRepository
    {
        public bool InsertGruposAcoes(int idGrupo, int idAcao, int idUsuarioInclusao)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO [user].[GruposAcoes] ");
                    strInsert.Append("          ([IdGrupo] ");
                    strInsert.Append("          ,[IdAcao] ");
                    strInsert.Append("          ,[IdUsuarioInclusao] ");
                    strInsert.Append("          ,[DtInclusao]) ");
                    strInsert.Append("    VALUES ");
                    strInsert.Append("          (@IdGrupo ");
                    strInsert.Append("          ,@IdAcao ");
                    strInsert.Append("          ,@IdUsuarioInclusao ");
                    strInsert.Append("          ,getDate())");

                    _db.Execute(strInsert.ToString(), new { IdGrupo = idGrupo, IdAcao = idAcao, IdUsuarioInclusao = idUsuarioInclusao });


                    return true;
                }
            }
            catch (Exception rx)
            {
                InsertLogError(rx, "GruposAcoesRepository", "InsertGruposAcoes");
                return false;
            }
        }

        private bool InsertGruposAcoes(int idGrupo, int idAcao, int idUsuarioInclusao, System.Data.IDbConnection _db)
        {
            try
            {

                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" INSERT INTO [user].[GruposAcoes] ");
                strInsert.Append("          ([IdGrupo] ");
                strInsert.Append("          ,[IdAcao] ");
                strInsert.Append("          ,[IdUsuarioInclusao] ");
                strInsert.Append("          ,[DtInclusao]) ");
                strInsert.Append("    VALUES ");
                strInsert.Append("          (@IdGrupo ");
                strInsert.Append("          ,@IdAcao ");
                strInsert.Append("          ,@IdUsuarioInclusao ");
                strInsert.Append("          ,getDate())");

                _db.Execute(strInsert.ToString(), new { IdGrupo = idGrupo, IdAcao = idAcao, IdUsuarioInclusao = idUsuarioInclusao });


                return true;

            }
            catch (Exception rx)
            {
                InsertLogError(rx, "GruposAcoesRepository", "InsertGruposAcoes");
                return false;
            }
        }

        public IEnumerable<Grupos> GetListGrupoXActions()
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder getGrupos = new StringBuilder();
                    getGrupos.Append(" SELECT [Id] ");
                    getGrupos.Append("      ,[Codigo] ");
                    getGrupos.Append("      ,[Nome] ");
                    getGrupos.Append("      ,[IdUsuarioInclusao] ");
                    getGrupos.Append("      ,[DtInclusao] ");
                    getGrupos.Append("      ,[IdUsuarioAlteracao] ");
                    getGrupos.Append("      ,[DtAlteracao] ");
                    getGrupos.Append("      ,[FlagAtivo] ");
                    getGrupos.Append("  FROM [user].[Grupos] where FlagAtivo =1");


                    var listaGrupos = _db.Query<Grupos>(getGrupos.ToString());

                    foreach (var item in listaGrupos)
                    {
                        StringBuilder strGet = new StringBuilder();
                        strGet.Append(" Select A.Id as IdArea, A.Nome as NomeAction, A.Id as IdAcao , C.Nome as NomeControle, AR.Nome as NomeArea, C.Id as IdControle ");
                        strGet.Append(" , (select count(*) from [user].[GruposAcoes] where IdGrupo = " + item.Id + " and IdAcao = A.Id) as Exist ");
                        strGet.Append(" from [user].acoes A  ");
                        strGet.Append(" inner join [user].Controles C on A.IdControle = C.Id ");
                        strGet.Append(" inner join [user].Areas AR on AR.Id = C.IdArea ");

                        item._GruposActions = _db.Query<GrupoXActions>(strGet.ToString()).AsEnumerable();

                    }

                    return listaGrupos;

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "GruposAcoesRepository", "GetListGrupoXActions");
                return null;
            }
        }



        public bool DeleteGruposAcoes(int idGrupo, int idAcao, System.Data.IDbConnection _db)
        {
            try
            {

                _db.Execute("delete [user].[GruposAcoes] where IdGrupo = @idGrupo and idAcao = @idAcao", new
                    {
                        idGrupo = idGrupo,
                        idAcao = idAcao
                    });

                return true;


            }
            catch (Exception ex)
            {
                InsertLogError(ex, "GruposAcoesRepository", "DeleteGruposAcoes");
                return false;
            }
        }


        public GruposAcoes SetGrupoXActions(int idGrupo, int idAcao, int idUsuario)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    string verificaExistencia = "select count(*) from [user].[GruposAcoes] where IdGrupo = @idGrupo and IdAcao = @idAcao";

                    var iCount = _db.Query<int>(verificaExistencia, new { idGrupo = idGrupo, idAcao = idAcao }).FirstOrDefault();
                    if (iCount > 0)
                    {
                        DeleteGruposAcoes(idGrupo, idAcao, _db);
                    }
                    else
                    {
                        InsertGruposAcoes(idGrupo, idAcao, idUsuario, _db);

                    }


                    return new GruposAcoes()
                    {
                        IdAcao = idAcao,
                        IdGrupo = idGrupo,
                        IdUsuarioInclusao = idUsuario
                    };
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "GruposAcoesRepository", "SetGrupoXActions");
                return null;
            }
        }



        public bool DeleteGruposAcoes(int idGrupo, int idAcao)
        {
            throw new NotImplementedException();
        }
    }
}
