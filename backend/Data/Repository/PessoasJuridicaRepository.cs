﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;


namespace Data.Repository
{
    public class PessoasJuridicaRepository : BaseRepository, IPessoasJuridicaRepository
    {

        public PessoasJuridica GetPessoasJuridicasByIdPessoa(int IdPessoa)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGetPessoaJuridica = new StringBuilder();

                    strGetPessoaJuridica.Append(" SELECT [IdPessoa] ");
                    strGetPessoaJuridica.Append("      ,[Fantasia] ");
                    strGetPessoaJuridica.Append("      ,[CNPJ] ");
                    strGetPessoaJuridica.Append("      ,[IdUsuarioInclusao] ");
                    strGetPessoaJuridica.Append("      ,[DtInclusao] ");
                    strGetPessoaJuridica.Append("      ,[IdUsuarioAlteracao] ");
                    strGetPessoaJuridica.Append("      ,[DtAlteracao] ");
                    strGetPessoaJuridica.Append("  FROM [crm].[PessoasJuridica] where IdPessoa = " + IdPessoa);

                    return _db.Query<PessoasJuridica>(strGetPessoaJuridica.ToString()).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasJuridicaRepository", "GetPessoasJuridicasByIdPessoa");
                return null;
            }
        }

        public PessoasJuridica InsertPessoasJuridicas(PessoasJuridica pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsertPessoaJuridica = new StringBuilder();
                    strInsertPessoaJuridica.Append(" INSERT INTO [crm].[PessoasJuridica] ");
                    strInsertPessoaJuridica.Append(" ([IdPessoa] ");
                    strInsertPessoaJuridica.Append(" ,[Fantasia] ");
                    strInsertPessoaJuridica.Append(" ,[CNPJ] ");
                    strInsertPessoaJuridica.Append(" ,[IdUsuarioInclusao] ");
                    strInsertPessoaJuridica.Append(" ,[DtInclusao] ");
                    strInsertPessoaJuridica.Append("  ");
                    strInsertPessoaJuridica.Append("  ) ");
                    strInsertPessoaJuridica.Append("       VALUES ");
                    strInsertPessoaJuridica.Append(" (@IdPessoa");
                    strInsertPessoaJuridica.Append(" ,@Fantasia");
                    strInsertPessoaJuridica.Append(" ,@CNPJ");
                    strInsertPessoaJuridica.Append(" ,@IdUsuarioInclusao");
                    strInsertPessoaJuridica.Append(" ,getDate()");
                    strInsertPessoaJuridica.Append(" )  SELECT IDENT_CURRENT('[crm].[PessoasJuridica]') AS [IDENT_CURRENT]                                                                                                                      ");

                    var idPessoaJuridica = _db.Query<int>(strInsertPessoaJuridica.ToString(), new
                    {
                        IPessoa = pes.IdPessoa,
                        Fantasia = pes.Fantasia,
                        CNPJ = pes.CNPJ,
                        IdUsuarioInclusao = pes.IdUsuarioInclusao
                    }).FirstOrDefault();

                    return GetPessoasJuridicasByIdPessoa(pes.IdPessoa);
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasJuridicaRepository", "InsertPessoasJuridicasByIdPessoa");
                return null;
            }
        }

        public PessoasJuridica UpdatePessoasJuridicasByIdPessoa(PessoasJuridica pes)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strUpdatePessoaJuridica = new StringBuilder();
                    strUpdatePessoaJuridica.Append(" UPDATE [crm].[PessoasJuridica] ");
                    strUpdatePessoaJuridica.Append("   SET [IdPessoa] = @IdPessoa");
                    strUpdatePessoaJuridica.Append("      ,[Fantasia] = @Fantasia");
                    strUpdatePessoaJuridica.Append("      ,[CNPJ] = @CNPJ");
                    strUpdatePessoaJuridica.Append("      ,[IdUsuarioAlteracao] = @IdUsuarioAlteracao");
                    strUpdatePessoaJuridica.Append("      ,[DtAlteracao] = getDate()");
                    strUpdatePessoaJuridica.Append(" WHERE IdPessoa = @IdPessoa");

                    _db.Execute(strUpdatePessoaJuridica.ToString(), new
                    {
                        IPessoa = pes.IdPessoa,
                        Fantasia = pes.Fantasia,
                        CNPJ = pes.CNPJ,
                        IdUsuarioInclusao = pes.IdUsuarioInclusao
                    });

                    return pes;
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "PessoasJuridicaRepository", "InsertPessoasJuridicasByIdPessoa");
                return null;
            }
        }

    }
}
