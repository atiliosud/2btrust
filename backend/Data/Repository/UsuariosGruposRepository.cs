﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Data.Repository
{
    public class UsuariosGruposRepository : BaseRepository, IUsuariosGruposRepository
    {
        public bool InsertUsuariosGrupos(Domain.Entities._2BTrust.UsuariosGrupos user)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO [user].[UsuariosGrupos] ");
                    strInsert.Append("        ([IdUsuario] ");
                    strInsert.Append("        ,[IdGrupo] ");
                    strInsert.Append("        ,[IdUsuarioInclusao] ");
                    strInsert.Append("        ,[DtInclusao]) ");
                    strInsert.Append("  VALUES ");
                    strInsert.Append("        (@IdUsuario ");
                    strInsert.Append("        ,@IdGrupo ");
                    strInsert.Append("        ,@IdUsuarioInclusao ");
                    strInsert.Append("        ,getDate())");


                    _db.Execute(strInsert.ToString(), new
                    {
                        IdUsuario = user.IdUsuario,
                        idGrupo = user.IdGrupo,
                        IdUsuarioInclusao = user.IdUsuarioInclusao

                    });

                    return true; 
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "UsuariosGruposRepository", "InsertUsuariosGrupos");

                return false;
            }
        }
    }
}
