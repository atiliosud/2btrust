﻿using Domain.IRepository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;

namespace Data.Repository
{
    public class RentabilidadeRepository :BaseRepository, IRentabilidadeRepository
    {
        public Domain.Entities._2BTrust.Rentabilidade InsertRentabilidade(Domain.Entities._2BTrust.Rentabilidade ren)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strINsert = new StringBuilder();


                    decimal valueCapital = Convert.ToDecimal(ren.CapitalInvestido);

                    decimal valueValor = Convert.ToDecimal(ren.Valor);

                    decimal valueLucroLiquido = Convert.ToDecimal(ren.LucroLiquido); 

                    strINsert.Append(" INSERT INTO [financ].[Rentabilidade] ");
                    strINsert.Append("    ([IdBem] ");
                    strINsert.Append("    ,[DtInicio] ");
                    strINsert.Append("    ,[DtFim] ");
                    strINsert.Append("    ,[LucroLiquido] ");
                    strINsert.Append("    ,[CapitalInvestido] ");
                    strINsert.Append("    ,[Valor] ");
                    strINsert.Append("    ,[IdUsuarioInclusao] ");
                    strINsert.Append("    ,[DtInclusao] ) ");
                    strINsert.Append(" VALUES ");
                    strINsert.Append("   (@IdBem ");
                    strINsert.Append("   ,@DtInicio ");
                    strINsert.Append("   ,@DtFim ");
                    strINsert.Append("   ,@LucroLiquido ");
                    strINsert.Append("   ,@CapitalInvestido ");
                    strINsert.Append("   ,@Valor ");
                    strINsert.Append("   ,@IdUsuarioInclusao ");
                    strINsert.Append("   ,getDate()) ");
                    strINsert.Append("  SELECT IDENT_CURRENT('[financ].[Rentabilidade]') AS [IDENT_CURRENT]");


                    var idRentabilidade = _db.Query<int>(strINsert.ToString(), new
                    {
                        IdBem = ren.IdBem,
                        DtInicio = ren.DtInicio,
                        DtFim = ren.DtFim,
                        LucroLiquido = valueLucroLiquido,
                        CapitalInvestido = valueCapital,
                        Valor = valueValor,
                        IdUsuarioInclusao = ren.IdUsuarioInclusao

                    }).FirstOrDefault();


                    ren.Id = idRentabilidade;
                    return ren;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "RentabilidadeRepository", "InsertRentabilidade");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Rentabilidade UpdateRentabilidade(Domain.Entities._2BTrust.Rentabilidade ren)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder strUpdate = new StringBuilder();
                    decimal valueCapital = Convert.ToDecimal(ren.CapitalInvestido);

                    decimal valueValor = Convert.ToDecimal(ren.Valor);

                    decimal valueLucroLiquido = Convert.ToDecimal(ren.LucroLiquido); 


                    strUpdate.Append(" UPDATE [financ].[Rentabilidade] ");
                    strUpdate.Append("   SET [IdBem] = @IdBem ");
                    strUpdate.Append("      ,[DtInicio] =  @DtInicio ");
                    strUpdate.Append("      ,[DtFim] =  @DtFim ");
                    strUpdate.Append("      ,[LucroLiquido] =  @LucroLiquido ");
                    strUpdate.Append("      ,[CapitalInvestido] =  @CapitalInvestido ");
                    strUpdate.Append("      ,[Valor] =  @Valor ");
                    strUpdate.Append("      ,[IdUsuarioAlteracao] =  @IdUsuarioAlteracao ");
                    strUpdate.Append("      ,[DtAlteracao] = getDate() ");
                    strUpdate.Append(" WHERE Id = @id");

                    _db.Execute(strUpdate.ToString(), new
                    {
                        id = ren.Id,
                        IdBem = ren.IdBem,
                        DtInicio = ren.DtInicio,
                        DtFim = ren.DtFim,
                        LucroLiquido = valueLucroLiquido,
                        CapitalInvestido = valueCapital,
                        Valor = valueValor,
                        IdUsuarioAlteracao = ren.IdUsuarioAlteracao
                    });

                    return ren;

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "RentabilidadeRepository", "UpdateRentabilidade");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Rentabilidade GetRentabilidadeByIdBem(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder stGet = new StringBuilder();

                    stGet.Append(" Select ");
                    stGet.Append("       [Id] ");
                    stGet.Append("      ,[IdBem] ");
                    stGet.Append("      ,[DtInicio]  ");
                    stGet.Append("      ,[DtFim]  ");
                    stGet.Append("      ,[LucroLiquido]  ");
                    stGet.Append("      ,[CapitalInvestido]  ");
                    stGet.Append("      ,[Valor]  ");
                    stGet.Append("      ,[IdUsuarioAlteracao]  ");
                    stGet.Append("      ,[DtAlteracao]  ");
                    stGet.Append("      ,[IdUsuarioInclusao]  ");
                    stGet.Append("      ,[DtInclusao]  from  [financ].[Rentabilidade] ");
                    stGet.Append(" WHERE IdBem = @IdBem");

                    return _db.Query<Rentabilidade>(stGet.ToString(), new { IdBem = idBem }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "RentabilidadeRepository", "GetRentabilidadeByIdBem");

                return null;
            }
        }

        public Domain.Entities._2BTrust.Rentabilidade GetRentabilidadeById(int id)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder stGet = new StringBuilder();

                    stGet.Append(" Select ");
                    stGet.Append("       [Id] ");
                    stGet.Append("      ,[IdBem] ");
                    stGet.Append("      ,[DtInicio]  ");
                    stGet.Append("      ,[DtFim]  ");
                    stGet.Append("      ,[LucroLiquido]  ");
                    stGet.Append("      ,[CapitalInvestido]  ");
                    stGet.Append("      ,[Valor]  ");
                    stGet.Append("      ,[IdUsuarioAlteracao]  ");
                    stGet.Append("      ,[DtAlteracao]  ");
                    stGet.Append("      ,[IdUsuarioInclusao]  ");
                    stGet.Append("      ,[DtInclusao]  from  [financ].[Rentabilidade] ");
                    stGet.Append(" WHERE Id = @Id");

                    return _db.Query<Rentabilidade>(stGet.ToString(), new { Id = id }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "RentabilidadeRepository", "GetRentabilidadeByIdBem");

                return null;
            }
        }


        public IEnumerable<Rentabilidade> GetListRentabilidadeByIdBem(int idBem)
        {
            try
            {
                using (_db = new SqlConnection(urlConectionPrincipal))
                {
                    StringBuilder stGet = new StringBuilder();

                    stGet.Append(" Select ");
                    stGet.Append("       [Id] ");
                    stGet.Append("      ,[IdBem] ");
                    stGet.Append("      ,[DtInicio]  ");
                    stGet.Append("      ,[DtFim]  ");
                    stGet.Append("      ,[LucroLiquido]  ");
                    stGet.Append("      ,[CapitalInvestido]  ");
                    stGet.Append("      ,[Valor]  ");
                    stGet.Append("      ,[IdUsuarioAlteracao]  ");
                    stGet.Append("      ,[DtAlteracao]  ");
                    stGet.Append("      ,[IdUsuarioInclusao]  ");
                    stGet.Append("      ,[DtInclusao]  from  [financ].[Rentabilidade] ");
                    stGet.Append(" WHERE IdBem = @IdBem");

                    return _db.Query<Rentabilidade>(stGet.ToString(), new { IdBem = idBem }).AsEnumerable();
                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "RentabilidadeRepository", "GetListRentabilidadeByIdBem");

                return null;
            }
        }
    }
}
