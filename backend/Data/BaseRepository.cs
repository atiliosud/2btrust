﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Domain.Entities._2BTrust;

namespace Data
{
    public class BaseRepository
    {

        protected IDbConnection _db;
        public string urlConectionPrincipal = ConfigurationManager.ConnectionStrings["Dapper2bTrust"].ConnectionString;


        public void InsertLogError(Exception ex, string controllerRepository, string metodo)
        {
            using (IDbConnection db = new SqlConnection(urlConectionPrincipal))
            {

                LogError log = new LogError()
                {
                    LOG_ControllerRepository = controllerRepository,
                    LOG_DataOcorrencia = DateTime.Now,
                    LOG_Descricao = ex.Message,
                    LOG_InnerException = (ex.InnerException != null) ? ex.InnerException.ToString() : ex.Message,
                    LOG_Metodo = metodo,
                };


                StringBuilder strInsert = new StringBuilder();
                strInsert.Append(" INSERT INTO [Log] ");
                strInsert.Append("        ( ");
                strInsert.Append("         [LOG_DataOcorrencia] ");
                strInsert.Append("        ,[LOG_ControllerRepository] ");
                strInsert.Append("        ,[LOG_Metodo] ");
                strInsert.Append("        ,[LOG_Descricao] ");
                strInsert.Append("        ,[LOG_InnerException]) ");
                strInsert.Append("  VALUES ");
                strInsert.Append("        (getdate()");
                strInsert.Append("        ,'" + log.LOG_ControllerRepository + "'");
                strInsert.Append("        ,'" + log.LOG_Metodo + "'");
                strInsert.Append("        ,'" + log.LOG_Descricao.Replace("'", "\"") + "'");
                strInsert.Append("        ,'" + log.LOG_InnerException.Replace("'", "\"") + "')");


                db.Execute(strInsert.ToString());

            }
        }


        public CarteiraLog GetCarteiraLogByDescIdPessoa(int idPessoa, string descricao)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append("SELECT [Id] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Data] ");
                    strGet.Append("      ,[Descricao] ");
                    strGet.Append("      ,[Tipo] ");
                    strGet.Append("      ,[Amount] ");
                    strGet.Append("      ,[AvaliableBalance] ");
                    strGet.Append("      ,[FlagAtivo] ");
                    strGet.Append("  FROM [CarteiraLog] where IdPessoa=@IdPessoa and Descricao =@Descricao  order by Id Desc");

                    return db.Query<CarteiraLog>(strGet.ToString(), new { IdPessoa = idPessoa, Descricao = descricao }).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BaseRepository", "GetCarteiraLogByDescIdPessoa");
                return null;
            }

        }


        public void InserLogCarteira(CarteiraLog Log)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO  [CarteiraLog] ");
                    strInsert.Append("    ( [IdPessoa] ");
                    strInsert.Append("    ,[Data] ");
                    strInsert.Append("    ,[Descricao] ");
                    strInsert.Append("    ,[Tipo] ");
                    strInsert.Append("    ,[Amount] ");
                    strInsert.Append("    ,[AvaliableBalance] ");
                    strInsert.Append("    ,[FlagAtivo]) ");
                    strInsert.Append("   VALUES ");
                    strInsert.Append("    (@IdPessoa ");
                    strInsert.Append("    ,@Data ");
                    strInsert.Append("    ,@Descricao ");
                    strInsert.Append("    ,@Tipo ");
                    strInsert.Append("    ,@Amount ");
                    strInsert.Append("    ,@AvaliableBalance ");
                    strInsert.Append("    ,@FlagAtivo)");

                    if (Log.Data == null)
                    {
                        Log.Data = DateTime.Now;
                    }

                    db.Execute(strInsert.ToString(), new
                    {
                        IdPessoa = Log.IdPessoa,
                        Data = Log.Data,
                        Descricao = Log.Descricao,
                        Tipo = Log.Tipo,
                        Amount = Log.Amount,
                        AvaliableBalance = Log.AvaliableBalance,
                        FlagAtivo = Log.FlagAtivo
                    });

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BaseRepository", "InserLogCarteira");
            }
        }

        public CarteiraLog GetLastLogCarteira(int idPessoa)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strGet = new StringBuilder();

                    strGet.Append("SELECT [Id] ");
                    strGet.Append("      ,[IdPessoa] ");
                    strGet.Append("      ,[Data] ");
                    strGet.Append("      ,[Descricao] ");
                    strGet.Append("      ,[Tipo] ");
                    strGet.Append("      ,[Amount] ");
                    strGet.Append("      ,[AvaliableBalance] ");
                    strGet.Append("      ,[FlagAtivo] ");
                    strGet.Append("  FROM [CarteiraLog] where IdPessoa=@IdPessoa  order by Id Desc");

                    return db.Query<CarteiraLog>(strGet.ToString(), new { IdPessoa = idPessoa }).FirstOrDefault();

                }

            }
            catch (Exception ex)
            {
                InsertLogError(ex, "BaseRepository", "GetLastLogCarteira");
                return null;
            }
        }

        public void UpdateLogCarteira(CarteiraLog car)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(urlConectionPrincipal))
                {

                    StringBuilder strUpdate = new StringBuilder();

                    strUpdate.Append(" UPDATE  [dbo].[CarteiraLog] ");
                    strUpdate.Append("   SET [IdPessoa] = @IdPessoa ");
                    strUpdate.Append("      ,[Data] = @Data ");
                    strUpdate.Append("      ,[Descricao] =@Descricao ");
                    strUpdate.Append("      ,[Tipo] = @Tipo ");
                    strUpdate.Append("      ,[Amount] = @Amount ");
                    strUpdate.Append("      ,[AvaliableBalance] = @AvaliableBalance ");
                    strUpdate.Append("      ,[FlagAtivo] = 1 ");
                    strUpdate.Append(" WHERE  Id = @Id ");

                    db.Execute(strUpdate.ToString(), new
                    {
                        IdPessoa = car.IdPessoa,
                        Data = car.Data,
                        Descricao = car.Descricao,
                        Tipo = car.Tipo,
                        Amount = car.Amount,
                        AvaliableBalance = car.AvaliableBalance,
                        Id = car.Id
                    });


                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


    }
}
