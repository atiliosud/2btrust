﻿using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using RestSharp;
using ServicesMailMensalStatement.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServicesMailMensalStatement
{
    class Program
    {
        public static string urlWebAPI = System.Configuration.ConfigurationManager.AppSettings["urlWebApiApontadores"];
        public static string urlWeb = System.Configuration.ConfigurationManager.AppSettings["urlWeb"];



        public static async Task<IRestResponse> GetRestAsync<T>(string ComplementoUrl)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;

        }

        public static async Task<IRestResponse> PostRestAsync<T>(string ComplementoUrl)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.POST);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;

        }

        public async Task<IRestResponse> GetParamtesRestAsync<T>(string ComplementoUrl, object objetoPut)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.GET);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");
            request.RequestFormat = DataFormat.Json;

            request.AddObject(objetoPut);

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;


        }

        public async Task<IRestResponse> PutObjectRestAsync<T>(string ComplementoUrl, object objetoPut)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.PUT);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");
            request.RequestFormat = DataFormat.Json;

            request.AddBody((T)objetoPut);

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;


        }

        public async Task<IRestResponse> PutParametersRestAsync<T>(string ComplementoUrl, object objetoPut)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.PUT);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");
            request.RequestFormat = DataFormat.Json;

            request.AddBody(objetoPut);




            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;


        }

        static void Main(string[] args)
        {
            MainAsync().Wait();
        }

        static async Task MainAsync()
        {

            FileCreator fileCreator = new FileCreator();
            try
            {

                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Iniciando a aplicação de lançamento dos emails com Statement Ref.: " + DateTime.Now.ToString("MMM"));
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Iniciando a aplicação de lançamento dos emails com Statement Ref.: " + DateTime.Now.ToString("MMM"));

                IEnumerable<Pessoas> model;
                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Iniciando a pesquisa no serviço");
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Iniciando a pesquisa no serviço");

                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetListPessoasIndex?fAll=false");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<Pessoas>>(response.Content);

                    Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Pesquisa concluida com " + model.Count() + " pessoas encontradas");
                    fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Pesquisa concluida com " + model.Count() + " pessoas encontradas");



                    foreach (var item in model)
                    {

                        try
                        {


                            Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Enviando solicitação de envio do email para " + item.Nome);
                            fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Enviando solicitação de envio do email para " + item.Nome);
                            WebClient client = new WebClient();
                            Stream stream = client.OpenRead(urlWeb + "Reports/CreatePdfStatement?idPessoa=" + item.Id);
                            StreamReader reader = new StreamReader(stream);
                            String content = reader.ReadToEnd();



                            Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Retorno do envio: " + content);
                            fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Retorno do envio: " + content);
                            Thread.Sleep(2000);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Ocorreu um erro. " + ex.Message);
                            fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -  Ocorreu um erro. " + ex.Message);
                        }
                    }

                }
                else
                {

                    Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Ocorreu um erro ao buscar os registro no serviço. " + response.StatusCode);
                    fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Ocorreu um erro ao buscar os registro no serviço. " + response.StatusCode);

                }


                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Serviço finalizado. " + response.StatusCode);
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Serviço finalizado. " + response.StatusCode);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Ocorreu um erro ao no console. " + ex.Message);
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -  Ocorreu um erro ao no console. " + ex.Message);


            }


        }
    }
}
