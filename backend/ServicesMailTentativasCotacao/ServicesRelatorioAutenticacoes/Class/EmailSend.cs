﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLancamentosRentabilidade.Class
{
    public static class EmailSend
    {
        static FileCreator fileCreator = new FileCreator(); 

        public static bool SendLime(string arquivoRelatorioDetalhado, string iQnt)
        {
            try
            {

                string emailPrincipal = System.Configuration.ConfigurationManager.AppSettings["EmailPrincial"];

                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];

                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["Login"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];

                string Assunto = "AXEI Saúde - Relatório Tentativas Cotações";

                var mail = new MailMessage();
                mail.To.Add(emailPrincipal);   //  mail suporte


                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", "AXEI Saúde - Relatórios", Encoding.UTF8));

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.CC.Add(new MailAddress(item, item, Encoding.UTF8));
                }
                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);

                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocation"]));

                html.GetElementbyId("spnBoasVindas").InnerHtml = string.Format(@"   Olá, você acabou de receber um relatório diário. 
                                                Segue em anexo o relatório de tentativas de cotações com ("+ iQnt + ") registros para análise. Vicê pode observar na última coluna quantas cotações o cliente conseguiu fazer. 0 cotações, significa que o mesmo não completou seu cadastro. ");

                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;


                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                //ADICIONAR ARQUIVO A SER ENVIADO COMO ANEXO
                mail.Attachments.Add(new Attachment(arquivoRelatorioDetalhado)); 


                try
                {
                    client.Send(mail);

                    return true;
                }
                catch (SmtpException ex)
                {
                    return false;
                    throw ex;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string RetornaMes(int iMes)
        {
            if (iMes == 1)
                return "Janeiro";
            else if (iMes == 2)
                return "Fevereiro";
            else if (iMes == 3)
                return "Março";
            else if (iMes == 4)
                return "Abril";
            else if (iMes == 5)
                return "Maio";
            else if (iMes == 6)
                return "Junho";
            else if (iMes == 7)
                return "Julho";
            else if (iMes == 8)
                return "Agosto";
            else if (iMes == 9)
                return "Setembro";
            else if (iMes == 10)
                return "Outubro";
            else if (iMes == 11)
                return "Novembro";
            else if (iMes == 12)
                return "Dezembro";
            else return "";
        }


    }
}
