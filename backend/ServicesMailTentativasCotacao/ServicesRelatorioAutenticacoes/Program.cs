﻿using ServicesLancamentosRentabilidade.Class;
using ServicesLancamentosRentabilidade.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServicesLancamentosRentabilidade.Domain;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using System.Globalization;
using System.Threading;
using System.Configuration;

namespace ServicesLancamentosRentabilidade
{
    public class Program
    {
        static FileCreator fileCreator = new FileCreator();




        static void Main(string[] args)
        {
            CultureInfo culture = new CultureInfo("pt-BR");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;


            FileCreator fileCreator = new FileCreator();
            Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Iniciando a aplicação de lançamento das rentabilidades Ref.: " + DateTime.Now.ToString("MMM"));
            fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Iniciando a aplicação de lançamento das rentabilidades Ref.: " + DateTime.Now.ToString("MMM"));


            Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Realizando consulta ao banco de dados");
            fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -  Realizando consulta ao banco de dados");


            // captura todos os Investments ativos
            var getAllInvestimentosPessoasPools = Repository.GetListPoolsBens();

            Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Consulta realizada com sucesso. Total de Investments = " + getAllInvestimentosPessoasPools.Count());
            fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Consulta realizada com sucesso. Total de Investments = " + getAllInvestimentosPessoasPools.Count()); ;



            if(getAllInvestimentosPessoasPools.Count()>0)
            {
                foreach (var item in getAllInvestimentosPessoasPools)
                {
                    Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Gerando rentabilidade para: "+item.Nome);
                    fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "]  - Gerando rentabilidade para: " + item.Nome);

                    var iMes = DateTime.Now.Month;
                    var iAno = DateTime.Now.Year;

                    var getRentabilidade = Repository.GetRentabilidadeMesBens(item.IdPoolBem, iMes, iAno);



                    decimal sumPercent = getRentabilidade.Sum(c => c.percentRentabilidade); // soma das porcentagens dos bens para o mes

                    decimal valorCotas = item.Cota * 100; //valor das minhas cotas


                    decimal valorRentabilidade = valorCotas * (sumPercent/10);
                     


                    Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Rentabilidade calculada: " + sumPercent.ToString("#0.00")+"%");
                    fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "]  - Rentabilidade calculada: " + sumPercent.ToString("#0.00") + "%");
                     

                    Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Realizando lançamento no valor de: "+ valorRentabilidade.ToString("#0.00"));
                    fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -   Realizando lançamento no valor de: "+ valorRentabilidade.ToString("#0.00"));


                    Repository.InsertLancamentoPessoa(item.IdPessoa, item.IdPoolBem, valorRentabilidade);


                }

            }
            else
            {


                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Nenhum investimento encontrado! Pressione ENTER para sair... ");
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - Nenhum investimento encontrado! ");
            }
              
        }


    }
   

}