﻿using ServicesLancamentosRentabilidade.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ServicesLancamentosRentabilidade.Class;

namespace ServicesLancamentosRentabilidade.Data
{
    public static class Repository
    {
        static FileCreator fileCreator = new FileCreator();


        public static IEnumerable<PoolBensPessoas> GetListPoolsBens()
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["Vortice"].ConnectionString))
                {

                    StringBuilder strGet = new StringBuilder();
                    strGet.Append(" SELECT PB.[Id] ");
                    strGet.Append("    ,PB.[IdPoolBem] ");
                    strGet.Append("    ,PB.[IdPessoa] ");
                    strGet.Append("    ,PB.[Cota] ");
                    strGet.Append("    ,PB.[IdStatus] ");
                    strGet.Append("    ,PB.[IdUsuarioInclusao] ");
                    strGet.Append("    ,PB.[DtInclusao] ");
                    strGet.Append("    ,PB.[IdUsuarioAlteracao] ");
                    strGet.Append("    ,PB.[DtAlteracao] ");
                    strGet.Append("    ,PB.[DtInclusaoAdm] ");
                    strGet.Append("    ,P.[Nome] ");
                    strGet.Append(" FROM  [financ].[PoolBensPessoas] PB  Inner Join crm.Pessoas P on P.Id = PB.IdPessoa  where PB.IdStatus = 1");

                    return db.Query<PoolBensPessoas>(strGet.ToString()).AsEnumerable();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - " + ex.ToString());
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -  " + ex.ToString());
                return null;
            }
        }

        public static IEnumerable<PainelRentabilidadeMes> GetRentabilidadeMesBens(int idPool, int iMes, int iAno)
        {

            try
            {

                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["Vortice"].ConnectionString))
                {

                    StringBuilder strGets = new StringBuilder();
                    strGets.Append("  select  B1.Id as idBem, PB.Id as idPool,  B.DividendYeld, ((B1.CapitalInvestido) + (select ISNULL(SUM(Valor),0)  ");
                    strGets.Append("  from dbo.BensGastos B2 where B2.IdBem = B1.Id)) as CapitalTotalInvestido, PB.Cotas,B.iMes,B.iAno, ISNULL(B.percentRentabilidade,0) as percentRentabilidade  ");
                    strGets.Append("  ");
                    strGets.Append("  from [dbo].[financ.BensBaseCalculoHistorico] B inner join financ.Bens B1 on B1.Id = B.IdBem ");
                    strGets.Append("  ");
                    strGets.Append(" inner join financ.PoolBens PB on PB.Id = B1.IdPool ");
                    strGets.Append("  ");
                    strGets.Append(" where B1.IdStatus = 1 and PB.Id = @idPool and B.iMes = @iMes and B.iAno = @iAno ");
                    strGets.Append("  ");
                    strGets.Append(" group by B.DividendYeld , B1.Id, PB.Id, B1.CapitalInvestido, PB.Cotas, B.iMes, B.iAno,  B.percentRentabilidade");

                    return db.Query<PainelRentabilidadeMes>(strGets.ToString(), new { idPool = idPool, iMes = iMes, iAno = iAno }).AsEnumerable();

                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - " + ex.ToString());
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -  " + ex.ToString());
                return null;
            }
        }

        public static decimal GetValorCota(int idPool)
        {
            try
            {

                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["Vortice"].ConnectionString))
                {

                    StringBuilder strGets = new StringBuilder();
                    strGets.Append(" select SUM(CapitalInvestido)/PB.Cotas   from financ.Bens B ");
                    strGets.Append("inner join  financ.PoolBens PB on PB.Id = B.IdPool where IdPool = @idPool and B.IdStatus = 1");
                    strGets.Append("group by PB.Cotas");

                    return db.Query<decimal>(strGets.ToString(), new { idPool = idPool}).FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - " + ex.ToString());
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -  " + ex.ToString());
                return 0;
                
            }
        }

        public static bool InsertLancamentoPessoa(int idPessoa, int idPool, decimal valor)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["Vortice"].ConnectionString))
                {

                    StringBuilder strInsert = new StringBuilder();
                    strInsert.Append(" INSERT INTO  [financ].[Lancamentos] ");
                    strInsert.Append("  ([IdTpLancamento] ");
                    strInsert.Append("  ,[IdPoolBem] ");
                    strInsert.Append("  ,[DtLancamento] ");
                    strInsert.Append("  ,[Valor] ");
                    strInsert.Append("  ,[IdPessoa] ");
                    strInsert.Append("  ,[Obs] ");
                    strInsert.Append("  ,[Credito] ");
                    strInsert.Append("  ,[NumParticao] ");
                    strInsert.Append("  ,[IdUsuarioInclusao] ");
                    strInsert.Append("  ,[DtInclusao]) ");
                    strInsert.Append("   VALUES ");
                    strInsert.Append(" (1 ");
                    strInsert.Append(" , @idPoolBem ");
                    strInsert.Append(" , GETDATE() ");
                    strInsert.Append(" , @Valor ");
                    strInsert.Append(" , @idPessoa ");
                    strInsert.Append(" , 'Lançamento via serviço automático' ");
                    strInsert.Append(" , 1 ");
                    strInsert.Append(" , 1 ");
                    strInsert.Append(" , 3 ");
                    strInsert.Append(" , GETDATE()) ");


                    db.Execute(strInsert.ToString(), new
                    {
                        idPoolBem = idPool,
                        Valor = valor,
                        idPessoa = idPessoa
                    });


                    string strUpdateCarteira = "update dbo.Carteira set Saldo = Saldo + @valor where IdPessoa =@idPessoa";

                    db.Execute(strUpdateCarteira.ToString(), new { valor = valor, idPessoa = idPessoa });

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] - " + ex.ToString());
                fileCreator.WriteTxtLog("[" + DateTime.Now.ToString("dd/MM/yyyy 'às' HH:mm") + "] -  " + ex.ToString());
                return false;
            }
        }

    }
}
