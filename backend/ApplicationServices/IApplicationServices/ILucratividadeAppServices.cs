﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface ILucratividadeAppServices
    {
        Lucratividade InsertLucratividade(Lucratividade luc);
        Lucratividade UpdateLucratividade(Lucratividade luc);
        Lucratividade GetLucratividadeByIdBem(int idBem);
        IEnumerable<Lucratividade> GetListLucratividadeByIdBem(int idBem);
        Lucratividade GetLucratividadeById(int id); 

    }
}
