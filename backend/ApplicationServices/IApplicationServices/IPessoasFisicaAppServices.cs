﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface IPessoasFisicaAppServices
    {
        PessoasFisicas GetPessoaFisicaByIdPessoa(int idPessoa);
        PessoasFisicas InsertPessoaFisica (PessoasFisicas pes);
        PessoasFisicas UpdatePessoaFisica (PessoasFisicas pes);
    }
}
