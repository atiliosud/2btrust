﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface IPessoasJuridicaAppServices
    {
        PessoasJuridica GetPessoasJuridicasByIdPessoa(int IdPessoa);
        PessoasJuridica InsertPessoasJuridicasByIdPessoa(PessoasJuridica pes);
        PessoasJuridica UpdatePessoasJuridicasByIdPessoa(PessoasJuridica pes);
    }
}
