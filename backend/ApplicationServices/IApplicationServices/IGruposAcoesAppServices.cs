﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface IGruposAcoesAppServices
    {
        bool InsertGruposAcoes(int idGrupo, int idAcao, int idUsuarioInclusao);
        bool DeleteGruposAcoes(int idGrupo, int idAcao);
        IEnumerable<Grupos> GetListGrupoXActions();

     
        GruposAcoes SetGrupoXActions(int idGrupo, int idAcao, int idUsuario);
    }
}
