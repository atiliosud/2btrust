﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface IPoolBensPessoasAppServices
    {
        PoolBensPessoas InsertPoolBensPessoas(PoolBensPessoas con);
        PoolBensPessoas UpdatePoolBensPessoas(PoolBensPessoas con);
        IEnumerable<PoolBensPessoas> GetListPoolBensPessoas();
        PoolBensPessoas GetPoolBensPessoasById(int id);
        IEnumerable<PoolBensPessoas> GetPoolBensPessoasByIdPessoa(int idPessoa);
        IEnumerable<PoolBensPessoas> GetPoolBensPessoasByIdPoolBem(int idPoolBem);
        IEnumerable< Pessoas>  GetListValidaVendaCota();
        IEnumerable< Pessoas>  GetListValidaCompraCota();
        IEnumerable< Pessoas>  GetListValidaRetirada();
    }
}
