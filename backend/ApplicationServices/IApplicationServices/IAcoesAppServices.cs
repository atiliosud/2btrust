﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface IAcoesAppServices
    {
        IEnumerable<Acoes> GetListAcoes();

        Acoes InsertAcoes(Acoes acoes);

        Acoes GetAcoesById(int idAcoes);

        Acoes UpdateAcoes(Acoes acoes);
    }
}
