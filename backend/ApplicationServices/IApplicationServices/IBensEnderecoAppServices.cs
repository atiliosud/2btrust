﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface IBensEnderecoAppServices
    {
        BensEnderecos InsertBensEnderecos(BensEnderecos bem);
        BensEnderecos UpdateBensEnderecos(BensEnderecos bem);
        BensEnderecos GetBensEnderecosByIdEndereco(int idBem);
        BensEnderecos GetBensEnderecosById(int id);

    }
}
