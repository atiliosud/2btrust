﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.IApplicationServices
{
    public interface IBensAcoesAppServices
    {
        BensAcoes InsertBensAcoes(BensAcoes bemAcao);
        BensAcoes UpdateBensAcoes(BensAcoes bemAcao);
        BensAcoes GetBensAcoesByIdBem(int idBem);
        BensAcoes GetBensAcoesById(int id);

    }
}
