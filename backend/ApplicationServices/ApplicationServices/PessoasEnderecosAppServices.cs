﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class PessoasEnderecosAppServices : IPessoasEnderecosAppServices
    {
        private readonly IPessoasEnderecosServices _pessoaEnderecoServices;
        public PessoasEnderecosAppServices(IPessoasEnderecosServices pessoaEnderecoServices)
        {
            _pessoaEnderecoServices = pessoaEnderecoServices;
        }


        public PessoasEnderecos GetEnderecoById(int idPessoaEndereco)
        {
            return _pessoaEnderecoServices.GetEnderecoById(idPessoaEndereco);
        }

        public PessoasEnderecos GetEnderecoByIdPessoa(int idPessoa)
        {
            return _pessoaEnderecoServices.GetEnderecoByIdPessoa(idPessoa);
        }

        public PessoasEnderecos InsertEnderecoByIdPessoa(PessoasEnderecos pes)
        {
            return _pessoaEnderecoServices.InsertEnderecoByIdPessoa(pes);
        }

        public PessoasEnderecos UpdateEnderecoByIdPessoa(PessoasEnderecos pes)
        {
            return _pessoaEnderecoServices.UpdateEnderecoByIdPessoa(pes);
        }
    }
}
