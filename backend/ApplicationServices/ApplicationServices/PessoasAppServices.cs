﻿using ApplicationServices.IApplicationServices;
using Domain.Entities;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class PessoasAppServices : IPessoasAppServices
    {
        private readonly IPessoasServices _pessoaServices;

        public PessoasAppServices(IPessoasServices pessoaServices)
        {
            _pessoaServices = pessoaServices;
        }
        public Domain.Entities._2BTrust.Pessoas LoginUsuario(string sLogin, string sSenha)
        {
            return _pessoaServices.LoginUsuario(sLogin, sSenha);
        }


        public bool CadastroInicial(Domain.Entities._2BTrust.Pessoas pessoa)
        {
            var retr = _pessoaServices.CadastroInicial(pessoa);

            return true;
        }


        public bool LiberacaoViaEmail(string saltKey)
        {
            return _pessoaServices.LiberacaoViaEmail(saltKey);
        }


        public Domain.Entities._2BTrust.Pessoas GetPessoasById(int idPessoa)
        {
            return _pessoaServices.GetPessoasById(idPessoa);
        }


        public Domain.Entities._2BTrust.Pessoas UpdatePessoaMenu(Domain.Entities._2BTrust.Pessoas pessoa)
        {
            return _pessoaServices.UpdatePessoaMenu(pessoa);
        }


        public Pessoas UpdatePessoaMenuEndereco(Domain.Entities._2BTrust.Pessoas pessoa)
        {
            return _pessoaServices.UpdatePessoaMenuEndereco(pessoa);

        }


        public Pessoas UpdatePessoaMenuDocumento(Pessoas pessoa)
        {
            return _pessoaServices.UpdatePessoaMenuDocumento(pessoa);
        }


        public Pessoas AtualizaDadosPessoa(int idPessoa)
        {
            return _pessoaServices.AtualizaDadosPessoa(idPessoa);
        }


        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoByIdCarteira(int idCarteira)
        {
            return _pessoaServices.GetListComprovanteDepositoByIdCarteira(idCarteira);
        }


        public ComprovanteDeposito InsertComprovanteDeposito(ComprovanteDeposito comp)
        {
            return _pessoaServices.InsertComprovanteDeposito(comp);
        }


        public ComprovanteDeposito GetComprovanteDepositoById(int id)
        {
            return _pessoaServices.GetComprovanteDepositoById(id);
        }

        public ComprovanteDeposito UpdateComprovanteDeposito(ComprovanteDeposito comp)
        {
            return _pessoaServices.UpdateComprovanteDeposito(comp);
        }


        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoPendentes()
        {
            return _pessoaServices.GetListComprovanteDepositoPendentes();
        }




        public IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaByIdCarteira(int idCarteira)
        {
            return _pessoaServices.GetListComprovanteRetiradaByIdCarteira(idCarteira);
        }


        public ComprovanteRetirada InsertComprovanteRetirada(ComprovanteRetirada comp)
        {
            return _pessoaServices.InsertComprovanteRetirada(comp);
        }


        public ComprovanteRetirada GetComprovanteRetiradaById(int id)
        {
            return _pessoaServices.GetComprovanteRetiradaById(id);
        }

        public ComprovanteRetirada UpdateComprovanteRetirada(ComprovanteRetirada comp)
        {
            return _pessoaServices.UpdateComprovanteRetirada(comp);
        }


        public IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaPendentes()
        {
            return _pessoaServices.GetListComprovanteRetiradaPendentes();
        }




        public Pessoas PutValidaUsuarioInicial(string sUsuario)
        {
            return _pessoaServices.PutValidaUsuarioInicial(sUsuario);
        }

        public Pessoas PutValidaEmailInicial(string sEmail)
        {
            return _pessoaServices.PutValidaEmailInicial(sEmail);
        }


        public IEnumerable<Pessoas> GetListPessoas(bool fAll)
        {
            return _pessoaServices.GetListPessoas(fAll);
        }


        public Carteira GetCarteiraByIdPessoa(int idPessoa)
        {
            return _pessoaServices.GetCarteiraByIdPessoa(idPessoa);
        }


        public IEnumerable<HistoricoCarteira> GetHistoricoByIdPessoa(int idPessoa)
        {
            return _pessoaServices.GetHistoricoByIdPessoa(idPessoa);
        }

        public IEnumerable<PessoasGenricList> GetListReduzidoPessoas()
        {
            return _pessoaServices.GetListReduzidoPessoas();
        }

        public InformacoesAdminHome GetInformacoesAdminHome()
        {
            return _pessoaServices.GetInformacoesAdminHome();
        }

        public IEnumerable<PessoaInvestimentoPoolsValores> GetListHomeInvestidores()
        {
            return _pessoaServices.GetListHomeInvestidores();
        }

        public ValidToken createTokenAcessMobile(int idUsuario, string Device)
        {
            return _pessoaServices.createTokenAcessMobile(idUsuario, Device);
        }

        public bool validaTokenAcessMobile(int idUsuario, string sHash)
        {
            return _pessoaServices.validaTokenAcessMobile(idUsuario, sHash);
        }

        public Pessoas InformacoaHomeValoresMobile(int idUsuario, string sHash)
        {
            return _pessoaServices.InformacoaHomeValoresMobile(idUsuario, sHash);
        }

        public IEnumerable<CarteiraLog> GetCarteiraLogByIdPessoa(int idPessoa)
        {
            return _pessoaServices.GetCarteiraLogByIdPessoa(idPessoa);
        }

        public Pessoas PutValidaCPFInicial(string sCPF)
        {
            return _pessoaServices.PutValidaCPFInicial(sCPF);
        }

        public Pessoas PutValidaTAXInicial(string sTAX)
        {
            return _pessoaServices.PutValidaTAXInicial(sTAX);
        }

        public Pessoas PutValidaSocialInicial(string sSocial)
        {
            return _pessoaServices.PutValidaSocialInicial(sSocial);
        }

        public IEnumerable<Pessoas> GetListPessoasIndex(bool fAll)
        {
            return _pessoaServices.GetListPessoasIndex(fAll);
        }

        public RecoveryPassword ReceveryPasswordByEmail(string sEmail)
        {
            return _pessoaServices.ReceveryPasswordByEmail(sEmail);
        }

        public bool RecoveryPasswordValidaToken(string sToken)
        {
            return _pessoaServices.RecoveryPasswordValidaToken(sToken);
        }

        public bool ReceveryPasswordUpdate(int idUsuario, string password, string sToken)
        {
            return _pessoaServices.ReceveryPasswordUpdate(idUsuario, password, sToken);
        }

        public bool InsertLogEmailApp(LogEmailApp log)
        {
            return _pessoaServices.InsertLogEmailApp(log);
        }

        public bool DeleteLogEmailApp(int idLog)
        {
            return _pessoaServices.DeleteLogEmailApp(idLog);
        }

        public IEnumerable<LogEmailApp> GetListLogEmailApp(int idPessoa)
        {
            return _pessoaServices.GetListLogEmailApp(idPessoa);
        }

        public HelpInterno InsertHelpInterno(HelpInterno help)
        {
            return _pessoaServices.InsertHelpInterno(help);
        }

        public HelpInterno GetHelpinternoById(int id)
        {
            return _pessoaServices.GetHelpinternoById(id);
        }

        public IEnumerable<HelpInterno> GetListHelpinterno()
        {
            return _pessoaServices.GetListHelpinterno();
        }

        public IEnumerable<Pessoas> getClientInvests(int idPessoa)
        {
            return _pessoaServices.getClientInvests(idPessoa);
        }

        public Pessoas LoginUsuarioSimples(string sLogin, string sSenha)
        {
            return _pessoaServices.LoginUsuarioSimples(sLogin, sSenha);
        }

        public Pessoas GetPessoaMeusLancamentos(int idPessoa)
        {
            return _pessoaServices.GetPessoaMeusLancamentos(idPessoa);
        }

        public Pessoas GetPessoaMeusPools(int idPessoa)
        {
            return _pessoaServices.GetPessoaMeusPools(idPessoa);
        }

        public Pessoas GetDadosPessoaById(int idPessoa)
        {
            return _pessoaServices.GetDadosPessoaById(idPessoa);
        }
    }
}
