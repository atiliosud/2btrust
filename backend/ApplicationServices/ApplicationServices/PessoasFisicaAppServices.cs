﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class PessoasFisicaAppServices : IPessoasFisicaAppServices
    {
         private readonly IPessoasFisicaServices _pessoaFisicaServices;
         public PessoasFisicaAppServices(IPessoasFisicaServices  pessoaFisicaServices)
        {
            _pessoaFisicaServices = pessoaFisicaServices;
        }

         public PessoasFisicas GetPessoaFisicaByIdPessoa(int idPessoa)
         {
             return _pessoaFisicaServices.GetPessoaFisicaByIdPessoa(idPessoa);
         }

         public PessoasFisicas InsertPessoaFisica(PessoasFisicas pes)
         {
             return _pessoaFisicaServices.InsertPessoaFisica(pes);
         }

         public PessoasFisicas UpdatePessoaFisica(PessoasFisicas pes)
         {
             return _pessoaFisicaServices.UpdatePessoaFisica(pes);
         }
    }
}
