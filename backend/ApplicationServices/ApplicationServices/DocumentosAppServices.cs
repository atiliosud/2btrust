﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class DocumentosAppServices : IDocumentosAppServices
    {
        private readonly IDocumentosServices _documentoServices;
        public DocumentosAppServices(IDocumentosServices  documentoServices)
        {
            _documentoServices = documentoServices;
        }
        public IEnumerable<Documentos> GetListDocumentos()
        {
            return _documentoServices.GetListDocumentos();
        }
    }
}
