﻿using ApplicationServices.IApplicationServices;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class BensAppServices : IBensAppServices
    {

        private readonly IBensServices _bensServices;

        public BensAppServices(IBensServices bensServices)
        {
            _bensServices = bensServices;
        }
        public Domain.Entities._2BTrust.Bens InsertBens(Domain.Entities._2BTrust.Bens bem)
        {
            return _bensServices.InsertBens(bem);
        }

        public Domain.Entities._2BTrust.Bens UpdateBens(Domain.Entities._2BTrust.Bens bem)
        {
            return _bensServices.UpdateBens(bem);
        }

        public Domain.Entities._2BTrust.Bens GetBensById(int id)
        {
            return _bensServices.GetBensById(id);
        }

        public IEnumerable<Domain.Entities._2BTrust.Bens> GetListBens()
        {
            return _bensServices.GetListBens();
        }

        public IEnumerable<Domain.Entities._2BTrust.BensImagens> GetListImagens(int idBem)
        {
            return _bensServices.GetListImagens(idBem);
        }

        public Domain.Entities._2BTrust.BensImagens InsertImagens(Domain.Entities._2BTrust.BensImagens bemImage)
        {
            return _bensServices.InsertImagens(bemImage);
        }

        public Domain.Entities._2BTrust.BensImagens UpdateImagens(Domain.Entities._2BTrust.BensImagens bemImage)
        {

            return _bensServices.UpdateImagens(bemImage);
        }
    }
}
