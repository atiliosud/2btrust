﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class GruposAcoesAppServices : IGruposAcoesAppServices
    {
        private readonly IGruposAcoesServices _gruposAcoes_Services;
        public GruposAcoesAppServices(IGruposAcoesServices gruposAcoes_Services)
        {
            _gruposAcoes_Services = gruposAcoes_Services;
        }
        public IEnumerable<Domain.Entities._2BTrust.Grupos> GetListGrupoXActions()
        {
            return _gruposAcoes_Services.GetListGrupoXActions();
        }

        public bool InsertGruposAcoes(int idGrupo, int idAcao, int idUsuarioInclusao)
        {
            return _gruposAcoes_Services.InsertGruposAcoes(idGrupo, idAcao, idUsuarioInclusao);
        }

        public bool DeleteGruposAcoes(int idGrupo, int idAcao)
        {
            return _gruposAcoes_Services.DeleteGruposAcoes(idGrupo, idAcao);
         }


        public  GruposAcoes SetGrupoXActions(int idGrupo, int idAcao, int idUsuario) 
        {
            return _gruposAcoes_Services.SetGrupoXActions(idGrupo, idAcao,idUsuario);
        }
    }
}
