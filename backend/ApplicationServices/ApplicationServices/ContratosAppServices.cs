﻿using ApplicationServices.IApplicationServices;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class ContratosAppServices : IContratosAppServices
    {
        private readonly IContratosServices _contratosServices;

        public ContratosAppServices(IContratosServices  contratosServices)
        {
            _contratosServices = contratosServices;
        }

        public Domain.Entities._2BTrust.Contratos InsertContratos(Domain.Entities._2BTrust.Contratos con)
        {
            return _contratosServices.InsertContratos(con);
        }

        public Domain.Entities._2BTrust.Contratos UpdateContratos(Domain.Entities._2BTrust.Contratos con)
        {
            return _contratosServices.UpdateContratos(con);
        }

        public IEnumerable<Domain.Entities._2BTrust.Contratos> GetListContratos()
        {
            return _contratosServices.GetListContratos();
        }

        public Domain.Entities._2BTrust.Contratos GetContratosById(int id)
        {
            return _contratosServices.GetContratosById(id);
        }
    }
}
