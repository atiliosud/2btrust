﻿using ApplicationServices.IApplicationServices;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class AcoesAppServices : IAcoesAppServices
    {
        private readonly IAcoesServices _acoesServices;
        public AcoesAppServices(IAcoesServices  acoesServices)
        {
            _acoesServices = acoesServices;
        }

        public IEnumerable<Domain.Entities._2BTrust.Acoes> GetListAcoes()
        {
            return _acoesServices.GetListAcoes();
        }

        public Domain.Entities._2BTrust.Acoes InsertAcoes(Domain.Entities._2BTrust.Acoes area)
        {
            return _acoesServices.InsertAcoes(area);
        }

        public Domain.Entities._2BTrust.Acoes GetAcoesById(int idArea)
        {
            return _acoesServices.GetAcoesById(idArea);
        }

        public Domain.Entities._2BTrust.Acoes UpdateAcoes(Domain.Entities._2BTrust.Acoes area)
        {
            return _acoesServices.UpdateAcoes(area);
        }
    }
}
