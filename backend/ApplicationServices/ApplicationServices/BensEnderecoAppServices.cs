﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class BensEnderecoAppServices : IBensEnderecoAppServices
    {
          private readonly IBensEnderecoServices _BensEnderecoServices;

          public BensEnderecoAppServices(IBensEnderecoServices BensEnderecoServices)
        {
            _BensEnderecoServices = BensEnderecoServices;
        }

        public BensEnderecos InsertBensEnderecos(BensEnderecos bem)
        {
            return _BensEnderecoServices.InsertBensEnderecos(bem);
        }

        public BensEnderecos UpdateBensEnderecos(BensEnderecos bem)
        {
            return _BensEnderecoServices.UpdateBensEnderecos(bem);
        }

        public BensEnderecos GetBensEnderecosByIdEndereco(int idBem)
        {
            return _BensEnderecoServices.GetBensEnderecosByIdEndereco(idBem);
        }

        public BensEnderecos GetBensEnderecosById(int id)
        {
            return _BensEnderecoServices.GetBensEnderecosById(id);
        }
    }
}
