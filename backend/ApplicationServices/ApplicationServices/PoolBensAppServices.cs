﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class PoolBensAppServices : IPoolBensAppServices
    {
        private readonly IPoolBensServices _poolBensServices;

        public PoolBensAppServices(IPoolBensServices  poolBensServices)
        {
            _poolBensServices =  poolBensServices;
        }

        public Domain.Entities._2BTrust.PoolBens InsertPoolBens(Domain.Entities._2BTrust.PoolBens con)
        {
            return _poolBensServices.InsertPoolBens(con);
        }

        public Domain.Entities._2BTrust.PoolBens UpdatePoolBens(Domain.Entities._2BTrust.PoolBens con)
        {
            return _poolBensServices.UpdatePoolBens(con);
        }

        public IEnumerable<Domain.Entities._2BTrust.PoolBens> GetListPoolBens()
        {
            return _poolBensServices.GetListPoolBens();
        }

        public Domain.Entities._2BTrust.PoolBens GetPoolBensById(int id)
        {
            return _poolBensServices.GetPoolBensById(id);
        }


        public IEnumerable<Domain.Entities._2BTrust.PoolBens> GetListPoolParaInvestimento()
        {
            return _poolBensServices.GetListPoolParaInvestimento();
        }


        public Domain.Entities._2BTrust.PoolBens GetDetailsPoolParaInvestimentoById(int id)
        {
            return _poolBensServices.GetDetailsPoolParaInvestimentoById(id);
        }


        public object GetListMeusPoolInvestidos(int idPessoa)
        {
            return _poolBensServices.GetListMeusPoolInvestidos(idPessoa);
        }

        public PoolsBensRentabilidadePerformance InsertPoolsBensRentabilidadePerformance(PoolsBensRentabilidadePerformance pools)
        {
            return _poolBensServices.InsertPoolsBensRentabilidadePerformance(pools);
        }

        public bool DesativarAtivarPoolsBensRentabilidadePerformance(int id, bool flgAtivo)
        {
            return _poolBensServices.DesativarAtivarPoolsBensRentabilidadePerformance(id, flgAtivo);
        }

        public IEnumerable<PoolsBensRentabilidadePerformance> GetListPoolsBensRentabilidadePerformance(int idPoolBem)
        {
            return _poolBensServices.GetListPoolsBensRentabilidadePerformance(idPoolBem);
        }
    }
}
