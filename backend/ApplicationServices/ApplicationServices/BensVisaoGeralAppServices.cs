﻿using ApplicationServices.IApplicationServices;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class BensVisaoGeralAppServices : IBensVisaoGeralAppServices
    {

        private readonly IBensVisaoGeralServices _bensVisaoGeralServices;

        public BensVisaoGeralAppServices(IBensVisaoGeralServices bensVisaoGeralServices)
        {
            _bensVisaoGeralServices = bensVisaoGeralServices;
        }
        public Domain.Entities._2BTrust.BensVisaoGeral InsertBensVisaoGeral(Domain.Entities._2BTrust.BensVisaoGeral bem)
        {
            return _bensVisaoGeralServices.InsertBensVisaoGeral(bem);
        }

        public Domain.Entities._2BTrust.BensVisaoGeral UpdateBensVisaoGeral(Domain.Entities._2BTrust.BensVisaoGeral bem)
        {
            return _bensVisaoGeralServices.UpdateBensVisaoGeral(bem);
        }

        public Domain.Entities._2BTrust.BensVisaoGeral GetBensVisaoGeralById(int id)
        {
            return _bensVisaoGeralServices.GetBensVisaoGeralById(id);
        }

        public IEnumerable<Domain.Entities._2BTrust.BensVisaoGeral> GetListBensVisaoGeral()
        {
            return _bensVisaoGeralServices.GetListBensVisaoGeral();
        }

        public IEnumerable<Domain.Entities._2BTrust.BensVisaoGeralImagens> GetListImagens(int idBem)
        {
            return _bensVisaoGeralServices.GetListImagens(idBem);
        }

        public Domain.Entities._2BTrust.BensVisaoGeralImagens InsertImagens(Domain.Entities._2BTrust.BensVisaoGeralImagens bemImagem)
        {
            return _bensVisaoGeralServices.InsertImagens(bemImagem);
        }

        public Domain.Entities._2BTrust.BensVisaoGeralImagens UpdateImagens(Domain.Entities._2BTrust.BensVisaoGeralImagens bemImagem)
        {

            return _bensVisaoGeralServices.UpdateImagens(bemImagem);
        }
    }
}
