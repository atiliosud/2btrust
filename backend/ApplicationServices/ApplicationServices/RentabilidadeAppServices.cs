﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class RentabilidadeAppServices : IRentabilidadeAppServices
    {

        
        private readonly IRentabilidadeServices _rentabilidadeServices;
        public RentabilidadeAppServices(IRentabilidadeServices  rentabilidadeServices)
        {
            _rentabilidadeServices = rentabilidadeServices;
        }
        public Rentabilidade InsertRentabilidade(Rentabilidade luc)
        {
            return _rentabilidadeServices.InsertRentabilidade(luc);
        }

        public Rentabilidade UpdateRentabilidade(Rentabilidade luc)
        {
            return _rentabilidadeServices.UpdateRentabilidade(luc);
        }

        public Rentabilidade GetRentabilidadeByIdBem(int idBem)
        {
            return _rentabilidadeServices.GetRentabilidadeByIdBem(idBem);
        }

        public IEnumerable<Rentabilidade> GetListRentabilidadeByIdBem(int idBem)
        {
            return _rentabilidadeServices.GetListRentabilidadeByIdBem(idBem);
        }

        public Rentabilidade GetRentabilidadeById(int id)
        {

            return _rentabilidadeServices.GetRentabilidadeById(id);
        }


    }
}
