﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class PessoasDocumentosAppServices : IPessoasDocumentosAppServices
    {
        private readonly IPessoasDocumentosServices _pessoaDocumentoServices;

        public PessoasDocumentosAppServices(IPessoasDocumentosServices pessoaDocumentoServices)
        {
            _pessoaDocumentoServices = pessoaDocumentoServices;
        }

        public IEnumerable<PessoasDocumentos> GetListPessoasDocumentosByIdPessoa(int idPessoa)
        {
            return _pessoaDocumentoServices.GetListPessoasDocumentosByIdPessoa(idPessoa);
        }

        public PessoasDocumentos InsertPessoasDocumentos(PessoasDocumentos pes)
        {
            return _pessoaDocumentoServices.InsertPessoasDocumentos(pes);
        }

        public PessoasDocumentos UpdatePessoasDocumentos(PessoasDocumentos pes)
        {
            return _pessoaDocumentoServices.UpdatePessoasDocumentos(pes);
        }
    }
}
