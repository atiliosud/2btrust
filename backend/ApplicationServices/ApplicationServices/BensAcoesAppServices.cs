﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class BensAcoesAppServices : IBensAcoesAppServices
    {
          private readonly IBensAcoesServices _BensAcoesServices;

          public BensAcoesAppServices(IBensAcoesServices BensAcoesServices)
        {
            _BensAcoesServices = BensAcoesServices;
        }

        public BensAcoes InsertBensAcoes(BensAcoes bemAcao)
        {
            return _BensAcoesServices.InsertBensAcoes(bemAcao);
        }

        public BensAcoes UpdateBensAcoes(BensAcoes bemAcao)
        {
            return _BensAcoesServices.UpdateBensAcoes(bemAcao);
        }

        public BensAcoes GetBensAcoesByIdBem(int idBem)
        {
            return _BensAcoesServices.GetBensAcoesByIdBem(idBem);
        }

        public BensAcoes GetBensAcoesById(int id)
        {
            return _BensAcoesServices.GetBensAcoesById(id);
        }
    }
}
