﻿using ApplicationServices.IApplicationServices;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class ControlesAppServices : IControlesAppServices
    {
        private readonly IControlesServices _controleServices;

        public ControlesAppServices(IControlesServices  controleServices)
        {
            _controleServices = controleServices;
        }

        public IEnumerable<Domain.Entities._2BTrust.Controles> GetControlesByIdGrupo(int idGrupo)
        {
            return _controleServices.GetControlesByIdGrupo(idGrupo);
        }


        public IEnumerable<Domain.Entities._2BTrust.Controles> GetListControles()
        {
            return _controleServices.GetListControles();
        }

        public Domain.Entities._2BTrust.Controles InsertControles(Domain.Entities._2BTrust.Controles Controles)
        {
            return _controleServices.InsertControles(Controles);
        }

        public Domain.Entities._2BTrust.Controles GetControlesById(int idAcoes)
        {
            return _controleServices.GetControlesById(idAcoes);
        }

        public Domain.Entities._2BTrust.Controles UpdateControles(Domain.Entities._2BTrust.Controles Controles)
        {
            return _controleServices.UpdateControles(Controles);
        }


        public IEnumerable<Domain.Entities._2BTrust.Controles> GetListControlesPai()
        {
            return _controleServices.GetListControlesPai();
        }
    }
}
