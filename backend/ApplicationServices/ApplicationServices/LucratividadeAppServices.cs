﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class LucratividadeAppServices : ILucratividadeAppServices
    {
         private readonly ILucratividadeServices _lucratividadeServices;

         public LucratividadeAppServices(ILucratividadeServices lucratividadeServices)
        {
            _lucratividadeServices =  lucratividadeServices;
        }

        public Lucratividade InsertLucratividade(Lucratividade luc)
        {
            return _lucratividadeServices.InsertLucratividade(luc);
        }

        public Lucratividade UpdateLucratividade(Lucratividade luc)
        {
            return _lucratividadeServices.UpdateLucratividade(luc);
        }

        public Lucratividade GetLucratividadeByIdBem(int idBem)
        {
            return _lucratividadeServices.GetLucratividadeByIdBem(idBem);
        }

        public IEnumerable< Lucratividade> GetListLucratividadeByIdBem(int idBem)
        {
            return _lucratividadeServices.GetListLucratividadeByIdBem(idBem);
        }

        public Lucratividade GetLucratividadeById(int id)
        {
            return _lucratividadeServices.GetLucratividadeById(id);
        }

    }
}
