﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class OrgaosEmissoresAppServices : IOrgaosEmissoresAppServices
    {
       private readonly IOrgaosEmissoresServices _orgaosEmissoresServices;
       public OrgaosEmissoresAppServices(IOrgaosEmissoresServices  orgaosEmissoresServices)
        {
            _orgaosEmissoresServices = orgaosEmissoresServices;
        }
        public IEnumerable<OrgaosEmissores> GetListOrgaosEmissoresByIdEstado(int idEstado)
        {
            return _orgaosEmissoresServices.GetListOrgaosEmissoresByIdEstado(idEstado);
        }
    }
}
