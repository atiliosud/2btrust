﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class CidadesAppServices : ICidadesAppServices
    {
        private readonly ICidadesServices _cidadeServices;
        public CidadesAppServices(ICidadesServices cidadeServices)
        {
            _cidadeServices =  cidadeServices;
        }

        public IEnumerable<Cidades> GetListCidadesByIdEstado(int idEstado)
        {
            return _cidadeServices.GetListCidadesByIdEstado(idEstado);
        }

        public  Cidades GetListCidadesByIdCidade(int idCidade)
        {
            return _cidadeServices.GetListCidadesByIdCidade(idCidade);
        }

        public  Cidades InsertCidade(Cidades cid)
        {
            return _cidadeServices.InsertCidade(cid);
        }
    }
}
