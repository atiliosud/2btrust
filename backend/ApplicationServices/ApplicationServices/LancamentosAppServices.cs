﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class LancamentosAppServices : ILancamentosAppServices
    {
        private readonly ILancamentosServices _lancamentosServices;
        public LancamentosAppServices(ILancamentosServices  lancamentosServices)
        {
            _lancamentosServices = lancamentosServices;
        }

        public Domain.Entities._2BTrust.Lancamentos InsertLancamentos(Domain.Entities._2BTrust.Lancamentos con)
        {
            return _lancamentosServices.InsertLancamentos(con);
        }

        public Domain.Entities._2BTrust.Lancamentos UpdateLancamentos(Domain.Entities._2BTrust.Lancamentos con)
        {
            return _lancamentosServices.UpdateLancamentos(con);
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetListLancamentos()
        {
            return _lancamentosServices.GetListLancamentos();
        }

        public Domain.Entities._2BTrust.Lancamentos GetLancamentosById(int id)
        {
            return _lancamentosServices.GetLancamentosById(id);
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetLancamentosByIdPessoa(int idPessoa)
        {
            return _lancamentosServices.GetLancamentosByIdPessoa(idPessoa);
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetLancamentosByIdPoolBem(int idPoolBem)
        {
            return _lancamentosServices.GetLancamentosByIdPoolBem(idPoolBem);
        }

        public IEnumerable<PessoaPoolsInvestidos> GetPessoasPoolsInvestido()
        {
            return _lancamentosServices.GetPessoasPoolsInvestido();
        }

        public PessoaPoolsInvestidos GetPessoasPoolsInvestido(int idPoolBemPessoa)
        {
          return _lancamentosServices.GetPessoasPoolsInvestido(idPoolBemPessoa);
        }
    }
}
