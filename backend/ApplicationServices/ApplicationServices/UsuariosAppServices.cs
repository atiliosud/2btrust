﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class UsuariosAppServices : IUsuariosAppServices
    {
        private readonly IUsuariosServices _usuariosServices;
        public UsuariosAppServices(IUsuariosServices  usuariosServices)
        {
            _usuariosServices = usuariosServices;
        }

        public Usuarios LoginUsuario(string sLogin, string sSenha)
        {
            return _usuariosServices.LoginUsuario(sLogin, sSenha);
        }

        public bool ValidaUsuarioPlataforma(int idUsuario, bool isValid)
        {
            return _usuariosServices.ValidaUsuarioPlataforma(idUsuario, isValid);
        }
    }
}
