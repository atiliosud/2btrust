﻿using ApplicationServices.IApplicationServices;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class AreasAppServices : IAreasAppServices
    {
        private readonly IAreasServices _areasServices;

        public AreasAppServices(IAreasServices  areasServices)
        {
            _areasServices = areasServices;
        }

        public IEnumerable<Domain.Entities._2BTrust.Areas> GetListAreas()
        {
            return _areasServices.GetListAreas();
        }

        public Domain.Entities._2BTrust.Areas InsertAreas(Domain.Entities._2BTrust.Areas area)
        {
            return _areasServices.InsertAreas(area);
        }

        public Domain.Entities._2BTrust.Areas GetAreasById(int idArea)
        {
            return _areasServices.GetAreasById(idArea);
        }

        public Domain.Entities._2BTrust.Areas UpdateAreas(Domain.Entities._2BTrust.Areas area)
        {
            return _areasServices.UpdateAreas(area);
        }
    }
}
