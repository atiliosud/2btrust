﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class PoolBensPessoasAppServices : IPoolBensPessoasAppServices
    {
        private readonly IPoolBensPessoasServices _poolBensPessoaServices;


        public PoolBensPessoasAppServices(IPoolBensPessoasServices poolBensPessoaServices)
        {
            _poolBensPessoaServices = poolBensPessoaServices;
        }

        public PoolBensPessoas InsertPoolBensPessoas(PoolBensPessoas con)
        {
            return _poolBensPessoaServices.InsertPoolBensPessoas(con);
        }

        public PoolBensPessoas UpdatePoolBensPessoas(PoolBensPessoas con)
        {
            return _poolBensPessoaServices.UpdatePoolBensPessoas(con);
        }

        public IEnumerable<PoolBensPessoas> GetListPoolBensPessoas()
        {
            return _poolBensPessoaServices.GetListPoolBensPessoas();
        }

        public PoolBensPessoas GetPoolBensPessoasById(int id)
        {
            return _poolBensPessoaServices.GetPoolBensPessoasById(id);
        }

        public IEnumerable<PoolBensPessoas> GetPoolBensPessoasByIdPessoa(int idPessoa)
        {
            return _poolBensPessoaServices.GetPoolBensPessoasByIdPessoa(idPessoa);
        }

        public IEnumerable<PoolBensPessoas> GetPoolBensPessoasByIdPoolBem(int idPoolBem)
        {
            return _poolBensPessoaServices.GetPoolBensPessoasByIdPoolBem(idPoolBem);
        }

        public IEnumerable< Pessoas>  GetListValidaVendaCota()
        {

            return _poolBensPessoaServices.GetListValidaVendaCota( );
        }

        public IEnumerable< Pessoas>  GetListValidaCompraCota()
        {
            return _poolBensPessoaServices.GetListValidaCompraCota( );
        }

        public IEnumerable< Pessoas>  GetListValidaRetirada()
        {
            return _poolBensPessoaServices.GetListValidaRetirada( );
        }
    }
}
