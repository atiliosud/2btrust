﻿using ApplicationServices.IApplicationServices;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class EstadosAppServices : IEstadosAppServices
    {
        private readonly IEstadosServices _estadosServices;
        public EstadosAppServices(IEstadosServices  estadosServices)
        {
            _estadosServices = estadosServices;
        }
        public IEnumerable<Domain.Entities._2BTrust.Estados> GetListEstados()
        {
            return _estadosServices.GetListEstados();
        }
    }
}
