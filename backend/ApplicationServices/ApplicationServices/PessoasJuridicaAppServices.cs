﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationServices.ApplicationServices
{
    public class PessoasJuridicaAppServices : IPessoasJuridicaAppServices
    {
        private readonly IPessoasJuridicaServices _pessoasJuridicaServices;
        public PessoasJuridicaAppServices(IPessoasJuridicaServices pessoasJuridicaServices)
        {
            _pessoasJuridicaServices = pessoasJuridicaServices;
        }


        public PessoasJuridica GetPessoasJuridicasByIdPessoa(int IdPessoa)
        {
            return _pessoasJuridicaServices.GetPessoasJuridicasByIdPessoa(IdPessoa);
        }

        public  PessoasJuridica InsertPessoasJuridicasByIdPessoa(PessoasJuridica pes)
        {
            return _pessoasJuridicaServices.InsertPessoasJuridicasByIdPessoa(pes);
        }

        public  PessoasJuridica UpdatePessoasJuridicasByIdPessoa( PessoasJuridica pes)
        {
            return _pessoasJuridicaServices.UpdatePessoasJuridicasByIdPessoa(pes);
        }


    }
}
