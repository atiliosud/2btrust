﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ObjetoContainerConteudoLabel
    {
        public int idContainerConteudoLabel { get; set; }
        public int idContainerConteudo { get; set; }
        public int idDataSource { get; set; }
        public string campoDescricao { get; set; }
        public string campoRetorno { get; set; }
        public bool flagAtivo { get; set; }
        public int idUsuarioCadastro { get; set; }
        public DateTime dataCadastro { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public DateTime dataAlteracao { get; set; }


        public DataSourceObjeto DataSource { get; set; }

        public List<IntegracaoFiltroDinamico> ListaFiltro { get; set; }

    }
}
