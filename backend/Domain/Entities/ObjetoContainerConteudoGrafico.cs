﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
  public  class ObjetoContainerConteudoGrafico
    {
      
      public int idContainerConteudoGrafico { get; set; }
      
      public int idContainerConteudo { get; set; }
      
      public int idDataSource { get; set; }
      
      public int iPeriodoGrafico { get; set; }
      
      public int iMesConsulta { get; set; }
      
      public string campoRetorno { get; set; }

    }
}
