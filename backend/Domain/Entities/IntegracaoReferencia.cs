﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class IntegracaoReferencia
    {
        public int id { get; set; }
        public string NOME_TABELA_PRINCIPAL { get; set; }
        public string CAMPO_TABELA_PRINCIPAL { get; set; }
        public string NOME_TABELA_REFERENCIA { get; set; }
        public string CAMPO_TABELA_REFERENCIA { get; set; }
        public string TIPO_CAMPO { get; set; }
        public string REPLACE { get; set; }

    }
}
