﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ObjetoPainelClinico
    {

        public int idPainelClinico { get; set; }
        
        public int idDataSource { get; set; }

        public DataSourceObjeto DataSource { get; set; }

        public EnumTipoQuery iTipoConsultaPrimeiro { get; set; }

        public EnumTipoQuery iTipoConsultaSegundo { get; set; }

        public int Meta1 { get; set; }

        public int Meta2 { get; set; }

        public string TituloPrimeiro { get; set; }

        public string TituloSegundo { get; set; }

        public string SubTituloPrimeiro { get; set; }

        public string SubTituloSegundo { get; set; }

        public string CorFundoPrimeiro { get; set; }

        public string CorFundoSegundo { get; set; }

        public string CorBarraPrimeiro { get; set; }

        public string CorBarraSegundo { get; set; }

        public List<string> sCampoRetornoPrimeiro { get; set; }

        public List<string> sCampoRetornoSegundo { get; set; }

        public string sCampoPrimeiro { get; set; }
                
        public string sCampoSegundo { get; set; }


        public string CampoFiltroPrimeiro { get; set; }

        public string CampoFiltroSegundo { get; set; }
        
        public bool FlagAtivo { get; set; }
        
        public int idUsuarioCadastro { get; set; }
        
        public DateTime DataCadastro { get; set; }
        
        public int idUsuarioAlteracao { get; set; }

        public DateTime DataAlteracao { get; set; }

        //lista de filtros
        public List<IntegracaoFiltroDinamico> ListaFiltro01 { get; set; }
        public List<IntegracaoFiltroDinamico> ListaFiltro02 { get; set; }
    }
}
