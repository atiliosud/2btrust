﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Perfil_X_Menu
    {

        public int idPerfilMenu { get; set; }
        public int idMenu { get; set; }
        public int idPerfil { get; set; }
        public bool FlagAtivo { get; set; }
        public int idUsuarioCadastro { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }

        public Menu menu { get; set; }
    }
}
