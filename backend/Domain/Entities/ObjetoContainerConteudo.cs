﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ObjetoContainerConteudo
    {
        public int idContainerConteudo { get; set; }

        public DataSourceObjeto dataSourceObjeto { get; set; }

        public int idContainer { get; set; }


        public int idDataSource { get; set; }

        public int idDataSource2 { get; set; }

        public int idDataSource3{ get; set; }


        public EnumTipoQuery iTipoConsulta { get; set; }

        public int iTipoConteudo { get; set; }

        public string descricao { get; set; }

        public string icon { get; set; }

        public string iconColor { get; set; }



        public EnumMeses iMesConsulta { get; set; }

        public EnumMeses iMesConsulta2 { get; set; }

        public EnumMeses iMesConsulta3 { get; set; }




        public EnumTipoGrafico iTipoGrafico { get; set; }



        public EnumPeriodoGrafico iPeriodoGrafico { get; set; }

        public EnumPeriodoGrafico iPeriodoGrafico2 { get; set; }

        public EnumPeriodoGrafico iPeriodoGrafico3 { get; set; }



        public string campoDescricao { get; set; }

        public int iOrderm { get; set; }


        public List<string> campoRetornoLi { get; set; }

        public string campoRetorno { get; set; }

        public string campoRetorno2 { get; set; }

        public string campoRetorno3 { get; set; }



        public int idUsuarioCadastro { get; set; }

        public DateTime dataCadastro { get; set; }

        public int idUsuarioAlteracao { get; set; }

        public DateTime dataAlteracao { get; set; }

        public bool flagAtivo { get; set; }

        public SP_Columns ColunasTabela { get; set; }

        public string CorFundo { get; set; }

        public bool flagGrafico { get; set; }


        public List<ObjetoContainerConteudoLabel> ListaLabels { get; set; }

        public List<IntegracaoFiltroDinamico> ListaFiltro { get; set; }

        public EnumTipoConteudoContainer tipoConteudoContainer { get; set; }
    }
}
