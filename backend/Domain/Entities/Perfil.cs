﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Perfil
    {
        public int idPerfil { get; set; }
        public string Descricao { get; set; }
        public string Observacao { get; set; }
        public bool FlagAtivo { get; set; }
        public int idUsuarioCadastro { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }

        public IEnumerable<Perfil_X_Menu> PerfilXMenu { get; set; }
    }
}
