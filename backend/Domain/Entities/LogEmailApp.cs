﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class LogEmailApp
    {
        public int Id { get; set; }
        public int IdPessoa { get; set; }
        public string Titulo { get; set; }
        public string Mensagem { get; set; }
        public DateTime DataEnvio { get; set; }
        public bool FlagAtivo { get; set; }
    }
}
