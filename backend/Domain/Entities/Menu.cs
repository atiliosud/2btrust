﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Menu
    {
        public int idMenu { get; set; }
        public int idMenuPai { get; set; }
        public string Descricao { get; set; }
        public string Observacao { get; set; }
        public bool FlagAtivo { get; set; }
        public int idUsuarioCadastro { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public string classCss { get; set; }
        public string idJqueryPai { get; set; }
        public string idJqueryFilho { get; set; }
        public string iconLI { get; set; }
        public string urlController { get; set; }
        public string Controller { get; set; }

        public Menu MenuPai { get; set; }
    }
}
