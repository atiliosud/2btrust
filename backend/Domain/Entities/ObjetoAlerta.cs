﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
   public class ObjetoAlerta
    {

        public int idObjetoAlerta { get; set; }
        public int idDataSourceObjeto { get; set; }
        public string descricao { get; set; }
        public string campoRetorno { get; set; }
        public string icon { get; set; }
        public EnumPrioridadeAlerta tipoPrioridade { get; set; }
        public int valorMin { get; set; }
        public int valorMax { get; set; }
        public string observacao { get; set; }
        public bool flagAtivo { get; set; }
        public DateTime dataCadastro { get; set; }
        public int idUsuarioCadastro { get; set; }
        public DateTime dataAlteracao { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public int iTamanhoAlerta { get; set; }
        public DataSourceObjeto dataSourceObjeto { get; set; }

        public string corValorMin { get; set; } 
        public string corValorIntermediario { get; set; } 
        public string corValorMax { get; set; }



        public string iconColor { get; set; }
    }
}
