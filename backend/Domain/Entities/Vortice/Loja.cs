﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Vortice
{
    public class Loja
    {

        public int id { get; set; }

        public int idLojaAntiga { get; set; }

        public string sNome { get; set; }

        public int iCodigo { get; set; }

        public string sNomeAgencia { get; set; }

        public string sCodigoAgencia { get; set; }

        public string sEmailAgencia { get; set; }

        public DateTime dtInicioOperacao { get; set; }

    }
}