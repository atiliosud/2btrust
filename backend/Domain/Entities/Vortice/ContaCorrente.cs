﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportacaoExcel_VorticeIndicadores.Entities
{
   public class ContaCorrente
    {
        public int Codigo { get; set; }
        public string Quiosque { get; set; }
        public int QtdTotal { get; set; }
        public int QtdAposentados { get; set; }
        public int QtdItinerante { get; set; } 
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int MesInicio { get; set; }
        public int AnoInicio { get; set; }
        public int MesReferencia { get; set; }
        public int MesInicioItinerante { get; set; }
        public int AnoInicioItinerante { get; set; }
        public int MesInicioAposentado { get; set; }
        public int AnoInicioAposentado { get; set; }


        public int QtdTotalHistorico { get; set; }


        public int QtdAposentadosHistorico { get; set; }


        public int QtdItineranteHistorico { get; set; }

    }
}
