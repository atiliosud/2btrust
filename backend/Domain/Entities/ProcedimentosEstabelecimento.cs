﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ProcedimentosEstabelecimento
    {
        public int idEstabelecimento { get; set; }
        public string NomeEstabelecimento { get; set; }
        public int idUnidade { get; set; }
        public string NomeUnidade { get; set; }
        public DateTime Data { get; set; }
        public string sData { get; set; }
        public string Procedimento { get; set; }
        public int Qnt { get; set; }
    }

    public class UnidadeProcedimentoHelper
    {
        public int idUnidade { get; set; }
        public string NomeUnidade { get; set; }
        public int QntDiaCorrente { get; set; }
        public int QntMesCorrente { get; set; }

        public IEnumerable<ProcedimentosEstabelecimento> ListaProcedimento { get; set; }

    }
}
