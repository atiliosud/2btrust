﻿using ImportacaoExcel_VorticeIndicadores.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class DemonstrativoProducao
    {

        public Transacao _Transacao { get; set; }
        public Lime _Lime { get; set; }
        public ContaCorrente _ContaCorrente { get; set; }
        public CestaServico _CestaServico { get; set; }
        public CartaoCredito _CartaoCredito { get; set; }


    }
}
