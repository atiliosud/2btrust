﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.EYE
{
    public class EYE_Clinicas
    {
        public int QUIOSQUE_ID { get; set; }
        public string QUIOSQUE_NOME { get; set; }
        public string QUIOSQUE_FLAGDELETADO { get; set; }
    }
}