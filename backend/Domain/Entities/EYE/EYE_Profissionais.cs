﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.EYE
{
    public class EYE_Profissionais
    {

        public int PROFISSIONAL_ID { get; set; }
        public string PROFISSIONAL_NOME { get; set; }
        public string PROFISSIONAL_CPF { get; set; }
        public string PROFISSIONAL_STATUS { get; set; }
        public string PROFISSIONAL_SIGLACONCELHO { get; set; }
        public string PROFISSIONAL_IDCLINICA { get; set; }
        public string PROFISSIONAL_ESPECS { get; set; }
        public string CARGA_HORARIA { get; set; }
        public string PROFISSIONAL_DTINGRESSO { get; set; }

    }
}
