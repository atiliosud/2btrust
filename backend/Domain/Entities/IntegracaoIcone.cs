﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class IntegracaoIcone
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Html { get; set; }
        public bool FlagAtivo { get; set; }

    }
}
