﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ObjetoContainer
    {
        public int idContainer { get; set; }
        public int idMenu { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public int iTamanho { get; set; }
        public int iTemplate { get; set; }
        public bool flagAtivo { get; set; }
        public DateTime dataCadastro { get; set; }
        public int idUsuarioCadastro { get; set; }
        public DateTime dataAlteracao { get; set; }
        public int idUsuarioAlteracao { get; set; }
      
        public Menu MenuContainer { get; set; }
        public IEnumerable<ObjetoContainerConteudo> ListaConteudo { get; set; }

        public int iPosicaoBD { get; set; }
        public int iPosicao { get; set; }
        public string CorFundoTitulo { get; set; }
    }
}
