﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class PacienteSexo
    {
        public int iMasculino { get; set; }
        public int iFeminino { get; set; }
        public int iIndefinido { get; set; }
        public int iNenhum { get; set; }

        public decimal iPorMasculino { get; set; }
        public decimal iPorFeminino { get; set; }
        public decimal iPorIndefinido { get; set; }
        public decimal iPorNenhum { get; set; }
    }

    public class PacienteSexoTable
    {
        public  int pes_idSexo { get; set; }
        public int Qtd { get; set; }
        public string SEX_Descricao { get; set; }
    }
}
