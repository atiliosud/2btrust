﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class IntegracaoFiltroDinamico
    {
        public int IdFiltroDinamico { get; set; }
        public int idContainerConteudo { get; set; }    
        public int idContainerConteudoLabel { get; set; }
        public int idPainelClinico1 { get; set; }
        public int idPainelClinico2 { get; set; }

        public string CampoFiltro { get; set; }
        public EnumVariavelValorFiltro VariavelFiltro { get; set; }
        public string ValorVariavelFiltro { get; set; }
        public EnumOperadorFiltro Operador { get; set; }

        public bool flagAtivo { get; set; }
        public int idUsuarioCadastro { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public DateTime dataAlteracao { get; set; }
        public DateTime dataCadastro { get; set; }
    }
}
