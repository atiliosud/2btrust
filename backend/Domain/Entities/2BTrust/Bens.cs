﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class BensBaseCalculoHistorico
    {
        public int Id { get; set; }
        public int IdBem { get; set; }
        public int iMes { get; set; }
        public int iAno { get; set; }
        public string Rent { get; set; }
        public string Hoa { get; set; }
        public string PropertyTaxProvision { get; set; }
        public string PropertyManagement { get; set; }
        public string AdministrationFee { get; set; }
        public string DividendYeld { get; set; }
        public string OutrasDespesas { get; set; }
        public string ObservacaoOutras { get; set; }
        public string percentRentabilidade { get; set; }
        public string performance { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public int? IdUsuarioAlteracao { get; set; }
        public DateTime? DtAlteracao { get; set; }

    }


    public class Bens
    {

        public int Id { get; set; }



        public int IdPool { get; set; }


        public string Nome { get; set; }

        public string Descritivo { get; set; }



        public string CapitalInvestido { get; set; }
        public string Custos { get; set; }
        public DateTime? DtReferenciaCusto { get; set; }

        public int iAno { get; set; }
        public int iMes { get; set; }

        public string Rent { get; set; }
        public string Hoa { get; set; }
        public string PropertyTaxProvision { get; set; }
        public string PropertyManagement { get; set; }
        public string AdministrationFee { get; set; }
        public string DividendYeld { get; set; }

        public int IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public PoolBens _Pool { get; set; }
        public BensEnderecos _Endereco { get; set; }
        public Lucratividade _Lucratividade { get; set; }
        public Rentabilidade _Rentabilidade { get; set; }
        public IEnumerable<BensImagens> _Imagens { get; set; }
        public IEnumerable<BensBaseCalculoHistorico> _BensBaseCalculoHistorico { get; set; }

        public IEnumerable<BensGastos> _BensGastos { get; set; }

        public string OutrasDespesas { get; set; }
        public string ObservacaoOutras { get; set; }
        public string percentRentabilidade { get; set; } 
        public string performance { get; set; }

        public EnumTipoBem TipoBem { get; set; }
        public BensAcoes _BensAcao { get; set; }
        public IEnumerable<BensAcoes> _BensAcoes { get; set; }
    }


    public class BensImagens
    {
        public int Id { get; set; }
        public int IdBem { get; set; }
        public string PathLocationIIS { get; set; }
        public string PathLocation { get; set; }
        public bool FlagAtivo { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public int IdUsuarioAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }
    }
}
