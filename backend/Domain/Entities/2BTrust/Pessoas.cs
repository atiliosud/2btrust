﻿using System;
using System.Collections.Generic; 

namespace Domain.Entities._2BTrust
{
    public class PessoasGenricList
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public DateTime DataCadastro { get; set; }

        public bool FlagResidenteAmericado { get; set; }

        public string Email { get; set; }

        public int IdGrupo { get; set; }

    }

    public class Pessoas
    {
        public ValidToken _ValidToken;
        public IEnumerable<Lancamentos> _MeusLancamentos { get; set; } 

        public int Id { get; set; }
        public string MaxBarProjection { get; set; }



        public string Nome { get; set; }

        public DateTime DtNascimento { get; set; }


        public int IdStatus { get; set; }
        public bool FlagResidenteAmericado { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }

        public string TelefoneCelular { get; set; }
        public string TelefoneComercial { get; set; }
        public string ValorInicial { get; set; }
        public bool fCondicao1 { get; set; }
        public bool fCondicao2 { get; set; }
        public bool fCondicao3 { get; set; }
        public bool fCondicao4 { get; set; }
        public bool fCondicao5 { get; set; }
        public bool fCondicao6 { get; set; }

        public bool fConfirmoDados { get; set; }

        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public Usuarios _Usuario { get; set; }

        public IEnumerable<PessoasDocumentos> _Documentos { get; set; }

        public PessoasEnderecos _Endereco { get; set; }

        public PessoasFisicas _PessoaFisica { get; set; }

        public PessoasJuridica _PessoaJuridica { get; set; }

        public Carteira _Carteira { get; set; }

        public IEnumerable<PoolBensPessoas> _MeusPools { get; set; }

        public PoolBensPessoas _ObjMeusPools { get; set; }

        public IEnumerable<PainelRentabilidadeMes> _ListaRentabilidade { get; set; }

        public InformacoesAdminHome _InformacoesAdminHome { get; set; }


    }
}
