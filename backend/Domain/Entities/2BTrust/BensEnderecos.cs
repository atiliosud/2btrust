﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
  public class BensEnderecos

{

    public int Id { get; set; }



    public int IdBem { get; set; }



    public int IdTpEndereco { get; set; }



    public int IdCidade { get; set; }



    public string Bairro { get; set; }
    public string Cidade { get; set; }
    public string Estado { get; set; }



    public string Logradouro { get; set; }



    public string Numero { get; set; }



    public string Complemento { get; set; }



    public string Cep { get; set; }



    public int IdUsuarioInclusao { get; set; }



    public DateTime DtInclusao { get; set; }



    public int? IdUsuarioAlteracao { get; set; }



    public DateTime? DtAlteracao { get; set; }

    public Cidades _Cidade { get; set; }

    public string sCidade { get; set; }
    public string sEstado { get; set; }


    public string sUF { get; set; }
}

}
