﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class Usuarios
    {

        public int Id { get; set; }


        public int IdPessoa { get; set; }

        public string Nome { get; set; }



        public string Email { get; set; }



        public bool Admin { get; set; }



        public bool Bloqueado { get; set; }



        public string Saltkey { get; set; }



        public int? IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }

         

        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }
        public bool? CadastroValido { get; set; }


        public DateTime? DtAlteracao { get; set; }

        public Logins _Login { get; set; }

        public Grupos _Grupo { get; set; }

        public UsuariosGrupos _UsuarioGrupo { get; set; }

        public IEnumerable<Controles> _Controles { get; set; }

        

    }

}
