﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class PessoasDocumentos

    {

        public int Id { get; set; }

        public int IdPessoa { get; set; }



        public int IdDocumento { get; set; }



        public string Description { get; set; }
        public int IdOrgaoEmissor { get; set; }

        public bool FlagAtivo { get; set; }

        public string Conteudo { get; set; }
        public string Descricao { get; set; }

        public Documentos _Documento { get; set; }

        public OrgaosEmissores _OrgaoEmissor { get; set; }

    }

}
