﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class ComprovanteRetirada
    {
        public int Id { get; set; }
        public int IdPessoa { get; set; }
        public int IdCarteira { get; set; }
        public string NomeCliente { get; set; }
        public string EmailCliente { get; set; }
        public string EmailCode { get; set; }
        public string ComprovantePathLocation { get; set; }
        public string ComprovantePathLocationIIS { get; set; }
        public bool FlagPendente { get; set; }
        public bool FlagNegado { get; set; }
        public bool FlagComputado { get; set; }
        public EnumStatusCarteira IdStatus { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public DateTime? DtInclusaoAdm { get; set; }
        public int IdUsuarioAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public DateTime DtAprovaReprova { get; set; }
        public decimal Valor { get; set; }

        public string SwiftCode { get; set; }
        public bool FlagAtivoSwiftCode { get; set; }
    }
}
