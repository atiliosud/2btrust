﻿ 

namespace Domain.Entities._2BTrust
{
    public class PessoaInvestimentoPoolsValores
    {
        public int idPessoa { get; set; }
        public string NomeCliente { get; set; }
        public string NomePool { get; set; }
        public decimal SaldoCarteira { get; set; }
        public decimal ValorInvestido { get; set; }
        public decimal DividendYeld { get; set; }

        public int Cotas { get; set; }
    }
}