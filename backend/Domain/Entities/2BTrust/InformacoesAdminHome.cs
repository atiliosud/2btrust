﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class InformacoesAdminHome
    {
        public decimal SaldoTotalClienteEmCarteira { get; set; }
        public decimal SaldoTotalClienteEmInvestimentos_e_Carteira { get; set; }
        public decimal SaldoTotalClienteEmInvestimentos { get; set; }
    }

    public class HelperInformacoes_Pools
    {
        public int IdPool { get; set; }
        public decimal ValorCota { get; set; }
    }
}
