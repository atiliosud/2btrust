﻿ 
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Domain.Entities._2BTrust
{
   public  class RecoveryPassword
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public string Token { get; set; }
        public DateTime dtSolicitacao { get; set; }
        public bool FlagValido { get; set; }



    }
}