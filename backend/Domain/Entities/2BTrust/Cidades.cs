﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class Cidades
    {

        public int Id { get; set; }



        public int IdEstado { get; set; }
        public Estados _Estado { get; set; }


        public string Nome { get; set; }



        public string CodigoIBGE { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }



    }

}
