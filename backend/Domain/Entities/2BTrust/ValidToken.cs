﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{

    public class ValidToken
    {
        public int Id { get; set; }

        public int IdUsuario { get; set; }

        public string Device { get; set; }

        public string HashValid { get; set; }

        public DateTime dtAcesso { get; set; }

        public bool FlagAtivo { get; set; }

    }
}
