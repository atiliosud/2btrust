﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class PoolBens
    {

        public int Id { get; set; }

        public int CotasDisponiveis { get; set; }
         
        public int Cotas { get; set; }
        
        
        public int IdContrato { get; set; }



        public string Nome { get; set; }

        public string PathLocationIISLogoMaior { get; set; }

        public string PathLocationIISLogoMenor { get; set; }
         
        public string PathLocationIIS { get; set; }

        public string PathLocation { get; set; }

        public int iUsarLogoApp { get; set; }


        public DateTime DtInicio { get; set; }



        public DateTime? DtVigencia { get; set; }



        public int IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public Contratos _Contrato { get; set; }
        public List<Bens> _Bens { get; set; }

        public List<PoolsBensRentabilidadePerformance> _PoolsBensRentabilidadePerformance { get; set; }
        public IEnumerable<PoolBensPessoas> _MeusPools { get; set; }

         
    }

}
