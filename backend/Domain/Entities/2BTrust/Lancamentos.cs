﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
  public class Lancamentos

{

    public int Id { get; set; }



    public int IdTpLancamento { get; set; }



    public int IdPoolBem { get; set; }



    public int? IdPoolBemPessoa { get; set; }



    public DateTime DtLancamento { get; set; }



    public decimal Valor { get; set; }



    public int? IdPessoa { get; set; }



    public string Obs { get; set; }



    public bool Credito { get; set; }



    public string NumParticao { get; set; }



    public int IdUsuarioInclusao { get; set; }



    public DateTime DtInclusao { get; set; }



    public int? IdUsuarioAlteracao { get; set; }



    public DateTime? DtAlteracao { get; set; }



}

}
