﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
   public class HistoricoCarteira
    {
        public int Id { get; set; }
        public string Valores { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
    }
}
