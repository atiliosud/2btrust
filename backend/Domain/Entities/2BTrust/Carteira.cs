﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class Carteira
    {

        public int Id { get; set; }
        public int IdPessoa { get; set; }
        public decimal Saldo { get; set; }
        public DateTime DtInclusao { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int IdUsuarioAlteracao { get; set; }

        public IEnumerable<ComprovanteDeposito> _Comprovantes { get; set; }

    }
}
