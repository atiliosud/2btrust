﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{

    public class PoolBensPessoas
    {

        public int Id { get; set; }



        public int IdPoolBem { get; set; }



        public int IdPessoa { get; set; }



        public decimal Cota { get; set; }



        public int IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }
        public DateTime? DtInclusaoAdm { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public PoolBens _PoolBens { get; set; }


        public decimal mValorCotas { get; set; }
    }

}
