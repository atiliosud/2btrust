﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class Controles
    {

        public int Id { get; set; }
         

        public int? IdControlePrincipal { get; set; }


        public int IdArea { get; set; }

        public Areas _Area { get; set; }   



        public string Codigo { get; set; }



        public string Nome { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public IEnumerable<Acoes> _Acoes { get; set; }
        
        public string UrlController { get; set; }

        public string iconLI { get; set; }

        public string idJqueryPai { get; set; }

        public string idJqueryFilho { get; set; }

        public string Controller { get; set; }

        public int iOrdem { get; set; }

        public bool FlagAtivo { get; set; }
    }

}
