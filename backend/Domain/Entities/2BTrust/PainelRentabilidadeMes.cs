﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
   public class PainelRentabilidadeMes
    {
        public int idBem { get; set; }
        public int idPool { get; set; }
        public decimal DividendYeld { get; set; }
        public decimal CapitalTotalInvestido { get; set; }
        public decimal percentRentabilidade { get; set; }
        public int Cotas { get; set; }
        public int iMes { get; set; }
        public int iAno { get; set; } 
    }
}
