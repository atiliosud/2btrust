﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class BensAcoes
    {
        public int Id { get; set; }
        public int IdBem { get; set; }
        
        public string CodigoAcao { get; set; }
        public string CotacaoAtual { get; set; }
        public int QtdeCotas { get; set; }

        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public int? IdUsuarioAlteracao { get; set; }
        public DateTime? DtAlteracao { get; set; }
    }
}
