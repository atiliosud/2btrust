﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class CarteiraLog

    {
        public bool fAproved { get; set; }
        public bool fRequest { get; set; }
        public bool fReproved { get; set; }

        public int Id { get; set; }



        public int? IdPessoa { get; set; }



        public DateTime? Data { get; set; }



        public string Descricao { get; set; }



        public string Tipo { get; set; }



        public decimal? Amount { get; set; }



        public decimal? AvaliableBalance { get; set; }



        public bool? FlagAtivo { get; set; }



    }

}
