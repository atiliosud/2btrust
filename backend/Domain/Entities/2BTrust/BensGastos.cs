﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class BensGastos

    {

        public int Id { get; set; }



        public int? IdBem { get; set; }



        public string PathLocation { get; set; }



        public string PathLocationIIS { get; set; }



        public string Descricao { get; set; }



        public decimal Valor { get; set; }


        public DateTime DtOcorrencia { get; set; }


        public bool FlagAtivo { get; set; }



        public int? IdUsuarioInclusao { get; set; }



        public DateTime? DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }



    }

}
