﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
    public class BensVisaoGeral
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Descritivo { get; set; }

        public int IdUsuarioInclusao { get; set; }

        public int IdStatus { get; set; }
        public DateTime DtInclusao { get; set; }

        public int? IdUsuarioAlteracao { get; set; }

        public DateTime? DtAlteracao { get; set; }

        public IEnumerable<BensVisaoGeralImagens> _Imagens { get; set; }
    }

    public class BensVisaoGeralImagens
    {
        public int Id { get; set; }
        public int IdBemVisaoGeral { get; set; }
        public string PathLocation { get; set; }
        public string PathLocationIIS { get; set; }
        public bool FlagAtivo { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public int IdUsuarioAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string Descritivo { get; set; }
    }
}
