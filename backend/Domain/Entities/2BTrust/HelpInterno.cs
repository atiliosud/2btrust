﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities._2BTrust
{
   public class HelpInterno
    {
        public int Id { get; set; }
        public int IdPessoa { get; set; }
        public string Conteudo { get; set; }
        public string Titulo { get; set; }
        public DateTime DtInclusao { get; set; }
    }
}
