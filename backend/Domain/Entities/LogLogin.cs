﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class LogLogin
    {
        public int LOL_id { get; set; }

        public int LOL_idUsuario { get; set; }

        public string LOL_IP { get; set; }

        public int? LOL_idClinica { get; set; }

        public DateTime LOL_Data { get; set; }
    }
}
