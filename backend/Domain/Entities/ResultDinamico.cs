﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [DataContract]
    public class ResultDinamico
    {
        [DataMember(Name = "resultado")]
        public dynamic resultado { get; set; }

        [DataMember(Name = "isError")]
        public bool isError { get; set; }
    }
}
