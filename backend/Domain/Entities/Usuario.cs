﻿using Domain.Entities.EYE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Usuario
    {
        public int id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public DateTime DtInclusao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int idPerfil { get; set; }
        public bool FlagDeletado { get; set; }
        public string pathFoto { get; set; }
        public string email { get; set; }

        public Perfil perfil { get; set; }
        public EYE_Profissionais _profissional { get; set; }

    }
     
}
