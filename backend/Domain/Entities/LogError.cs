﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class LogError
    {

        public int LOG_ID { get; set; }
        public DateTime LOG_DataOcorrencia { get; set; }
        public string LOG_ControllerRepository { get; set; }
        public string LOG_Metodo { get; set; }
        public string LOG_Descricao { get; set; }
        public string LOG_InnerException { get; set; }


    }
}
