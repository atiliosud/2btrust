﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class GruposServices : IGruposServices
    {

        private readonly IGruposRepository _gruposRepository;
        public GruposServices(IGruposRepository gruposRepository)
        {
            _gruposRepository = gruposRepository;
        }


        public Grupos GetGrupoByIdUsuario(int idUsuario)
        {
            return  _gruposRepository.GetGrupoByIdUsuario(idUsuario);
        }
    }
}
