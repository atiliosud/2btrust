﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class BensServices : IBensServices
    {
        private readonly IBensRepository _bensRepository;
        private readonly IPoolBensRepository _PoolbensRepository;
        private readonly IBensEnderecoRepository _bensEnderecoRepository;
        private readonly ICidadesRepository _CidadesRepository;
        private readonly ILucratividadeRepository _LucratividadeRepository;
        private readonly IRentabilidadeRepository _RentabilidadeRepository;
        private readonly IBensAcoesRepository _bensAcoesRepository;

        public BensServices(IPoolBensRepository PoolbensRepository, IBensAcoesRepository BensAcoesRepository, ILucratividadeRepository LucratividadeRepository, IRentabilidadeRepository RentabilidadeRepository, ICidadesRepository CidadesRepository, IBensRepository bensRepository, IBensEnderecoRepository bensEnderecoRepository)
        {
            _RentabilidadeRepository = RentabilidadeRepository;
            _LucratividadeRepository = LucratividadeRepository;
            _CidadesRepository = CidadesRepository;
            _bensEnderecoRepository = bensEnderecoRepository;
            _bensRepository = bensRepository;
            _PoolbensRepository = PoolbensRepository;
            _bensAcoesRepository = BensAcoesRepository;
        }
        public Domain.Entities._2BTrust.Bens InsertBens(Domain.Entities._2BTrust.Bens bem)
        {

            var benInsert = _bensRepository.InsertBens(bem);
            if (benInsert != null)
            {
                bem.Id = benInsert.Id;
            }
            var endereco = bem._Endereco;
            if (endereco != null)
            {
                endereco.IdBem = bem.Id;
                endereco.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                endereco.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (endereco.Id > 0)
                {
                    _bensEnderecoRepository.UpdateBensEnderecos(endereco);
                }
                else
                {
                    _bensEnderecoRepository.InsertBensEnderecos(endereco);
                }
            }

            var acao = bem._BensAcao;
            if (acao != null)
            {
                acao.IdBem = bem.Id;
                acao.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                acao.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (acao.Id > 0)
                {
                    _bensAcoesRepository.UpdateBensAcoes(acao);
                }
                else
                {
                    _bensAcoesRepository.InsertBensAcoes(acao);
                }
            }

            var rentabilidade = bem._Rentabilidade;
            if (rentabilidade != null)
            {
                rentabilidade.IdBem = bem.Id;
                rentabilidade.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                rentabilidade.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (rentabilidade.Id > 0)
                {
                    _RentabilidadeRepository.UpdateRentabilidade(rentabilidade);
                }
                else
                {
                    _RentabilidadeRepository.InsertRentabilidade(rentabilidade);
                }
            }


            var lucratividade = bem._Lucratividade;
            if (lucratividade != null)
            {
                lucratividade.IdBem = bem.Id;
                lucratividade.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                lucratividade.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (lucratividade.Id > 0)
                {
                    _LucratividadeRepository.UpdateLucratividade(lucratividade);
                }
                else
                {
                    _LucratividadeRepository.InsertLucratividade(lucratividade);
                }
            }



            return bem;

        }

        public Domain.Entities._2BTrust.Bens UpdateBens(Domain.Entities._2BTrust.Bens bem)
        {
            var endereco = bem._Endereco;
            if (endereco != null)
            {
                endereco.IdBem = bem.Id;
                endereco.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                endereco.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (endereco.Id > 0)
                {
                    _bensEnderecoRepository.UpdateBensEnderecos(endereco);
                }
                else
                {
                    _bensEnderecoRepository.InsertBensEnderecos(endereco);
                }
            }

            var acao = bem._BensAcao;
            if (acao != null)
            {
                acao.IdBem = bem.Id;
                acao.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                acao.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (acao.Id > 0)
                {
                    _bensAcoesRepository.UpdateBensAcoes(acao);
                }
                else
                {
                    _bensAcoesRepository.InsertBensAcoes(acao);
                }
            }

            var rentabilidade = bem._Rentabilidade;
            if (rentabilidade != null)
            {
                rentabilidade.IdBem = bem.Id;
                rentabilidade.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                rentabilidade.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (rentabilidade.Id > 0)
                {
                    _RentabilidadeRepository.UpdateRentabilidade(rentabilidade);
                }
                else
                {
                    _RentabilidadeRepository.InsertRentabilidade(rentabilidade);
                }
            }


            var lucratividade = bem._Lucratividade;
            if (lucratividade != null)
            {
                lucratividade.IdBem = bem.Id;
                lucratividade.IdUsuarioAlteracao = bem.IdUsuarioAlteracao;
                lucratividade.IdUsuarioInclusao = bem.IdUsuarioInclusao;
                if (lucratividade.Id > 0)
                {
                    _LucratividadeRepository.UpdateLucratividade(lucratividade);
                }
                else
                {
                    _LucratividadeRepository.InsertLucratividade(lucratividade);
                }
            }

            //atualiza bem
            var atualizaBem = _bensRepository.UpdateBens(bem);

            //atualiza cotas pool
            var listaBensPool = _bensRepository.GetListBensIdPool(bem.IdPool);
            decimal somaTotal = 0;
            foreach (var item in listaBensPool)
            {
                decimal value = Convert.ToDecimal(item.CapitalInvestido) + item._BensGastos.Where(c=>c.FlagAtivo).Sum(c=>c.Valor);
                somaTotal += value;
            }

            var poolBem = _PoolbensRepository.GetPoolBensById(bem.IdPool);
            poolBem.Cotas = (int)(somaTotal / 100);
            var atualiza = _PoolbensRepository.UpdatePoolBens(poolBem);

            return atualizaBem;
        }

        public Domain.Entities._2BTrust.Bens GetBensById(int id)
        {
            var bem = _bensRepository.GetBensById(id);
            bem._Endereco = _bensEnderecoRepository.GetBensEnderecosByIdEndereco(bem.Id);
            //bem._Endereco.sCidade = _CidadesRepository.GetListCidadesByIdCidade(bem._Endereco.IdCidade).Nome;
            //bem._Endereco.sEstado = _CidadesRepository.getEstadoByIdCidade(bem._Endereco.IdCidade).Sigla;

            bem._BensAcao = _bensAcoesRepository.GetBensAcoesByIdBem(bem.Id);
            bem._Lucratividade = _LucratividadeRepository.GetListLucratividadeByIdBem(bem.Id).FirstOrDefault();
            bem._Rentabilidade = _RentabilidadeRepository.GetListRentabilidadeByIdBem(bem.Id).FirstOrDefault();

            return bem;
        }

        public IEnumerable<Domain.Entities._2BTrust.Bens> GetListBens()
        {
            return _bensRepository.GetListBens();
        }

        public IEnumerable<Domain.Entities._2BTrust.BensImagens> GetListImagens(int idBem)
        {
            return _bensRepository.GetListImagens(idBem);
        }

        public Domain.Entities._2BTrust.BensImagens InsertImagens(Domain.Entities._2BTrust.BensImagens bemImage)
        {
            return _bensRepository.InsertImagens(bemImage);
        }

        public Domain.Entities._2BTrust.BensImagens UpdateImagens(Domain.Entities._2BTrust.BensImagens bemImage)
        {

            return _bensRepository.UpdateImagens(bemImage);
        }
    }
}
