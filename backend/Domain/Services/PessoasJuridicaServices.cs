﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class PessoasJuridicaServices : IPessoasJuridicaServices
    {
        private readonly IPessoasJuridicaRepository _pessoasJuridicaRepository;
        public PessoasJuridicaServices(IPessoasJuridicaRepository  pessoasJuridicaRepository)
        {
            _pessoasJuridicaRepository = pessoasJuridicaRepository;
        }
        public Entities._2BTrust.PessoasJuridica GetPessoasJuridicasByIdPessoa(int IdPessoa)
        {
            return _pessoasJuridicaRepository.GetPessoasJuridicasByIdPessoa(IdPessoa);
        }

        public Entities._2BTrust.PessoasJuridica InsertPessoasJuridicasByIdPessoa(Entities._2BTrust.PessoasJuridica pes)
        {
            return _pessoasJuridicaRepository.InsertPessoasJuridicas(pes);
        }

        public Entities._2BTrust.PessoasJuridica UpdatePessoasJuridicasByIdPessoa(Entities._2BTrust.PessoasJuridica pes)
        {
            return _pessoasJuridicaRepository.UpdatePessoasJuridicasByIdPessoa(pes);
        }
    }
}
