﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class LucratividadeServices : ILucratividadeServices
    {
        private readonly ILucratividadeRepository _lucratividadeRepository;

        public LucratividadeServices(ILucratividadeRepository lucratividadeRepository)
        {
            _lucratividadeRepository =  lucratividadeRepository;
        }

        public Entities._2BTrust.Lucratividade InsertLucratividade(Entities._2BTrust.Lucratividade luc)
        {
            return _lucratividadeRepository.InsertLucratividade(luc);
        }

        public Entities._2BTrust.Lucratividade UpdateLucratividade(Entities._2BTrust.Lucratividade luc)
        {
            return _lucratividadeRepository.UpdateLucratividade(luc);
        }

        public Entities._2BTrust.Lucratividade GetLucratividadeByIdBem(int idBem)
        {
            return _lucratividadeRepository.GetLucratividadeByIdBem(idBem);
        }

        public IEnumerable<Entities._2BTrust.Lucratividade> GetListLucratividadeByIdBem(int idBem)
        {
            return _lucratividadeRepository.GetListLucratividadeByIdBem(idBem);
        }

        public Entities._2BTrust.Lucratividade GetLucratividadeById(int id)
        {
            return _lucratividadeRepository.GetLucratividadeById(id);
        }
    }
}
