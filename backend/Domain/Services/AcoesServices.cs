﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class AcoesServices : IAcoesServices
    {
        private readonly IAcoesRepository _acoesRepository;
        public AcoesServices(IAcoesRepository acoesRepository)
        {
            _acoesRepository = acoesRepository;
        }

        public IEnumerable<Domain.Entities._2BTrust.Acoes> GetListAcoes()
        {
            return _acoesRepository.GetListAcoes();
        }

        public Domain.Entities._2BTrust.Acoes InsertAcoes(Domain.Entities._2BTrust.Acoes area)
        {
            return _acoesRepository.InsertAcoes(area);
        }

        public Domain.Entities._2BTrust.Acoes GetAcoesById(int idArea)
        {
            return _acoesRepository.GetAcoesById(idArea);
        }

        public Domain.Entities._2BTrust.Acoes UpdateAcoes(Domain.Entities._2BTrust.Acoes area)
        {
            return _acoesRepository.UpdateAcoes(area);
        }
    }
}
