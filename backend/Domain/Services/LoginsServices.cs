﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class LoginsServices : ILoginsServices
    {

        private readonly ILoginsRepository _loginsRepository;
        private readonly IUsuariosRepository _usuariosRepository;
        private readonly IPessoasRepository _pessoasRepository;
        public LoginsServices(ILoginsRepository loginsRepository, IUsuariosRepository usuariosRepository, IPessoasRepository  pessoasRepository)
        {
            _loginsRepository = loginsRepository;
            _usuariosRepository = usuariosRepository;
            _pessoasRepository = pessoasRepository;
        }

        public Logins LoginUsuario(string sLogin, string sSenha)
        {
            var login = _loginsRepository.LoginUsuario(sLogin, sSenha);
            if(login!=null)
            {
                //capturar obj usuario + obj pessoa
            }


            return login;
        }



    }
}
