﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class BensAcoesServices : IBensAcoesServices
    {
        private readonly IBensAcoesRepository _BensAcoesRepository;

        public BensAcoesServices(IBensAcoesRepository BensAcoesRepository)
        {
            _BensAcoesRepository = BensAcoesRepository;
        }

        public Entities._2BTrust.BensAcoes InsertBensAcoes(Entities._2BTrust.BensAcoes bemAcao)
        {
            return _BensAcoesRepository.InsertBensAcoes(bemAcao);
        }

        public Entities._2BTrust.BensAcoes UpdateBensAcoes(Entities._2BTrust.BensAcoes bemAcao)
        {
            return _BensAcoesRepository.UpdateBensAcoes(bemAcao);
        }

        public Entities._2BTrust.BensAcoes GetBensAcoesByIdBem(int idBem)
        {
            return _BensAcoesRepository.GetBensAcoesByIdBem(idBem);
        }

        public Entities._2BTrust.BensAcoes GetBensAcoesById(int id)
        {
            return _BensAcoesRepository.GetBensAcoesById(id);
        }
    }
}
