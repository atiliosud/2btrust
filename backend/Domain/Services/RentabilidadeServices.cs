﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class RentabilidadeServices : IRentabilidadeServices
    {
        private readonly IRentabilidadeRepository _rentabilidadeRepository;
        public RentabilidadeServices(IRentabilidadeRepository  rentabilidadeRepository)
        {
            _rentabilidadeRepository = rentabilidadeRepository;
        }
        public Entities._2BTrust.Rentabilidade InsertRentabilidade(Entities._2BTrust.Rentabilidade luc)
        {
            return _rentabilidadeRepository.InsertRentabilidade(luc);
        }

        public Entities._2BTrust.Rentabilidade UpdateRentabilidade(Entities._2BTrust.Rentabilidade luc)
        {
            return _rentabilidadeRepository.UpdateRentabilidade(luc);
        }

        public Entities._2BTrust.Rentabilidade GetRentabilidadeByIdBem(int idBem)
        {
            return _rentabilidadeRepository.GetRentabilidadeByIdBem(idBem);
        }

        public IEnumerable<Entities._2BTrust.Rentabilidade> GetListRentabilidadeByIdBem(int idBem)
        {
            return _rentabilidadeRepository.GetListRentabilidadeByIdBem(idBem);
        }

        public Entities._2BTrust.Rentabilidade GetRentabilidadeById(int id)
        {

            return _rentabilidadeRepository.GetRentabilidadeById(id);
        }
    }
}
