﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class ContratosServices : IContratosServices
    {
        private readonly IContratosRepository _contratosRepository;

        public ContratosServices(IContratosRepository contratosRepository)
        {
            _contratosRepository = contratosRepository;
        }

        public Domain.Entities._2BTrust.Contratos InsertContratos(Domain.Entities._2BTrust.Contratos con)
        {
            return _contratosRepository.InsertContratos(con);
        }

        public Domain.Entities._2BTrust.Contratos UpdateContratos(Domain.Entities._2BTrust.Contratos con)
        {
            return _contratosRepository.UpdateContratos(con);
        }

        public IEnumerable<Domain.Entities._2BTrust.Contratos> GetListContratos()
        {
            return _contratosRepository.GetListContratos();
        }

        public Domain.Entities._2BTrust.Contratos GetContratosById(int id)
        {
            return _contratosRepository.GetContratosById(id);
        }
    }
}
