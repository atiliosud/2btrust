﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class EstadosServices : IEstadosServices
    {
        private readonly IEstadosRepository _estadosRepository;
        public EstadosServices(IEstadosRepository estadosRepository)
        {
            _estadosRepository = estadosRepository;
        }
        public IEnumerable<Entities._2BTrust.Estados> GetListEstados()
        {
            return _estadosRepository.GetListEstados();
        }
    }
}
