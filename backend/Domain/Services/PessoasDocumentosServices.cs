﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class PessoasDocumentosServices : IPessoasDocumentosServices
    {
        private readonly IPessoasDocumentosRepository _pessoaDocumentoRepository;
       
        public PessoasDocumentosServices(IPessoasDocumentosRepository  pessoaDocumentoRepository)
        {
            _pessoaDocumentoRepository = pessoaDocumentoRepository;
        }
         
        public IEnumerable<Entities._2BTrust.PessoasDocumentos> GetListPessoasDocumentosByIdPessoa(int idPessoa)
        {
            return _pessoaDocumentoRepository.GetListPessoasDocumentosByIdPessoa(idPessoa);
        }

        public Entities._2BTrust.PessoasDocumentos InsertPessoasDocumentos(Entities._2BTrust.PessoasDocumentos pes)
        {
            return _pessoaDocumentoRepository.InsertPessoasDocumentos(pes);
        }

        public Entities._2BTrust.PessoasDocumentos UpdatePessoasDocumentos(Entities._2BTrust.PessoasDocumentos pes)
        {
            return _pessoaDocumentoRepository.UpdatePessoasDocumentos(pes);
        }
    }
}
