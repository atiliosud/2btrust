﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class DocumentosServices : IDocumentosServices
    {
        private readonly IDocumentosRepository _documentoRepository;
        public DocumentosServices(IDocumentosRepository  documentoRepository)
        {
            _documentoRepository = documentoRepository;
        }
        public IEnumerable<Entities._2BTrust.Documentos> GetListDocumentos()
        {
            return _documentoRepository.GetListDocumentos();
        }
    }
}
