﻿using Domain.Entities._2BTrust;
using Domain.Enuns;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class PoolBensPessoasServices : IPoolBensPessoasServices
    {
        private readonly IPoolBensPessoasRepository _poolBensPessoaRepository;
        private readonly IPoolBensRepository _poolBensRepository;
        private readonly IPessoasRepository _PessoaRepository;


        public PoolBensPessoasServices(IPoolBensRepository  poolBensRepository, IPoolBensPessoasRepository poolBensPessoaRepository, IPessoasRepository PessoaRepository)
        {
            _poolBensRepository = poolBensRepository;
               _PessoaRepository = PessoaRepository;
            _poolBensPessoaRepository = poolBensPessoaRepository;
        }

        public Entities._2BTrust.PoolBensPessoas InsertPoolBensPessoas(Entities._2BTrust.PoolBensPessoas con)
        {
            var pbp = _poolBensPessoaRepository.InsertPoolBensPessoas(con);

            return pbp;
        }

        public Entities._2BTrust.PoolBensPessoas UpdatePoolBensPessoas(Entities._2BTrust.PoolBensPessoas con)
        {
            var pbp = _poolBensPessoaRepository.UpdatePoolBensPessoas(con);


            if (pbp != null && pbp.IdStatus == (int)EnumInvestimento.CompraAprovada)
            {
                var bens = _poolBensRepository.GetDetailsPoolParaInvestimentoById(pbp.IdPoolBem);
                //var valorCota = bens._Bens.Sum(c => Convert.ToDecimal(c.CapitalInvestido)) / bens.Cotas;
                var valorCota = 100;
                var carteira = _PessoaRepository.GetCarteiraByIdPessoa(con.IdPessoa);
                carteira.Saldo -= valorCota * pbp.Cota;

                var updateCarteira = _PessoaRepository.UpdateCarteira(carteira);
            }


            if (pbp != null && pbp.IdStatus == (int)EnumInvestimento.VendaAprovada)
            {

                var bens = _poolBensRepository.GetDetailsPoolParaInvestimentoById(pbp.IdPoolBem);
                var valorCota = 100;

                var carteira = _PessoaRepository.GetCarteiraByIdPessoa(con.IdPessoa);
                carteira.Saldo += valorCota * pbp.Cota;

                var updateCarteira = _PessoaRepository.UpdateCarteira(carteira);

            }

            return pbp;

        }

        public IEnumerable<Entities._2BTrust.PoolBensPessoas> GetListPoolBensPessoas()
        {
            return _poolBensPessoaRepository.GetListPoolBensPessoas();
        }

        public Entities._2BTrust.PoolBensPessoas GetPoolBensPessoasById(int id)
        {
            return _poolBensPessoaRepository.GetPoolBensPessoasById(id);
        }

        public IEnumerable<Entities._2BTrust.PoolBensPessoas> GetPoolBensPessoasByIdPessoa(int idPessoa)
        {
            return _poolBensPessoaRepository.GetPoolBensPessoasByIdPessoa(idPessoa);
        }

        public IEnumerable<Entities._2BTrust.PoolBensPessoas> GetPoolBensPessoasByIdPoolBem(int idPoolBem)
        {
            return _poolBensPessoaRepository.GetPoolBensPessoasByIdPoolBem(idPoolBem);
        }

        public IEnumerable<Pessoas> GetListValidaVendaCota()
        {
            return _poolBensPessoaRepository.GetListValidaVendaCota();
        }

        public IEnumerable<Pessoas> GetListValidaCompraCota()
        {
            return _poolBensPessoaRepository.GetListValidaCompraCota();
        }

        public IEnumerable<Pessoas> GetListValidaRetirada()
        {
            return _poolBensPessoaRepository.GetListValidaRetirada();
        }
    }
}
