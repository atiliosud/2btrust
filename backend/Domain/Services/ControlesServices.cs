﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class ControlesServices : IControlesServices
    {

        private readonly IControlesRepository _controlerepository;
        private readonly IAcoesRepository _acoesrepository;
        private readonly IGruposAcoesRepository _gruposAcoesrepository;

        public ControlesServices(IGruposAcoesRepository gruposAcoesrepository, IControlesRepository controlerepository, IAcoesRepository acoesrepository)
        {
            _controlerepository = controlerepository;
            _acoesrepository = acoesrepository;
            _gruposAcoesrepository = gruposAcoesrepository;
        }
        public IEnumerable<Entities._2BTrust.Controles> GetControlesByIdGrupo(int idGrupo)
        {
            return _controlerepository.GetControlesByIdGrupo(idGrupo);
        }
        public IEnumerable<Domain.Entities._2BTrust.Controles> GetListControles()
        {
            return _controlerepository.GetListControles();
        }

        public Domain.Entities._2BTrust.Controles InsertControles(Domain.Entities._2BTrust.Controles Controles)
        {
            var controle = _controlerepository.InsertControles(Controles);

            foreach (var item in controle._Acoes)
            {
                item.IdControle = controle.Id;
                var insert = _acoesrepository.InsertAcoes(item);


                _gruposAcoesrepository.InsertGruposAcoes(2, insert.Id, controle.IdUsuarioInclusao);
                _gruposAcoesrepository.InsertGruposAcoes(3, insert.Id, controle.IdUsuarioInclusao);

            }

            return controle;
        }

        public Domain.Entities._2BTrust.Controles GetControlesById(int idAcoes)
        {
            return _controlerepository.GetControlesById(idAcoes);
        }

        public Domain.Entities._2BTrust.Controles UpdateControles(Domain.Entities._2BTrust.Controles Controles)
        {
            var controle = _controlerepository.UpdateControles(Controles);

            foreach (var item in controle._Acoes)
            {
                item.IdControle = controle.Id;
                if (item.Id > 0)
                    _acoesrepository.UpdateAcoes(item);
                else
                {
                    item.IdUsuarioInclusao = controle.IdUsuarioInclusao;
                    var obj = _acoesrepository.InsertAcoes(item);

                    if (obj != null)
                    {
                        //sempre colocar os ids dos grupos aki
                        //se aumentar um na table, colocar aki
                        _gruposAcoesrepository.InsertGruposAcoes(2, obj.Id, controle.IdUsuarioInclusao);
                        _gruposAcoesrepository.InsertGruposAcoes(3, obj.Id, controle.IdUsuarioInclusao);
                    }

                }
            }

            return controle;
        }


        public IEnumerable<Entities._2BTrust.Controles> GetListControlesPai()
        {
            return _controlerepository.GetListControlesPai();
        }
    }
}
