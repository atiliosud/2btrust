﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class PessoasFisicaServices : IPessoasFisicaServices
    {
        private readonly IPessoasFisicaRepository _pessoaFisicaRepository;
        public PessoasFisicaServices(IPessoasFisicaRepository pessoaFisicaRepository)
        {
            _pessoaFisicaRepository = pessoaFisicaRepository;
        }

        public Entities._2BTrust.PessoasFisicas GetPessoaFisicaByIdPessoa(int idPessoa)
        {
            return _pessoaFisicaRepository.GetPessoaFisicaByIdPessoa(idPessoa);
        }

        public Entities._2BTrust.PessoasFisicas InsertPessoaFisica(Entities._2BTrust.PessoasFisicas pes)
        {
            return _pessoaFisicaRepository.InsertPessoaFisica(pes);
        }

        public Entities._2BTrust.PessoasFisicas UpdatePessoaFisica(Entities._2BTrust.PessoasFisicas pes)
        {
            return _pessoaFisicaRepository.UpdatePessoaFisica(pes);
        }
    }
}
