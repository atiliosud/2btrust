﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class LancamentosServices : ILancamentosServices
    {
        private readonly ILancamentosRepository _lancamentosRepository;
        public LancamentosServices(ILancamentosRepository lancamentosRepository)
        {
            _lancamentosRepository = lancamentosRepository;
        }

        public Domain.Entities._2BTrust.Lancamentos InsertLancamentos(Domain.Entities._2BTrust.Lancamentos con)
        {
            return _lancamentosRepository.InsertLancamentos(con);
        }

        public Domain.Entities._2BTrust.Lancamentos UpdateLancamentos(Domain.Entities._2BTrust.Lancamentos con)
        {
            return _lancamentosRepository.UpdateLancamentos(con);
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetListLancamentos()
        {
            return _lancamentosRepository.GetListLancamentos();
        }

        public Domain.Entities._2BTrust.Lancamentos GetLancamentosById(int id)
        {
            return _lancamentosRepository.GetLancamentosById(id);
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetLancamentosByIdPessoa(int idPessoa)
        {
            return _lancamentosRepository.GetLancamentosByIdPessoa(idPessoa);
        }

        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetLancamentosByIdPoolBem(int idPoolBem)
        {
            return _lancamentosRepository.GetLancamentosByIdPoolBem(idPoolBem);
        }

        public IEnumerable<PessoaPoolsInvestidos> GetPessoasPoolsInvestido()
        {
            return _lancamentosRepository.GetPessoasPoolsInvestido();
        }

        public PessoaPoolsInvestidos GetPessoasPoolsInvestido(int idPoolBemPessoa)
        {
            return _lancamentosRepository.GetPessoasPoolsInvestido(idPoolBemPessoa);
        }
    }
}
