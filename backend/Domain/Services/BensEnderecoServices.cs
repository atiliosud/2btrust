﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class BensEnderecoServices : IBensEnderecoServices
    {
        private readonly IBensEnderecoRepository _BensEnderecoRepository;

        public BensEnderecoServices(IBensEnderecoRepository BensEnderecoRepository)
        {
            _BensEnderecoRepository = BensEnderecoRepository;
        }

        public Entities._2BTrust.BensEnderecos InsertBensEnderecos(Entities._2BTrust.BensEnderecos bem)
        {
            return _BensEnderecoRepository.InsertBensEnderecos(bem);
        }

        public Entities._2BTrust.BensEnderecos UpdateBensEnderecos(Entities._2BTrust.BensEnderecos bem)
        {
            return _BensEnderecoRepository.UpdateBensEnderecos(bem);
        }

        public Entities._2BTrust.BensEnderecos GetBensEnderecosByIdEndereco(int idBem)
        {
            return _BensEnderecoRepository.GetBensEnderecosByIdEndereco(idBem);
        }

        public Entities._2BTrust.BensEnderecos GetBensEnderecosById(int id)
        {
            return _BensEnderecoRepository.GetBensEnderecosById(id);
        }
    }
}
