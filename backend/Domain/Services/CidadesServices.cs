﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class CidadesServices : ICidadesServices
    {
        private readonly ICidadesRepository _cidadeRepository;
        public CidadesServices(ICidadesRepository  cidadeRepository)
        {
            _cidadeRepository = cidadeRepository;
        }

        public IEnumerable<Entities._2BTrust.Cidades> GetListCidadesByIdEstado(int idEstado)
        {
            return _cidadeRepository.GetListCidadesByIdEstado(idEstado);
        }

        public Entities._2BTrust.Cidades GetListCidadesByIdCidade(int idCidade)
        {
            return _cidadeRepository.GetListCidadesByIdCidade(idCidade);
        }

        public Entities._2BTrust.Cidades InsertCidade(Entities._2BTrust.Cidades cid)
        {
            return _cidadeRepository.InsertCidade(cid);
        }
    }
}
