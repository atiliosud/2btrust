﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class BensVisaoGeralServices : IBensVisaoGeralServices
    {
        private readonly IBensVisaoGeralRepository _bensVisaoGeralRepository;

        public BensVisaoGeralServices(IBensVisaoGeralRepository bensVisaoGeralRepository)
        {
            _bensVisaoGeralRepository = bensVisaoGeralRepository;
        }

        public IEnumerable<BensVisaoGeral> GetListBensVisaoGeral()
        {
            return _bensVisaoGeralRepository.GetListBensVisaoGeral();
        }

        public IEnumerable<BensVisaoGeralImagens> GetListImagens(int idBem)
        {
            return _bensVisaoGeralRepository.GetListImagens(idBem);
        }

        public BensVisaoGeral InsertBensVisaoGeral(BensVisaoGeral bemVisGeral)
        {
            var bemInsert = _bensVisaoGeralRepository.InsertBensVisaoGeral(bemVisGeral);
            if (bemInsert != null)
            {
                bemVisGeral.Id = bemInsert.Id;
            }
            return bemVisGeral;
        }

        public BensVisaoGeral UpdateBensVisaoGeral(BensVisaoGeral bemVisGeral)
        {
            var atualizaBem = _bensVisaoGeralRepository.UpdateBensVisaoGeral(bemVisGeral);
            return atualizaBem;
        }

        public BensVisaoGeral GetBensVisaoGeralById(int id)
        {
            var bem = _bensVisaoGeralRepository.GetBensVisaoGeralById(id);
            return bem;
        }

        public BensVisaoGeralImagens InsertImagens(BensVisaoGeralImagens bemImagem)
        {
            return _bensVisaoGeralRepository.InsertImagens(bemImagem);
        }

        public BensVisaoGeralImagens UpdateImagens(BensVisaoGeralImagens bemImagem)
        {
            return _bensVisaoGeralRepository.UpdateImagens(bemImagem);
        }
    }
}
