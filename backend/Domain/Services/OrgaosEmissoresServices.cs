﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class OrgaosEmissoresServices : IOrgaosEmissoresServices
    {
        private readonly IOrgaosEmissoresRepository _orgaosEmissores;
        public OrgaosEmissoresServices(IOrgaosEmissoresRepository  orgaosEmissores)
        {
            _orgaosEmissores = orgaosEmissores;
        }
        public IEnumerable<Entities._2BTrust.OrgaosEmissores> GetListOrgaosEmissoresByIdEstado(int idEstado)
        {
            return _orgaosEmissores.GetListOrgaosEmissoresByIdEstado(idEstado);
        }
    }
}
