﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class GruposAcoesServices : IGruposAcoesServices
    {
        private readonly IGruposAcoesRepository _grupoAcoesRepository;
        public GruposAcoesServices(IGruposAcoesRepository grupoAcoesRepository)
        {
            _grupoAcoesRepository = grupoAcoesRepository;
        }
        public IEnumerable<Entities._2BTrust.Grupos> GetListGrupoXActions()
        {
            return _grupoAcoesRepository.GetListGrupoXActions();
        }

        public bool InsertGruposAcoes(int idGrupo, int idAcao, int idUsuarioInclusao)
        {
            return _grupoAcoesRepository.InsertGruposAcoes(idGrupo, idAcao, idUsuarioInclusao);

        }

        public bool DeleteGruposAcoes(int idGrupo, int idAcao)
        {
            return _grupoAcoesRepository.DeleteGruposAcoes(idGrupo, idAcao);
        }


        public GruposAcoes SetGrupoXActions(int idGrupo, int idAcao, int idUsuario)
        {
            return _grupoAcoesRepository.SetGrupoXActions(idGrupo, idAcao, idUsuario);
        }
    }
}
