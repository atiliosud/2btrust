﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class PoolBensServices : IPoolBensServices
    {
        private readonly IPoolBensRepository _poolBensRepository;

        public PoolBensServices(IPoolBensRepository poolBensRepository)
        {
            _poolBensRepository = poolBensRepository;
        }

        public Domain.Entities._2BTrust.PoolBens InsertPoolBens(Domain.Entities._2BTrust.PoolBens con)
        {
            return _poolBensRepository.InsertPoolBens(con);
        }

        public Domain.Entities._2BTrust.PoolBens UpdatePoolBens(Domain.Entities._2BTrust.PoolBens con)
        {
            return _poolBensRepository.UpdatePoolBens(con);
        }

        public IEnumerable<Domain.Entities._2BTrust.PoolBens> GetListPoolBens()
        {
            return _poolBensRepository.GetListPoolBens();
        }

        public Domain.Entities._2BTrust.PoolBens GetPoolBensById(int id)
        {
            return _poolBensRepository.GetPoolBensById(id);
        }


        public IEnumerable<Entities._2BTrust.PoolBens> GetListPoolParaInvestimento()
        {
            return _poolBensRepository.GetListPoolParaInvestimento();
        }


        public Entities._2BTrust.PoolBens GetDetailsPoolParaInvestimentoById(int id)
        {
            return _poolBensRepository.GetDetailsPoolParaInvestimentoById(id);
        }


        public object GetListMeusPoolInvestidos(int idPessoa)
        {
            return _poolBensRepository.GetListMeusPoolInvestidos(idPessoa);
        }

        public PoolsBensRentabilidadePerformance InsertPoolsBensRentabilidadePerformance(PoolsBensRentabilidadePerformance pools)
        {  
            return _poolBensRepository.InsertPoolsBensRentabilidadePerformance(pools);
        }

        public bool DesativarAtivarPoolsBensRentabilidadePerformance(int id, bool flgAtivo)
        {
            return _poolBensRepository.DesativarAtivarPoolsBensRentabilidadePerformance(id, flgAtivo);
        }

        public IEnumerable<PoolsBensRentabilidadePerformance>  GetListPoolsBensRentabilidadePerformance(int idPoolBem)
        {
            return _poolBensRepository.GetListPoolsBensRentabilidadePerformance(idPoolBem);
        }
    }
}
