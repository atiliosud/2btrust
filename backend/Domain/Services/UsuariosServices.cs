﻿using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class UsuariosServices : IUsuariosServices
    {
        private readonly IUsuariosRepository _usuarioRepository;
        private readonly IGruposRepository _gruposRepository;
        private readonly IControlesRepository _controlesRepository;


        public UsuariosServices(IControlesRepository  controlesRepository, IUsuariosRepository  usuarioRepository,IGruposRepository  gruposRepository  )
        {
            _usuarioRepository = usuarioRepository;
            
            _gruposRepository = gruposRepository;

            _controlesRepository = controlesRepository;
        }
        public  Usuarios LoginUsuario(string sLogin, string sSenha)
        {
            var user = _usuarioRepository.LoginUsuario(sLogin, sSenha);
            if(user!=null)
            {
                user._Grupo = _gruposRepository.GetGrupoByIdUsuario(user.Id);
                user._Controles = _controlesRepository.GetControlesByIdGrupo(user._Grupo.Id);
            }

            return user;
        }

        public bool ValidaUsuarioPlataforma(int idUsuario, bool isValid)
        {
            return _usuarioRepository.ValidaUsuarioPlataforma(idUsuario, isValid);
        }
    }
}
