﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class PessoasEnderecosServices : IPessoasEnderecosServices
    {
        private readonly IPessoasEnderecosRepository _pessoaEnderecoRepository;
        public PessoasEnderecosServices(IPessoasEnderecosRepository  pessoaEnderecoRepository)
        {
            _pessoaEnderecoRepository = pessoaEnderecoRepository;
        }

        public Entities._2BTrust.PessoasEnderecos GetEnderecoById(int idPessoaEndereco)
        {
            return _pessoaEnderecoRepository.GetEnderecoById(idPessoaEndereco);
        }

        public Entities._2BTrust.PessoasEnderecos GetEnderecoByIdPessoa(int idPessoa)
        {
            return _pessoaEnderecoRepository.GetEnderecoByIdPessoa(idPessoa);
        }

        public Entities._2BTrust.PessoasEnderecos InsertEnderecoByIdPessoa(Entities._2BTrust.PessoasEnderecos pes)
        {
            return _pessoaEnderecoRepository.InsertEndereco(pes);
        }

        public Entities._2BTrust.PessoasEnderecos UpdateEnderecoByIdPessoa(Entities._2BTrust.PessoasEnderecos pes)
        {
            return _pessoaEnderecoRepository.UpdateEndereco(pes);
        }
    }
}
