﻿using Domain.Entities;
using Domain.Entities._2BTrust;
using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class PessoasServices : IPessoasServices
    {

        private readonly ILoginsRepository _loginsRepository;
        private readonly IUsuariosRepository _usuariosRepository;
        private readonly IPessoasFisicaRepository _pessoasFisicaRepository;
        private readonly IPessoasJuridicaRepository _pessoasJuridicaRepository;
        private readonly IPessoasDocumentosRepository _pessoasDocumentosRepository;
        private readonly IPessoasEnderecosRepository _pessoasEnderecoRepository;
        private readonly IPessoasRepository _pessoasRepository;
        private readonly IGruposRepository _gruposRepository;
        private readonly IUsuariosGruposRepository _usuariosgruposRepository;

        private readonly ICidadesRepository _CidadesRepository;
        private readonly IControlesRepository _controlesRepository;
        private readonly ILancamentosRepository _lancamentoRepository;

        public PessoasServices(ILancamentosRepository lancamentoRepository, ICidadesRepository CidadesRepository, IUsuariosGruposRepository usuariosgruposRepository, IPessoasEnderecosRepository pessoasEnderecoRepository, IPessoasDocumentosRepository pessoasDocumentosRepository, IPessoasJuridicaRepository pessoasJuridicaRepository, IPessoasFisicaRepository pessoasFisicaRepository, IControlesRepository controlesRepository, IGruposRepository gruposRepository, IPessoasRepository pessoasRepository, IUsuariosRepository usuariosRepository, ILoginsRepository loginsRepository)
        {
            _lancamentoRepository = lancamentoRepository;
            _CidadesRepository = CidadesRepository;
            _pessoasRepository = pessoasRepository;
            _usuariosRepository = usuariosRepository;
            _loginsRepository = loginsRepository;
            _gruposRepository = gruposRepository;
            _controlesRepository = controlesRepository;
            _pessoasJuridicaRepository = pessoasJuridicaRepository;
            _pessoasFisicaRepository = pessoasFisicaRepository;
            _pessoasDocumentosRepository = pessoasDocumentosRepository;
            _pessoasEnderecoRepository = pessoasEnderecoRepository;
            _usuariosgruposRepository = usuariosgruposRepository;
        }

        public IEnumerable<Entities._2BTrust.Pessoas> getClientInvests(int idPessoa)
        {
            try
            {


                Usuarios user = new Usuarios();
                Pessoas pess;

                pess = _pessoasRepository.GetPessoasById(idPessoa);

                //item._Usuario = _usuariosRepository.GetUsuarioByIdPessoa(item.Id);
                pess._Carteira = _pessoasRepository.GetCarteiraByIdPessoa(pess.Id);
                pess._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pess.Id);
                pess._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pess.Id);

                pess._ListaRentabilidade = _pessoasRepository.GetRentabilidadeMesBens(0);



                List<Pessoas> lst = new List<Pessoas>();
                lst.Add(pess);

                return lst;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Entities._2BTrust.Pessoas LoginUsuarioSimples(string sLogin, string sSenha)
        {

            Usuarios user = new Usuarios();
            Pessoas pess = new Pessoas();

            var login = _loginsRepository.LoginUsuario(sLogin, sSenha);
            if (login != null)
            {
                //capturar obj usuario + obj pessoa 
                user = _usuariosRepository.GetUsuarioById(login.IdUsuario);
            }
            else return null;

            if (user != null)
            {
                user._Login = login;
                pess = _pessoasRepository.GetPessoaById(user.IdPessoa);
            }

            pess._Usuario = user;
            pess._Carteira = _pessoasRepository.GetCarteiraByIdPessoa(pess.Id);
            pess._Carteira._Comprovantes = null;
            //  pess._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pess.Id);
            //  pess._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pess.Id);

            //   pess._ListaRentabilidade = _pessoasRepository.GetRentabilidadeMesBens(0);


            return pess;
        }


        public Entities._2BTrust.Pessoas GetPessoaMeusPools(int idPessoa)
        {

            Usuarios user = new Usuarios();
            Pessoas pess = new Pessoas();

            pess._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoaSimples(idPessoa);
            //  pess._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pess.Id);

            //   pess._ListaRentabilidade = _pessoasRepository.GetRentabilidadeMesBens(0);


            return pess;
        }

        public Entities._2BTrust.Pessoas GetPessoaMeusLancamentos(int idPessoa)
        {

            Usuarios user = new Usuarios();
            Pessoas pess = new Pessoas();

            //   pess._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pess.Id);
            pess._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(idPessoa);

            //   pess._ListaRentabilidade = _pessoasRepository.GetRentabilidadeMesBens(0);


            return pess;
        }
        public Entities._2BTrust.Pessoas GetPessoaListaRentabilidade(int idPessoa)
        {

            Usuarios user = new Usuarios();
            Pessoas pess = new Pessoas();

            //   pess._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pess.Id);
            //  pess._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pess.Id);

            pess._ListaRentabilidade = _pessoasRepository.GetRentabilidadeMesBens(0);


            return pess;
        }



        public Entities._2BTrust.Pessoas LoginUsuario(string sLogin, string sSenha)
        {

            Usuarios user = new Usuarios();
            Pessoas pess = new Pessoas();

            var login = _loginsRepository.LoginUsuario(sLogin, sSenha);
            if (login != null)
            {
                //capturar obj usuario + obj pessoa 
                user = _usuariosRepository.GetUsuarioById(login.IdUsuario);
            }
            else return null;

            if (user != null)
            {
                user._Login = login;
                user._Grupo = _gruposRepository.GetGrupoByIdUsuario(user.Id);
                user._Controles = _controlesRepository.GetControlesByIdGrupo(user._Grupo.Id);

                pess = _pessoasRepository.GetPessoaById(user.IdPessoa);

                if (user._Grupo.Id == 7 || user._Grupo.Id == 2)
                {
                    pess._InformacoesAdminHome = _pessoasRepository.GetInformacoesAdminHome();

                }
            }

            pess._Usuario = user;
            pess._Carteira = _pessoasRepository.GetCarteiraByIdPessoa(pess.Id);
            pess._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pess.Id);
            pess._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pess.Id);

            pess._ListaRentabilidade = _pessoasRepository.GetRentabilidadeMesBens(0);


            return pess;
        }

        public Entities._2BTrust.Pessoas InformacoaHomeValoresMobile(int idUsuario, string sHash)
        {

            Usuarios user = new Usuarios();
            Pessoas pess = new Pessoas();

            var login = _loginsRepository.LoginUsuarioByID(idUsuario);
            if (login != null)
            {
                //capturar obj usuario + obj pessoa 
                user = _usuariosRepository.GetUsuarioById(login.IdUsuario);
            }
            else return null;

            if (user != null)
            {
                user._Login = login;
                user._Grupo = _gruposRepository.GetGrupoByIdUsuario(user.Id);
                // user._Controles = _controlesRepository.GetControlesByIdGrupo(user._Grupo.Id);

                pess = _pessoasRepository.GetPessoaById(user.IdPessoa);

                if (user._Grupo.Id == 7 || user._Grupo.Id == 2)
                {
                    pess._InformacoesAdminHome = _pessoasRepository.GetInformacoesAdminHome();

                }
            }

            pess._Usuario = user;
            pess._Carteira = _pessoasRepository.GetCarteiraByIdPessoa(pess.Id);
            pess._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pess.Id);
            pess._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pess.Id);
            pess._ListaRentabilidade = _pessoasRepository.GetRentabilidadeMesBens(0);


            return pess;
        }



        public bool CadastroInicial(Pessoas pessoa)
        {
            bool fValido = true;

            // Cadastrar pessoa
            var pessoaReturn = _pessoasRepository.InsertPessoa(pessoa);

            // Cadastrar pessoaFisica
            if (pessoaReturn != null)
            {
                pessoa._PessoaFisica.IdPessoa = pessoaReturn.Id;
                var pessoaFReturn = _pessoasFisicaRepository.InsertPessoaFisica(pessoa._PessoaFisica);


                // Cadastrar pessoaJuridica se necessario
                if (pessoaFReturn != null)
                {


                    pessoa._PessoaJuridica.IdPessoa = pessoaReturn.Id;
                    if (!string.IsNullOrEmpty(pessoa._PessoaJuridica.CNPJ))
                    {
                        var pessoaJReturn = _pessoasJuridicaRepository.InsertPessoasJuridicas(pessoa._PessoaJuridica);
                        if (pessoaJReturn == null)
                            fValido = false;
                    }



                    // Cadastrar Endereço
                    pessoa._Endereco.IdPessoa = pessoaReturn.Id;
                    var pessoaEnderecoReturn = _pessoasEnderecoRepository.InsertEndereco(pessoa._Endereco);
                    if (pessoaEnderecoReturn != null)
                    {


                        // Cadastrar Documentos
                        foreach (var item in pessoa._Documentos)
                        {
                            item.IdPessoa = pessoaReturn.Id;
                            var pessoaDocumentoReturn = _pessoasDocumentosRepository.InsertPessoasDocumentos(item);
                            if (pessoaDocumentoReturn == null)
                            {
                                fValido = false;
                                break;
                            }
                        }



                        // cadastrar usuario
                        pessoa._Usuario.IdPessoa = pessoaReturn.Id;
                        var usuarioCadReturn = _usuariosRepository.InsertUsuario(pessoa._Usuario);
                        if (usuarioCadReturn != null)
                        {

                            pessoa._Usuario._UsuarioGrupo = new UsuariosGrupos()
                            {
                                IdUsuarioInclusao = 3,
                                IdGrupo = 3,
                                IdUsuario = usuarioCadReturn.Id
                            };
                            _usuariosgruposRepository.InsertUsuariosGrupos(pessoa._Usuario._UsuarioGrupo);


                            // cadastrar login
                            pessoa._Usuario._Login.IdUsuario = usuarioCadReturn.Id;
                            var loginCadReturn = _loginsRepository.Insertlogins(pessoa._Usuario._Login);
                        }
                        else
                            fValido = false;
                    }
                    else
                        fValido = false;
                }
                else
                    fValido = false;
            }
            else
                fValido = false;


            return fValido;


        }


        public bool LiberacaoViaEmail(string saltKey)
        {
            var idPessoa = _usuariosRepository.LiberacaoViaEmail(saltKey);

            return _pessoasRepository.LiberacaoViaEmail(idPessoa);
        }


        public Pessoas GetPessoasById(int idPessoa)
        {
            var pessoa = _pessoasRepository.GetPessoasById(idPessoa);

            pessoa._Endereco = _pessoasEnderecoRepository.GetEnderecoByIdPessoa(pessoa.Id);

            pessoa._Documentos = _pessoasDocumentosRepository.GetListPessoasDocumentosByIdPessoa(pessoa.Id);

            //pessoa._Endereco.sCidade = _CidadesRepository.GetListCidadesByIdCidade(pessoa._Endereco.IdCidade).Nome;
            //pessoa._Endereco.sEstado = _CidadesRepository.getEstadoByIdCidade(pessoa._Endereco.IdCidade).Sigla;
            if (pessoa._Usuario == null)
                pessoa._Usuario = _usuariosRepository.GetUsuarioByIdPessoa(idPessoa);

            pessoa._Carteira = _pessoasRepository.GetCarteiraByIdPessoa(idPessoa);

            pessoa._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pessoa.Id);

            pessoa._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pessoa.Id);
            return pessoa;

        }


        public Pessoas UpdatePessoaMenu(Pessoas pessoa)
        {
            bool fValido = true;

            var pessoaReturn = _pessoasRepository.UpdatePessoa(pessoa);
            if (pessoaReturn == null)
                return null;

            var pessoaFReturn = _pessoasFisicaRepository.UpdatePessoaFisica(pessoa._PessoaFisica);
            if (pessoaFReturn == null)
                return null;

            if (!string.IsNullOrEmpty(pessoa._PessoaJuridica.CNPJ))
            {
                var pessoaJReturn = _pessoasJuridicaRepository.UpdatePessoasJuridicasByIdPessoa(pessoa._PessoaJuridica);
                if (pessoaJReturn == null)
                    return null;
            }

            var usuarioCadReturn = _usuariosRepository.UpdateUsuario(pessoa._Usuario);
            if (usuarioCadReturn == null)
                return null;
            var loginCadReturn = _loginsRepository.Updatelogins(pessoa._Usuario._Login);
            if (loginCadReturn == null)
                return null;

            return pessoa;
        }


        public Pessoas UpdatePessoaMenuEndereco(Pessoas pessoa)
        {
            if (pessoa._Endereco.Id > 0)
            {
                var pessoaEndereco = _pessoasEnderecoRepository.UpdateEndereco(pessoa._Endereco);
                if (pessoaEndereco == null) return null;
                return pessoa;
            }
            else
            {
                pessoa._Endereco.IdPessoa = pessoa.Id;
                var pessoaEndereco = _pessoasEnderecoRepository.InsertEndereco(pessoa._Endereco);
                if (pessoaEndereco == null) return null;
                return pessoa;

            }
        }


        public Pessoas UpdatePessoaMenuDocumento(Pessoas pessoa)
        {

            _pessoasDocumentosRepository.FlagAllDelete(pessoa.Id);

            foreach (var item in pessoa._Documentos)
            {
                item.IdPessoa = pessoa.Id;
                if (item.Id >= 0)
                {
                    item.FlagAtivo = true;

                    var pesDoc = _pessoasDocumentosRepository.UpdatePessoasDocumentos(item);
                }
                else
                {

                    var pesDoc = _pessoasDocumentosRepository.InsertPessoasDocumentos(item);
                }
            }
            return pessoa;
        }


        public Pessoas AtualizaDadosPessoa(int idPessoa)
        {
            var pessoa = _pessoasRepository.GetPessoasById(idPessoa);
            pessoa._Carteira = _pessoasRepository.GetCarteiraByIdPessoa(pessoa.Id);
            pessoa._MeusPools = _pessoasRepository.GetListPoolBensByIdPessoa(pessoa.Id);

            pessoa._MeusLancamentos = _lancamentoRepository.GetLancamentosByIdPessoa(pessoa.Id);

            return pessoa;
        }


        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoByIdCarteira(int idCarteira)
        {
            return _pessoasRepository.GetListComprovanteDepositoByIdCarteira(idCarteira);
        }


        public ComprovanteDeposito InsertComprovanteDeposito(ComprovanteDeposito comp)
        {
            return _pessoasRepository.InsertComprovanteDeposito(comp);
        }


        public ComprovanteDeposito GetComprovanteDepositoById(int id)
        {
            return _pessoasRepository.GetComprovanteDepositoById(id);
        }

        public ComprovanteDeposito UpdateComprovanteDeposito(ComprovanteDeposito comp)
        {
            return _pessoasRepository.UpdateComprovanteDeposito(comp);
        }


        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoPendentes()
        {
            return _pessoasRepository.GetListComprovanteDepositoPendentes();
        }









        public IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaByIdCarteira(int idCarteira)
        {
            return _pessoasRepository.GetListComprovanteRetiradaByIdCarteira(idCarteira);
        }


        public ComprovanteRetirada InsertComprovanteRetirada(ComprovanteRetirada comp)
        {
            return _pessoasRepository.InsertComprovanteRetirada(comp);
        }


        public ComprovanteRetirada GetComprovanteRetiradaById(int id)
        {
            return _pessoasRepository.GetComprovanteRetiradaById(id);
        }

        public ComprovanteRetirada UpdateComprovanteRetirada(ComprovanteRetirada comp)
        {
            return _pessoasRepository.UpdateComprovanteRetirada(comp);
        }


        public IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaPendentes()
        {
            return _pessoasRepository.GetListComprovanteRetiradaPendentes();
        }


        public Pessoas PutValidaEmailInicial(string sEmail)
        {
            return _pessoasRepository.PutValidaEmailInicial(sEmail);
        }

        public Pessoas PutValidaUsuarioInicial(string sUsuario)
        {
            return _pessoasRepository.PutValidaUsuarioInicial(sUsuario);
        }


        public IEnumerable<Pessoas> GetListPessoas(bool fAll)
        {
            return _pessoasRepository.GetListPessoas(fAll);
        }


        public Carteira GetCarteiraByIdPessoa(int idPessoa)
        {
            return _pessoasRepository.GetCarteiraByIdPessoa(idPessoa);
        }


        public IEnumerable<HistoricoCarteira> GetHistoricoByIdPessoa(int idPessoa)
        {
            return _pessoasRepository.GetHistoricoByIdPessoa(idPessoa);
        }

        public IEnumerable<CarteiraLog> GetCarteiraLogByIdPessoa(int idPessoa)
        {
            return _pessoasRepository.GetCarteiraLogByIdPessoa(idPessoa);
        }

        public IEnumerable<PessoasGenricList> GetListReduzidoPessoas()
        {
            return _pessoasRepository.GetListReduzidoPessoas();
        }

        public InformacoesAdminHome GetInformacoesAdminHome()
        {
            return _pessoasRepository.GetInformacoesAdminHome();
        }

        public IEnumerable<PessoaInvestimentoPoolsValores> GetListHomeInvestidores()
        {
            return _pessoasRepository.GetListHomeInvestidores();
        }

        IEnumerable<PainelRentabilidadeMes> GetRentabilidadeMesBens(int idPool, int iMes, int iAno)
        {

            return _pessoasRepository.GetRentabilidadeMesBens(idPool, iMes, iAno);
        }

        public ValidToken createTokenAcessMobile(int idUsuario, string Device)
        {
            return _pessoasRepository.createTokenAcessMobile(idUsuario, Device);
        }

        public bool validaTokenAcessMobile(int idUsuario, string sHash)
        {
            return _pessoasRepository.validaTokenAcessMobile(idUsuario, sHash);
        }

        public Pessoas PutValidaCPFInicial(string sCPF)
        {
            return _pessoasRepository.PutValidaCPFInicial(sCPF);
        }

        public Pessoas PutValidaTAXInicial(string sTAX)
        {
            return _pessoasRepository.PutValidaTAXInicial(sTAX);
        }

        public Pessoas PutValidaSocialInicial(string sSocial)
        {
            return _pessoasRepository.PutValidaSocialInicial(sSocial);
        }

        public IEnumerable<Pessoas> GetListPessoasIndex(bool fAll)
        {
            return _pessoasRepository.GetListPessoasIndex(fAll);
        }

        public RecoveryPassword ReceveryPasswordByEmail(string sEmail)
        {
            return _pessoasRepository.ReceveryPasswordByEmail(sEmail);
        }

        public bool RecoveryPasswordValidaToken(string sToken)
        {
            return _pessoasRepository.RecoveryPasswordValidaToken(sToken);

        }

        public bool ReceveryPasswordUpdate(int idUsuario, string password, string sToken)
        {
            return _pessoasRepository.ReceveryPasswordUpdate(idUsuario, password, sToken);

        }

        public bool InsertLogEmailApp(LogEmailApp log)
        {
            return _pessoasRepository.InsertLogEmailApp(log);
        }

        public bool DeleteLogEmailApp(int idLog)
        {
            return _pessoasRepository.DeleteLogEmailApp(idLog);
        }

        public IEnumerable<LogEmailApp> GetListLogEmailApp(int idPessoa)
        {
            return _pessoasRepository.GetListLogEmailApp(idPessoa);
        }

        public HelpInterno InsertHelpInterno(HelpInterno help)
        {
            return _pessoasRepository.InsertHelpInterno(help);
        }

        public HelpInterno GetHelpinternoById(int id)
        {
            return _pessoasRepository.GetHelpinternoById(id);
        }

        public IEnumerable<HelpInterno> GetListHelpinterno()
        {
            return _pessoasRepository.GetListHelpinterno();
        }

        public Pessoas GetDadosPessoaById(int idPessoa)
        {
            Usuarios user = new Usuarios();
            Pessoas pess = new Pessoas();


            //capturar obj usuario + obj pessoa 
            user = _usuariosRepository.GetUsuarioByIdPessoa(idPessoa);
              
            pess = _pessoasRepository.GetPessoaById(user.IdPessoa);


            pess._Usuario = user;
            pess._Carteira = _pessoasRepository.GetCarteiraByIdPessoa(pess.Id);
            pess._Carteira._Comprovantes = null; 
            return pess;
        }
    }
}
