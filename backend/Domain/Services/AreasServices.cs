﻿using Domain.IRepository;
using Domain.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Services
{
    public class AreasServices : IAreasServices
    {private readonly IAreasRepository _areasRepository;

    public AreasServices(IAreasRepository  areasRepository)
        {
            _areasRepository = areasRepository;
        }

        public IEnumerable<Domain.Entities._2BTrust.Areas> GetListAreas()
        {
            return _areasRepository.GetListAreas();
        }

        public Domain.Entities._2BTrust.Areas InsertAreas(Domain.Entities._2BTrust.Areas area)
        {
            return _areasRepository.InsertAreas(area);
        }

        public Domain.Entities._2BTrust.Areas GetAreasById(int idArea)
        {
            return _areasRepository.GetAreasById(idArea);
        }

        public Domain.Entities._2BTrust.Areas UpdateAreas(Domain.Entities._2BTrust.Areas area)
        {
            return _areasRepository.UpdateAreas(area);
        }
    }
}
