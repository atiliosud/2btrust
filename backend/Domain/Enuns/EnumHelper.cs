﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Enuns
{
    public static class EnumHelper
    {
        /// <summary>
        /// Retrieve the description on the enum, e.g.
        /// [Description("Bright Pink")]
        /// BrightPink = 2,
        /// Then when you pass in the enum, it will retrieve the description
        /// </summary>
        /// <param name="en">The Enumeration</param>
        /// <returns>A string representing the friendly name</returns>
        public static string GetDescription(System.Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }

        public static string GetDescription<TEnum>(this TEnum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            if (fi != null)
            {
                var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    return attributes[0].Description;
                }
            }

            return value.ToString();
        }

        /// <summary>
        /// Build a select list for an enum
        /// </summary>
        public static SelectList SelectListFor<T>() where T : struct
        {
            Type t = typeof(T);
            return !t.IsEnum ? null
                             : new SelectList(BuildSelectListItems(t), "Value", "Text");
        }

        /// <summary>
        /// Build a select list for an enum with a particular value selected 
        /// </summary>
        public static SelectList SelectListFor<T>(T selected) where T : struct
        {
            Type t = typeof(T);
            return !t.IsEnum ? null
                             : new SelectList(BuildSelectListItems(t), "Value", "Text", selected.ToString());
        }

        private static IEnumerable<SelectListItem> BuildSelectListItems(Type t)
        {
            return System.Enum.GetValues(t)
                       .Cast<System.Enum>()
                       .Select(e => new SelectListItem { Value = e.ToString(), Text = e.GetDescription() });
        }

    }


    public class EnumListElem
    {
        public string Nome { get; set; }
        public int ID { get; set; }

    }
    public enum EnumVariavelValorFiltro
    {
        [Description("Minha Clínica")]
        idClinica = 1,
        [Description("Meu Usuário")]
        idUsuario = 2,
        [Description("Meu Profissional")]
        idProfissional = 3,
        [Description("Minha Especialidade")]
        idEspecialidade = 4,
        [Description("Digitável")]
        Aberto = 30

    }

    public enum EnumTipoConteudoContainer
    {
        [Description("Alerta")]
        Alerta = 1,
        [Description("Gráfico")]
        Grafico = 2,
        [Description("Cabeçalho")]
        Cabeçalho = 3

    }


    public enum EnumOperadorFiltro
    {
        [Description("Maior")]
        maior = 1,
        [Description("Maior Igual")]
        maior_igual = 2,
        [Description("Menor")]
        menor = 3,
        [Description("Menor Igual")]
        menor_igual = 4,
        [Description("Igual")]
        igual = 5,
        [Description("Diferente")]
        diferente = 6,
        [Description("Contem")]
        like = 7,
        [Description("Intervalo")]
        between = 8,
        [Description("OrderBy")]
        OrderBy = 9

    }
    public enum EnumPrioridadeAlerta
    {
        [Description("Alta Prioridade")]
        Alta = 1,
        [Description("Média Prioridade")]
        Media = 2,
        [Description("Baixa Prioridade")]
        Baixa = 3

    }
    public enum EnumTipoRetorno
    {

        Lista = 0,
        Unico = 1

    }

    public enum EnumTipoCampo
    {

        Inteiro = 1,
        String = 2,
        Date = 3,
        Boolean = 4

    }

    public enum EnumTipoQuery
    {
        [Description("Primeiro")]
        FirstOrDefault = 1,
        [Description("Primeiro - Gráfico")]
        FirstOrDefaultGrafico = 9,
        [Description("Último")]
        LastOrDefault = 2,
        [Description("Último - Gráfico")]
        LastOrDefaultGrafico = 8,
        [Description("Total (Count)")]
        Count = 3,
        [Description("Total (Count) - Gráfico")]
        CountGrafico = 7,
        [Description("Lista")]
        Lista = 4,
        [Description("Soma")]
        Sum = 5,
        [Description("Soma - Gráfico")]
        SumGrafico = 10,
        [Description("Top (5)")]
        Rank5 = 6,


    }

    public enum EnumObjetoDataSourceTipoRetorno
    {

        Alerta = 1,
        Gráfico = 2,
        Lista = 3,
        Único = 4

    }
    public enum EnumIconAlerta
    {

        Exclamacao = 1,
        Sino = 2,
        Interrogacao = 3,
        Error = 4,
        Paciente = 5,
        Atendimento = 6

    }

    public enum EnumPerfil
    {

        Administrador = 2,
        Investidor_Inicial = 3,
        Investidor_Manual = 6,
        Administrador_Super = 7,
        Inicio = 5

    }


    public enum EnumTipoGrafico
    {
        [Description("Linha")]
        Linha = 1,
        [Description("Barra")]
        Barra = 2,
        //[Description("Pizza")]
        //Pizza = 2,
        //[Description("Barra Vertical")]
        //BarraVertical = 3,
        //[Description("Barra Horizontal")]
        //BarraHorizontal = 4,

    }

    public enum EnumPeriodoGrafico
    {
        [Description("Cinco (5) dias")]
        Cinco = 5,
        [Description("Dez (10) dias")]
        Dez = 10,
        [Description("Quinze (15) dias")]
        Quinze = 15,
        [Description("Vinte (20) dias")]
        Vinte = 20,
        [Description("Vinte e Cinco (25) dias")]
        VinteCinco = 25,
        [Description("Trinta (30) dias")]
        Trinta = 30,
        [Description("Sessenta (60) dias")]
        Sessenta = 60,
        [Description("Noventa (90) dias")]
        Noventa = 90,

    }

    public enum EnumMeses
    {
        [Description("Sempre Mês Atual")]
        Atual = 0,

        [Description("Janeiro")]
        Janeiro = 1,

        [Description("Fevereiro")]
        Fevereiro = 2,

        [Description("Março")]
        Marco = 3,

        [Description("Abril")]
        Abril = 4,

        [Description("Maio")]
        Maio = 5,

        [Description("Junho")]
        Junho = 6,

        [Description("Julho")]
        Julho = 7,

        [Description("Agosto")]
        Agosto = 8,

        [Description("Setembro")]
        Setembro = 9,

        [Description("Outubro")]
        Outubro = 10,

        [Description("Novembro")]
        Novembro = 11,

        [Description("Dezembro")]
        Dezembro = 11,



    }

    public enum EnumStatusCarteira
    {
        Inativo = 2,
        Aguardando = 5,
        Aprovado = 6,
        Recusado = 7,
        Processando = 8
    }

    public enum EnumStatusGeral
    {
        Ativo = 1,
        Inativo = 2,
        Bloqueado = 3,
        Strike = 4
    }


    public enum EnumInvestimento
    {
        Ativo = 1,
        SoicitacaoVenda = 9,
        VendaAprovada = 10,
        VendaRecusada = 11,
        SoicitacaoCompra = 12,
        CompraAprovada= 13,
        CompraRecusada = 14
    }

    public enum EnumTipoBem
    {
        [Description("Propriedade")]
        Propriedade,
        [Description("Ação")]
        Ação
    }
}