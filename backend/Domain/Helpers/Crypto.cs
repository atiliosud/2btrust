﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System;

namespace Domain.Helpers
{
    public static class Crypto
    {
        // Chaves de criptografias abritrárias
        private const string ActionKey = "EA81AA1D5FC1EC53E84F30AA746139EEBAFF8A9B76638895";
        private const string ActionIv = "87AF7EA221F3FFF5";


        public static string getMD5Hash(string input)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }


            return sb.ToString();
        }
        public static string getNewSalt(int iLength)
        {
            StringBuilder sb = new StringBuilder(iLength);
            Random rand = new Random();
            for (int i = 0; i < iLength; i++)
            {
                sb.Append(Convert.ToChar(rand.Next(33, 126)));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Criptografia automatizada
        /// </summary>
        /// <param name="data">String que será criptografada.</param>
        /// <returns>String criptografada</returns>
        public static string ActionEncrypt(string data)
        {
            return getMD5Hash(data);
        }
    }
}
