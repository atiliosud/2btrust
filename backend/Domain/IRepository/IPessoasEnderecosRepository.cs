﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IPessoasEnderecosRepository
    {
        PessoasEnderecos GetEnderecoById(int idPessoaEndereco);
        PessoasEnderecos GetEnderecoByIdPessoa(int idPessoa);
        PessoasEnderecos InsertEndereco(PessoasEnderecos pes);
        PessoasEnderecos UpdateEndereco(PessoasEnderecos pes);
    }
}
