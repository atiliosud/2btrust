﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IPoolBensRepository
    {
        PoolBens InsertPoolBens(PoolBens con);
        PoolBens UpdatePoolBens(PoolBens con);
        IEnumerable<PoolBens> GetListPoolBens();
        PoolBens GetPoolBensById(int id);
        IEnumerable<PoolBens> GetListPoolParaInvestimento();

        PoolBens GetDetailsPoolParaInvestimentoById(int id);

        IEnumerable<PoolsBensRentabilidadePerformance> GetListPoolsBensRentabilidadePerformance(int idPoolBem);
        IEnumerable<PoolBensPessoas> GetListMeusPoolInvestidos(int idPessoa);
        PoolsBensRentabilidadePerformance InsertPoolsBensRentabilidadePerformance(PoolsBensRentabilidadePerformance pools);
        bool DesativarAtivarPoolsBensRentabilidadePerformance(int id, bool flgAtivo); 
    }
}
