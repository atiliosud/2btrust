﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IRentabilidadeRepository
    {
        Rentabilidade InsertRentabilidade(Rentabilidade luc);
        Rentabilidade UpdateRentabilidade(Rentabilidade luc);
        Rentabilidade GetRentabilidadeByIdBem(int idBem);
        IEnumerable<Rentabilidade> GetListRentabilidadeByIdBem(int idBem);
        Rentabilidade GetRentabilidadeById(int id); 

    }
}
