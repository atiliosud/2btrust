﻿using Domain.Entities;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IPessoasRepository
    {
        IEnumerable<PoolBensPessoas> GetListPoolBensByIdPessoaSimples(int idPessoa);

        Pessoas GetPessoaById(int idPessoa);
        Pessoas InsertPessoa(Pessoas pes);
        bool LiberacaoViaEmail(int idPessoa);
        Pessoas UpdatePessoa(Pessoas pessoa);

        Pessoas GetPessoasById(int idPessoa);
        Carteira GetCarteiraByIdPessoa(int idPessoa);

        IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoByIdCarteira(int idCarteira);
        ComprovanteDeposito GetComprovanteDepositoById(int id);
        ComprovanteDeposito InsertComprovanteDeposito(ComprovanteDeposito comp);
        ComprovanteDeposito UpdateComprovanteDeposito(ComprovanteDeposito comp);
        IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoPendentes();


        IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaByIdCarteira(int idCarteira);
        ComprovanteRetirada GetComprovanteRetiradaById(int id);
        ComprovanteRetirada InsertComprovanteRetirada(ComprovanteRetirada comp);
        ComprovanteRetirada UpdateComprovanteRetirada(ComprovanteRetirada comp);
        IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaPendentes();

        Carteira InsertCarteira(Carteira car);
        Carteira UpdateCarteira(Carteira car);

        IEnumerable<PoolBensPessoas> GetListPoolBensByIdPessoa(int idPessoa);

        Pessoas AtualizaDadosPessoa(int idPessoa);


        Pessoas PutValidaUsuarioInicial(string sUsuario);

        Pessoas PutValidaEmailInicial(string sEmail);

        IEnumerable<Pessoas> GetListPessoas(bool fAll);

        IEnumerable<HistoricoCarteira> GetHistoricoByIdPessoa(int idPessoa);

        IEnumerable<PessoasGenricList> GetListReduzidoPessoas();

        InformacoesAdminHome GetInformacoesAdminHome();

        IEnumerable<PessoaInvestimentoPoolsValores> GetListHomeInvestidores();

        IEnumerable<PainelRentabilidadeMes> GetRentabilidadeMesBens(int idPool, int iMes, int iAno);

        IEnumerable<PainelRentabilidadeMes> GetRentabilidadeMesBens(int idPool);

        ValidToken createTokenAcessMobile(int idUsuario, string Device);

        bool validaTokenAcessMobile(int idUsuario, string sHash);

        IEnumerable<CarteiraLog> GetCarteiraLogByIdPessoa(int idPessoa);
        Pessoas PutValidaCPFInicial(string sCPF);
        Pessoas PutValidaTAXInicial(string sTAX);
        Pessoas PutValidaSocialInicial(string sSocial);
        IEnumerable<Pessoas> GetListPessoasIndex(bool fAll);

        RecoveryPassword ReceveryPasswordByEmail(string sEmail);
        bool RecoveryPasswordValidaToken(string sToken);
        bool ReceveryPasswordUpdate(int idUsuario, string password, string sToken);


        bool InsertLogEmailApp(LogEmailApp log);
        bool DeleteLogEmailApp(int idLog);
        IEnumerable<LogEmailApp> GetListLogEmailApp(int idPessoa);

        HelpInterno InsertHelpInterno(HelpInterno help);
        HelpInterno GetHelpinternoById(int id);
        IEnumerable<HelpInterno> GetListHelpinterno();
    }
}
