﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface ILancamentosRepository
    {
        Lancamentos InsertLancamentos(Lancamentos con);
        Lancamentos UpdateLancamentos(Lancamentos con);
        IEnumerable<Lancamentos> GetListLancamentos();
        Lancamentos GetLancamentosById(int id);
        IEnumerable<Lancamentos> GetLancamentosByIdPessoa(int idPessoa);
        IEnumerable<Lancamentos> GetLancamentosByIdPoolBem(int idPoolBem);
        IEnumerable<PessoaPoolsInvestidos> GetPessoasPoolsInvestido();
        PessoaPoolsInvestidos GetPessoasPoolsInvestido(int idPoolBemPessoa);
    }
}
