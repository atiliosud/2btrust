﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IBensRepository
    {
        Bens InsertBens(Bens bem);
        Bens UpdateBens(Bens bem); 
        Bens GetBensById(int id);
        IEnumerable<Bens> GetListBens();

        IEnumerable<Domain.Entities._2BTrust.Bens> GetListBensIdPool(int idPool);
        IEnumerable<BensImagens> GetListImagens(int idBem);
        BensImagens InsertImagens(BensImagens bemImage);
        BensImagens UpdateImagens(BensImagens bemImage);

        IEnumerable<BensGastos> GetListBensGastosByIdBem(int idBem);
        BensGastos InsertBensGasto(BensGastos bemG);
        BensGastos UpdateBensGasto(BensGastos bemG);
        BensGastos GetBensGastoById(int intIdBemGastos);
    }
}
