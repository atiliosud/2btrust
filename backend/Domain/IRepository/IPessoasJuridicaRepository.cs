﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IPessoasJuridicaRepository
    {
        PessoasJuridica GetPessoasJuridicasByIdPessoa(int IdPessoa);
        PessoasJuridica InsertPessoasJuridicas(PessoasJuridica pes);
        PessoasJuridica UpdatePessoasJuridicasByIdPessoa(PessoasJuridica pes);
    }
}
