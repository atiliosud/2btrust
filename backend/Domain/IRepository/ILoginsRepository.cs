﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface ILoginsRepository
    {

        Logins LoginUsuario(string sLogin, string sSenha);
        Logins Insertlogins(Logins login);
        Logins Updatelogins(Logins login);
        Logins LoginUsuarioByID(int idUsuario);
    }
}
