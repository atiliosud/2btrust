﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IControlesRepository
    {
        IEnumerable<Controles> GetControlesByIdGrupo(int idGrupo);

        IEnumerable<Controles> GetListControles();

        Controles InsertControles(Controles Controles);

        Controles GetControlesById(int idAcoes);

        Controles UpdateControles(Controles Controles);

        IEnumerable<Controles> GetListControlesPai();
    }
}
