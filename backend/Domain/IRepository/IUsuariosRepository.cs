﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IUsuariosRepository
    {
        Usuarios LoginUsuario(string sLogin, string sSenha);

        Usuarios GetUsuarioById(int idUsuario);

        Usuarios InsertUsuario(Usuarios user);

        int LiberacaoViaEmail(string saltKey);

        Usuarios UpdateUsuario(Usuarios usuarios);

        Usuarios GetUsuarioByIdPessoa(int idPessoa);

        bool ValidaUsuarioPlataforma(int idUsuario, bool isValid);
    }
}
