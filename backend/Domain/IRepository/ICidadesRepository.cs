﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface ICidadesRepository
    {
        IEnumerable<Cidades> GetListCidadesByIdEstado(int idEstado);

        Cidades GetListCidadesByIdCidade(int idCidade);

        Cidades InsertCidade(Cidades cid);
        Estados getEstadoByIdCidade(int idCidade);
    }
}
