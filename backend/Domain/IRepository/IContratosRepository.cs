﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IRepository
{
    public interface IContratosRepository
    {
        Contratos InsertContratos(Contratos con);
        Contratos UpdateContratos(Contratos con); 
        IEnumerable<Contratos> GetListContratos();
        Contratos GetContratosById(int id); 
    }
}
