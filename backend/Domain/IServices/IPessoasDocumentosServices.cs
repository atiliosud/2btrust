﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IServices
{
    public interface IPessoasDocumentosServices
    {
        IEnumerable<PessoasDocumentos> GetListPessoasDocumentosByIdPessoa(int idPessoa);
        PessoasDocumentos InsertPessoasDocumentos(PessoasDocumentos pes);
        PessoasDocumentos UpdatePessoasDocumentos(PessoasDocumentos pes);
    }
}
