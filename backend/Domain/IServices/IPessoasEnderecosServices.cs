﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IServices
{
    public interface IPessoasEnderecosServices
    {
        PessoasEnderecos GetEnderecoById(int idPessoaEndereco);
        PessoasEnderecos GetEnderecoByIdPessoa(int idPessoa);
        PessoasEnderecos InsertEnderecoByIdPessoa(PessoasEnderecos pes);
        PessoasEnderecos UpdateEnderecoByIdPessoa(PessoasEnderecos pes);
    }
}
