﻿using Domain.Entities;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IServices
{
    public interface IPessoasServices
    {
        IEnumerable<Entities._2BTrust.Pessoas> getClientInvests(int idPessoa);

        Pessoas LoginUsuarioSimples(string sLogin, string sSenha);
        Pessoas GetPessoaMeusLancamentos(int idPessoa);
        Pessoas GetPessoaMeusPools(int idPessoa);


        Pessoas LoginUsuario(string sLogin, string sSenha); 

        Pessoas InformacoaHomeValoresMobile(int idUsuario, string sHash);

        bool CadastroInicial(Pessoas pessoa);

        bool LiberacaoViaEmail(string saltKey);

        Pessoas GetPessoasById(int idPessoa);

        Pessoas UpdatePessoaMenu(Pessoas pessoa);

        Pessoas UpdatePessoaMenuEndereco(Pessoas pessoa);

        Pessoas UpdatePessoaMenuDocumento(Pessoas pessoa);

        Pessoas AtualizaDadosPessoa(int idPessoa);

        IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoByIdCarteira(int idCarteira);

        ComprovanteDeposito InsertComprovanteDeposito(ComprovanteDeposito comp);

        ComprovanteDeposito GetComprovanteDepositoById(int id);

        ComprovanteDeposito UpdateComprovanteDeposito(ComprovanteDeposito comp);

        IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoPendentes();




        IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaByIdCarteira(int idCarteira);

        ComprovanteRetirada InsertComprovanteRetirada(ComprovanteRetirada comp);

        ComprovanteRetirada GetComprovanteRetiradaById(int id);

        ComprovanteRetirada UpdateComprovanteRetirada(ComprovanteRetirada comp);

        IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaPendentes();


        Pessoas PutValidaEmailInicial(string sEmail);

        Pessoas PutValidaUsuarioInicial(string sUsuario);

        IEnumerable<Pessoas> GetListPessoas(bool fAll);

        Carteira GetCarteiraByIdPessoa(int idPessoa);

        IEnumerable<HistoricoCarteira> GetHistoricoByIdPessoa(int idPessoa);
        IEnumerable<PessoasGenricList> GetListReduzidoPessoas();
        InformacoesAdminHome GetInformacoesAdminHome();
        IEnumerable<PessoaInvestimentoPoolsValores> GetListHomeInvestidores();

        ValidToken createTokenAcessMobile(int idUsuario, string Device);
        bool validaTokenAcessMobile(int idUsuario, string sHash);
        IEnumerable<CarteiraLog> GetCarteiraLogByIdPessoa(int idPessoa);
        Pessoas PutValidaCPFInicial(string sCPF);
        Pessoas PutValidaTAXInicial(string sTAX);
        Pessoas PutValidaSocialInicial(string sSocial);
        IEnumerable<Pessoas> GetListPessoasIndex(bool fAll);

        RecoveryPassword ReceveryPasswordByEmail(string sEmail);
        bool RecoveryPasswordValidaToken(string sToken);
        bool ReceveryPasswordUpdate(int idUsuario, string password, string sToken);

        bool InsertLogEmailApp(LogEmailApp log);
        bool DeleteLogEmailApp(int idLog);
        IEnumerable<LogEmailApp> GetListLogEmailApp(int idPessoa);
        HelpInterno InsertHelpInterno(HelpInterno help);
        HelpInterno GetHelpinternoById(int id);
        IEnumerable<HelpInterno> GetListHelpinterno();
        Pessoas GetDadosPessoaById(int idPessoa);
    }
}
