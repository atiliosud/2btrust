﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IServices
{
    public interface IAreasServices
    {
        IEnumerable<Areas> GetListAreas();

        Areas InsertAreas(Areas area);

        Areas GetAreasById(int idArea);

        Areas UpdateAreas(Areas area);
    }
}
