﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IServices
{
    public interface IUsuariosServices
    {
        Usuarios LoginUsuario(string sLogin, string sSenha);
        bool ValidaUsuarioPlataforma(int idUsuario, bool isValid);
    }
}
