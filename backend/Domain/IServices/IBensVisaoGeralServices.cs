﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IServices
{
    public interface IBensVisaoGeralServices
    {
        BensVisaoGeral InsertBensVisaoGeral(BensVisaoGeral bem);
        BensVisaoGeral UpdateBensVisaoGeral(BensVisaoGeral bem);
        BensVisaoGeral GetBensVisaoGeralById(int id);
        IEnumerable<BensVisaoGeral> GetListBensVisaoGeral();


        IEnumerable<BensVisaoGeralImagens> GetListImagens(int idBem);
        BensVisaoGeralImagens InsertImagens(BensVisaoGeralImagens bemImagem);
        BensVisaoGeralImagens UpdateImagens(BensVisaoGeralImagens bemImagem);
    }
}
