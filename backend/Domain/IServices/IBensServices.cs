﻿using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.IServices
{
    public interface IBensServices
    {
        Bens InsertBens(Bens bem);
        Bens UpdateBens(Bens bem);
        Bens GetBensById(int id);
        IEnumerable<Bens> GetListBens();


        IEnumerable<BensImagens> GetListImagens(int idBem);
        BensImagens InsertImagens(BensImagens bemImage);
        BensImagens UpdateImagens(BensImagens bemImage);
    }
}
