﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Presentation.Controllers
{
    public class MeuCadastroController : BaseController
    {
        // GET: MeuCadastro
        public async Task<ActionResult> DadosPessoais()
        {

            try
            {


                PessoasModel model = new PessoasModel();


                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + SessionManager.usuarioLogado.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<PessoasModel>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;
                    model._Usuario._Login.UltimaSenha = model._Usuario._Login.Senha;

                    model.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    model._Usuario.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    model._Usuario._Login.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    model._PessoaFisica.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    if (model._PessoaJuridica != null)
                        model._PessoaJuridica.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;


                    return View(model);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(model);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpPost]
        public async Task<ActionResult> DadosPessoais(PessoasModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var dto = Mapper.Map<Pessoas>(model);
                    var sLocation = "/Pessoa/PutCadastroPessoaMenu";

                    if (string.IsNullOrEmpty(dto._Usuario._Login.Senha))
                        dto._Usuario._Login.Senha = model._Usuario._Login.UltimaSenha;

                    IRestResponse response = await PutObjectRestAsync<Pessoas>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<Pessoas>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }



            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Endereco(PessoasModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var dto = Mapper.Map<Pessoas>(model);
                    var sLocation = "/Pessoa/PutCadastroPessoaEnderecoMenu";


                    IRestResponse response = await PutObjectRestAsync<Pessoas>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<Pessoas>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }




            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> Endereco()
        {

            try
            {


                PessoasModel model = new PessoasModel();


                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + SessionManager.usuarioLogado.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<PessoasModel>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;
                    model._Usuario._Login.UltimaSenha = model._Usuario._Login.Senha;

                    model.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    if (model._Endereco == null) model._Endereco = new PessoasEnderecosModel();
                    model._Endereco.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    model._Endereco.IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id;


                    return View(model);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(model);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<ActionResult> Documentos()
        {
            try
            {
                await populaViewBagEstados();


                await populaViewBagDocumentos();

                var listaOrgaos = new List<OrgaosEmissores>();
                listaOrgaos.Add(new OrgaosEmissores()
                {
                    Id = 0,
                    Nome = "Escolha um estado antes"
                });
                ViewBag.OrgaoEmissor = listaOrgaos;


                PessoasModel model = new PessoasModel();


                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + SessionManager.usuarioLogado.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<PessoasModel>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;
                    model._Usuario._Login.UltimaSenha = model._Usuario._Login.Senha;

                    model.objDocumento = new PessoasDocumentosModel();
                    SessionManager.ListaDocumentosFirstCad = model._Documentos.ToList();


                    return View(model);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(model);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        [HttpPost]
        public async Task<ActionResult> AtualizaDados()
        {
            try
            {
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/AtualizaDadosPessoa?idPessoa=" + SessionManager.usuarioLogado.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var model = JsonConvert.DeserializeObject<PessoasModel>(response.Content);
                    if (model._MeusPools != null)
                        SessionManager.usuarioLogado._MeusPools = model._MeusPools;
                    if (model._Carteira != null)
                        SessionManager.usuarioLogado._Carteira = model._Carteira;

                    return Json("true", JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json("false", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception rx)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Documento(PessoasModel model)
        {

            try
            { 

                    model._Documentos = SessionManager.ListaDocumentosFirstCad;
                    var dto = Mapper.Map<Pessoas>(model);
                    var sLocation = "/Pessoa/PutCadastroPessoaDocumentoMenu";


                    IRestResponse response = await PutObjectRestAsync<Pessoas>(sLocation, dto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {

                        dto = JsonConvert.DeserializeObject<Pessoas>(response.Content);
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = dto;

                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                    retorno.objetoRetorno = null;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }




            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }


        public async Task<JsonResult> SetDocumentosCadastro(int idPessoaDocumento, int idDocumento, string sDocumento, string sNumero, int idUF, string sUF, int idOrgao, string sOrgao)
        {
            try
            {
                List<PessoasDocumentos> ListaDocumento = new List<PessoasDocumentos>();
                PessoasDocumentosModel novo = new PessoasDocumentosModel()
                {
                    Id = idPessoaDocumento,
                    IdDocumento = idDocumento,
                    Conteudo = sNumero,
                    IdEstado = idUF,
                    IdOrgaoEmissor = idOrgao, IdPessoa = SessionManager.usuarioLogado.Id
                };


                SessionManager.ListaDocumentosFirstCad.Add(novo);



                return Json(novo.Id, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                //createLog(SessionManager.UsuarioLogado.ID, "MSPessoa", "Error - dellTelefone idTelefone = " + idTelefone, ex.Message);
                return Json(0, JsonRequestBehavior.AllowGet);
                throw;
            }
        }


        public async Task<JsonResult> deleteDocumentoCadastro(int idPessoaDocumento)
        {
            try
            {
                var documentToRemove = SessionManager.ListaDocumentosFirstCad.Where(c => c.Id == idPessoaDocumento).FirstOrDefault();

                SessionManager.ListaDocumentosFirstCad.Remove(documentToRemove);

                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);

            }
        }


        private async Task populaViewBagDocumentos()
        {
            try
            {

                List<DocumentosModel> lstDocumentos = new List<DocumentosModel>();
                IEnumerable<DocumentosModel> documentos = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Documentos>>("/Documentos/GetDocumentos");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Documentos>>(response.Content);

                    documentos = Mapper.Map<IEnumerable<DocumentosModel>>(dto);

                    if (documentos != null)
                    {
                        ViewBag.Documentos = documentos;
                    }
                    else
                    {
                        lstDocumentos.Add(new DocumentosModel()
                        {
                            Id = 0,
                            Nome = "INDEFINIDO"
                        });

                        ViewBag.Documentos = lstDocumentos;
                    }

                }
                else
                {
                    lstDocumentos.Add(new DocumentosModel()
                    {
                        Id = 0,
                        Nome = "INDEFINIDO"
                    });

                    ViewBag.Documentos = lstDocumentos;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private async Task populaViewBagEstados()
        {
            try
            {
                List<EstadosModel> LstEstados = new List<EstadosModel>();
                IEnumerable<EstadosModel> estados = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Estados>>("/estados/getEstados");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Estados>>(response.Content);

                    estados = Mapper.Map<IEnumerable<EstadosModel>>(dto);

                    if (estados != null)
                    {
                        ViewBag.Estados = estados;
                    }
                    else
                    {
                        LstEstados.Add(new EstadosModel()
                        {
                            Sigla = "NF",
                            Nome = "INDEFINIDO"
                        });

                        ViewBag.Estados = LstEstados;
                    }

                }
                else
                {
                    LstEstados.Add(new EstadosModel()
                    {
                        Sigla = "NF",
                        Nome = "INDEFINIDO"
                    });

                    ViewBag.Estados = LstEstados;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }

}