﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Domain.Enuns;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class lancamentoRetirada
    {
        public string DtInclusaoAdm { get; set; }
        public string mValorLancamento { get; set; }
        public int idCarteira { get; set; }
        public int idPessoa { get; set; }
    }
    public class lancamentoDeposito
    {
        public string DtInclusaoAdm { get; set; }
        public string mValorLancamento { get; set; }
        public int idCarteira { get; set; }
    }
    public class CarteiraController : BaseController
    {
        public CarteiraController()
        {
            

        }

        // GET: Carteira


        public async Task<ActionResult> Retirada(int? m)
        {

            try
            {


                IEnumerable<ComprovanteRetiradaModel> model;

                if (SessionManager.usuarioLogado._Usuario._Grupo.Id == (int)EnumPerfil.Administrador_Super)
                {
                    IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetListPessoas?fAll=false");
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var models = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = models;


                        SessionManager.ListaInvestidoresManuais = models.ToList();

                        //visualiza sessao como um investidor selecionado



                        if (m != null)
                        {

                            #region investidor interno
                            IRestResponse responseCarteira = await PostRestAsync<Pessoas>("/Pessoa/GetCarteiraByIdPessoa?idPessoa=" + (int)m);
                            if (responseCarteira.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                var pessoaModel = JsonConvert.DeserializeObject<PessoasModel>(responseCarteira.Content);

                                IRestResponse responseInterno = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetListComprovanteRetiradaByIdCarteira?idCarteira=" + pessoaModel._Carteira.Id);
                                if (responseInterno.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteRetiradaModel>>(responseInterno.Content);

                                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseInterno.StatusCode);
                                    retorno.objetoRetorno = model;

                                    ViewBag.IdPessoa = pessoaModel.Id;
                                    ViewBag.IdInvestidorManual = pessoaModel.Id;
                                    ViewBag.NomeInvestidorManual = pessoaModel.Nome;
                                    ViewBag.IdCarteiraInvestidorManual = pessoaModel._Carteira.Id;
                                    ViewBag.SaldoCarteiraInvestidorManual = pessoaModel._Carteira.Saldo;

                                    return View(retorno);

                                }
                                else
                                {
                                    ViewBag.IdPessoa = SessionManager.usuarioLogado.Id;
                                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseInterno.StatusCode);
                                    retorno.objetoRetorno = null;

                                    return View(retorno);
                                }

                            }
                            else
                            {
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseCarteira.StatusCode);
                                retorno.objetoRetorno = null;

                                return View(retorno);
                            }


                            #endregion

                        }
                        else
                        {
                            response = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetListComprovanteRetiradaByIdCarteira?idCarteira=" + SessionManager.usuarioLogado._Carteira.Id);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteRetiradaModel>>(response.Content);
                                ViewBag.IdPessoa = SessionManager.usuarioLogado.Id;
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = model;

                                return View(retorno);

                            }
                            else
                            {
                                ViewBag.IdPessoa = SessionManager.usuarioLogado.Id;
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = null;

                                return View(retorno);
                            }

                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                            return View(retorno);
                        }


                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = null;
                        SessionManager.ListaInvestidoresManuais = null;

                        return View(retorno);
                    }
                }
                else
                {

                    IRestResponse response = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetListComprovanteRetiradaByIdCarteira?idCarteira=" + SessionManager.usuarioLogado._Carteira.Id);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteRetiradaModel>>(response.Content);
                        ViewBag.IdPessoa = SessionManager.usuarioLogado.Id;
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = model;

                        return View(retorno);

                    }
                    else
                    {
                        ViewBag.IdPessoa = SessionManager.usuarioLogado.Id;
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = null;

                        return View(retorno);
                    }

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                    return View(retorno);


                }







            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<ActionResult> Depositos(int? m)
        {

            try
            {


                IEnumerable<ComprovanteDepositoModel> model;

                if (SessionManager.usuarioLogado._Usuario._Grupo.Id == (int)EnumPerfil.Administrador_Super)
                {
                    IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetListPessoas?fAll=false");
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var models = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = models;


                        SessionManager.ListaInvestidoresManuais = models.ToList();

                        //visualiza sessao como um investidor selecionado



                        if (m != null)
                        {

                            #region investidor interno
                            IRestResponse responseCarteira = await PostRestAsync<Pessoas>("/Pessoa/GetCarteiraByIdPessoa?idPessoa=" + (int)m);
                            if (responseCarteira.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                var pessoaModel = JsonConvert.DeserializeObject<PessoasModel>(responseCarteira.Content);

                                IRestResponse responseInterno = await PostRestAsync<ComprovanteDeposito>("/Pessoa/GetListComprovanteDepositoByIdCarteira?idCarteira=" + pessoaModel._Carteira.Id);
                                if (responseInterno.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteDepositoModel>>(responseInterno.Content);

                                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseInterno.StatusCode);
                                    retorno.objetoRetorno = model;

                                    ViewBag.IdInvestidorManual = pessoaModel.Id;
                                    ViewBag.NomeInvestidorManual = pessoaModel.Nome;
                                    ViewBag.IdCarteiraInvestidorManual = pessoaModel._Carteira.Id;
                                    ViewBag.SaldoCarteiraInvestidorManual = pessoaModel._Carteira.Saldo;

                                    return View(retorno);

                                }
                                else
                                {
                                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseInterno.StatusCode);
                                    retorno.objetoRetorno = null;

                                    return View(retorno);
                                }

                            }
                            else
                            {
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseCarteira.StatusCode);
                                retorno.objetoRetorno = null;

                                return View(retorno);
                            }


                            #endregion

                        }
                        else
                        {
                            response = await PostRestAsync<ComprovanteDeposito>("/Pessoa/GetListComprovanteDepositoByIdCarteira?idCarteira=" + SessionManager.usuarioLogado._Carteira.Id);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteDepositoModel>>(response.Content);

                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = model;

                                return View(retorno);

                            }
                            else
                            {
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = null;

                                return View(retorno);
                            }

                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                            return View(retorno);
                        }


                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = null;
                        SessionManager.ListaInvestidoresManuais = null;

                        return View(retorno);
                    }
                }
                else
                {

                    IRestResponse response = await PostRestAsync<ComprovanteDeposito>("/Pessoa/GetListComprovanteDepositoByIdCarteira?idCarteira=" + SessionManager.usuarioLogado._Carteira.Id);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteDepositoModel>>(response.Content);

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = model;

                        return View(retorno);

                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = null;

                        return View(retorno);
                    }

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                    return View(retorno);


                }







            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public async Task<ActionResult> AnexarComprovante(int id)
        {

            try
            {


                //capturar comprovante by id
                var dto = new ComprovanteDeposito();
                IRestResponse response = await PostRestAsync<ComprovanteDeposito>("/Pessoa/GetComprovanteDepositoById?id=" + id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<ComprovanteDeposito>(response.Content);
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpPost]
        public async Task<ActionResult> Depositar(lancamentoDeposito model)
        {
            try
            {
                ComprovanteDeposito dto = new ComprovanteDeposito()
                {
                    ComprovantePathLocation = "",
                    ComprovantePathLocationIIS = "",
                    FlagComputado = false,
                    FlagNegado = false,
                    FlagPendente = true,
                    IdCarteira = model.idCarteira,
                    IdStatus = Domain.Enuns.EnumStatusCarteira.Aguardando,
                    IdUsuarioInclusao = SessionManager.usuarioLogado.Id,
                    Valor = Convert.ToDecimal(model.mValorLancamento),
                    DtInclusaoAdm = (string.IsNullOrEmpty(model.DtInclusaoAdm)) ? (DateTime?)null : Convert.ToDateTime(model.DtInclusaoAdm),
                    EmailCliente = SessionManager.usuarioLogado._Usuario.Email,
                    EmailCode = ""
                };

                IRestResponse response = await PutObjectRestAsync<ComprovanteDeposito>("/Pessoa/InsertComprovanteDeposito", dto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        dto = JsonConvert.DeserializeObject<ComprovanteDeposito>(response.Content);
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = dto;

                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = "response.Content null";
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                    retorno.objetoRetorno = "errorStatusCode";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Retirar(lancamentoRetirada model)
        {
            try
            {
                ComprovanteRetirada dto = new ComprovanteRetirada()
                {
                    ComprovantePathLocation = "",
                    ComprovantePathLocationIIS = "",
                    FlagComputado = false,
                    FlagNegado = false,
                    FlagPendente = true,
                    IdCarteira = model.idCarteira,
                    IdStatus = Domain.Enuns.EnumStatusCarteira.Aguardando,
                    IdUsuarioInclusao = SessionManager.usuarioLogado.Id,
                    Valor = Convert.ToDecimal(model.mValorLancamento),
                    DtInclusaoAdm = (string.IsNullOrEmpty(model.DtInclusaoAdm)) ? (DateTime?)null : Convert.ToDateTime(model.DtInclusaoAdm),
                    EmailCliente = SessionManager.usuarioLogado._Usuario.Email,
                    EmailCode = ""
                };

                //verificar saldo disponivel
                IRestResponse responseCarteira = await PostRestAsync<Pessoas>("/Pessoa/GetCarteiraByIdPessoa?idPessoa=" + model.idPessoa);
                if (responseCarteira.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var pessoaModel = JsonConvert.DeserializeObject<PessoasModel>(responseCarteira.Content);

                    if (pessoaModel._Carteira.Saldo >= dto.Valor)
                    {
                        IRestResponse response = await PutObjectRestAsync<ComprovanteRetirada>("/Pessoa/InsertComprovanteRetirada", dto);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (response.Content != null)
                            {


                                dto = JsonConvert.DeserializeObject<ComprovanteRetirada>(response.Content);
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = dto;

                            }
                            else
                            {
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                                retorno.objetoRetorno = null;
                            }

                            return Json(retorno, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                            return Json(retorno, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {
                        retorno.statusRetorno = "Insufficient balance to complete this action.";
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }


                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<FineUploaderResult> UploadFile(FineUpload upload, string idComprovante)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\ImagemComprovantes\Carteira_" + SessionManager.usuarioLogado._Carteira.Id + @"\Comprovante_" + idComprovante;
            string meioPathIIS = @"Image/ImagemComprovantes/Carteira_" + SessionManager.usuarioLogado._Carteira.Id + @"/Comprovante_" + idComprovante;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
                ResizeJpg(dir + @"\" + upload.Filename, 800, 600);



                //capturar comprovante by id
                var dto = new ComprovanteDeposito();
                IRestResponse response = await PostRestAsync<ComprovanteDeposito>("/Pessoa/GetComprovanteDepositoById?id=" + idComprovante);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<ComprovanteDeposito>(response.Content);

                    dto.ComprovantePathLocation = meioPath + "/" + upload.Filename;
                    dto.ComprovantePathLocationIIS = meioPathIIS + "/" + upload.Filename;
                    dto.IdUsuarioAlteracao = SessionManager.usuarioLogado.Id;


                    //atualizar entrada do comprovante
                    response = await PutObjectRestAsync<ComprovanteDeposito>("/Pessoa/UpdateComprovanteDeposito", dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content == null)
                        {
                            return new FineUploaderResult(false, error: "Erro ao atualizar as entradas de registro do comprovante - 2!");
                        }

                    }
                    else
                    {
                        return new FineUploaderResult(false, error: "Não foi possivel acessar o método de update - 2!");
                    }

                }
                else
                {
                    return new FineUploaderResult(false, error: "Não foi possivel acessar o método de captura! - 1!");

                }



            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }



    }
}