﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Domain.Enuns;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Presentation.Controllers
{
    public class EntradaPool
    {
        public string mValorCotas { get; set; }
        public int Cotas { get; set; }
        public int idPessoa { get; set; }
        public int idPoolBens { get; set; }
        public DateTime? DtInclusaoAdm { get; set; }
    }
    public class InvestimentoController : BaseController
    {
        // GET: Investimento  
        public async Task<ActionResult> ValidaRetiradaPlataforma()
        {
            IEnumerable<ComprovanteRetiradaModel> model;


            IRestResponse response = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetListRetiradaPendentes");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteRetiradaModel>>(response.Content);

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = model;

                return View(retorno);

            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;

                return View(retorno);
            }

            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


            return View(retorno);
        }


        [HttpPost]
        public async Task<ActionResult> ValidaRetiradaPlataforma(int idComprovante, int idStatus)
        {
            //capturar comprovante by id
            var dto = new ComprovanteRetirada();
            IRestResponse response = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetComprovanteRetiradaById?id=" + idComprovante);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                dto = JsonConvert.DeserializeObject<ComprovanteRetirada>(response.Content);

                dto.IdUsuarioAlteracao = SessionManager.usuarioLogado.Id;


                //dto.FlagComputado = !fReprovado;
                dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Aguardando;

                if (idStatus == 2)
                    dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Inativo;
                if (idStatus == 5)
                    dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Aguardando;
                if (idStatus == 6)
                    dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Aprovado;
                if (idStatus == 8)
                    dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Processando;



                //atualizar entrada do comprovante
                response = await PutObjectRestAsync<ComprovanteRetirada>("/Pessoa/UpdateComprovanteRetirada", dto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content == null)
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.GatewayTimeout);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    }
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                }



                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> AnexarComprovanteRetirada(int id)
        {

            try
            {

                //capturar comprovante by id
                var dto = new ComprovanteRetirada();
                IRestResponse response = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetComprovanteRetiradaById?id=" + id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<ComprovanteRetirada>(response.Content);
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public async Task<ActionResult> ComprovanteCliente(int id)
        {

            try
            {

                //capturar comprovante by id
                var dto = new ComprovanteRetirada();
                IRestResponse response = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetComprovanteRetiradaById?id=" + id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<ComprovanteRetirada>(response.Content);
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    return View("AnexarComprovanteRetiradaCliente", retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View("AnexarComprovanteRetiradaCliente", retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View("AnexarComprovanteRetiradaCliente", retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpPost]
        public async Task<FineUploaderResult> UploadFileWithdraw(FineUpload upload, string idComprovante, string dt)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\ImagemComprovantesWithdraw\Carteira_" + SessionManager.usuarioLogado._Carteira.Id + @"\Comprovante_" + idComprovante;
            string meioPathIIS = @"Image/ImagemComprovantesWithdraw/Carteira_" + SessionManager.usuarioLogado._Carteira.Id + @"/Comprovante_" + idComprovante;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
                ResizeJpg(dir + @"\" + upload.Filename, 800, 600);



                //capturar comprovante by id
                var dto = new ComprovanteRetirada();
                IRestResponse response = await PostRestAsync<ComprovanteRetirada>("/Pessoa/GetComprovanteRetiradaById?id=" + idComprovante);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<ComprovanteRetirada>(response.Content);

                    dto.ComprovantePathLocation = meioPath + "/" + upload.Filename;
                    dto.ComprovantePathLocationIIS = meioPathIIS + "/" + upload.Filename;
                    dto.IdUsuarioAlteracao = SessionManager.usuarioLogado.Id;
                    dto.IdStatus = EnumStatusCarteira.Aprovado;
                    dto.FlagPendente = false;
                    dto.FlagComputado = true;
                    dto.DtAlteracao =Convert.ToDateTime( dt);

                    //atualizar entrada do comprovante
                    response = await PutObjectRestAsync<ComprovanteRetirada>("/Pessoa/UpdateComprovanteRetirada", dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content == null)
                        {
                            return new FineUploaderResult(false, error: "Erro ao atualizar as entradas de registro do comprovante - 2!");
                        }

                    }
                    else
                    {
                        return new FineUploaderResult(false, error: "Não foi possivel acessar o método de update - 2!");
                    }

                }
                else
                {
                    return new FineUploaderResult(false, error: "Não foi possivel acessar o método de captura! - 1!");

                }



            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }



        [HttpPost]
        public async Task<ActionResult> ValidaCompraCota(int idPBP, bool isValid, string dt)
        {
            try
            {
                IRestResponse response = await PostRestAsync<PoolBensPessoas>("/Investimento/GetPoolBensPessoaById?id=" + idPBP);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var modelPoolBensPessoas = JsonConvert.DeserializeObject<PoolBensPessoas>(response.Content);
                    if (isValid)
                    {
                        modelPoolBensPessoas.IdStatus = (int)EnumInvestimento.CompraAprovada;
                    }
                    else
                    {
                        modelPoolBensPessoas.IdStatus = (int)EnumInvestimento.CompraRecusada;
                    }
                    modelPoolBensPessoas.IdUsuarioAlteracao = SessionManager.usuarioLogado.Id;

                    modelPoolBensPessoas.DtAlteracao = Convert.ToDateTime(dt);

                    response = await PutObjectRestAsync<PoolBensPessoas>("/Investimento/UpdateSolicitarEntradaPoolBens", modelPoolBensPessoas);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var model = JsonConvert.DeserializeObject<PoolBensPessoas>(response.Content);

                        if (model != null)
                        {
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<ActionResult> ValidaCompraCota()
        {
            try
            {
                IEnumerable<PessoasModel> model;


                IRestResponse response = await PostRestAsync<PessoasModel>("/Investimento/GetListValidaCompraCota");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        public async Task<ActionResult> ValidaCotaVendida(int idPBP, bool isValid)
        {
            try
            {
                IRestResponse response = await PostRestAsync<PoolBensPessoas>("/Investimento/GetPoolBensPessoaById?id=" + idPBP);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var modelPoolBensPessoas = JsonConvert.DeserializeObject<PoolBensPessoas>(response.Content);
                    if (isValid)
                    {
                        modelPoolBensPessoas.IdStatus = (int)EnumInvestimento.VendaAprovada;
                    }
                    else
                    {
                        modelPoolBensPessoas.IdStatus = (int)EnumInvestimento.VendaRecusada;
                    }
                    modelPoolBensPessoas.IdUsuarioAlteracao = SessionManager.usuarioLogado.Id;



                    response = await PutObjectRestAsync<PoolBensPessoas>("/Investimento/UpdatePoolBensPessoa", modelPoolBensPessoas);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var model = JsonConvert.DeserializeObject<PoolBensPessoas>(response.Content);

                        if (model != null)
                        {
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> ValidaCotaVendida()
        {
            try
            {
                IEnumerable<PessoasModel> model;


                IRestResponse response = await PostRestAsync<PessoasModel>("/Investimento/GetListValidaVendaCota");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public async Task<ActionResult> Novo()
        {
            try
            {


                IEnumerable<PoolBensModel> model;


                IRestResponse response = await PostRestAsync<PoolBens>("/Investimento/GetListPoolParaInvestimento");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<PoolBensModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<ActionResult> DetalhePool(int id, int? m)
        {
            try
            {
                PoolBensModel model;

                if (SessionManager.usuarioLogado._Usuario._Grupo.Id == (int)EnumPerfil.Administrador_Super)
                {
                    IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetListPessoas?fAll=false");
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var models = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = models;


                        SessionManager.ListaInvestidoresManuais = models.ToList();



                        if (m != null)
                        {


                            IRestResponse responseCarteira = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + (int)m);
                            if (responseCarteira.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                var pessoaModel = JsonConvert.DeserializeObject<PessoasModel>(responseCarteira.Content);


                                ViewBag.IdInvestidorManual = pessoaModel.Id;
                                ViewBag.NomeInvestidorManual = pessoaModel.Nome;
                                ViewBag.SaldoManual = pessoaModel._Carteira.Saldo;


                                response = await PostRestAsync<PoolBens>("/Investimento/GetDetailsPoolParaInvestimentoById?id=" + id);
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    model = JsonConvert.DeserializeObject<PoolBensModel>(response.Content);

                                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                    retorno.objetoRetorno = model;

                                    return View(retorno);

                                }
                                else
                                {
                                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                    retorno.objetoRetorno = null;

                                    return View(retorno);
                                }


                            }
                            else
                            {
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseCarteira.StatusCode);
                                retorno.objetoRetorno = null;
                                return View(retorno);

                            }


                        }
                        else
                        {
                            response = await PostRestAsync<PoolBens>("/Investimento/GetDetailsPoolParaInvestimentoById?id=" + id);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                model = JsonConvert.DeserializeObject<PoolBensModel>(response.Content);

                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = model;

                                return View(retorno);

                            }
                            else
                            {
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = null;

                                return View(retorno);
                            }

                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                            return View(retorno);
                        }





                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = null;
                        SessionManager.ListaInvestidoresManuais = null;
                        return View(retorno);
                    }
                }
                else
                {
                    IRestResponse response = await PostRestAsync<PoolBens>("/Investimento/GetDetailsPoolParaInvestimentoById?id=" + id);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        model = JsonConvert.DeserializeObject<PoolBensModel>(response.Content);

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = model;

                        return View(retorno);

                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = null;

                        return View(retorno);
                    }

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                    return View(retorno);

                }




                #region investidor interno



                #endregion












            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public async Task<ActionResult> DetalhePoolAtivo(int id, int? m)
        {
            try
            {
                if (SessionManager.usuarioLogado._Usuario._Grupo.Id == (int)EnumPerfil.Administrador_Super)
                {
                    await GetListInvestidoresManuais();
                }


                PoolBensModel model;




                #region investidor interno

                if (m != null)
                {


                    IRestResponse responseCarteira = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + (int)m);
                    if (responseCarteira.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var pessoaModel = JsonConvert.DeserializeObject<PessoasModel>(responseCarteira.Content);


                        ViewBag.IdInvestidorManual = pessoaModel.Id;
                        ViewBag.NomeInvestidorManual = pessoaModel.Nome;



                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseCarteira.StatusCode);
                        retorno.objetoRetorno = null;

                    }


                }

                #endregion











                IRestResponse response = await PostRestAsync<PoolBens>("/Investimento/GetDetailsPoolParaInvestimentoById?id=" + id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<PoolBensModel>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public async Task<ActionResult> DetalhePoolVenda(int id, int? m)
        {
            try
            {
                if (SessionManager.usuarioLogado._Usuario._Grupo.Id == (int)EnumPerfil.Administrador_Super)
                {
                    await GetListInvestidoresManuais();
                }


                PoolBensModel model;




                #region investidor interno

                if (m != null)
                {


                    IRestResponse responseCarteira = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + (int)m);
                    if (responseCarteira.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var pessoaModel = JsonConvert.DeserializeObject<PessoasModel>(responseCarteira.Content);


                        ViewBag.IdInvestidorManual = pessoaModel.Id;
                        ViewBag.NomeInvestidorManual = pessoaModel.Nome;
                        ViewBag.pessoaModelSelecionada = pessoaModel;



                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(responseCarteira.StatusCode);
                        retorno.objetoRetorno = null;

                    }


                }

                #endregion







                IRestResponse response = await PostRestAsync<PoolBens>("/Investimento/GetDetailsPoolParaInvestimentoById?id=" + id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<PoolBensModel>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ActionResult> Ativos()
        {
            try
            {


                IEnumerable<PoolBensPessoasModel> model;


                IRestResponse response = await PostRestAsync<PoolBensPessoas>("/Investimento/GetListMeusPoolInvestidos?idPessoa=" + SessionManager.usuarioLogado.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<PoolBensPessoasModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ActionResult> SolicitarEntradaPool(EntradaPool model)
        {
            try
            {
                DateTime? dtAdm = (model.DtInclusaoAdm == null) ? DateTime.Now : (DateTime?)null;

                string valCotas = model.mValorCotas.Replace(".", ",").Replace("-", ".");
                int index = valCotas.LastIndexOf(@",");
                StringBuilder sb = new StringBuilder(valCotas);
                sb[index] = '.'; 
                valCotas = sb.ToString();

                PoolBensPessoas dto = new PoolBensPessoas()
                {
                    DtInclusaoAdm = dtAdm,
                    Cota = model.Cotas,
                    IdPessoa = model.idPessoa,
                    IdPoolBem = model.idPoolBens,
                    IdStatus = (int)EnumInvestimento.SoicitacaoCompra,
                    IdUsuarioInclusao = SessionManager.usuarioLogado.Id,
                    mValorCotas = Convert.ToDecimal(valCotas)
                };


                //verificar saldo disponivel
                IRestResponse responseCarteira = await PostRestAsync<Pessoas>("/Pessoa/GetCarteiraByIdPessoa?idPessoa=" + model.idPessoa);
                if (responseCarteira.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var pessoaModel = JsonConvert.DeserializeObject<PessoasModel>(responseCarteira.Content);

                    if (pessoaModel._Carteira.Saldo >= Convert.ToDecimal(valCotas))
                    {

                        //realiza a solicitação

                        IRestResponse response = await PutObjectRestAsync<PoolBensPessoas>("/Investimento/SolicitarEntradaPoolBens", dto);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (response.Content != null)
                            {
                                dto = JsonConvert.DeserializeObject<PoolBensPessoas>(response.Content);
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                                retorno.objetoRetorno = dto;

                            }
                            else
                            {
                                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                                retorno.objetoRetorno = null;
                            }

                            return Json(retorno, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                            return Json(retorno, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        retorno.statusRetorno = "Insufficient balance to complete this action.";
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                    retorno.objetoRetorno = null;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }



                return Json(true, JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ActionResult> SolicitarVendaPool(EntradaPool model)
        {
            try
            {
                PoolBensPessoas dto = new PoolBensPessoas()
                {
                    DtInclusaoAdm = model.DtInclusaoAdm,
                    Cota = model.Cotas,
                    IdPessoa = model.idPessoa,
                    IdPoolBem = model.idPoolBens,
                    IdStatus = (int)EnumInvestimento.SoicitacaoVenda,
                    IdUsuarioInclusao = SessionManager.usuarioLogado.Id
                };


                //realiza a solicitacao de venda

                IRestResponse response = await PutObjectRestAsync<PoolBensPessoas>("/Investimento/SolicitarVendaPoolBens", dto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        dto = JsonConvert.DeserializeObject<PoolBensPessoas>(response.Content);
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                        retorno.objetoRetorno = dto;

                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                    retorno.objetoRetorno = null;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }





                return Json(true, JsonRequestBehavior.AllowGet);



            }
            catch (Exception ex)
            {

                throw;
            }
        }





        [HttpPost]
        public async Task<ActionResult> ValidarInvestidorPlataforma(int idPessoa, bool isValid)
        {
            try
            {
                IRestResponse response = await PostRestAsync<bool>("/Administrativo/ValidaUsuarioPlataforma?idPessoa=" + idPessoa+ "&isValid="+isValid);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var modelPoolBensPessoas = JsonConvert.DeserializeObject<bool>(response.Content);
                    if (modelPoolBensPessoas)
                    {
                        retorno.statusRetorno = "success";
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        retorno.statusRetorno = "error";
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    } 
                     
                }
                else
                {

                    retorno.statusRetorno = "error";
                    retorno.objetoRetorno = null;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
                 
            }
            catch (Exception ex)
            {

                retorno.statusRetorno = "error";
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<ActionResult> ValidarInvestidorPlataforma()
        {
            try
            {
                IEnumerable<PessoasModel> model;


                IRestResponse response = await PostRestAsync<IEnumerable<PessoasModel>>("/Pessoa/GetListPessoasIndex?fAll=false");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}