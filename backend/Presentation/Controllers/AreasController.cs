﻿using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Presentation.Controllers
{
    public class AreasController : BaseController
    {
        // GET: Areas
        public async Task<ActionResult> Index()
        {
            try
            {
                IEnumerable<AreasModel> model;

                

                IRestResponse response = await PostRestAsync<Areas>("/Areas/GetListAreas");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<AreasModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;


                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
            return View();
        }


        public async Task<ActionResult> Area(int? idArea)
        {
            try
            {
                if (idArea == null)
                {
                    var modelInsert = new AreasModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        FlgAtivo = true,
                        Id = 0
                    };


                    return View(modelInsert);
                }

                var model = new AreasModel();
                

                IRestResponse response = await PostRestAsync<Areas>("/Areas/GetAreasById?idArea=" + idArea);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<AreasModel>(response.Content);


                    return View(model);

                }
                else
                {
                    var modelInsert = new AreasModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        FlgAtivo = true,
                        Id = 0
                    };

                    return View(modelInsert);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Area(AreasModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    string sLocation = "/Areas/InsertAreas";
                    var dto = Mapper.Map<Areas>(model);
                    if (dto.Id > 0) sLocation = "/Areas/EditAreas";

                    

                    IRestResponse response = await PutObjectRestAsync<Areas>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<Areas>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }



            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<bool> EditArea(Areas area)
        {
            try
            {
                var dto = new Areas();
                

                IRestResponse response = await PutObjectRestAsync<Areas>("/Areas/EditAreas", area);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        return true;
                    }

                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Ativar(int idArea)
        {
            try
            {
                //buscar area 
                var dto = new Areas();
                

                IRestResponse response = await PostRestAsync<Areas>("/Areas/GetAreasById?idArea=" + idArea);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Areas>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.FlgAtivo = true;

                    // ativar (UPDATE__
                    var fUpdate = await EditArea(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Desativar(int idArea)
        {
            try
            {
                //buscar area 
                var dto = new Areas();
                

                IRestResponse response = await PostRestAsync<Areas>("/Areas/GetAreasById?idArea=" + idArea);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Areas>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.FlgAtivo = false;

                    // ativar (UPDATE__
                    var fUpdate = await EditArea(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }






    }
}