﻿using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Presentation.Controllers
{
    public class AuthController : BaseController
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(AuthController));

        // GET: Auth
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index(string ret = "")
        {

            if (!string.IsNullOrEmpty(ret))
                ViewBag.AlertaLogin = ret;


            if (VerificaMobile())
                return View("IndexMobile");

            return View();
        }
         




        // GET: Auth
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Recovery(string token = "")
        {

            if (string.IsNullOrEmpty(token))
            {
                ViewBag.AlertaLogin = "Invalid token";
                ViewBag.TokenValido = false;
                ViewBag.Token = token;
            }
            else
            {

                RecoveryPassword rec = null;
                

                IRestResponse response = await PostRestAsync<bool>("/Auth/RecoveryPasswordValidaToken?sToken=" + token);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var retornoRecovery = JsonConvert.DeserializeObject<bool>(response.Content);

                    if (retornoRecovery == false)
                    {
                        ViewBag.TokenValido = false;
                        ViewBag.Token = token;
                    }
                    else
                    {
                        ViewBag.TokenValido = true;
                        ViewBag.Token = token;
                    }
                }

            }

            return View();
        }




        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Recovery(string sToken, string sSenha)
        {

            RecoveryPassword rec = null;
            

            IRestResponse response = await PostRestAsync<bool>("/Auth/ReceveryPasswordUpdate?idUsuario=0&password=" + sSenha + "&sToken=" + sToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            { 
                var retornoRecovery = JsonConvert.DeserializeObject<bool>(response.Content);

                return Json(retornoRecovery, JsonRequestBehavior.AllowGet);

            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RecoveryPasswordByEmail(string sEmail)
        {

            RecoveryPassword rec = null;
            

            IRestResponse response = await PostRestAsync<RecoveryPassword>("/Auth/ReceveryPasswordByEmail?sEmail=" + sEmail);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {

                var retornoRecovery = JsonConvert.DeserializeObject<RecoveryPassword>(response.Content);
                if (retornoRecovery != null && retornoRecovery.Id > 0)
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = retornoRecovery;
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;
                }


            }
            else
            {

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;
            }


            return Json(retorno, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Index(Logins model)
        {

            try
            {

                _log.Info("AuthController Index 1");
;                PessoasModel pessoas = null;
                
                IRestResponse response = await GetRestAsync<PessoasModel>("/Auth/getLogin?sLogin=" + model.Login + "&sSenha=" + model.Senha);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    _log.Info("AuthController Index 2");
                    var usuarioLogado = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = usuarioLogado;

                    pessoas = Mapper.Map<PessoasModel>(retorno.objetoRetorno);

                    if (pessoas == null)
                    {

                        SessionManager.usuarioLogado = null;
                        return RedirectToAction("Index", new { ret = "Username and/or password is incorrect!" });

                    }
                    else
                        if (pessoas.IdStatus == 3)
                    {

                        SessionManager.usuarioLogado = null;

                        return RedirectToAction("Index", new { ret = "User inactive!" });

                    }
                    else
                            if (pessoas._Usuario.Bloqueado)
                    {

                        SessionManager.usuarioLogado = null;

                        return RedirectToAction("Index", new { ret = "Your registration has been declined. Not allowed to access!" });

                    }
                    else
                    {
                        //liberado



                        if(usuarioLogado._Usuario.CadastroValido==null)
                        { 
                            return RedirectToAction("Index", new { ret = "We are analyzing your registration, soon we will respond via email!" });
                        }

                        if (usuarioLogado._Usuario.CadastroValido.Value == false)
                        {
                            return RedirectToAction("Index", new { ret = "We are analyzing your registration, soon we will respond via email!" });
                        }


                        SessionManager.usuarioLogado = pessoas;
                        var authTicket = new FormsAuthenticationTicket(
                                                                   1,                                   // version
                                                                   SessionManager.usuarioLogado.Nome,// user name
                                                                   DateTime.Now,                        // created
                                                                   DateTime.Now.AddMinutes(20),         // expires
                                                                   false,                               // persistent?
                                                                   SessionManager.usuarioLogado._Usuario._Grupo.Nome,// can be used to store roles
                                                                   FormsAuthentication.FormsCookiePath);


                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                        var authCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                        HttpContext.Response.Cookies.Add(authCookie);


                        return RedirectToAction("Index", "Home");
                    }


                }
                else
                {
                    _log.Info("AuthController Index 3 Error : REsponse:" + response.StatusCode + response.Content + response.ErrorMessage);
                    SessionManager.usuarioLogado = null;

                    return RedirectToAction("Index", new { ret = "There was an error while trying to log in. Contact your system administrator!" });

                }



            }
            catch (Exception ex)
            {
                _log.Error("AuthController Index 4: " + ex.Message + "-" + ex.StackTrace);
                throw;
            }

        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            SessionManager.logOut();
            SessionManager.usuarioLogado = null;
            return RedirectToAction("Index", "Auth");
        }
    }
}