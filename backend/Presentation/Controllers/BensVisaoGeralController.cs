﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class BensVisaoGeralController : BaseController
    {
        // GET: Bens
        public BensVisaoGeralController()
        {
            

        }
        public async Task<ActionResult> Index()
        {

            try
            {
                IEnumerable<BensVisaoGeralModel> model;

                IRestResponse response = await PostRestAsync<BensVisaoGeral>("/Bens_VisaoGeral/GetListBensVisaoGeral");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<BensVisaoGeralModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                    if (model == null)
                        retorno.objetoRetorno = null;
                    else
                        retorno.objetoRetorno = model;
                    
                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public async Task<ActionResult> OverView()
        {
            try
            {
                IEnumerable<BensVisaoGeralModel> model;

                IRestResponse response = await PostRestAsync<BensVisaoGeral>("/Bens_VisaoGeral/GetListBensVisaoGeral");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<BensVisaoGeralModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                    if (!Presentation.Helpers.EnumExtentions.IsNullOrEmpty(model))
                        retorno.objetoRetorno = model.Where(x => x.IdStatus == 1).OrderBy(x => x.DtInclusao).Last();

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ActionResult> Bem(int? idBem)
        {
            try
            {
                if (idBem == null)
                {
                    
                    
                    
                    var modelInsert = new BensVisaoGeralModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,           
                        IdStatus = 1,
                        Id = 0
                    };
                    SessionManager.ListaBensImagensVisaoGeral = new List<BensVisaoGeralImagensModel>();

                    return View(modelInsert);
                }

                var model = new BensVisaoGeralModel();
                IRestResponse response = await PostRestAsync<Bens>("/Bens_VisaoGeral/GetBensVisaoGeralById?idBem=" + idBem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<BensVisaoGeralModel>(response.Content);
                    model.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;

                    SessionManager.ListaBensImagensVisaoGeral = model._Imagens.ToList();
                }
                return View(model);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Bem(BensVisaoGeralModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    model._Imagens = SessionManager.ListaBensImagensVisaoGeral;
                    
                    if (model._Imagens == null || model._Imagens.Count() == 0)
                    {
                        retorno.statusRetorno = "É necessário incluir pelo menos 1 imagem na visão geral!";
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }

                    bool fInsert = true;
                    string sLocation = "/Bens_VisaoGeral/InsertBemVisaoGeral";
                    var dto = Mapper.Map<BensVisaoGeralModel>(model);
                    if (dto.Id > 0) { sLocation = "/Bens_VisaoGeral/EditBemVisaoGeral"; fInsert = false; }


                    IRestResponse response = await PutObjectRestAsync<BensVisaoGeralModel>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<BensVisaoGeralModel>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                            if (fInsert)
                            {

                                string meioPath = @"Image\BensVisaoGeralImagens\BemVisaoGeralImagem_0";
                                var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
                                string meioPathReal = @"Image\BensVisaoGeralImagens\BemVisaoGeralImagem_" + dto.Id;
                                var dirReal = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPathReal;

                                Directory.Move(dir, dirReal);
                                SessionManager.ListaBensImagens = null;

                            }
                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }



            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<bool> EditBem(BensVisaoGeral bem)
        {
            try
            {
                var dto = new Bens();
                IRestResponse response = await PutObjectRestAsync<BensVisaoGeral>("/Bens_VisaoGeral/EditBemVisaoGeral", bem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        return true;
                    }

                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Ativar(int idBem)
        {
            try
            {
                //buscar contrato  
                var dto = new BensVisaoGeral();
                IRestResponse response = await PostRestAsync<BensVisaoGeral>("/Bens_VisaoGeral/GetBensVisaoGeralById?idBem=" + idBem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<BensVisaoGeral>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 1;

                    // ativar (UPDATE__
                    var fUpdate = await EditBem(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Desativar(int idBem)
        {
            try
            {
                //buscar contrato 
                var dto = new BensVisaoGeral();
                IRestResponse response = await PostRestAsync<BensVisaoGeral>("/Bens_VisaoGeral/GetBensVisaoGeralById?idBem=" + idBem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<BensVisaoGeral>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 2;

                    // ativar (UPDATE__
                    var fUpdate = await EditBem(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        public ActionResult RemoveArquivoUpload(int idFile)
        {

            try
            {
                var imagem = SessionManager.ListaBensImagensVisaoGeral.Where(c => c.Id == idFile).FirstOrDefault();
                if (imagem != null)
                {
                    SessionManager.ListaBensImagens.Where(c => c.Id == idFile).FirstOrDefault().FlagAtivo = false;
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(false, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);


                throw;
            }

        }

        [HttpPost]
        public FineUploaderResult UploadFile(FineUpload upload, int idBem)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\BensVisaoGeralImagens\BemVisaoGeralImagem_" + idBem;
            string meioPathIIS = @"Image/BensVisaoGeralImagens/BemVisaoGeralImagem_" + idBem;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
                ResizeJpg(dir + @"\" + upload.Filename, 800, 600);
                BensVisaoGeralImagensModel obj = new BensVisaoGeralImagensModel()
                {
                    Id = 0,
                    FlagAtivo = true,
                    IdBemVisaoGeral = idBem,
                    PathLocation = dir + "/" + upload.Filename,
                    PathLocationIIS = dirIIS,
                    IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,

                };

                if (SessionManager.ListaBensImagensVisaoGeral == null) SessionManager.ListaBensImagensVisaoGeral = new List<BensVisaoGeralImagensModel>();
                SessionManager.ListaBensImagensVisaoGeral.Add(obj);

            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }

    }
}