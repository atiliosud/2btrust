﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Domain.Enuns;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class ClientesController : BaseController
    {
        public ClientesController()
        {
            

        }
        // GET: Clientes
        public async Task<ActionResult> Index()
        {
            IEnumerable<PessoasModel> model;
            IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetListPessoasIndex?fAll=false");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = model;
                 
            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;

            }

            return View(retorno);
        }

        [HttpPost]
        public async Task<ActionResult> Ativar(int idCliente)
        {
            try
            {
                //buscar contrato  
                var dto = new Pessoas();
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + idCliente);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 1;

                    // ativar (UPDATE__
                    var fUpdate = await EditCliente(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Desativar(int idCliente)
        {
            try
            {
                //buscar contrato  
                var dto = new Pessoas();
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + idCliente);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus =2;

                    // ativar (UPDATE__
                    var fUpdate = await EditCliente(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private async Task<bool> EditCliente(Pessoas pes)
        {
            try
            {
                var sLocation = "/Pessoa/PutCadastroPessoaMenu";
                var dto = new Bens();
                IRestResponse response = await PutObjectRestAsync<Pessoas>(sLocation, pes);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {

                        return true;

                    }
                    else
                    {
                        return false;
                    }

                    
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public async Task<ActionResult> Info(int idCliente)
        {
            PessoasModel model = new PessoasModel() ;
            IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetPessoasById?idPessoa=" + idCliente);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model = JsonConvert.DeserializeObject<PessoasModel>(response.Content);

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = model;

            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;

            }

            return View(model);
        }

    }
}