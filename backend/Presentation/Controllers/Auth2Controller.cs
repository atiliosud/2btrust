﻿using AutoMapper;
using Domain.Entities;
using Domain.Entities.EYE;
using Domain.Enuns;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models;
using Presentation.Models.EYE;
using Presentation.Models.Vortice;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Presentation.Controllers
{
    public class Auth2Controller : BaseController
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(AuthController));

        // GET: Auth
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [HttpGet]
        public async Task<ActionResult> SelectClinica()
        {
            try
            {
                

                IRestResponse response = await GetRestAsync<IEnumerable<EYE_QuiosquesModel>>("/EYEClinicas/Getlist");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var ListaClinicas = JsonConvert.DeserializeObject<IEnumerable<EYE_QuiosquesModel>>(response.Content);


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = ListaClinicas;
                    retorno.possuiDados = true;

                    ViewBag.ListaClinica = retorno.objetoRetorno;



                    return View();
                }
                else
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.possuiDados = false;
                    Warning(retorno.statusRetorno);

                    return View();
                }
            }
            catch (Exception ex)
            {
                retorno.statusRetorno = "Ocorreu um erro! Entre em contato com o administrador do sistema!";
                Warning(retorno.statusRetorno);
                InsertLogError(ex, "AuthController", "SelectClinica GET");
                return View();
            }




        }

        [HttpPost]
        public async Task<ActionResult> SelectQuiosque(int idQuiosque)
        {

            try
            {
                

                IRestResponse response = await GetRestAsync<EYE_QuiosquesModel>("/EYEClinicas/GetById?id=" + idQuiosque);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var quisoqueSelecionada = JsonConvert.DeserializeObject<EYE_QuiosquesModel>(response.Content);


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = quisoqueSelecionada;
                    retorno.possuiDados = true;

                   


                    return RedirectToAction("Index", "Alertas");
                }
                else
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.possuiDados = false;
                    Warning(retorno.statusRetorno);

                    return View(idQuiosque);
                }
            }
            catch (Exception ex)
            {
                retorno.statusRetorno = "Ocorreu um erro! Entre em contato com o administrador do sistema!";
                Warning(retorno.statusRetorno);
                InsertLogError(ex, "AuthController", "SelectQuiosque POST");
                return View(idQuiosque);
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(string sLogin, string sSenha)
        {
            try
            {

                _log.Info("Auth2Controller Index 1: " + sLogin);
                UsuarioModel model = null;
                

                IRestResponse response = await GetRestAsync<UsuarioModel>("/Auth/getLogin?sLogin=" + sLogin + "&sSenha=" + sSenha);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var usuarioLogado = JsonConvert.DeserializeObject<Usuario>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = usuarioLogado;

                    model = Mapper.Map<UsuarioModel>(retorno.objetoRetorno);

                    if (model == null)
                    {
                        Warning("Login e/ou Senha inválido(s)!");
                        SessionManager.usuarioLogado = null;
                        return View();
                    }
                    else
                        if (model.FlagDeletado)
                        {
                            Warning("Usuário com acesso inativo!");
                            SessionManager.usuarioLogado = null;
                            return View();
                        }
                }
                else
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    Warning(ErrorMessageHttp.GetErrorMessage(response.StatusCode));

                    return View();
                }





                //    return RedirectToAction("SelectClinica", "Auth");

                return RedirectToAction("Index", "Alertas");

            }
            catch (Exception ex)
            {
                _log.Error("Login: " + ex.Message + " - " + ex.StackTrace);
                InsertLogError(ex, "AuthController", "Login POST");
                throw;
            }
        }
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            SessionManager.logOut();
            return RedirectToAction("Index", "Auth");
        }

    }
}