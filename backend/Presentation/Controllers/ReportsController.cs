﻿using AutoMapper;
using Domain.Entities._2BTrust;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using Rotativa.Core.Options;
using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    [AllowAnonymous]
    public class ReportsController : BaseController
    {

        static string RenderViewToString(ControllerContext context,
                                    string viewPath,
                                    object model = null,
                                    bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }

        // GET: Reports
        public async Task<ActionResult> Index(int idPessoa)
        {
            var ExtractTime = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ExtractTime"]);
            PessoasModel pessoas = null;
            IRestResponse response = await GetRestAsync<PessoasModel>("/Auth/getClientInvests?idPessoa=" + idPessoa);


            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var usuarioLogados = JsonConvert.DeserializeObject<IEnumerable<Pessoas>>(response.Content);



                var usuarioLogado = usuarioLogados.FirstOrDefault();

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = usuarioLogado;

                pessoas = Mapper.Map<PessoasModel>(retorno.objetoRetorno);



                var dtFilterInicio = Convert.ToDateTime(DateTime.Now.AddDays(ExtractTime).ToString("yyyy-MM-'01'"));

                var dtFilterFim = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 23:59:59"));


                ViewBag.usuarioLogado = pessoas;
                ViewBag.meusLancamentos = pessoas._MeusLancamentos;


                if (pessoas._MeusPools == null || pessoas._MeusPools.Count() == 0)
                {
                    return null;
                }

                response = await PostRestAsync<CarteiraLogModel>("/Investimento/GetCarteiraLogByIdPessoa?idPessoa=" + pessoas.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    // ViewBag.minhaCaeteira var modelLogCompleted = JsonConvert.DeserializeObject<IEnumerable<CarteiraLogModel>>(response.Content);
                    var model = JsonConvert.DeserializeObject<IEnumerable<CarteiraLogModel>>(response.Content);


                    model = model.Where(c => c.Data >= dtFilterInicio && c.Data <= dtFilterFim).AsEnumerable();
                    ViewBag.minhaCaeteira = model;

                    var listaCredit = new List<CarteiraLogModel>();
                    var listaDebit = new List<CarteiraLogModel>();
                    var listaYields = new List<CarteiraLogModel>();
                    var listaSimplifield = new List<CarteiraLogModel>();

                    foreach (var item in model)
                    {
                        if (item.Tipo == "retirada aprovado" || item.Tipo == "deposito aprovado" || item.Tipo == "venda cota aprovado" || item.Tipo == "compra cota aprovado")
                        {
                            listaSimplifield.Add(item);
                        }
                        if (item.Tipo == "dividendo")
                        {
                            listaYields.Add(item);
                        }
                        else
                        if (item.Tipo == "deposito aprovado" || item.Tipo == "dividendo")
                        {
                            listaCredit.Add(item);
                        }
                        else if (item.Tipo == "retirada aprovado")
                        {
                            listaDebit.Add(item);
                        }
                    }
                    ViewBag.listaCredit = listaCredit;
                    ViewBag.listaDebit = listaDebit;
                    ViewBag.listaYields = listaYields;
                    ViewBag.listaSimplifield = listaSimplifield;

                }



                return View(retorno);


            }
            return View();
        }


        public async Task<ActionResult> CreatePdfStatement(int idPessoa)
        {
            try
            {



                PessoasModel pessoas = null;
                IRestResponse response = await GetRestAsync<PessoasModel>("/Auth/getClientInvests?idPessoa=" + idPessoa);


                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var usuarioLogados = JsonConvert.DeserializeObject<IEnumerable<Pessoas>>(response.Content);



                    var usuarioLogado = usuarioLogados.FirstOrDefault();

                    pessoas = Mapper.Map<PessoasModel>(usuarioLogado);


                    if (pessoas._MeusPools == null || pessoas._MeusPools.Count() == 0)
                    {
                        return Content("Não enviado. Cliente sem investimento ativo: " + idPessoa);
                    }

                }
                else
                {
                    return Content("Ocorre um erro ao buscar dados do clienteId: " + idPessoa);
                }






                Rotativa.Core.DriverOptions options = new Rotativa.Core.DriverOptions()
                {
                    PageSize = Size.A4,
                    PageMargins = new Margins(10, 2, 10, 2)
                };

                var pdfResult = new ActionAsPdf("Index", new { idPessoa = idPessoa })
                { FileName = "Test.pdf", RotativaOptions = options };

                var binary = pdfResult.BuildPdf(this.ControllerContext);


                //  string html = RenderViewToString(ControllerContext, "Index", retorno, true);
                var verificaEnvio = System.Configuration.ConfigurationManager.AppSettings["ChaveEmailStatement"];
                if (verificaEnvio.ToUpper() == "TEST")
                {
                    var retorno = SendMailValidaUsuario("marcus@2btrust.com", idPessoa.ToString(), "", binary, pessoas.Nome);

                    return Content(retorno);
                }
                else
                {
                    if (pessoas._Usuario != null && pessoas._Usuario.Email != "")
                    {
                        var retorno = SendMailValidaUsuario(pessoas._Usuario.Email, idPessoa.ToString(), "", binary, pessoas.Nome);

                        return Content(retorno);
                    }
                    else
                    {

                        return Content("Ocorre um erro ao enviar o PDF. Cliente nao possui email cadastrado");
                    }

                }


            }
            catch (Exception ex)
            {

                return Content("Ocorre um erro ao enviar o PDF. Erro: " + ex.Message);
            }
        }


        public async Task<ActionResult> CreatePdfStatementNow(int idPessoa, string type)
        {
            try
            {

                ViewBag.type = type;
                var ExtractTime = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ExtractTime"]);
                PessoasModel pessoas = null;
                IRestResponse response = await GetRestAsync<PessoasModel>("/Auth/getClientInvests?idPessoa=" + idPessoa);


                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var usuarioLogados = JsonConvert.DeserializeObject<IEnumerable<Pessoas>>(response.Content);



                    var usuarioLogado = usuarioLogados.FirstOrDefault();

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = usuarioLogado;

                    pessoas = Mapper.Map<PessoasModel>(retorno.objetoRetorno);



                    var dtFilterInicio = Convert.ToDateTime(DateTime.Now.AddDays(ExtractTime).ToString("yyyy-MM-'01'"));

                    var dtFilterFim = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd 23:59:59"));


                    ViewBag.usuarioLogado = pessoas;
                    ViewBag.meusLancamentos = pessoas._MeusLancamentos;


                    if (pessoas._MeusPools == null || pessoas._MeusPools.Count() == 0)
                    {
                        return null;
                    }

                    response = await PostRestAsync<CarteiraLogModel>("/Investimento/GetCarteiraLogByIdPessoa?idPessoa=" + pessoas.Id);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {

                        // ViewBag.minhaCaeteira var modelLogCompleted = JsonConvert.DeserializeObject<IEnumerable<CarteiraLogModel>>(response.Content);
                        var model = JsonConvert.DeserializeObject<IEnumerable<CarteiraLogModel>>(response.Content);


                        model = model.Where(c => c.Data >= dtFilterInicio && c.Data <= dtFilterFim).AsEnumerable();
                        ViewBag.minhaCaeteira = model;

                        var listaCredit = new List<CarteiraLogModel>();
                        var listaDebit = new List<CarteiraLogModel>();
                        var listaYields = new List<CarteiraLogModel>();
                        var listaSimplifield = new List<CarteiraLogModel>();


                        if (type == "Simplifield")
                        {

                            ViewBag.titleInit = "2bTrust - Investments | Statement Simplified";
                        }
                        else if (type == "Yields")
                        {

                            ViewBag.titleInit = "2bTrust - Investments | Statement Yield";
                        }
                        else if (type == "Credit")
                        {

                            ViewBag.titleInit = "2bTrust - Investments | Statement Credit";
                        }
                        else if (type == "Debit")
                        {

                            ViewBag.titleInit = "2bTrust - Investments | Statement Debit";
                        }
                        else if (type == "Complete")
                        {

                            ViewBag.titleInit = "2bTrust - Investments | Statement Complete";
                        }
                        else
                        {
                            ViewBag.titleInit = "STATEMENT OF INVESTMENT PORTFOLIO";
                        }

                        foreach (var item in model)
                        {
                            if (item.Tipo == "retirada aprovado" || item.Tipo == "deposito aprovado" || item.Tipo == "venda cota aprovado" || item.Tipo == "compra cota aprovado")
                            {
                                listaSimplifield.Add(item);
                            }
                            if (item.Tipo == "dividendo")
                            {
                                listaYields.Add(item);
                            }
                            else
                            if (item.Tipo == "deposito aprovado" || item.Tipo == "dividendo")
                            {
                                listaCredit.Add(item);
                            }
                            else if (item.Tipo == "retirada aprovado")
                            {
                                listaDebit.Add(item);
                            }
                        }
                        ViewBag.listaCredit = listaCredit;
                        ViewBag.listaDebit = listaDebit;
                        ViewBag.listaYields = listaYields;
                        ViewBag.listaSimplifield = listaSimplifield;

                    }
                    Rotativa.Core.DriverOptions options = new Rotativa.Core.DriverOptions()
                    {
                        PageSize = Size.A4,
                        PageMargins = new Margins(10, 2, 10, 2)
                    };
                    var relatorioPDF = new ViewAsPdf
                    {
                        ViewName = "IndexPersonal",
                        FileName = "Statement_of_investment_portfolio_" + usuarioLogado.Id.ToString().PadLeft(6, '0') + "-8_" + DateTime.Now.ToString("MM_yyyy") + ".pdf",
                        Model = retorno,
                        RotativaOptions = options
                    };
                    return relatorioPDF;


                    //  string html = RenderViewToString(ControllerContext, "Index", retorno, true);
                    //var verificaEnvio = System.Configuration.ConfigurationManager.AppSettings["ChaveEmailStatement"];
                    //if (verificaEnvio.ToUpper() == "TEST")
                    //{
                    //    var retorno = SendMailValidaUsuario("flavio.ap.camilo@gmail.com", idPessoa.ToString(), "", binary, pessoas.Nome);

                    //    return Content(retorno);
                    //}
                    //else
                    //{
                    //    if (pessoas._Usuario != null && pessoas._Usuario.Email != "")
                    //    {
                    //        var retorno = SendMailValidaUsuario(pessoas._Usuario.Email, idPessoa.ToString(), "", binary, pessoas.Nome);

                    //        return Content(retorno);
                    //    }
                    //    else
                    //    {

                    //        return Content("Ocorre um erro ao enviar o PDF. Cliente nao possui email cadastrado");
                    //    }

                    //}


                }
                return View();
            }
            catch (Exception ex)
            {

                return Content("Ocorre um erro ao enviar o PDF. Erro: " + ex.Message);
            }
        }

        public static string SendMailValidaUsuario(string sEmail, string codigoCliente, string htmls, byte[] asPDF, string nomeCliente)
        {
            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];
                var ExtractTime = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ExtractTime"]);

                string urlSistemaLogin = System.Configuration.ConfigurationManager.AppSettings["urlSistemaLogin"];


                string Assunto = "2bTrust - Your statement is available";

                var dataRef = DateTime.Now.AddMonths(-1);


                var mail = new MailMessage();
                mail.To.Add(sEmail);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;
                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item, Assunto, System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", Assunto, System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);
                Attachment att = new Attachment(new MemoryStream(asPDF), "Statement_of_investment_portfolio_" + codigoCliente.PadLeft(6, '0') + "-8_" + dataRef.ToString("MM_yyyy") + ".pdf");
                mail.Attachments.Add(att);

                mail.IsBodyHtml = true;
                var html = new HtmlDocument();
                html.LoadHtml(System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailStatementBodyLocation"]));

                //   html.GetElementbyId("spnBoasVindas").InnerHtml = string.Format(@"Thank you for your pre-registration. We have received your information and are in the process of reviewing. Within 24-48 hours we will be notifying you or your acceptance or if we need any further information to process your application. ");

                // html.GetElementbyId("btnConfirmacao").InnerHtml = string.Format(" <a href=" + urlValidaEmailCadastro + "/CadastreAqui/ValidaEmail?hash=" + pessoa._Usuario.Saltkey + " class='btn-primary'>Verify Account</a>");
                html.GetElementbyId("accountnumber").InnerHtml = string.Format("ACCOUNT ENDING: " + codigoCliente.PadLeft(6, '0') + "-8");
                html.GetElementbyId("accountname").InnerHtml = string.Format("Dear: " + nomeCliente + ", ");
                html.GetElementbyId("accountdatestatement").InnerHtml = string.Format("Your " + dataRef.ToString("MMMM yyyy") + " statement is ready");
                html.GetElementbyId("spninfomes").InnerHtml = string.Format("As follows is the current investment position and statement from " + dataRef.ToString("MMMM dd'th,' yyyy"));

                mail.Body = html.DocumentNode.InnerHtml;

                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                try
                {

                    client.Send(mail);




                    return "Enviado com sucesso";
                }
                catch (SmtpException ex)
                {
                    string erro = "Erro SMTP: " + ex.Message;
                    if (ex.InnerException != null)
                        erro += " - " + ex.StackTrace;

                    if (ex.InnerException != null)
                        erro += " - " + ex.InnerException.ToString();

                    return erro;
                }

            }
            catch (Exception ex)
            {
                string erro = "Erro : " + ex.Message;
                if (ex.InnerException != null)
                    erro += " - " + ex.StackTrace;

                if (ex.InnerException != null)
                    erro += " - " + ex.InnerException.ToString();

                return erro;
            }
        }
    }
}