﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class BensController : BaseController
    {
        // GET: Bens
        public async Task<ActionResult> Index()
        {

            try
            {


                IEnumerable<BensModel> model;

                

                IRestResponse response = await PostRestAsync<Bens>("/Bens/GetListBens");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<BensModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }



        public async Task<ActionResult> Bem(int? idBem)
        {
            try
            {
                await populaViewBagPools();
                if (idBem == null)
                {
                    var modelInsert = new BensModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,           
                        IdStatus = 1,
                        Id = 0,
                        _Endereco = new BensEnderecosModel() { Id = 0 }
                    };
                    SessionManager.ListaBensGastos = new List<BensGastosModel>();

                    return View(modelInsert);
                }

                var model = new BensModel();
                

                IRestResponse response = await PostRestAsync<Bens>("/Bens/GetBensById?idBem=" + idBem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<BensModel>(response.Content);
                    model.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    model.CapitalInvestido = Convert.ToDecimal(model.CapitalInvestido).ToString("#,##0.00");
                    model.Custos = Convert.ToDecimal(model.Custos).ToString("#,##0.00");


                    model.Rent = Convert.ToDecimal(model.Rent).ToString("#,##0.00");
                    model.Hoa = Convert.ToDecimal(model.Hoa).ToString("#,##0.00");
                    model.PropertyTaxProvision = Convert.ToDecimal(model.PropertyTaxProvision).ToString("#,##0.00");
                    model.PropertyManagement = Convert.ToDecimal(model.PropertyManagement).ToString("#,##0.00");
                    model.AdministrationFee = Convert.ToDecimal(model.AdministrationFee).ToString("#,##0.00");
                    model.DividendYeld = Convert.ToDecimal(model.DividendYeld).ToString("#,##0.00");
                    if (string.IsNullOrEmpty(model.OutrasDespesas))
                        model.OutrasDespesas = "0";
                    model.OutrasDespesas = Convert.ToDecimal(model.OutrasDespesas).ToString("#,##0.00");
                    

                    SessionManager.ListaBensGastos = model._BensGastos.ToList();


                    model.sMesAno = model.iMes.ToString() + "/" + model.iAno.ToString();
                    model.sMesAno = model.sMesAno.PadLeft(7, '0');

                    if (model._Rentabilidade != null)
                    {   
                        model._Rentabilidade.CapitalInvestido = Convert.ToDecimal(model._Rentabilidade.CapitalInvestido).ToString("#,##0.00");
                        model._Rentabilidade.LucroLiquido = Convert.ToDecimal(model._Rentabilidade.LucroLiquido).ToString("#,##0.00");
                        model._Rentabilidade.Valor = Convert.ToDecimal(model._Rentabilidade.Valor).ToString("#,##0.00");
                    }

                    if (model._Lucratividade != null)
                    {
                        model._Lucratividade.ReceitaBruta = Convert.ToDecimal(model._Lucratividade.ReceitaBruta).ToString("#,##0.00");
                        model._Lucratividade.LucroLiquido = Convert.ToDecimal(model._Lucratividade.LucroLiquido).ToString("#,##0.00");
                        model._Lucratividade.Valor = Convert.ToDecimal(model._Lucratividade.Valor).ToString("#,##0.00");
                    }


                    SessionManager.ListaBensImagens = model._Imagens.ToList();

                    return View(model);

                }
                else
                {
                    var modelInsert = new PoolBensModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        IdStatus = 1,
                        Id = 0
                    };

                    return View(modelInsert);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Bem(BensModel model)
        {

            try
            {
                //if (ModelState.IsValid)
                //{
                if (string.IsNullOrEmpty(model.Nome)) model.Nome = model.Descritivo;    
                model._Imagens = SessionManager.ListaBensImagens;
                    model._BensGastos = SessionManager.ListaBensGastos;
                    if (model._Imagens == null || model._Imagens.Count() == 0)
                    {
                        retorno.statusRetorno = "É necessário incluir pelo menos 1 imagem do Imóvel!";
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }


                    bool fInsert = true;
                    string sLocation = "/Bens/InsertBem";
                    var dto = Mapper.Map<BensModel>(model);
                    if (dto.Id > 0) { sLocation = "/Bens/EditBem"; fInsert = false; }


                    model.iMes = int.Parse(model.sMesAno.Split('/')[0]);
                    model.iAno = int.Parse(model.sMesAno.Split('/')[1]);

                    

                    IRestResponse response = await PutObjectRestAsync<BensModel>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<BensModel>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                            if (fInsert)
                            {

                                string meioPath = @"Image\ImagemBens\Bem_0";
                                var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
                                string meioPathReal = @"Image\ImagemBens\Bem_" + dto.Id;
                                var dirReal = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPathReal;

                                Directory.Move(dir, dirReal);
                                SessionManager.ListaBensImagens = null;

                            }
                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                //}
                //else
                //{
                //    retorno.statusRetorno = "Verifique a validação dos campos!";
                //    return Json(retorno, JsonRequestBehavior.AllowGet);
                //}



            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<bool> EditBem(Bens bem)
        {
            try
            {
                var dto = new Bens();
                

                IRestResponse response = await PutObjectRestAsync<Bens>("/Bens/EditBem", bem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        return true;
                    }

                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Ativar(int idBem)
        {
            try
            {
                //buscar contrato  
                var dto = new Bens();
                

                IRestResponse response = await PostRestAsync<Bens>("/Bens/GetBensById?idBem=" + idBem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Bens>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 1;

                    // ativar (UPDATE__
                    var fUpdate = await EditBem(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Desativar(int idBem)
        {
            try
            {
                //buscar contrato 
                var dto = new Bens();
                

                IRestResponse response = await PostRestAsync<Bens>("/Bens/GetBensById?idBem=" + idBem);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Bens>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 2;

                    // ativar (UPDATE__
                    var fUpdate = await EditBem(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }




        private async Task populaViewBagPools()
        {
            try
            {
                List<PoolBensModel> LstPools = new List<PoolBensModel>();
                IEnumerable<PoolBensModel> contratos = null;
                

                IRestResponse response = await PostRestAsync<IEnumerable<PoolBens>>("/PoolsBens/GetListPoolBens");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<PoolBens>>(response.Content);

                    contratos = Mapper.Map<IEnumerable<PoolBensModel>>(dto);

                    if (contratos != null)
                    {
                        ViewBag.Pools = contratos.Where(c => c.IdStatus == 1).AsEnumerable();
                    }
                    else
                    {
                        LstPools.Add(new PoolBensModel()
                        {

                            Nome = "INDEFINIDO",
                            Id = 0
                        });

                        ViewBag.Pools = LstPools;
                    }

                }
                else
                {

                    LstPools.Add(new PoolBensModel()
                    {

                        Nome = "INDEFINIDO",
                        Id = 0
                    });

                    ViewBag.Pools = LstPools;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }



        [HttpPost]
        public ActionResult RemoveImageUpload(int idImage)
        {

            try
            {
                var imagem = SessionManager.ListaBensImagens.Where(c => c.Id == idImage).FirstOrDefault();
                if (imagem != null)
                {
                    SessionManager.ListaBensImagens.Where(c => c.Id == idImage).FirstOrDefault().FlagAtivo = false;
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(false, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);


                throw;
            }

        }

        [HttpPost]
        public FineUploaderResult UploadFile(FineUpload upload, int idBem)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\ImagemBens\Bem_" + idBem;
            string meioPathIIS = @"Image/ImagemBens/Bem_" + idBem;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
                ResizeJpg(dir + @"\" + upload.Filename, 800, 600);
                BensImagensModel obj = new BensImagensModel()
                {
                    Id = 0,
                    FlagAtivo = true,
                    IdBem = idBem,
                    PathLocation = dir + "/" + upload.Filename,
                    PathLocationIIS = dirIIS,
                    IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,

                };

                if (SessionManager.ListaBensImagens == null) SessionManager.ListaBensImagens = new List<BensImagensModel>();
                SessionManager.ListaBensImagens.Add(obj);

            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }

        [HttpPost]
        public FineUploaderResult UploadFileGastos(FineUpload upload, int idBem, int idGastos)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\GastosBens\Bem_" + idBem;
            string meioPathIIS = @"Image/GastosBens/Bem_" + idBem;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
                if (Path.GetExtension(filePath).ToUpper() != ".PDF")
                {

                    ResizeJpg(dir + @"\" + upload.Filename, 800, 600);

                }
                if (SessionManager.ListaBensGastos != null)
                {
                    var obj = SessionManager.ListaBensGastos.Where(c => c.Id == idGastos).FirstOrDefault();
                    if (obj != null)
                        SessionManager.ListaBensGastos.Remove(obj);
                }
                else SessionManager.ListaBensGastos = new List<BensGastosModel>();

                int id = idGastos - 1;
                BensGastosModel novo = new BensGastosModel()
                {
                    Id = id,
                    PathLocation = dir + "/" + upload.Filename,
                    PathLocationIIS = dirIIS,
                    FlagAtivo = true
                };


                SessionManager.ListaBensGastos.Add(novo);

            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }


        public async Task<JsonResult> SetGastosBens(int idBemGastos, string sDescricao, string sValor, string sData, string sUrlDocumento)
        {
            try
            {

                var obj = SessionManager.ListaBensGastos.Where(c => c.Id == idBemGastos).FirstOrDefault();
                if (obj != null)
                {
                    SessionManager.ListaBensGastos.Remove(obj);
                    obj.Descricao = sDescricao;
                    obj.Valor = Convert.ToDecimal(sValor);
                    obj.DtOcorrencia = Convert.ToDateTime(sData);
                    obj.FlagAtivo = true;

                    SessionManager.ListaBensGastos.Add(obj);

                    return Json(obj.Id, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    BensGastosModel novo = new BensGastosModel()
                    {
                        Id = idBemGastos,
                        Descricao = sDescricao,
                        Valor = Convert.ToDecimal(sValor),
                        DtOcorrencia = Convert.ToDateTime(sData),
                        FlagAtivo = true
                    };

                    SessionManager.ListaBensGastos.Add(novo);

                    return Json(novo.Id, JsonRequestBehavior.AllowGet);
                }




            }
            catch (Exception ex)
            {

                //createLog(SessionManager.UsuarioLogado.ID, "MSPessoa", "Error - dellTelefone idTelefone = " + idTelefone, ex.Message);
                return Json(0, JsonRequestBehavior.AllowGet);
                throw;
            }
        }



        [AllowAnonymous]
        public async Task<JsonResult> deleteGastos(int idBemGastos)
        {
            try
            {
                var documentToRemove = SessionManager.ListaBensGastos.Where(c => c.Id == idBemGastos).FirstOrDefault();

                SessionManager.ListaBensGastos.Remove(documentToRemove);

                documentToRemove.FlagAtivo = false;
                SessionManager.ListaBensGastos.Add(documentToRemove);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);

            }
        }


    }

    public class FineUploaderResult : ActionResult
    {
        public const string ResponseContentType = "text/plain";

        private readonly bool _success;
        private readonly string _error;
        private readonly bool? _preventRetry;
        private readonly JObject _otherData;

        public FineUploaderResult(bool success, object otherData = null, string error = null, bool? preventRetry = null)
        {
            _success = success;
            _error = error;
            _preventRetry = preventRetry;

            if (otherData != null)
                _otherData = JObject.FromObject(otherData);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = ResponseContentType;

            response.Write(BuildResponse());
        }

        public string BuildResponse()
        {
            var response = _otherData ?? new JObject();
            response["success"] = _success;

            if (!string.IsNullOrWhiteSpace(_error))
                response["error"] = _error;

            if (_preventRetry.HasValue)
                response["preventRetry"] = _preventRetry.Value;

            return response.ToString();
        }
    }

    [ModelBinder(typeof(ModelBinder))]
    public class FineUpload
    {
        public string Filename { get; set; }
        public Stream InputStream { get; set; }

        public void SaveAs(string destination, bool overwrite = false, bool autoCreateDirectory = true)
        {
            if (autoCreateDirectory)
            {
                var directory = new FileInfo(destination).Directory;
                if (directory != null) directory.Create();
            }

            using (var file = new FileStream(destination, overwrite ? FileMode.Create : FileMode.CreateNew))
                InputStream.CopyTo(file);
        }

        public class ModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                var request = controllerContext.RequestContext.HttpContext.Request;
                var formUpload = request.Files.Count > 0;

                // find filename
                var xFileName = request.Headers["X-File-Name"];
                var qqFile = request["qqfile"];
                var formFilename = formUpload ? request.Files[0].FileName : null;

                var upload = new FineUpload
                {
                    Filename = xFileName ?? qqFile ?? formFilename,
                    InputStream = formUpload ? request.Files[0].InputStream : request.InputStream
                };

                return upload;
            }
        }

    }
}