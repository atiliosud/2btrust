﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class ControlesController : BaseController
    {
        public ControlesController()
        {
            

        }

        public async Task<ActionResult> Index()
        {

            try
            {


                IEnumerable<ControlesModel> model;


                IRestResponse response = await PostRestAsync<Controles>("/Controles/GetListControles");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<ControlesModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<ActionResult> Controle(int? idControle)
        {

            try
            {

                await populaViewBagMenuPai();
                await populaViewBagAreas();
                List<AcoesModel> ListaAcoes = new List<AcoesModel>();
                ListaAcoes.Add(new AcoesModel()
                {
                    Nome = "View",
                    Codigo = "001"
                });
                ListaAcoes.Add(new AcoesModel()
                {
                    Nome = "Extra",
                    Codigo = "002"
                }); 

                if (idControle == null)
                {
                    


                    var modelInsert = new ControlesModel()
                    {
                        _Acoes = ListaAcoes,
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        FlagAtivo = true,
                        Id = 0
                    };


                    return View(modelInsert);
                }

                var model = new ControlesModel();
                IRestResponse response = await PostRestAsync<Controles>("/Controles/GetControlesById?idControle=" + idControle);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<ControlesModel>(response.Content);

                    foreach (var item in ListaAcoes)
                    {
                        if(!model._Acoes.Where(c=>c.Nome.Contains(item.Nome)).Any())
                        {
                            model._Acoes.Add(item);
                        }
                    }
                    model.IdUsuarioAlteracao = SessionManager.usuarioLogado._Usuario.Id;
                    return View(model);

                }
                else
                {
                    var modelInsert = new ControlesModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        FlagAtivo = true,
                        Id = 0
                    };

                    return View(modelInsert);
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpPost]
        public async Task<ActionResult> Controle(ControlesModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    List<AcoesModel> ListaAcoes = new List<AcoesModel>();
                    ListaAcoes.Add(new AcoesModel()
                    {
                        Nome = "View",
                        Codigo = "001"
                    });
                    ListaAcoes.Add(new AcoesModel()
                    {
                        Nome = "Extra",
                        Codigo = "002"
                    });

                    model._Acoes = ListaAcoes;
                    string sLocation = "/Controles/InsertControle";
                    var dto = Mapper.Map<Controles>(model);


                    if (dto.Id > 0) sLocation = "/Controles/EditControle";


                    IRestResponse response = await PutObjectRestAsync<Controles>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<Controles>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }



            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task populaViewBagAreas()
        {
            try
            {

                List<AreasModel> lstAreas = new List<AreasModel>();
                IEnumerable<AreasModel> areas = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Areas>>("/Areas/GetListAreas");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Areas>>(response.Content);

                    areas = Mapper.Map<IEnumerable<AreasModel>>(dto);

                    if (areas != null)
                    {
                        ViewBag.Areas = areas;
                    }
                    else
                    {
                        lstAreas.Add(new AreasModel()
                        {
                            Id = 0,
                            Nome = "INDEFINIDO"
                        });

                        ViewBag.Areas = lstAreas;
                    }

                }
                else
                {
                    lstAreas.Add(new AreasModel()
                    {
                        Id = 0,
                        Nome = "INDEFINIDO"
                    });

                    ViewBag.Areas = lstAreas;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private async Task populaViewBagMenuPai()
        {
            try
            {

                List<ControlesModel> lstControles = new List<ControlesModel>();
                IEnumerable<ControlesModel> controle = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Controles>>("/Controles/GetListControlesPai");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Controles>>(response.Content);

                    controle = Mapper.Map<IEnumerable<ControlesModel>>(dto);

                    if (controle != null)
                    {
                        ViewBag.MeuPai = controle;
                    }
                    else
                    {
                        lstControles.Add(new ControlesModel()
                        {
                            Id = 0,
                            Nome = "INDEFINIDO"
                        });

                        ViewBag.MeuPai = lstControles;
                    }

                }
                else
                {
                    lstControles.Add(new ControlesModel()
                    {
                        Id = 0,
                        Nome = "INDEFINIDO"
                    });

                    ViewBag.MeuPai = lstControles;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }




        private async Task<bool> EditControle(Controles controle)
        {
            try
            {
                var dto = new Controles();
                IRestResponse response = await PutObjectRestAsync<Controles>("/Controles/EditControle", controle);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        return true;
                    }

                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Ativar(int idControle)
        {
            try
            {
                //buscar area 
                var dto = new Controles();
                IRestResponse response = await PostRestAsync<Controles>("/Controles/GetControlesById?idControle=" + idControle);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Controles>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.FlagAtivo = true;

                    // ativar (UPDATE__
                    var fUpdate = await EditControle(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Desativar(int idControle)
        {
            try
            {
                //buscar area 
                var dto = new Controles();
                IRestResponse response = await PostRestAsync<Controles>("/Controles/GetControlesById?idControle=" + idControle);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Controles>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.FlagAtivo = false;

                    // ativar (UPDATE__
                    var fUpdate = await EditControle(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}