﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{

    public class HomeController : BaseController
    {
        // GET: Home
        public async Task<ActionResult> Index(string dtI = "", string dtE = "", int iType = 0)
        {
            try
            {
                if (SessionManager.usuarioLogado._Usuario._Grupo.Id == 2 || SessionManager.usuarioLogado._Usuario._Grupo.Id == 7)
                {

                    IEnumerable<LancamentosModel> modelLancamento;


                    IRestResponse responseLancamento = await PostRestAsync<LancamentosModel>("/PoolsBens/GetAllLancamentos");
                    if (responseLancamento.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        modelLancamento = JsonConvert.DeserializeObject<IEnumerable<LancamentosModel>>(responseLancamento.Content);

                        ViewBag.modelLancamento = modelLancamento;
                    }
                    else
                    {
                        ViewBag.modelLancamento = null;
                    }


                    IEnumerable<PoolBensModel> poolBensInvestimentoAtivo;


                    IRestResponse responsepoolBensInvestimentoAtivo = await PostRestAsync<PoolBensModel>("/Investimento/GetListPoolParaInvestimento");
                    if (responsepoolBensInvestimentoAtivo.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        poolBensInvestimentoAtivo = JsonConvert.DeserializeObject<IEnumerable<PoolBensModel>>(responsepoolBensInvestimentoAtivo.Content);

                        ViewBag.poolBensInvestimentoAtivo = poolBensInvestimentoAtivo;
                    }
                    else
                    {
                        ViewBag.poolBensInvestimentoAtivo = null;
                    }

                    return View("IndexAdmin");
                }


                IEnumerable<CarteiraLogModel> model;


                IRestResponse response = await PostRestAsync<CarteiraLogModel>("/Investimento/GetCarteiraLogByIdPessoa?idPessoa=" + SessionManager.usuarioLogado.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    DateTime? dtFilterInicio = null;
                    DateTime? dtFilterFim = null;
                    if (dtI != "" & dtE == "")
                    {
                        dtFilterInicio = Convert.ToDateTime(dtI + " 00:00:00");
                        dtFilterFim = Convert.ToDateTime(dtI + " 23:59:59");
                    }
                    else if (dtI != "" & dtE != "")
                    {

                        dtFilterInicio = Convert.ToDateTime(dtI + " 00:00:00");
                        dtFilterFim = Convert.ToDateTime(dtE + " 23:59:59");
                    }



                    model = JsonConvert.DeserializeObject<IEnumerable<CarteiraLogModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);


                    if (model.Count() > 0 && dtFilterInicio != null)
                    {
                        model = model.Where(c => c.Data >= dtFilterInicio && c.Data <= dtFilterFim).AsEnumerable();
                       
                    } ViewBag.iType = iType;
                    retorno.objetoRetorno = model;
                    var listaCredit = new List<CarteiraLogModel>();
                    var listaDebit = new List<CarteiraLogModel>();
                    var listaYields = new List<CarteiraLogModel>();
                    var listaSimplifield = new List<CarteiraLogModel>();

                    foreach (var item in model)
                    {
                        if (item.Tipo == "retirada aprovado" 
                            || item.Tipo == "deposito aprovado" 
                            || item.Tipo == "venda cota aprovado" 
                            || item.Tipo == "compra cota aprovado")
                        {
                            listaSimplifield.Add(item);
                        } 
                        if (item.Tipo == "dividendo")
                        {
                            listaYields.Add(item);
                        }
                        else
                        if (item.Tipo == "deposito aprovado" 
                            || item.Tipo == "dividendo")
                        {
                            listaCredit.Add(item);
                        }
                        else if (item.Tipo == "retirada aprovado")
                        {
                            listaDebit.Add(item);
                        }
                    }
                    ViewBag.listaCredit = listaCredit;
                    ViewBag.listaDebit = listaDebit;
                    ViewBag.listaYields = listaYields;
                    ViewBag.listaSimplifield = listaSimplifield;


                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public async Task<ActionResult> GetInvestidoresSaldosInvestimentos()
        {

            IEnumerable<PessoaInvestimentoPoolsValoresModel> model;


            IRestResponse response = await PostRestAsync<PessoaInvestimentoPoolsValoresModel>("/Administrativo/GetListHomeInvestidores");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model = JsonConvert.DeserializeObject<IEnumerable<PessoaInvestimentoPoolsValoresModel>>(response.Content);

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = model;

                return View("_lstInvestidoresSaldosInvestimentos", model);

            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;

                return View("_lstInvestidoresSaldosInvestimentos", retorno);
            }
        }


        [HttpPost]
        public async Task<ActionResult> InsertHelp(string title, string desc)
        {
            try
            {
                HelpInterno help = new HelpInterno()
                {
                    Conteudo = desc,
                    IdPessoa = SessionManager.usuarioLogado.Id,
                    Titulo = title
                };

                IRestResponse responseLancamento = await PutObjectRestAsync<HelpInterno>("/Pessoa/InsertHelpInterno", help);
                if (responseLancamento.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var objs = JsonConvert.DeserializeObject<HelpInterno>(responseLancamento.Content);

                    retorno = new ResponseJson() { objetoRetorno = objs, possuiDados = true, statusRetorno = "sucesso" };

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    retorno = new ResponseJson() { objetoRetorno = null, possuiDados = false, statusRetorno = responseLancamento.StatusCode.ToString() };

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {

                retorno = new ResponseJson() { objetoRetorno = null, possuiDados = false, statusRetorno = "error" };

                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

    }
}