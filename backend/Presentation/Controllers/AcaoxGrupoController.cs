﻿using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Presentation.Controllers
{
    public class AcaoxGrupoController : BaseController
    {
        // GET: AcaoxGrupo
        public async Task<ActionResult> Index()
        {
            try
            {
                IEnumerable<GruposModel> model;

                
                IRestResponse response = await PostRestAsync<Grupos>("/GrupoAcoes/GetListGrupoXActions");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<GruposModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;


                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);

            }
            catch (Exception ex)
            {

                throw;
            }
            return View();
        }




        [HttpPost]
        public async Task<ActionResult> SetAcaoGrupo(int idGrupo, int idAcao)
        {

            try
            {
                
                IRestResponse response = await PostRestAsync<GruposAcoes>("/GrupoAcoes/SetGrupoXActions?idGrupo=" + idGrupo + "&idAcao=" + idAcao + "&idUsuario=" + SessionManager.usuarioLogado._Usuario.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<GruposAcoes>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }


            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}