﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Domain.Enuns;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class DividendoController : BaseController
    {

        public DividendoController()
        {
            

        }
        // GET: Dividendo
        public async Task<ActionResult> Index()
        {
            IEnumerable<PessoaPoolsInvestidosModel> model;
            IRestResponse response = await PostRestAsync<PessoaPoolsInvestidos>("/PoolsBens/GetPessoasPoolsInvestido");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model = JsonConvert.DeserializeObject<IEnumerable<PessoaPoolsInvestidosModel>>(response.Content);

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = model;

            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;

            }

            return View(retorno);
        }


        [HttpPost]
        public async Task<ActionResult> AddDividendo(string dtReferencia, string obs, string dividendo, int idPoolPessoaBem, int cotas, int idPessoa, int IdPoolBem)
        {

            try
            {

                var splitDate = dtReferencia.Split('/');


                DateTime dtLancamento = new DateTime(int.Parse(splitDate[1]), int.Parse(splitDate[0]), 3);
                Lancamentos objDto = new Lancamentos()
                {
                    Credito = true,
                    DtInclusao = DateTime.Now,
                    DtLancamento = dtLancamento,
                    IdPessoa = idPessoa,
                    IdPoolBem = IdPoolBem,
                    IdPoolBemPessoa = idPoolPessoaBem,
                    NumParticao = cotas.ToString(),
                    Obs = obs,
                    Valor = Convert.ToDecimal(dividendo),
                    IdTpLancamento = 1,
                    IdUsuarioInclusao = SessionManager.usuarioLogado.Id

                };

                IRestResponse response = await PutObjectRestAsync<Lancamentos>("/PoolsBens/InsertLancamento", objDto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Lancamentos model = JsonConvert.DeserializeObject<Lancamentos>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    if (model == null)
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);

                    }

                    return Json(true, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(false, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {

                return Json(false, JsonRequestBehavior.AllowGet);

                throw;
            }

        }


        public async Task<ActionResult> AddDividendo(int id)
        {
            try
            {
                PessoaPoolsInvestidosModel model;
                IRestResponse response = await PostRestAsync<PessoaPoolsInvestidos>("/PoolsBens/GetPessoasPoolsInvestidoById?id=" + id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<PessoaPoolsInvestidosModel>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                }

                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}