﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Presentation.Controllers
{
    public class DepositosController : BaseController
    {
        public DepositosController()
        {
            

        }
        // GET: Depositos
        public async Task<ActionResult> Validar()
        {

            IEnumerable<ComprovanteDepositoModel> model;


            IRestResponse response = await PostRestAsync<ComprovanteDeposito>("/Depositos/GetListComprovanteDepositoPendentes");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model = JsonConvert.DeserializeObject<IEnumerable<ComprovanteDepositoModel>>(response.Content);

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = model;

                return View(retorno);

            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;

                return View(retorno);
            }

            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


            return View(retorno);
        }

        public async Task<ActionResult> ValidarComprovante(int id)
        {

            try
            {


                //capturar comprovante by id
                var dto = new ComprovanteDeposito();
                IRestResponse response = await PostRestAsync<ComprovanteDeposito>("/Depositos/GetComprovanteDepositoById?id=" + id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<ComprovanteDeposito>(response.Content);
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        [HttpPost]
        public async Task<ActionResult> AprovaReprovaComprovante(int idComprovante, bool fReprovado, string dtAdm)
        {
            //capturar comprovante by id
            var dto = new ComprovanteDeposito();
            IRestResponse response = await PostRestAsync<ComprovanteDeposito>("/Pessoa/GetComprovanteDepositoById?id=" + idComprovante);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                dto = JsonConvert.DeserializeObject<ComprovanteDeposito>(response.Content);

                dto.IdUsuarioAlteracao = SessionManager.usuarioLogado.Id;
                dto.DtAprovaReprova = Convert.ToDateTime(dtAdm);
                dto.FlagPendente = false;
                dto.FlagNegado = fReprovado;
                //dto.FlagComputado = !fReprovado;

                if (fReprovado)
                    dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Recusado;
                else
                    dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Aprovado;
                //atualizar entrada do comprovante
                response = await PutObjectRestAsync<ComprovanteDeposito>("/Pessoa/UpdateComprovanteDeposito", dto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content == null)
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.GatewayTimeout);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    }
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                }



                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public async Task<ActionResult> DeletarComprovante(int idComprovante)
        {
            //capturar comprovante by id
            var dto = new ComprovanteDeposito();
            IRestResponse response = await PostRestAsync<ComprovanteDeposito>("/Pessoa/GetComprovanteDepositoById?id=" + idComprovante);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                dto = JsonConvert.DeserializeObject<ComprovanteDeposito>(response.Content);

                dto.IdUsuarioAlteracao = SessionManager.usuarioLogado.Id;


                //dto.FlagComputado = !fReprovado;

                dto.IdStatus = Domain.Enuns.EnumStatusCarteira.Inativo;

                //atualizar entrada do comprovante
                response = await PutObjectRestAsync<ComprovanteDeposito>("/Pessoa/UpdateComprovanteDeposito", dto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content == null)
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.GatewayTimeout);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    }
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                }



                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

    }
}