﻿using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class CadastreAquiController : BaseController
    {

        public CadastreAquiController()
        {
            

        }
        [AllowAnonymous]
        public ActionResult PrivacyPolicy()
        {

            return View();
        }
        [AllowAnonymous]
        public ActionResult TermsServices()
        {

            return View();
        }


        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
      
            var model = new PessoasModel() { IdUsuarioInclusao = 3, IdStatus = 3 };
            model.DtNascimento = DateTime.Now;
            model._Usuario = new UsuariosModel() { IdUsuarioInclusao = 3, Admin = false };
            model._PessoaJuridica = new PessoasJuridicaModel() { IdUsuarioInclusao = 3 };
            model._PessoaFisica = new PessoasFisicasModel() { IdUsuarioInclusao = 3 };
            model._Endereco = new PessoasEnderecosModel() { IdUsuarioInclusao = 3 };
            model.objDocumento = new PessoasDocumentosModel();
            model._Usuario._Login = new LoginsModel() { IdUsuarioInclusao = 3 };
            SessionManager.ListaDocumentosFirstCad = new List<PessoasDocumentosModel>();

            await populaViewBagEstados();
            await populaViewBagDocumentos();


            var listaOrgaos = new List<OrgaosEmissores>();
            listaOrgaos.Add(new OrgaosEmissores()
            {
                Id = 0,
                Nome = "Escolha um estado antes"
            });
            ViewBag.OrgaoEmissor = listaOrgaos;


            if (VerificaMobile())
                return View("IndexMobile", model);

            return View(model);
        }

        [AllowAnonymous] 
        [HttpPost]
        public async Task<ActionResult> Index(PessoasModel model)
        {
            try
            {

                // salvar pessoa
                model._Documentos = SessionManager.ListaDocumentosFirstCad;

                if (!model.FlagResidenteAmericado)
                {

                    model._Endereco.Bairro = "";
                    model._Endereco.Logradouro = model._Endereco.LogradouroUSA;
                    model._Endereco.Complemento = model._Endereco.ComplementoUSA;
                    model._Endereco.Cep = model._Endereco.CepUSA;

                    model.FlagResidenteAmericado = true;
                }
                else
                    model.FlagResidenteAmericado = false;



                if (model.ValorInicial == "0")
                {
                    model.ValorInicial = model.ValorInicialOther;
                }

                var dto = Mapper.Map<Pessoas>(model);
                dto._Usuario.Bloqueado = true;





                IRestResponse response = await PutObjectRestAsync<Pessoas>("/Pessoa/PutCadastroPessoaInicial", dto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    var pInsert = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    if (pInsert.Id > 0)
                    {
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(false, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        private async Task populaViewBagDocumentos()
        {
            try
            {

                List<DocumentosModel> lstDocumentos = new List<DocumentosModel>();
                IEnumerable<DocumentosModel> documentos = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Documentos>>("/Documentos/GetDocumentos");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Documentos>>(response.Content);

                    documentos = Mapper.Map<IEnumerable<DocumentosModel>>(dto);

                    if (documentos != null)
                    {
                        ViewBag.Documentos = documentos;
                    }
                    else
                    {
                        lstDocumentos.Add(new DocumentosModel()
                        {
                            Id = 0,
                            Nome = "INDEFINIDO"
                        });

                        ViewBag.Documentos = lstDocumentos;
                    }

                }
                else
                {
                    lstDocumentos.Add(new DocumentosModel()
                    {
                        Id = 0,
                        Nome = "INDEFINIDO"
                    });

                    ViewBag.Documentos = lstDocumentos;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private async Task populaViewBagEstados()
        {
            try
            {
                List<EstadosModel> LstEstados = new List<EstadosModel>();
                IEnumerable<EstadosModel> estados = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Estados>>("/estados/getEstados");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Estados>>(response.Content);

                    estados = Mapper.Map<IEnumerable<EstadosModel>>(dto);

                    if (estados != null)
                    {
                        ViewBag.Estados = estados;
                    }
                    else
                    {
                        LstEstados.Add(new EstadosModel()
                        {
                            Sigla = "NF",
                            Nome = "INDEFINIDO"
                        });

                        ViewBag.Estados = LstEstados;
                    }

                }
                else
                {
                    LstEstados.Add(new EstadosModel()
                    {
                        Sigla = "NF",
                        Nome = "INDEFINIDO"
                    });

                    ViewBag.Estados = LstEstados;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<JsonResult> GetCidadesByIdEstado(int idEstado)
        {
            try
            {
                List<CidadesModel> ListaCidades = new List<CidadesModel>();
                IEnumerable<CidadesModel> cidade = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Cidades>>("/Cidades/GetCidadesByEstado");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Cidades>>(response.Content);

                    cidade = Mapper.Map<IEnumerable<CidadesModel>>(dto);

                    if (cidade == null)
                    {
                        return Json(cidade, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ListaCidades.Add(new CidadesModel()
                        {
                            Nome = "INDEFINIDO"
                        });
                        return Json(ListaCidades, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    ListaCidades.Add(new CidadesModel()
                    {
                        Nome = "INDEFINIDO"
                    });
                    return Json(ListaCidades, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<JsonResult> InsertCidadeInexistente(int idEstado, string sNome)
        {
            try
            {
                Cidades cidadeDto = new Cidades()
                {
                    CodigoIBGE = "",
                    IdEstado = idEstado,
                    IdUsuarioInclusao = 3,
                    Nome = sNome
                };

                IRestResponse response = await PutObjectRestAsync<IEnumerable<Cidades>>("/Cidades/InsertCidade", cidadeDto);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Cidades>>(response.Content);

                    var cidadeModel = Mapper.Map<IEnumerable<CidadesModel>>(dto);

                    if (cidadeModel == null)
                    {
                        return Json(cidadeModel, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        return Json(false, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }



        [AllowAnonymous]
        public async Task<JsonResult> GetOrgaosEmissoresByIdEstado(int idEstado)
        {
            try
            {
                List<OrgaosEmissoresModel> ListaOrgaos = new List<OrgaosEmissoresModel>();
                IEnumerable<OrgaosEmissoresModel> orgaosEmissores = null;
                IRestResponse response = await PostRestAsync<IEnumerable<OrgaosEmissores>>("/OrgaosEmissores/GetListOrgaosEmissoresByIdEstado?idEstado=" + idEstado);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<OrgaosEmissores>>(response.Content);

                    orgaosEmissores = Mapper.Map<IEnumerable<OrgaosEmissoresModel>>(dto);

                    if (orgaosEmissores != null)
                    {
                        return Json(orgaosEmissores, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ListaOrgaos.Add(new OrgaosEmissoresModel()
                        {
                            Id = 0,
                            Nome = "INDEFINIDO"
                        });
                        return Json(ListaOrgaos, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    ListaOrgaos.Add(new OrgaosEmissoresModel()
                    {
                        Id = 0,
                        Nome = "INDEFINIDO"
                    });
                    return Json(ListaOrgaos, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [AllowAnonymous]
        public async Task<JsonResult> SetDocumentosCadastro(int idPessoaDocumento, int idDocumento, string sDocumento, string Description, string sNumero, int idUF, string sUF, int idOrgao, string sOrgao)
        {
            try
            {
                List<PessoasDocumentos> ListaDocumento = new List<PessoasDocumentos>();
                PessoasDocumentosModel novo = new PessoasDocumentosModel()
                {
                    Id = idPessoaDocumento,
                    IdDocumento = idDocumento,
                    Conteudo = sNumero,
                    IdEstado = idUF,
                    IdOrgaoEmissor = idOrgao,
                    Descricao = Description
                };


                SessionManager.ListaDocumentosFirstCad.Add(novo);



                return Json(novo.Id, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                //createLog(SessionManager.UsuarioLogado.ID, "MSPessoa", "Error - dellTelefone idTelefone = " + idTelefone, ex.Message);
                return Json(0, JsonRequestBehavior.AllowGet);
                throw;
            }
        }



        [AllowAnonymous]
        public async Task<JsonResult> deleteDocumentoCadastro(int idPessoaDocumento)
        {
            try
            {
                var documentToRemove = SessionManager.ListaDocumentosFirstCad.Where(c => c.Id == idPessoaDocumento).FirstOrDefault();

                SessionManager.ListaDocumentosFirstCad.Remove(documentToRemove);

                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);

            }
        }

        [AllowAnonymous]
        public async Task<JsonResult> deleteDocumentoCadastroExist(int id)
        {
            try
            {
                var documentToRemove = SessionManager.ListaDocumentosFirstCad.Where(c => c.Id == id).FirstOrDefault();

                SessionManager.ListaDocumentosFirstCad.Remove(documentToRemove);

                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);

            }
        }
        [AllowAnonymous]
        public async Task<ActionResult> ValidaEmail(string hash)
        {
            IRestResponse response = await PostRestAsync<ReturnGeneric>("/Pessoa/PutValidaCadastroInicial?saltKey=" + hash);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {

                var pInsert = JsonConvert.DeserializeObject<ReturnGeneric>(response.Content);

                if (pInsert.isError == false)
                {
                    ViewBag.complete = true;
                    return View();
                }
                else
                {

                    ViewBag.complete = false;
                    return View();
                }
            }

            ViewBag.complete = false;
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ValidaEmailInicial(string sEmail)
        {
            try
            {
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/PutValidaEmailInicial?sEmail=" + sEmail);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                   
                    var pInsert = JsonConvert.DeserializeObject<Pessoas>(response.Content);
                    if (pInsert != null)
                    {

                        retorno.statusRetorno = "This email already has a registration on the platform.";
                        retorno.objetoRetorno = false;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.objetoRetorno = true;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = false;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ValidaUsuarioInicial(string sUsuario)
        {
            try
            {
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/PutValidaUsuarioInicial?sUsuario=" + sUsuario);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                    var pInsert = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    if (pInsert!=null)
                    {

                        retorno.statusRetorno = "This user already has a user account on the platform.";
                        retorno.objetoRetorno = false;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.objetoRetorno = true;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = false;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ValidaCPFInicial(string sCPF)
        {
            try
            {
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/PutValidaCPFInicial?sCPF=" + sCPF);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                    var pInsert = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    if (pInsert != null)
                    {

                        retorno.statusRetorno = "This CPF is already in use on the platform.";
                        retorno.objetoRetorno = false;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.objetoRetorno = true;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = false;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ValidaTAXInicial(string sTax)
        {
            try
            {
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/PutValidaTAXInicial?sTAX=" + sTax);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                    var pInsert = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    if (pInsert != null)
                    {

                        retorno.statusRetorno = "This TAX ID is already in use on the platform.";
                        retorno.objetoRetorno = false;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.objetoRetorno = true;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = false;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ValidaSocialNumberInicial(string sSocial)
        {
            try
            {
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/PutValidaSocialInicial?sSocial=" + sSocial);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);

                    var pInsert = JsonConvert.DeserializeObject<Pessoas>(response.Content);

                    if (pInsert != null)
                    {

                        retorno.statusRetorno = "This Social Number is already in use on the platform.";
                        retorno.objetoRetorno = false;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.objetoRetorno = true;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = false;
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}