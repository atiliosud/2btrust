﻿using AutoMapper;
using Domain.Entities;
using Domain.Entities._2BTrust;
using Domain.Enuns;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Presentation.Controllers
{
    [IndicadoresAuth]
    public class BaseController : Controller
    {
        public BaseController()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        public static void ResizeJpg(string path, int nWidth, int nHeight)
        {
            using (var result = new Bitmap(nWidth, nHeight))
            {
                using (var input = new Bitmap(path))
                {
                    using (Graphics g = Graphics.FromImage((System.Drawing.Image)result))
                    {
                        g.DrawImage(input, 0, 0, nWidth, nHeight);
                    }
                }

                var ici = ImageCodecInfo.GetImageEncoders().FirstOrDefault(ie => ie.MimeType == "image/jpeg");
                var eps = new EncoderParameters(1);
                eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
                result.Save(path, ici, eps);
            }
        }
        public void InsertLogError(Exception ex, string controllerRepository, string metodo)
        {
            //_userLog.InsertLogErroControllers(ex, controllerRepository, metodo);
        }


        public async Task GetListInvestidoresManuais()
        {
            

            IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/GetListPessoas?fAll=false");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var model = JsonConvert.DeserializeObject<IEnumerable<PessoasModel>>(response.Content);

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = model;


                SessionManager.ListaInvestidoresManuais = model.ToList();

            }
            else
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                retorno.objetoRetorno = null;
                SessionManager.ListaInvestidoresManuais = null;
            }
        }


        public async Task inserLogLogin(LogLogin log)
        {
            

            IRestResponse response = await PutParametersRestAsync<bool>("/Usuarios/PutLogLogin", log);


            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var alerta = JsonConvert.DeserializeObject<ObjetoAlerta>(response.Content);
            }
        }

        public string urlWebAPI = System.Configuration.ConfigurationManager.AppSettings["urlWebApiApontadores"];

        public ResponseJson retorno = new ResponseJson();

        public async Task<IRestResponse> GetRestAsync<T>(string ComplementoUrl)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;

        }

        public async Task<IRestResponse> PostRestAsync<T>(string ComplementoUrl)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.POST);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;

        }
        public async Task<IRestResponse> PutRestAsync<T>(string ComplementoUrl)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.PUT);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;

        }

        public async Task<IRestResponse> GetParamtesRestAsync<T>(string ComplementoUrl, object objetoPut)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.GET);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");
            request.RequestFormat = DataFormat.Json;

            request.AddObject(objetoPut);

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;


        }

        public async Task<IRestResponse> PutObjectRestAsync<T>(string ComplementoUrl, object objetoPut)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.PUT);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");
            request.RequestFormat = DataFormat.Json;

            request.AddBody((T)objetoPut);

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;


        }

        public async Task<IRestResponse> PutParametersRestAsync<T>(string ComplementoUrl, object objetoPut)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.PUT);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=");
            request.RequestFormat = DataFormat.Json;

            request.AddBody(objetoPut);

            //var splitParamters = sParametros.Split('&');
            //foreach (var item in splitParamters)
            //{
            //    var splitDentro = item.Split('=');
            //    request.AddParameter(splitDentro[0], splitDentro[1]);
            //}



            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;


        }






        public async Task<ResultDinamico> getInformatioQuery(string sNomeTable, EnumTipoQuery tipoQuerys, List<IntegracaoFiltroDinamico> listaFiltros, string sCampoRetorno, string conn)
        {
            try
            {
                var obj = new
                {
                    nomeTable = sNomeTable,
                    tipoQuery = tipoQuerys,
                    ListaFilters = listaFiltros,
                    sCampoRetorno = sCampoRetorno,
                    conn = conn,
                };
                //faz uma chamada na api para retornar todos os alertas
                

                IRestResponse response = await PutParametersRestAsync<ResultDinamico>("/InformationSql/GetInfoTableDynamic", obj);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    var infor = JsonConvert.DeserializeObject<ResultDinamico>(response.Content, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });
                    return infor;
                }


                return null;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<ResultDinamico> getInfoTable(string p, string c)
        {
            try
            {
                var objJson = new { nomeTabela = p, campoConsulta = c };

                

                IRestResponse response = await GetRestAsync<ResultDinamico>("/InformationSql/GetInfoTable?nomeTabela=" + objJson.nomeTabela + "&campoConsulta=" + c);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var listaInfo = JsonConvert.DeserializeObject<ResultDinamico>(response.Content);


                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = listaInfo;



                    return retorno.objetoRetorno;
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    Warning(ErrorMessageHttp.GetErrorMessage(response.StatusCode));
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ResultDinamico> getInfoTableGrafico(string table, int periodo, string campoB)
        {
            try
            {
                var objJson = new { nomeTabela = table, iPeriodo = periodo, campoBetween = campoB };

                

                IRestResponse response = await GetRestAsync<ResultDinamico>("/InformationSql/GetInfoTableGrafico?nomeTabela=" + objJson.nomeTabela + "&iPeriodo=" + objJson.iPeriodo + "&campoBetween=" + objJson.campoBetween);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var listaInfo = JsonConvert.DeserializeObject<ResultDinamico>(response.Content,
                   new JsonSerializerSettings
                   {
                       NullValueHandling = NullValueHandling.Ignore
                   });

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = listaInfo;



                    return retorno.objetoRetorno;
                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    Warning(ErrorMessageHttp.GetErrorMessage(response.StatusCode));
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        public string returnIconAlerta(EnumIconAlerta icons)
        {

            if (icons == EnumIconAlerta.Error)
                return "fa fa-times fa-4x";
            else if (icons == EnumIconAlerta.Exclamacao)
                return "fa fa-exclamation  fa-4x";
            else if (icons == EnumIconAlerta.Interrogacao)
                return "fa fa-question fa-4x";
            else if (icons == EnumIconAlerta.Sino)
                return "fa fa-bell fa-4x";
            else if (icons == EnumIconAlerta.Paciente)
                return "fa fa-users fa-4x";
            else if (icons == EnumIconAlerta.Atendimento)
                return "fa fa-medkit fa-4x";


            return "";

        }



        public bool VerificaMobile()
        {
            if (this.Request.Browser.IsMobileDevice || Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null ||
              (Request.ServerVariables["HTTP_ACCEPT"] != null && Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap")))
            {
                return true;
            }
            else if (Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                string[] mobiles = new[]
                {
                    "midp", "j2me", "avant", "docomo",
                    "novarra", "palmos", "palmsource",
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/",
                    "blackberry", "mib/", "symbian",
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio",
                    "SIE-", "SEC-", "samsung", "HTC",
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx",
                    "NEC", "philips", "mmm", "xx",
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java",
                    "pt", "pg", "vox", "amoi",
                    "bird", "compal", "kg", "voda",
                    "sany", "kdd", "dbt", "sendo",
                    "sgh", "gradi", "jb", "dddi",
                    "moto", "iphone", "ipad", "windows phone"
                };

                foreach (string s in mobiles)
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains(s.ToLower()))
                    {
                        return true;
                    }
                }

            }

            return false;
        }




        public void AddAlert(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey) ?
                (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Dismissable = dismissable,
                Message = message
            });

            TempData[Alert.TempDataKey] = alerts;

        }
        public void Success(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Success, message, dismissable);
        }
        public void Information(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Information, message, dismissable);
        }
        public void Warning(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Warning, message, dismissable);
        }
        public void Danger(string message, bool dismissable = false)
        {
            AddAlert(AlertStyles.Danger, message, dismissable);
        }


    }



    public class HelpInterno
    {
        public int Id { get; set; }
        public int IdPessoa { get; set; }
        public string Conteudo { get; set; }
        public string Titulo { get; set; }
        public DateTime DtInclusao { get; set; }
    }


}
