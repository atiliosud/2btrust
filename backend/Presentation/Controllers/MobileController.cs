﻿using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Presentation.Controllers
{
    public class MobileController : BaseController
    {
        // GET: Mobile

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> IndexValores(int idUsuario, string sHash)
        {
            ViewBag.IdUsuario = idUsuario;
            ViewBag.sHash = sHash;

            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> IndexHistorico(int idPessoa, string sHash)
        {
            ViewBag.idPessoa = idPessoa;
            ViewBag.sHash = sHash;

            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Valores(int idUsuario, string sHash)
        {

            try
            {

                PessoasModel pessoas = null;
                IRestResponse response = await PostRestAsync<PessoasModel>("/Mobile/GetInfoHomeValores?idUsuario=" + idUsuario + "&sHash=" + sHash);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var usuarioLogado = JsonConvert.DeserializeObject<PessoasModel>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = usuarioLogado;

                    pessoas = Mapper.Map<PessoasModel>(retorno.objetoRetorno);

                    return View(retorno);


                }
                return View();
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Historico(int idPessoa, string sHash)

        {

            try
            {

                IEnumerable<CarteiraLogModel> model;


                IRestResponse response = await PostRestAsync<CarteiraLogModel>("/Investimento/GetCarteiraLogByIdPessoa?idPessoa=" + idPessoa);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<CarteiraLogModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                 

                 
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}