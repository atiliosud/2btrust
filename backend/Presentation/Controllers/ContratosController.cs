﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class ContratosController : BaseController
    {
        // GET: Contratos
        public async Task<ActionResult> Index()
        {

            try
            {


                IEnumerable<ContratosModel> model;


                IRestResponse response = await PostRestAsync<Contratos>("/Contratos/GetListContratos");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<ContratosModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<ActionResult> Contrato(int? idContrato)
        {
            try
            {
                if (idContrato == null)
                {
                    var modelInsert = new ContratosModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        IdStatus = 1,
                        Id = 0
                    };


                    return View(modelInsert);
                }

                var model = new ContratosModel();
                IRestResponse response = await PostRestAsync<Contratos>("/Contratos/GetContratosById?idContrato=" + idContrato);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<ContratosModel>(response.Content);


                    return View(model);

                }
                else
                {
                    var modelInsert = new ContratosModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        IdStatus = 1,
                        Id = 0
                    };

                    return View(modelInsert);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }




        [HttpPost]
        public async Task<ActionResult> Contrato(ContratosModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    string sLocation = "/Contratos/InsertContrato";
                    var dto = Mapper.Map<Contratos>(model);
                    if (dto.Id > 0) sLocation = "/Contratos/EditContrato";


                    IRestResponse response = await PutObjectRestAsync<Contratos>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<Contratos>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }



            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<bool> EditContrato(Contratos contrato)
        {
            try
            {
                var dto = new Areas();
                IRestResponse response = await PutObjectRestAsync<Contratos>("/Contratos/EditContrato", contrato);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        return true;
                    }

                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        [HttpPost]
        public async Task<ActionResult> Ativar(int idContrato)
        {
            try
            {
                //buscar contrato  
                var dto = new Contratos();
                IRestResponse response = await PostRestAsync<Contratos>("/Contratos/GetContratosById?idContrato=" + idContrato);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Contratos>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 1;

                    // ativar (UPDATE__
                    var fUpdate = await EditContrato(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Desativar(int idContrato)
        {
            try
            {
                //buscar contrato 
                var dto = new Contratos();
                IRestResponse response = await PostRestAsync<Contratos>("/Contratos/GetContratosById?idContrato=" + idContrato);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<Contratos>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 2;

                    // ativar (UPDATE__
                    var fUpdate = await EditContrato(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }






    }
}