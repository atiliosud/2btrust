﻿using ApplicationServices.IApplicationServices;
using AutoMapper;
using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Presentation.Controllers
{
    public class PoolsBensController : BaseController
    {
        // GET: PoolsBens
        public async Task<ActionResult> Index()
        {

            try
            {


                IEnumerable<PoolBensModel> model;


                IRestResponse response = await PostRestAsync<PoolBens>("/PoolsBens/GetListPoolBens");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<PoolBensModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }


        public async Task<ActionResult> PoolsBem(int? idPool)
        {
            try
            {
                await populaViewBagContratos();
                if (idPool == null)
                {
                    var modelInsert = new PoolBensModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        IdStatus = 1,
                        Id = 0
                    };


                    return View(modelInsert);
                }

                var model = new PoolBensModel();
                IRestResponse response = await PostRestAsync<PoolBens>("/PoolsBens/GetPoolBensById?idPoolBem=" + idPool);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<PoolBensModel>(response.Content);


                    return View(model);

                }
                else
                {
                    var modelInsert = new PoolBensModel()
                    {
                        IdUsuarioInclusao = SessionManager.usuarioLogado._Usuario.Id,
                        IdStatus = 1,
                        Id = 0
                    };

                    return View(modelInsert);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        public async Task<ActionResult> PoolsBem(PoolBensModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    string sLocation = "/PoolsBens/InsertPoolBens";
                    var dto = Mapper.Map<PoolBens>(model);
                    if (dto.Id > 0) sLocation = "/PoolsBens/EditPoolBens";




                    if (!string.IsNullOrEmpty(SessionManager.SaveLogoPrincipal))
                        dto.PathLocationIIS = SessionManager.SaveLogoPrincipal;


                    if (!string.IsNullOrEmpty(SessionManager.SaveLogoMaior))
                        dto.PathLocationIISLogoMaior = SessionManager.SaveLogoMaior;


                    if (!string.IsNullOrEmpty(SessionManager.SaveLogoMenor))
                        dto.PathLocationIISLogoMenor = SessionManager.SaveLogoMenor;

                    IRestResponse response = await PutObjectRestAsync<PoolBens>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<PoolBens>(response.Content);
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                            retorno.objetoRetorno = dto;

                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }



            }
            catch (Exception ex)
            {
                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                retorno.objetoRetorno = null;
                return Json(retorno, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task<bool> EditPool(PoolBens contrato)
        {
            try
            {
                var dto = new PoolBens();
                IRestResponse response = await PutObjectRestAsync<PoolBens>("/PoolsBens/EditPoolBens", contrato);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.Content != null)
                    {
                        return true;
                    }

                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }





        [HttpPost]
        public FineUploaderResult UploadFilePrincipal(FineUpload upload, int idPoolBem)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\ImagemLogo\Logo_principal_" + idPoolBem;
            string meioPathIIS = @"Image/ImagemLogo/Logo_principal_" + idPoolBem;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;


            if(!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
             //   ResizeJpg(dir + @"\" + upload.Filename, 800, 600);
                SessionManager.SaveLogoPrincipal = dirIIS;

            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }

        [HttpPost]
        public FineUploaderResult UploadFileMaior(FineUpload upload, int idPoolBem)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\ImagemLogo\Logo_maior_" + idPoolBem;
            string meioPathIIS = @"Image/ImagemLogo/Logo_maior_" + idPoolBem;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;


            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
               // ResizeJpg(dir + @"\" + upload.Filename, 800, 600);
                SessionManager.SaveLogoMaior = dirIIS;

            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }

        [HttpPost]
        public FineUploaderResult UploadFileMenor(FineUpload upload, int idPoolBem)
        {
            // asp.net mvc will set extraParam1 and extraParam2 from the params object passed by Fine-Uploader
            string meioPath = @"Image\ImagemLogo\Logo_menor_" + idPoolBem;
            string meioPathIIS = @"Image/ImagemLogo/Logo_menor_" + idPoolBem;

            var dir = System.Configuration.ConfigurationManager.AppSettings["localPath"] + meioPath;
            var dirIIS = System.Configuration.ConfigurationManager.AppSettings["localPathIIS"] + meioPathIIS + "/" + upload.Filename;


            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            var filePath = Path.Combine(dir, upload.Filename);

            try
            {
                upload.SaveAs(filePath);
             //   ResizeJpg(dir + @"\" + upload.Filename, 800, 600);
                SessionManager.SaveLogoMenor = dirIIS;

            }
            catch (Exception ex)
            {
                return new FineUploaderResult(false, error: ex.Message);
            }

            // the anonymous object in the result below will be convert to json and set back to the browser
            return new FineUploaderResult(true, new { extraInformation = 12345 });
        }


        [HttpPost]
        public async Task<ActionResult> Ativar(int idPool)
        {
            try
            {
                //buscar contrato  
                var dto = new PoolBens();
                IRestResponse response = await PostRestAsync<PoolBens>("/PoolsBens/GetPoolBensById?idPoolBem=" + idPool);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<PoolBens>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 1;

                    // ativar (UPDATE__
                    var fUpdate = await EditPool(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }

                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Desativar(int idPool)
        {
            try
            {
                //buscar contrato 
                var dto = new PoolBens();
                IRestResponse response = await PostRestAsync<PoolBens>("/PoolsBens/GetPoolBensById?idPoolBem=" + idPool);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dto = JsonConvert.DeserializeObject<PoolBens>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;

                    dto.IdStatus = 2;

                    // ativar (UPDATE__
                    var fUpdate = await EditPool(dto);

                    if (!fUpdate)
                    {

                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                    }


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;


                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }




        private async Task populaViewBagContratos()
        {
            try
            {
                List<ContratosModel> LstContratos = new List<ContratosModel>();
                IEnumerable<ContratosModel> contratos = null;
                IRestResponse response = await PostRestAsync<IEnumerable<Contratos>>("/Contratos/GetListContratos");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<Contratos>>(response.Content);

                    contratos = Mapper.Map<IEnumerable<ContratosModel>>(dto);

                    if (contratos != null)
                    {
                        ViewBag.Contratos = contratos.Where(c => c.IdStatus == 1).AsEnumerable();
                    }
                    else
                    {
                        LstContratos.Add(new ContratosModel()
                        {

                            Descricao = "INDEFINIDO",
                            Id = 0
                        });

                        ViewBag.Contratos = LstContratos;
                    }

                }
                else
                {

                    LstContratos.Add(new ContratosModel()
                    {

                        Descricao = "INDEFINIDO",
                        Id = 0
                    });

                    ViewBag.Contratos = LstContratos;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        } // GET: PoolsBens



        public async Task<ActionResult> Rentabilidade()
        {

            try
            {


                IEnumerable<PoolBensModel> model;


                IRestResponse response = await PostRestAsync<PoolBens>("/PoolsBens/GetListPoolBens");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = JsonConvert.DeserializeObject<IEnumerable<PoolBensModel>>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = model;

                    return View(retorno);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return View(retorno);
                }

                retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);


                return View(retorno);
            }
            catch (Exception ex)
            {

                throw;
            }

        }



        public async Task<ActionResult> CadRentabilidade()
        {
            try
            {
                await populaViewBagPools();

                PoolsBensRentabilidadePerformanceModel model = new PoolsBensRentabilidadePerformanceModel();
                return View(model);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<ActionResult> CadRentabilidade(PoolsBensRentabilidadePerformanceModel model)
        {
            try
            {
                await populaViewBagPools();


                if (ModelState.IsValid)
                {
                    var dto = Mapper.Map<PoolsBensRentabilidadePerformance>(model);
                    var sLocation = "/PoolsBens/InsertPoolsBensRentabilidadePerformance";
                    dto.idUsuarioinclusao = SessionManager.usuarioLogado._Usuario.Id;
                    dto.flgAtivo = true;
                    IRestResponse response = await PutObjectRestAsync<PoolsBensRentabilidadePerformance>(sLocation, dto);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response.Content != null)
                        {

                            dto = JsonConvert.DeserializeObject<PoolsBensRentabilidadePerformance>(response.Content);
                            retorno.statusRetorno = "success";
                            retorno.objetoRetorno = dto;

                        }
                        else
                        {
                            retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                            retorno.objetoRetorno = null;
                        }

                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(System.Net.HttpStatusCode.ExpectationFailed);
                        retorno.objetoRetorno = null;
                        return Json(retorno, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    retorno.statusRetorno = "Verifique a validação dos campos!";
                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {

                throw;
            }
        }


        private async Task populaViewBagPools()
        {
            try
            {
                List<PoolBensModel> LstPools = new List<PoolBensModel>();
                IEnumerable<PoolBensModel> contratos = null;
                IRestResponse response = await PostRestAsync<IEnumerable<PoolBens>>("/PoolsBens/GetListPoolBens");
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<IEnumerable<PoolBens>>(response.Content);

                    contratos = Mapper.Map<IEnumerable<PoolBensModel>>(dto);

                    if (contratos != null)
                    {
                        ViewBag.Pools = contratos.Where(c => c.IdStatus == 1).AsEnumerable();
                    }
                    else
                    {
                        LstPools.Add(new PoolBensModel()
                        {

                            Nome = "INDEFINIDO",
                            Id = 0
                        });

                        ViewBag.Pools = LstPools;
                    }

                }
                else
                {

                    LstPools.Add(new PoolBensModel()
                    {

                        Nome = "INDEFINIDO",
                        Id = 0
                    });

                    ViewBag.Pools = LstPools;
                }



            }
            catch (Exception ex)
            {

                throw;
            }
        }




        [HttpPost]
        public async Task<ActionResult> AtivarDesativarRent(int id, bool flgAtivo)
        {
            try
            {
                //buscar contrato   
                IRestResponse response = await PutRestAsync<bool>("/PoolsBens/DesativarAtivarPoolsBensRentabilidadePerformance?id=" + id + "&flgAtivo=" + flgAtivo);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var dto = JsonConvert.DeserializeObject<bool>(response.Content);

                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = dto;


                    return Json(retorno, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    retorno.statusRetorno = ErrorMessageHttp.GetErrorMessage(response.StatusCode);
                    retorno.objetoRetorno = null;

                    return Json(retorno, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }



    }
}