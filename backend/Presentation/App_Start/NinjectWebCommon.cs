[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Presentation.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Presentation.App_Start.NinjectWebCommon), "Stop")]

namespace Presentation.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
     
    using Data.Repository;
    using ApplicationServices.ApplicationServices;
    using ApplicationServices.IApplicationServices;
    using Domain.Services;
    using Domain.IServices;
    using Domain.IRepository;          

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        { 
            
            //APP SERVICES
            kernel.Bind<ICidadesAppServices>().To<CidadesAppServices>(); 
            kernel.Bind<IOrgaosEmissoresAppServices>().To<OrgaosEmissoresAppServices>();
            kernel.Bind<IPessoasDocumentosAppServices>().To<PessoasDocumentosAppServices>();
            kernel.Bind<IPessoasEnderecosAppServices>().To<PessoasEnderecosAppServices>();
            kernel.Bind<IUsuariosGruposAppServices>().To<UsuariosGruposAppServices>();
            kernel.Bind<IPoolBensPessoasAppServices>().To<PoolBensPessoasAppServices>();
            kernel.Bind<IValoresAppServices>().To<ValoresAppServices>();
            kernel.Bind<IUsuariosParametrosAppServices>().To<UsuariosParametrosAppServices>();
            kernel.Bind<IControlesAppServices>().To<ControlesAppServices>();
            kernel.Bind<IAreasAppServices>().To<AreasAppServices>();
            kernel.Bind<IAcoesAppServices>().To<AcoesAppServices>();
            kernel.Bind<IUsuariosAppServices>().To<UsuariosAppServices>();
            kernel.Bind<ILancamentosAppServices>().To<LancamentosAppServices>();
            kernel.Bind<IGruposAppServices>().To<GruposAppServices>();
            kernel.Bind<IGruposAcoesAppServices>().To<GruposAcoesAppServices>();
            kernel.Bind<IParametrosAppServices>().To<ParametrosAppServices>();
            kernel.Bind<IRentabilidadeAppServices>().To<RentabilidadeAppServices>();
            kernel.Bind<ILucratividadeAppServices>().To<LucratividadeAppServices>();
            kernel.Bind<IContratosAppServices>().To<ContratosAppServices>();
            kernel.Bind<ITiposAppServices>().To<TiposAppServices>();
            kernel.Bind<IPoolBensAppServices>().To<PoolBensAppServices>();
            kernel.Bind<ISessaoAppServices>().To<SessaoAppServices>();
            kernel.Bind<IBensAppServices>().To<BensAppServices>();
            kernel.Bind<IPessoasAppServices>().To<PessoasAppServices>();
            kernel.Bind<ILoginsAppServices>().To<LoginsAppServices>();
            kernel.Bind<IPessoasFisicaAppServices>().To<PessoasFisicaAppServices>();
            kernel.Bind<IRequisicoesAppServices>().To<RequisicoesAppServices>();
            kernel.Bind<IPessoasJuridicaAppServices>().To<PessoasJuridicaAppServices>();
            kernel.Bind<IRespostaAppServices>().To<RespostaAppServices>();
            kernel.Bind<IDocumentosAppServices>().To<DocumentosAppServices>();
            kernel.Bind<IBensEnderecoAppServices>().To<BensEnderecoAppServices>();
            kernel.Bind<IEstadosAppServices>().To<EstadosAppServices>();
            kernel.Bind<IPaisesAppServices>().To<PaisesAppServices>();
            kernel.Bind<IBensVisaoGeralAppServices>().To<BensVisaoGeralAppServices>();



            //SERVICES
            kernel.Bind<ICidadesServices>().To<CidadesServices>();
            kernel.Bind<IOrgaosEmissoresServices>().To<OrgaosEmissoresServices>();
            kernel.Bind<IPessoasDocumentosServices>().To<PessoasDocumentosServices>();
            kernel.Bind<IPessoasEnderecosServices>().To<PessoasEnderecosServices>();
            kernel.Bind<IUsuariosGruposServices>().To<UsuariosGruposServices>();
            kernel.Bind<IPoolBensPessoasServices>().To<PoolBensPessoasServices>();
            kernel.Bind<IValoresServices>().To<ValoresServices>();
            kernel.Bind<IUsuariosParametrosServices>().To<UsuariosParametrosServices>();
            kernel.Bind<IControlesServices>().To<ControlesServices>();
            kernel.Bind<IAreasServices>().To<AreasServices>();
            kernel.Bind<IAcoesServices>().To<AcoesServices>();
            kernel.Bind<IUsuariosServices>().To<UsuariosServices>();
            kernel.Bind<ILancamentosServices>().To<LancamentosServices>();
            kernel.Bind<IGruposServices>().To<GruposServices>();
            kernel.Bind<IGruposAcoesServices>().To<GruposAcoesServices>();
            kernel.Bind<IParametrosServices>().To<ParametrosServices>();
            kernel.Bind<IRentabilidadeServices>().To<RentabilidadeServices>();
            kernel.Bind<ILucratividadeServices>().To<LucratividadeServices>();
            kernel.Bind<IContratosServices>().To<ContratosServices>();
            kernel.Bind<ITiposServices>().To<TiposServices>();
            kernel.Bind<IPoolBensServices>().To<PoolBensServices>();
            kernel.Bind<ISessaoServices>().To<SessaoServices>();
            kernel.Bind<IBensServices>().To<BensServices>();
            kernel.Bind<IPessoasServices>().To<PessoasServices>();
            kernel.Bind<ILoginsServices>().To<LoginsServices>();
            kernel.Bind<IPessoasFisicaServices>().To<PessoasFisicaServices>();
            kernel.Bind<IRequisicoesServices>().To<RequisicoesServices>();
            kernel.Bind<IPessoasJuridicaServices>().To<PessoasJuridicaServices>();
            kernel.Bind<IRespostaServices>().To<RespostaServices>();
            kernel.Bind<IDocumentosServices>().To<DocumentosServices>();
            kernel.Bind<IBensEnderecoServices>().To<BensEnderecoServices>();
            kernel.Bind<IEstadosServices>().To<EstadosServices>();
            kernel.Bind<IPaisesServices>().To<PaisesServices>();
            kernel.Bind<IBensVisaoGeralServices>().To<BensVisaoGeralServices>();



            //Repository
            kernel.Bind<ICidadesRepository>().To<CidadesRepository>();
            kernel.Bind<IOrgaosEmissoresRepository>().To<OrgaosEmissoresRepository>();
            kernel.Bind<IPessoasDocumentosRepository>().To<PessoasDocumentosRepository>();
            kernel.Bind<IPessoasEnderecosRepository>().To<PessoasEnderecosRepository>();
            kernel.Bind<IUsuariosGruposRepository>().To<UsuariosGruposRepository>();
            kernel.Bind<IPoolBensPessoasRepository>().To<PoolBensPessoasRepository>();
            kernel.Bind<IValoresRepository>().To<ValoresRepository>();
            kernel.Bind<IUsuariosParametrosRepository>().To<UsuariosParametrosRepository>();
            kernel.Bind<IControlesRepository>().To<ControlesRepository>();
            kernel.Bind<IAreasRepository>().To<AreasRepository>();
            kernel.Bind<IAcoesRepository>().To<AcoesRepository>();
            kernel.Bind<IUsuariosRepository>().To<UsuariosRepository>();
            kernel.Bind<ILancamentosRepository>().To<LancamentosRepository>();
            kernel.Bind<IGruposRepository>().To<GruposRepository>();
            kernel.Bind<IGruposAcoesRepository>().To<GruposAcoesRepository>();
            kernel.Bind<IParametrosRepository>().To<ParametrosRepository>();
            kernel.Bind<IRentabilidadeRepository>().To<RentabilidadeRepository>();
            kernel.Bind<ILucratividadeRepository>().To<LucratividadeRepository>();
            kernel.Bind<IContratosRepository>().To<ContratosRepository>();
            kernel.Bind<ITiposRepository>().To<TiposRepository>();
            kernel.Bind<IPoolBensRepository>().To<PoolBensRepository>();
            kernel.Bind<ISessaoRepository>().To<SessaoRepository>();
            kernel.Bind<IBensRepository>().To<BensRepository>();
            kernel.Bind<IPessoasRepository>().To<PessoasRepository>();
            kernel.Bind<ILoginsRepository>().To<LoginsRepository>();
            kernel.Bind<IPessoasFisicaRepository>().To<PessoasFisicaRepository>();
            kernel.Bind<IRequisicoesRepository>().To<RequisicoesRepository>();
            kernel.Bind<IPessoasJuridicaRepository>().To<PessoasJuridicaRepository>();
            kernel.Bind<IRespostaRepository>().To<RespostaRepository>();
            kernel.Bind<IDocumentosRepository>().To<DocumentosRepository>();
            kernel.Bind<IBensEnderecoRepository>().To<BensEnderecoRepository>();
            kernel.Bind<IEstadosRepository>().To<EstadosRepository>();
            kernel.Bind<IPaisesRepository>().To<PaisesRepository>();
            kernel.Bind<IBensVisaoGeralRepository>().To<BensVisaoGeralRepository>();

        }        
    }
}


          