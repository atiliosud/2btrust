﻿using AutoMapper;
using Domain.Entities;
using Domain.Entities._2BTrust;
using Domain.Entities.EYE;
using Domain.Entities.Vortice;
using ImportacaoExcel_VorticeIndicadores.Entities;
using Presentation.Models;
using Presentation.Models._2BTrustModel;
using Presentation.Models.EYE;
using Presentation.Models.Vortice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Presentation.Mappers
{
    public class DomainToViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {


            Mapper.CreateMap<PoolsBensRentabilidadePerformance, PoolsBensRentabilidadePerformanceModel>();
            Mapper.CreateMap<PainelRentabilidadeMes, PainelRentabilidadeMesModel>();
            Mapper.CreateMap<BensBaseCalculoHistorico, BensBaseCalculoHistoricoModel>();
            Mapper.CreateMap<InformacoesAdminHome, InformacoesAdminHomeModel>();
            Mapper.CreateMap<ComprovanteDeposito, ComprovanteDepositoModel>();
            Mapper.CreateMap<Carteira, CarteiraModel>();
            Mapper.CreateMap<Acoes, AcoesModel>();
            Mapper.CreateMap<Areas, AreasModel>();
            Mapper.CreateMap<BensImagens, BensImagensModel>();
            Mapper.CreateMap<BensEnderecos, BensEnderecosModel>();
            Mapper.CreateMap<Bens, BensModel>();
            Mapper.CreateMap<BensVisaoGeral, BensVisaoGeralModel>();
            Mapper.CreateMap<BensVisaoGeralImagens, BensVisaoGeralImagensModel>();
            Mapper.CreateMap<Cidades, CidadesModel>();
            Mapper.CreateMap<Contratos, ContratosModel>();
            Mapper.CreateMap<Controles, ControlesModel>();
            Mapper.CreateMap<Documentos, DocumentosModel>();
            Mapper.CreateMap<Estados, EstadosModel>();
            Mapper.CreateMap<GruposAcoes, GruposAcoesModel>();
            Mapper.CreateMap<GrupoXActions, GrupoXActionsModel>();
            Mapper.CreateMap<Grupos, GruposModel>();
            Mapper.CreateMap<Lancamentos, LancamentosModel>();
            Mapper.CreateMap<Logins, LoginsModel>();
            Mapper.CreateMap<Lucratividade, LucratividadeModel>();
            Mapper.CreateMap<OrgaosEmissores, OrgaosEmissoresModel>();
            Mapper.CreateMap<Paises, PaisesModel>();
            Mapper.CreateMap<Parametros, ParametrosModel>();
            Mapper.CreateMap<PessoasDocumentos, PessoasDocumentosModel>();
            Mapper.CreateMap<PessoasEnderecos, PessoasEnderecosModel>();
            Mapper.CreateMap<PessoasFisicas, PessoasFisicasModel>();
            Mapper.CreateMap<PessoasJuridica, PessoasJuridicaModel>();
            Mapper.CreateMap<Pessoas, PessoasModel>();
            Mapper.CreateMap<PoolBens, PoolBensModel>();
            Mapper.CreateMap<PoolBensPessoas, PoolBensPessoasModel>();
            Mapper.CreateMap<Rentabilidade, RentabilidadeModel>();
            Mapper.CreateMap<Requisicoes, RequisicoesModel>();
            Mapper.CreateMap<Respostas, RespostasModel>();
            Mapper.CreateMap<Sessao, SessaoModel>();
            Mapper.CreateMap<Tipos, TiposModel>();
            Mapper.CreateMap<Usuarios, UsuariosModel>();
            Mapper.CreateMap<UsuariosGrupos, UsuariosGruposModel>();
            Mapper.CreateMap<UsuariosParametros, UsuariosParametrosModel>();
            Mapper.CreateMap<Valores, ValoresModel>();
        }

    }
}