﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;
using Presentation.Models;
using Presentation.Models.EYE;
using Domain.Entities.EYE;
using Presentation.Models.Vortice;
using ImportacaoExcel_VorticeIndicadores.Entities;
using Domain.Entities.Vortice;
using Domain.Entities._2BTrust;
using Presentation.Models._2BTrustModel;

namespace Presentation.Mappers
{
    public class ViewModelToDomainMappingProfile : Profile
    {

        protected override void Configure()
        {
            Mapper.CreateMap<PoolsBensRentabilidadePerformanceModel, PoolsBensRentabilidadePerformance>();
            Mapper.CreateMap<BensBaseCalculoHistoricoModel, BensBaseCalculoHistorico>();
            Mapper.CreateMap<InformacoesAdminHomeModel, InformacoesAdminHome>();
            Mapper.CreateMap<ComprovanteDepositoModel, ComprovanteDeposito>();
            Mapper.CreateMap<CarteiraModel, Carteira>();
            Mapper.CreateMap<AcoesModel, Acoes>();
            Mapper.CreateMap<AreasModel, Areas>();
            Mapper.CreateMap<BensEnderecosModel, BensEnderecos>();
            Mapper.CreateMap<BensImagensModel, BensImagens>();
            Mapper.CreateMap<BensModel, Bens>();
            Mapper.CreateMap<BensVisaoGeralModel, BensVisaoGeral>();
            Mapper.CreateMap<BensVisaoGeralImagensModel, BensVisaoGeralImagens>();
            Mapper.CreateMap<CidadesModel, Cidades>();
            Mapper.CreateMap<ContratosModel, Contratos>();
            Mapper.CreateMap<ControlesModel, Controles>();
            Mapper.CreateMap<DocumentosModel, Documentos>();
            Mapper.CreateMap<EstadosModel, Estados>();
            Mapper.CreateMap<GruposAcoesModel, GruposAcoes>();
            Mapper.CreateMap<GrupoXActionsModel, GrupoXActions>();
            Mapper.CreateMap<GruposModel, Grupos>();
            Mapper.CreateMap<LancamentosModel, Lancamentos>();
            Mapper.CreateMap<LoginsModel, Logins>();
            Mapper.CreateMap<LucratividadeModel, Lucratividade>();
            Mapper.CreateMap<OrgaosEmissoresModel, OrgaosEmissores>();
            Mapper.CreateMap<PaisesModel, Paises>();
            Mapper.CreateMap<ParametrosModel, Parametros>();
            Mapper.CreateMap<PessoasDocumentosModel, PessoasDocumentos>();
            Mapper.CreateMap<PessoasEnderecosModel, PessoasEnderecos>();
            Mapper.CreateMap<PessoasFisicasModel, PessoasFisicas>();
            Mapper.CreateMap<PessoasJuridicaModel, PessoasJuridica>();
            Mapper.CreateMap<PessoasModel, Pessoas>();
            Mapper.CreateMap<PoolBensModel, PoolBens>();
            Mapper.CreateMap<PoolBensPessoasModel, PoolBensPessoas>();
            Mapper.CreateMap<RentabilidadeModel, Rentabilidade>();
            Mapper.CreateMap<RequisicoesModel, Requisicoes>();
            Mapper.CreateMap<RespostasModel, Respostas>();
            Mapper.CreateMap<SessaoModel, Sessao>();
            Mapper.CreateMap<TiposModel, Tipos>();
            Mapper.CreateMap<UsuariosModel, Usuarios>();
            Mapper.CreateMap<UsuariosGruposModel, UsuariosGrupos>();
            Mapper.CreateMap<UsuariosParametrosModel, UsuariosParametros>();
            Mapper.CreateMap<ValoresModel, Valores>();
        }
    }
}