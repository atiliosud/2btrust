﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers
{
    public static class ErrorMessageHttp
    {
        public static string GetErrorMessage(System.Net.HttpStatusCode status)
        {
            if (status == System.Net.HttpStatusCode.OK)
                return "success"; 
            else if (status == System.Net.HttpStatusCode.NotFound)
                return "Nenhuma ação encontrada!";
            else if (status == System.Net.HttpStatusCode.NotImplemented)
                return "Nenhuma ação implementada!";
            else if (status == System.Net.HttpStatusCode.Unauthorized)
                return "Acesso não autorizado!";
            else if (status == System.Net.HttpStatusCode.RequestTimeout)
                return "Serviço retornou timeOut!";
            else if (status == System.Net.HttpStatusCode.ProxyAuthenticationRequired)
                return "Necessário autenticação no proxy!";
            else if (status == System.Net.HttpStatusCode.InternalServerError)
                return "Ocorreu um erro interno no serviço!";
            else if (status == System.Net.HttpStatusCode.BadRequest)
                return "Verifique a chamada do serviço!";
            else if (status == System.Net.HttpStatusCode.Gone)
                return "success";
            else if (status == System.Net.HttpStatusCode.Accepted)
                return "Chamada ao serviço foi aceita!";
            else return "Ocorreu um erro inesperado - " + status.ToString();



        }
    }
}