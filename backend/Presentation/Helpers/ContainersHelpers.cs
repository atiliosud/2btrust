﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers
{
    public class ContainersHelpers
    {
        public string ID { get; set; }
        public string Titulo { get; set; }
        public string iTamanho { get; set; }
        public string backGroundTitle { get; set; }
        public int iTemplate { get; set; }
        public bool isGrafico { get; set; }

        public List<ContainersConteudoHelpers> ListaConteudo { get; set; }
        public List<ContainersConteudoLabelHelpers> ListaLabels { get; set; }
    }

    public class ContainersConteudoLabelHelpers
    {
        public string Descricao { get; set; }
        public string Resultado { get; set; }
    }

    public class ContainersConteudoHelpers
    {
        public string Descricao { get; set; }
        public string valor { get; set; }
        public string icon { get; set; }
        public string iconColor { get; set; }
        public string backGround { get; set; }
        public int iTipoGrafico { get; set; }

        public List<ContainersConteudoGraficoDiaValorHelpers> ListaConteudoGrafico { get; set; }

    }
    public class ContainersConteudoGraficoDiaValorHelpers
    {

        public string Data1 { get; set; }
        public string valor1 { get; set; }
        public string Data2 { get; set; }
        public string valor2 { get; set; }
        public string Data3 { get; set; }
        public string valor3 { get; set; }

    }
}