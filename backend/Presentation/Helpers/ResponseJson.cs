﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers
{
    public class ResponseJson
    {
        public string statusRetorno { get; set; }

        public bool possuiDados { get; set; }

        public dynamic objetoRetorno { get; set; }
    }
}