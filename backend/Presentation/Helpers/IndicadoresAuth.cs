﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Presentation.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true)]
    public class IndicadoresAuth : AuthorizeAttribute
    {
        private ActionDescriptor _actionDescriptor;

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            //string cookie = filterContext.HttpContext.Request.Headers["Cookie"];
            //if (cookie.IndexOf("frmLogin") >= 0)
            //    returnAuthorize(filterContext, "Sessão expirada");
            //else
            returnAuthorize(filterContext, "Acesso negado");
            
        }

        private static void returnAuthorize(AuthorizationContext filterContext, String mensagem)
        {

            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {

                filterContext.Result = new HttpUnauthorizedResult();
            }
            else
            {
                var route = new RouteValueDictionary();
                route["action"] = "Index";
                route["controller"] = "Auth"; 
                filterContext.Result = new RedirectToRouteResult(route);
            }

        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            _actionDescriptor = filterContext.ActionDescriptor;
            base.OnAuthorization(filterContext);

        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            bool auth = base.AuthorizeCore(httpContext);

            if (SessionManager.usuarioLogado == null)
                auth = false;

            return auth;

        }

        //public void OnAuthorization(AuthorizationContext filterContext)
        //{

        //    // var key = filterContext.HttpContext.Request.QueryString["param_name"];
        //    if (filterContext.HttpContext.Request.IsAuthenticated && SessionManager.usuarioLogado != null)
        //    {
        //        HttpCookie authCookie = filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
        //        if (authCookie == null || authCookie.Value == "")
        //            return;
        //        else
        //            filterContext.Result = new HttpUnauthorizedResult();


        //        //verificar se perfil tem acesso ao menu solicitado 

        //        var routeData = filterContext.RouteData;
        //        var controller = routeData.GetRequiredString("controller");

        //        if (!SessionManager.usuarioLogado.perfil.PerfilXMenu.Where(c => c.menu.Controller == controller).Any())
        //        {
        //            filterContext.Result = new HttpUnauthorizedResult();
        //        }
        //        else
        //        {

        //            //let us take out the username now                
        //            FormsAuthenticationTicket authTicket;
        //            try
        //            {
        //                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        //            }
        //            catch
        //            {
        //                return;
        //            }
        //            string[] roles = authTicket.UserData.Split(';');

        //            if (filterContext.HttpContext.User != null)
        //                filterContext.HttpContext.User = new GenericPrincipal(filterContext.HttpContext.User.Identity, roles);

        //            filterContext.HttpContext.User = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity(authTicket.Name, "Forms"), authTicket.UserData.Split(';'));
        //            return;
        //        }


        //        //FormsAuthenticationTicket authTicket;
        //        //try
        //        //{
        //        //    authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        //        //}
        //        //catch
        //        //{
        //        //    return;
        //        //}
        //        //// retrieve roles from UserData
        //        //string[] roles = authTicket.UserData.Split(';');

        //        //if (Context.User != null)

        //        //    Context.User = new GenericPrincipal(Context.User.Identity, roles);
        //    }
        //    else
        //    {
        //        filterContext.Result = new HttpUnauthorizedResult();
        //    }
        //    var key = "TESTE";
        //    if (!IsValid(key))
        //    {
        //        // Unauthorized!
        //        filterContext.Result = new HttpUnauthorizedResult();
        //    }

        //}


        private bool IsValid(string key)
        {

            return true;
        }
    }
}