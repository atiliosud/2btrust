﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers
{
    public class AlertasHelpers
    {
        public string Descricao { get; set; }
        public string urlIcon { get; set; }
        public string Prioridade { get; set; }
        public string Valor { get; set; }
        public string corFundo { get; set; }
        public string tamanhoAlerta { get; set; }
        public string style { get; set; }
        public string proximaAtualizacao { get; set; }

        public string iconColor { get; set; }

        public string corHexa { get; set; }
    }
}