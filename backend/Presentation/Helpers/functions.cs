﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers
{
    public static class functions
    {

        public static string getColorFromBackground(string hex, string valor)
        {
            
                if(valor.Contains("Error"))
                {
                    return "red";
                }
         

            hex = hex.Replace("#", "");

            var r = Convert.ToUInt32(hex.Substring(0, 2), 16);
            var g = Convert.ToUInt32(hex.Substring(2, 2), 16);
            var b = Convert.ToUInt32(hex.Substring(4, 2), 16);
            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;

            return (yiq >= 128) ? "black" : "white";
        }
    }

}
