﻿using Presentation.Models;
using Presentation.Models._2BTrustModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Presentation.Helpers
{
    public static class SessionManager
    {
        public static PessoasModel usuarioLogado
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["usuario_logado_2bTrust"] == null)
                {
                    return null;
                }
                var usuarioLogado = HttpContext.Current.Session["usuario_logado_2bTrust"];
                if (usuarioLogado is PessoasModel)
                    return (PessoasModel)HttpContext.Current.Session["usuario_logado_2bTrust"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["usuario_logado_2bTrust"] = value;
            }
        }

        public static List<ObjetoContainerConteudoModel> ObjContainerConteudo
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["Container_Conteudo"] == null)
                {
                    return null;
                }
                var listaConteudo = HttpContext.Current.Session["Container_Conteudo"];
                if (listaConteudo is List<ObjetoContainerConteudoModel>)
                    return (List<ObjetoContainerConteudoModel>)HttpContext.Current.Session["Container_Conteudo"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["Container_Conteudo"] = value;
            }
        }

        public static List<PessoasDocumentosModel> ListaDocumentosFirstCad
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["ListaDocumentosFirstCad_2bTrust"] == null)
                {
                    return null;
                }
                var listaConteudo = HttpContext.Current.Session["ListaDocumentosFirstCad_2bTrust"];
                if (listaConteudo is List<PessoasDocumentosModel>)
                    return (List<PessoasDocumentosModel>)HttpContext.Current.Session["ListaDocumentosFirstCad_2bTrust"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["ListaDocumentosFirstCad_2bTrust"] = value;
            }
        }

        public static List<BensGastosModel> ListaBensGastos
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["ListaBensGastosModel_2bTrust"] == null)
                {
                    return null;
                }
                var listaConteudo = HttpContext.Current.Session["ListaBensGastosModel_2bTrust"];
                if (listaConteudo is List<BensGastosModel>)
                    return (List<BensGastosModel>)HttpContext.Current.Session["ListaBensGastosModel_2bTrust"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["ListaBensGastosModel_2bTrust"] = value;
            }
        }

        public static List<PessoasModel> ListaInvestidoresManuais
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["ListaInvestidoresManuais_2bTrust"] == null)
                {
                    return null;
                }
                var listaConteudo = HttpContext.Current.Session["ListaInvestidoresManuais_2bTrust"];
                if (listaConteudo is List<PessoasModel>)
                    return (List<PessoasModel>)HttpContext.Current.Session["ListaInvestidoresManuais_2bTrust"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["ListaInvestidoresManuais_2bTrust"] = value;
            }
        }


        public static List<BensImagensModel> ListaBensImagens
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["ListaBensImagens_2bTrust"] == null)
                {
                    return null;
                }
                var listaConteudo = HttpContext.Current.Session["ListaBensImagens_2bTrust"];
                if (listaConteudo is List<BensImagensModel>)
                    return (List<BensImagensModel>)HttpContext.Current.Session["ListaBensImagens_2bTrust"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["ListaBensImagens_2bTrust"] = value;
            }
        }

        public static List<BensVisaoGeralImagensModel> ListaBensImagensVisaoGeral
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["ListaBensImagensVisaoGeral_2bTrust"] == null)
                {
                    return null;
                }
                var listaConteudo = HttpContext.Current.Session["ListaBensImagensVisaoGeral_2bTrust"];
                if (listaConteudo is List<BensVisaoGeralImagensModel>)
                    return (List<BensVisaoGeralImagensModel>)HttpContext.Current.Session["ListaBensImagensVisaoGeral_2bTrust"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["ListaBensImagensVisaoGeral_2bTrust"] = value;
            }
        }



        public static List<MenuModel> ListaMenuIndex
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["ListaMenuIndex"] == null)
                {
                    return null;
                }
                var listaMenu = HttpContext.Current.Session["ListaMenuIndex"];
                if (listaMenu is List<MenuModel>)
                    return (List<MenuModel>)HttpContext.Current.Session["ListaMenuIndex"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["ListaMenuIndex"] = value;
            }
        }



        public static string SaveLogoPrincipal
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["SaveLogoPrincipal"] == null)
                {
                    return null;
                }
                var listaMenu = HttpContext.Current.Session["SaveLogoPrincipal"];
                if (listaMenu is string)
                    return (string)HttpContext.Current.Session["SaveLogoPrincipal"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["SaveLogoPrincipal"] = value;
            }
        }
        public static string SaveLogoMaior
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["SaveLogoMaior"] == null)
                {
                    return null;
                }
                var listaMenu = HttpContext.Current.Session["SaveLogoMaior"];
                if (listaMenu is string)
                    return (string)HttpContext.Current.Session["SaveLogoMaior"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["SaveLogoMaior"] = value;
            }
        }
        public static string SaveLogoMenor
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Session == null || HttpContext.Current.Session["SaveLogoMenor"] == null)
                {
                    return null;
                }
                var listaMenu = HttpContext.Current.Session["SaveLogoMenor"];
                if (listaMenu is string)
                    return (string)HttpContext.Current.Session["SaveLogoMenor"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["SaveLogoMenor"] = value;
            }
        }
        public static void logOut()
        {

            FormsAuthentication.SignOut();
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Session.Abandon();

        }

    }
}