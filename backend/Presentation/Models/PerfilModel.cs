﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class PerfilModel
    {
        public int idPerfil { get; set; }
        public string Descricao { get; set; }
        public string Observacao { get; set; }
        public bool FlagAtivo { get; set; }
        public int idUsuarioCadastro { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }

        public IEnumerable<Perfil_X_MenuModel> PerfilXMenu { get; set; }

    }
}