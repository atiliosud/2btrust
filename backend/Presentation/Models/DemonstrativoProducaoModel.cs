﻿using Presentation.Models.Vortice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class DemonstrativoProducaoModel
    {
        public TransacaoModel _Transacao { get; set; }
        public LimeModel _Lime { get; set; }
        public ContaCorrenteModel _ContaCorrente { get; set; }
        public CestaServicoModel _CestaServico { get; set; }
        public CartaoCreditoModel _CartaoCredito { get; set; }

        public bool isInit { get; set; }
        public int idLoja { get; set; }
        public int iAno { get; set; }
        public int iMes { get; set; }
        public string sMes { get; set; }

    }
}