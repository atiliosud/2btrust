﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class IntegracaoIconeModel
    { 
        public int Id { get; set; }
        public int Descricao { get; set; }
        public int Html { get; set; }
        public int FlagAtivo { get; set; }

    }
}