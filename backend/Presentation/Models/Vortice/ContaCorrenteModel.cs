﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models.Vortice
{
   public class ContaCorrenteModel
    {
       const int iFixo = 450;

        public int Codigo { get; set; }
        public string Quiosque { get; set; }
        public int QtdTotal { get; set; }
        public int QtdAposentados { get; set; }
        public int QtdItinerante { get; set; } 
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int MesInicio { get; set; }
        public int AnoInicio { get; set; }
        public int MesReferencia { get; set; }

        public int MesInicioItinerante { get; set; }
        public int AnoInicioItinerante { get; set; }
        public int MesInicioAposentado { get; set; }
        public int AnoInicioAposentado { get; set; }

        public int QtdTotalHistorico { get; set; }
        public int QtdAposentadosHistorico { get; set; }
        public int QtdItineranteHistorico { get; set; }

       public int getPorcentagem(int iTotal, int iMaior)
       {
          // decimal percent = ((decimal) iTotal/this.QtdTotal);
           decimal percent = ((decimal)iTotal / iMaior);
           percent = percent * 100;
           if (percent > 0  && percent<1)
           {

               return 1;
           }

           return(int) percent;
       }


       public int getHeight(int porcent)
       {  
           int iHeight = iFixo * porcent;
           iHeight = iHeight / 100;

           return iHeight;

       }

       public int getTop(int iHeight)
       {
           int iTop = iFixo - iHeight;

           return iTop;
       }



    }
}
