﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models.Vortice
{
   public class CestaServicoModel
    {
        public int Codigo { get; set; }
        public string Quiosque { get; set; } 
        public int QtdTotalCesta5 { get; set; }
        public int QtdTotalCesta6 { get; set; }
        public int MesReferencia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int MesInicio { get; set; }
        public int AnoInicio { get; set; }


        public int QtdTotalHistorico { get; set; }

       public int totalCesta()
        {
            return this.QtdTotalCesta5 + this.QtdTotalCesta6;
        }

    }
}
