﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models.Vortice
{
   public class CartaoCreditoModel
    {
        public int Codigo { get; set; }
        public string Quiosque { get; set; }
        public int QtdTotal { get; set; }
        public int QtdInss { get; set; }
        public int MesReferencia { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int MesInicio { get; set; }
        public int AnoInicio { get; set; }


        public int QtdTotalHistorico { get; set; }
    }
}
