﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models.Vortice
{
    public class LojaModel
    {
        public int id { get; set; }
       
        public string sNome { get; set; }
        
        public int iCodigo { get; set; }
        
        public string sNomeAgencia { get; set; }
        
        public string sCodigoAgencia { get; set; }
        
        public string sEmailAgencia { get; set; }

        public DateTime dtInicioOperacao { get; set; }
    }
}