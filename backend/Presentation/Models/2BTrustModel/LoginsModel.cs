﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
   public class LoginsModel

{

    public int IdUsuario { get; set; }



    public string Login { get; set; }



    public string UltimaSenha { get; set; }
    public string Senha { get; set; }

    public string ReSenha { get; set; }



    public bool Bloqueado { get; set; }



    public byte Tentativas { get; set; }



    public int IdUsuarioInclusao { get; set; }



    public DateTime DtInclusao { get; set; }



    public int? IdUsuarioAlteracao { get; set; }



    public DateTime? DtAlteracao { get; set; }



}

}
