﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class CidadesModel
    {

        public int Id { get; set; }



        public int IdEstado { get; set; }
        public EstadosModel _Estado { get; set; }


        public string Nome { get; set; }



        public string CodigoIBGE { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }



    }

}
