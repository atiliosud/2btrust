﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
 public class  ContratosModel

{

    public int Id { get; set; }



    public string Codigo { get; set; }



    public string Descricao { get; set; }



    public DateTime DtInicio { get; set; }



    public DateTime? DtVigencia { get; set; }



    public int IdStatus { get; set; }



    public int IdUsuarioInclusao { get; set; }



    public DateTime DtInclusao { get; set; }



    public int? IdUsuarioAlteracao { get; set; }



    public DateTime? DtAlteracao { get; set; }



}

}
