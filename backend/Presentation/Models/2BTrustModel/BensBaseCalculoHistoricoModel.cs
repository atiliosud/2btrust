﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class BensBaseCalculoHistoricoModel
    {
        public int Id { get; set; }
        public int IdBem { get; set; }
        public int iMes { get; set; }
        public int iAno { get; set; }
        public string Rent { get; set; }
        public string Hoa { get; set; }
        public string PropertyTaxProvision { get; set; }
        public string PropertyManagement { get; set; }
        public string AdministrationFee { get; set; }
        public string DividendYeld { get; set; }
        public string OutrasDespesas { get; set; }
        public string ObservacaoOutras { get; set; }
        public string percentRentabilidade { get; set; }
        public string performance { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public int? IdUsuarioAlteracao { get; set; }
        public DateTime? DtAlteracao { get; set; }
    }
}