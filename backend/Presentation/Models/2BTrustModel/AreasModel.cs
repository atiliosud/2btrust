﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
  public class AreasModel

{

    public int Id { get; set; }



    [Required(ErrorMessage = "Campo Obrigatório")]
    public string Codigo { get; set; }


    [Required(ErrorMessage = "Campo Obrigatório")] 
    public string Nome { get; set; }



    public int IdUsuarioInclusao { get; set; }



    public DateTime DtInclusao { get; set; }


    public bool FlgAtivo { get; set; }

    public int? IdUsuarioAlteracao { get; set; }



    public DateTime? DtAlteracao { get; set; }



}

}
