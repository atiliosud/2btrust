﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class PessoasFisicasModel

    {

        public int IdPessoa { get; set; }



        public string CPF { get; set; }

        public string TaxId { get; set; }

        public string SocialNumber { get; set; }

        public string ITIN { get; set; }

        public int EmploymentStatus { get; set; }
        public string Employer { get; set; }
        public string NatureOfBusiness { get; set; }
        public string Ocupation { get; set; }


        public string Apelido { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }



    }

}
