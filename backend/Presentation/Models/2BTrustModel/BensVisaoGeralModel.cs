﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class BensVisaoGeralModel
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Descritivo { get; set; }

        public int IdUsuarioInclusao { get; set; }
        public int IdStatus { get; set; }

        public DateTime DtInclusao { get; set; }

        public int? IdUsuarioAlteracao { get; set; }

        public DateTime? DtAlteracao { get; set; }

        public IEnumerable<BensVisaoGeralImagensModel> _Imagens { get; set; }
    }

    public class BensVisaoGeralImagensModel
    {
        public int Id { get; set; }
        public int IdBemVisaoGeral { get; set; }
        public string PathLocation { get; set; }
        public string PathLocationIIS { get; set; }
        public bool FlagAtivo { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public int IdUsuarioAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string Descritivo { get; set; }
    }
}
