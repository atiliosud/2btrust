﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class GruposModel
    {

        public int Id { get; set; }



        public string Codigo { get; set; }



        public string Nome { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public IEnumerable<GrupoXActionsModel> _GruposActions { get; set; }

    }


    public class GrupoXActionsModel
    {
        public int IdArea { get; set; }
        public string NomeAction { get; set; }
        public int IdAcao { get; set; }
        public string NomeControle { get; set; }
        public string NomeArea { get; set; }
        public int IdControle { get; set; }
        public int Exist { get; set; }
    }

}
