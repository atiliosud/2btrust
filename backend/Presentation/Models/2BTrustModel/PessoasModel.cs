﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class PessoasModel
    {

        public int Id { get; set; }

        public IEnumerable<LancamentosModel> _MeusLancamentos { get; set; } 


        public string Nome { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DtNascimento { get; set; }


        public int IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }


        public bool FlagResidenteAmericado { get; set; }

        public int? IdUsuarioAlteracao { get; set; }


        public string TelefoneCelular { get; set; }
        public string TelefoneComercial { get; set; }
        public string ValorInicial { get; set; }
        public string ValorInicialOther { get; set; }
        public bool fCondicao1 { get; set; }
        public bool fCondicao2 { get; set; }
        public bool fCondicao3 { get; set; }
        public bool fCondicao4 { get; set; }
        public bool fCondicao5 { get; set; }
        public bool fCondicao6 { get; set; }

        public bool fConfirmoDados { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public UsuariosModel _Usuario { get; set; }

        public PessoasEnderecosModel _Endereco { get; set; }

        public PessoasFisicasModel _PessoaFisica { get; set; }

        public PessoasJuridicaModel _PessoaJuridica { get; set; }

        public IEnumerable<PessoasDocumentosModel> _Documentos { get; set; }

        public PessoasDocumentosModel objDocumento { get; set; }

        public CarteiraModel _Carteira { get; set; }

        public IEnumerable<PoolBensPessoasModel> _MeusPools { get; set; }

        public PoolBensPessoasModel _ObjMeusPools { get; set; }

        public IEnumerable<PainelRentabilidadeMesModel> _ListaRentabilidade { get; set; }

        public InformacoesAdminHomeModel _InformacoesAdminHome { get; set; }
    }

}
