﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class ValidTokenModel
    {
        public int Id { get; set; }

        public int IdUsuario { get; set; }

        public int Device { get; set; }

        public string HashValid { get; set; }

        public DateTime dtAcesso { get; set; }

        public bool FlagAtivo { get; set; }

    }
}