﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class UsuariosModel
    {

        public int Id { get; set; }


        public int IdPessoa { get; set; }

        public string Nome { get; set; }



        public string Email { get; set; }



        public bool Admin { get; set; }



        public bool Bloqueado { get; set; }



        public string Saltkey { get; set; }



        public int? IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }


        public bool? CadastroValido { get; set; }

        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public LoginsModel _Login { get; set; }

        public GruposModel _Grupo { get; set; }

        public UsuariosGruposModel _UsuarioGrupo { get; set; }

        public IEnumerable<ControlesModel> _Controles { get; set; }

    }

}
