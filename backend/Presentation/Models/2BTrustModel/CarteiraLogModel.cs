﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class CarteiraLogModel

    {

        public int Id { get; set; }



        public int? IdPessoa { get; set; }



        public DateTime? Data { get; set; }



        public string Descricao { get; set; }



        public string Tipo { get; set; }



        public decimal? Amount { get; set; }



        public decimal? AvaliableBalance { get; set; }



        public bool? FlagAtivo { get; set; }



    }

}