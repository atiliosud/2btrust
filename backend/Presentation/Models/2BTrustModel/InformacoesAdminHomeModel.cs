﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class InformacoesAdminHomeModel
    {
        public decimal SaldoTotalClienteEmCarteira { get; set; }
        public decimal SaldoTotalClienteEmInvestimentos_e_Carteira { get; set; }
        public decimal SaldoTotalClienteEmInvestimentos { get; set; }
    }
}