﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
  public class RequisicoesModel

{

    public Guid Id { get; set; }



    public DateTime DtRequisicao { get; set; }



    public string CdArea { get; set; }



    public string CdControle { get; set; }



    public string CdAcao { get; set; }



    public Guid AccessToken { get; set; }



    public string Header { get; set; }



    public string Body { get; set; }



    public string QueryString { get; set; }



    public string Verbo { get; set; }



    public string IP { get; set; }



}

}
