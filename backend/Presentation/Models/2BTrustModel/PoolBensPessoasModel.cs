﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{

    public class PoolBensPessoasModel
    {

        public int Id { get; set; }



        public int IdPoolBem { get; set; }



        public int IdPessoa { get; set; }



        public decimal Cota { get; set; }



        public int IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }


        public PoolBensModel _PoolBens { get; set; }

    }

}
