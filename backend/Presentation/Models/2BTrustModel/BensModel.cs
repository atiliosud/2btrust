﻿using Domain.Entities._2BTrust;
using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class BensModel
    {
        public string sMesAno { get; set; }


        public int Id { get; set; }



        public int IdPool { get; set; }



        public string Nome { get; set; }
        public string Descritivo { get; set; }


        [DataType(DataType.Currency)]
        public string CapitalInvestido { get; set; }

        [DataType(DataType.Currency)]
        public string Custos { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DtReferenciaCusto { get; set; }

        public string DescricaoGasto { get; set; }

        public bool fComprovante { get; set; }



        [DataType(DataType.Currency)]
        public string Rent { get; set; }

        [DataType(DataType.Currency)]
        public string Hoa { get; set; }

        [DataType(DataType.Currency)]
        public string PropertyTaxProvision { get; set; }

        [DataType(DataType.Currency)]
        public string PropertyManagement { get; set; }

        [DataType(DataType.Currency)]
        public string AdministrationFee { get; set; }

        [DataType(DataType.Currency)]
        public string DividendYeld { get; set; }

        public string percentRentabilidade { get; set; }
        public string performance { get; set; }

        public int iAno { get; set; }

        public int iMes { get; set; }


        public int IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }



        public DateTime? DtAlteracao { get; set; }

        public PoolBensModel _Pool { get; set; }
        public BensEnderecosModel _Endereco { get; set; }
        public IEnumerable<BensImagensModel> _Imagens { get; set; }

        public LucratividadeModel _Lucratividade { get; set; }
        public RentabilidadeModel _Rentabilidade { get; set; }

        public IEnumerable<BensGastosModel> _BensGastos { get; set; }
        public IEnumerable<BensBaseCalculoHistoricoModel> _BensBaseCalculoHistorico { get; set; }

        

        [DataType(DataType.Currency)]
        public string OutrasDespesas { get; set; }
        public string ObservacaoOutras { get; set; }


        public EnumTipoBem TipoBem { get; set; }
        public BensAcoes _BensAcao { get; set; }
        public IEnumerable<BensAcoes> _BensAcoes { get; set; }
    }
    public class BensImagensModel
    {
        public int Id { get; set; }
        public int IdBem { get; set; }
        public string PathLocation { get; set; }
        public string PathLocationIIS { get; set; }
        public bool FlagAtivo { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtInclusao { get; set; }
        public int IdUsuarioAlteracao { get; set; }
        public DateTime DtAlteracao { get; set; }


    }
}
