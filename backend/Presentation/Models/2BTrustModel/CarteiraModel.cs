﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class CarteiraModel
    {
        public int Id { get; set; }
        public int IdPessoa { get; set; }
        public decimal Saldo { get; set; }
        public DateTime DtInclusao { get; set; }
        public int IdUsuarioInclusao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int IdUsuarioAlteracao { get; set; }


        public IEnumerable<ComprovanteDepositoModel> _Comprovantes { get; set; }
    }
}