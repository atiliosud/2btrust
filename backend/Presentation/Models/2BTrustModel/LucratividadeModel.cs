﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
public class LucratividadeModel

{

    public int? Id { get; set; }



    public int IdBem { get; set; }



    public DateTime? DtInicio { get; set; }



    public DateTime? DtFim { get; set; }



    public string LucroLiquido { get; set; }



    public string ReceitaBruta { get; set; }



    public string Valor { get; set; }



    public int IdUsuarioInclusao { get; set; }



    public DateTime DtInclusao { get; set; }



    public int? IdUsuarioAlteracao { get; set; }



    public DateTime? DtAlteracao { get; set; }



}

}
