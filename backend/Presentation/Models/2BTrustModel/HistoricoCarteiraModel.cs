﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class HistoricoCarteiraModel
    {
        public int Id { get; set; }
        public string Valores { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
    }
}