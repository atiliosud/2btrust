﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class PoolsBensRentabilidadePerformanceModel
    {
        public int Id { get; set; }
        public int IdPoolBens { get; set; }
        public int iAno { get; set; }
        public int iMes { get; set; }
        public decimal mRentabilidade { get; set; }
        public decimal mPerformance { get; set; }
        public bool flgAtivo { get; set; }
        public DateTime dtInclusao { get; set; }
        public int idUsuarioinclusao { get; set; }
    }
}