﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.Models._2BTrustModel
{
    public class PoolBensModel
    {

        public int Id { get; set; }

        public int Cotas { get; set; }

        public int CotasDisponiveis { get; set; }


        public int IdContrato { get; set; }



        public string Nome { get; set; }



        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DtInicio { get; set; }

        public DateTime? DtInclusaoAdm { get; set; }


        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DtVigencia { get; set; }



        public int IdStatus { get; set; }



        public int IdUsuarioInclusao { get; set; }



        public DateTime DtInclusao { get; set; }



        public int? IdUsuarioAlteracao { get; set; }

        public string PathLocationIIS { get; set; }
        public string PathLocationIISLogoMaior { get; set; }
        public string PathLocationIISLogoMenor { get; set; }

        public int iUsarLogoApp { get; set; }


        public DateTime? DtAlteracao { get; set; }

        public ContratosModel _Contrato { get; set; }

        public IEnumerable<PoolsBensRentabilidadePerformanceModel> _PoolsBensRentabilidadePerformance { get; set; }

        public List<BensModel> _Bens { get; set; }

    }

}
