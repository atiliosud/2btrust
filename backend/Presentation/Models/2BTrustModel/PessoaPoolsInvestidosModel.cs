﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models._2BTrustModel
{
    public class PessoaPoolsInvestidosModel
    {
        public int Id { get; set; }
        public int IdPoolBem { get; set; }
        public int IdPessoa { get; set; }
        public int MyProperty { get; set; }
        public string NomePool { get; set; }
        public string NomeCliente { get; set; }
        public int Cotas { get; set; }
        public DateTime? UltimoLancamento { get; set; }

        public IEnumerable<LancamentosModel> _Lancamentos { get; set; }
    }
}