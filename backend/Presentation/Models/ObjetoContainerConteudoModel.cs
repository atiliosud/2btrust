﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class ObjetoContainerConteudoModel
    {

        public int idContainerConteudo { get; set; }

        public DataSourceObjetoModel dataSourceObjeto { get; set; }

        public int idContainer { get; set; }



        [Display(Name = "Data Source")] 
        public int? idDataSource { get; set; }

        [Display(Name = "Data Source")]
        public int idDataSource2 { get; set; }

        [Display(Name = "Data Source")]
        public int idDataSource3 { get; set; }



        [Display(Name = "Tipo Consulta")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public EnumTipoQuery iTipoConsulta { get; set; }

        [Display(Name = "Tipo Conteúdo")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int iTipoConteudo { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string descricao { get; set; }



        [Display(Name = "Campo Retorno")] 
        public string campoRetorno { get; set; }

        [Display(Name = "Campo Retorno")]  
        public List<string> campoRetornoLi { get; set; }


        [Display(Name = "Campo Descrição")]
        public string campoDescricao{ get; set; }




        [Display(Name = "Campo Retorno")]
        public string campoRetorno2 { get; set; }

        [Display(Name = "Campo Retorno")]
        public string campoRetorno3 { get; set; }





        [Display(Name = "Cor Fundo")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string CorFundo { get; set; }

        [Display(Name = "Icon")]
        public string icon { get; set; }

        [Display(Name = "Cor Icon")]
        public string iconColor { get; set; }



        [Display(Name = "Mês Consulta")]
        public EnumMeses iMesConsulta { get; set; }

        [Display(Name = "Mês Consulta")]
        public EnumMeses iMesConsulta2 { get; set; }

        [Display(Name = "Mês Consulta")]
        public EnumMeses iMesConsulta3 { get; set; }




        [Display(Name = "Tipo Gráfico")]
        public EnumTipoGrafico iTipoGrafico { get; set; }



        [Display(Name = "Período")]
        public EnumPeriodoGrafico iPeriodoGrafico { get; set; }

        [Display(Name = "Período")]
        public EnumPeriodoGrafico iPeriodoGrafico2 { get; set; }

        [Display(Name = "Período")]
        public EnumPeriodoGrafico iPeriodoGrafico3 { get; set; } 



        public int iOrderm { get; set; }

        public int idUsuarioCadastro { get; set; }
      
        public DateTime dataCadastro { get; set; }
       
        public int idUsuarioAlteracao { get; set; }

        public DateTime dataAlteracao { get; set; }

        public bool flagAtivo { get; set; }
         
        public EnumTipoConteudoContainer tipoConteudoContainer { get; set; }


        public List<ObjetoContainerConteudoLabelModel> ListaLabels { get; set; }


        //lista de filtros
        public List<IntegracaoFiltroDinamicoModel> ListaFiltro { get; set; }

        [Display(Name = "Filtrar por:")]
        public string CampoFiltro{ get; set; }

        [Display(Name = "Variável:")]
        public EnumVariavelValorFiltro VariavelFiltro { get; set; }

        [Display(Name = "Valor:")]
        public string valorVariavelFiltro { get; set; } 

        [Display(Name = "Operador: ")]
        public EnumOperadorFiltro OperadorFiltro { get; set; }

    }
}