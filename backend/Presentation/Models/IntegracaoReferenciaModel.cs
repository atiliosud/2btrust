﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class IntegracaoReferenciaModel
    {
        public int id { get; set; }
        public string NOME_TABELA_PRINCIPAL { get; set; }
        public string CAMPO_TABELA_PRINCIPAL { get; set; }
        public string NOME_TABELA_REFERENCIA { get; set; }
        public string CAMPO_TABELA_REFERENCIA { get; set; }

    }
}