﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class ObjetoContainerModel
    {
        public int idContainer { get; set; }

        [Display(Name = "Menu")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int idMenu { get; set; }

        [Display(Name="Título")]
        [Required(ErrorMessage="Campo Obrigatório!")]
        public string titulo { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string descricao { get; set; }

        [Display(Name = "Tamanho")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int iTamanho { get; set; }

        [Display(Name = "Template")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int iTemplate { get; set; }


        [Display(Name = "Cor Fundo Título")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string CorFundoTitulo { get; set; }




        public int iPosicaoBD { get; set; }
        public int iPosicao { get; set; }
        public bool flagAtivo { get; set; }
        public DateTime dataCadastro { get; set; }
        public int idUsuarioCadastro { get; set; }
        public DateTime dataAlteracao { get; set; }
        public int idUsuarioAlteracao { get; set; }

        public MenuModel MenuContainer { get; set; }

        public IEnumerable<ObjetoContainerConteudoModel> ListaConteudo { get; set; }
    }
}