﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class ObjetoAlertaModel
    {
        public int idObjetoAlerta { get; set; }

        [Display(Name = "Data Source")]
        [Required(ErrorMessage="Campo Obrigatório!")]
        public int idDataSourceObjeto { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string descricao { get; set; }

        [Display(Name = "Icone")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string icon { get; set; }

        [Display(Name = "Cor Icone")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string iconColor { get; set; }

        [Display(Name = "Prioridade")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public EnumPrioridadeAlerta tipoPrioridade { get; set; }

        [Display(Name = "Valor Mínimo")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int valorMin { get; set; }

        [Display(Name = "Valor Máximo")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int valorMax { get; set; }

        [Display(Name = "Obaservação")] 
        public string observacao { get; set; }

        [Display(Name = "Campo Exibição")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string campoRetorno { get; set; }  
        
        public bool flagAtivo { get; set; }
        
        public DateTime dataCadastro { get; set; }
        
        public int idUsuarioCadastro { get; set; }
        
        public DateTime dataAlteracao { get; set; }
        
        public int idUsuarioAlteracao { get; set; }
        
        public int iTamanhoAlerta { get; set; }

        public DataSourceObjetoModel dataSourceObjeto { get; set; }

        [Display(Name = "Cor Valor Mínimo")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string corValorMin { get; set; }

        [Display(Name = "Cor Valor Intermediário")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string corValorIntermediario{ get; set; }

        [Display(Name = "Cor Valor Máximo")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string corValorMax { get; set; }
    }
}