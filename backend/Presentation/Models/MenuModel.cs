﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class MenuModel
    {
        [Display(Name = "Menu")]
        public int idMenu { get; set; }

        [Display(Name = "Escolha o Menu Pai")]
        public int idMenuPai { get; set; }

        [Display(Name = "Descrição")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string Descricao { get; set; }


        [Display(Name = "Observação")] 
        public string Observacao { get; set; }


        [Display(Name = "Ativo")] 
        public bool FlagAtivo { get; set; }


        public int idUsuarioCadastro { get; set; }


        public DateTime DataCadastro { get; set; }


        public DateTime DataAlteracao { get; set; }


        public int idUsuarioAlteracao { get; set; }

        [Display(Name = "Classe CSS")]
        public string classCss { get; set; }


        [Display(Name = "ID Nome Pai")] 
        public string idJqueryPai { get; set; }


        [Display(Name = "ID Nome Filho")] 
        public string idJqueryFilho { get; set; }


        [Display(Name = "Icon FontAwesome")] 
        public string iconLI { get; set; }


        [Display(Name = "URL Controller")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string urlController { get; set; }


        [Display(Name = "Nome Controller")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string Controller { get; set; }


        public MenuModel MenuPai { get; set; }


        [Display(Name = "Menu Pai?")] 
        public bool isMenuPai { get; set; }
    }


}



