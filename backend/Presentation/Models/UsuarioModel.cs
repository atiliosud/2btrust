﻿using Presentation.Models.EYE;
using Presentation.Models.Vortice;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class UsuarioModel
    {
        public HttpPostedFileBase MyFile { get; set; }

        public int id { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string Nome { get; set; }

        [Display(Name = "Login")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string Login { get; set; }

        [Display(Name = "Senha")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string Password { get; set; }

        public DateTime DtInclusao { get; set; }

        public DateTime DtAlteracao { get; set; }

        [Display(Name = "Perfil")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int idPerfil { get; set; }

        public bool FlagDeletado { get; set; }

        [Display(Name = "Foto de Perfil")]
        public string pathFoto { get; set; }


        [Display(Name = "Email")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string email { get; set; }


        [Display(Name = "CPF")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string CPF { get; set; }

        public PerfilModel perfil { get; set; }

        public EYE_ProfissionaisModel _profissional { get; set; }

        public EYE_QuiosquesModel _quiosque { get; set; }

        public LojaModel _dadosLoja { get; set; }

    }
}