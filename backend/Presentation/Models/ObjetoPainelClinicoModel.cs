﻿using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class ObjetoPainelClinicoModel
    {

        public int idPainelClinico { get; set; }

        [Display(Name = "Data Source")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int idDataSource { get; set; }

        public DataSourceObjetoModel DataSource { get; set; }





        [Display(Name = "Campo(s) de Retorno")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public List<string> sCampoRetornoPrimeiro { get; set; }



        [Display(Name = "Campo(s) de Retorno")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public List<string> sCampoRetornoSegundo { get; set; }


        public  string  sCampoPrimeiro { get; set; }
                       
        public  string  sCampoSegundo { get; set; }




        [Display(Name = "Tipo Consulta")] 
        [Range(1, int.MaxValue,ErrorMessage="Campo Obrigatório!")]
        public EnumTipoQuery iTipoConsultaPrimeiro { get; set; }

        [Display(Name = "Tipo Consulta")]
        [Range(1, int.MaxValue, ErrorMessage = "Campo Obrigatório!")]
        public EnumTipoQuery iTipoConsultaSegundo { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string TituloPrimeiro { get; set; }

        [Display(Name = "Título")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string TituloSegundo { get; set; }

        [Display(Name = "Sub Título")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string SubTituloPrimeiro { get; set; }

        [Display(Name = "Sub Título")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string SubTituloSegundo { get; set; }

        [Display(Name = "Cor Fundo Gráfico")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string CorFundoPrimeiro { get; set; }

        [Display(Name = "Cor Fundo Gráfico")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string CorFundoSegundo { get; set; }

        [Display(Name = "Cor Barra Gráfico")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string CorBarraPrimeiro { get; set; }

        [Display(Name = "Cor Barra Gráfico")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public string CorBarraSegundo { get; set; }

        [Display(Name = "Filtrar por:")]
        public string CampoFiltroPrimeiro { get; set; }

        [Display(Name = "Variável:")]
        public EnumVariavelValorFiltro VariavelFiltro { get; set; }

        [Display(Name = "Valor:")]
        public string valorVariavelFiltro { get; set; }

        [Display(Name = "Filtrar por:")]
        public string CampoFiltroSegundo { get; set; }

        [Display(Name = "Operador: ")]
        public EnumOperadorFiltro OperadorFiltro { get; set; }

        public bool FlagAtivo { get; set; }

        public int idUsuarioCadastro { get; set; }

        public DateTime DataCadastro { get; set; }

        public int idUsuarioAlteracao { get; set; }

        public DateTime DataAlteracao { get; set; }

        [Display(Name = "Meta")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int Meta1 { get; set; }

        [Display(Name = "Meta")]
        [Required(ErrorMessage = "Campo Obrigatório!")]
        public int Meta2 { get; set; }

        public ContentGrafico ConteudoGrafico { get; set; }



        //lista de filtros
        public List<IntegracaoFiltroDinamicoModel> ListaFiltro01{ get; set; }
        public List<IntegracaoFiltroDinamicoModel> ListaFiltro02 { get; set; }

    }




    public class ContentGrafico
    {
        public int Valor_1 { get; set; }
        public int Procentagem_1 { get; set; }
        public int Valor_2 { get; set; }
        public int Procentagem_2 { get; set; }
    }
}