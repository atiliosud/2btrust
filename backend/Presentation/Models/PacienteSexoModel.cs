﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class PacienteSexoModel
    {
        public int iMasculino { get; set; }
        public int iFeminino { get; set; }
        public int iIndefinido { get; set; }
        public int iNenhum { get; set; }

        public decimal iPorMasculino { get; set; }
        public decimal iPorFeminino { get; set; }
        public decimal iPorIndefinido { get; set; }
        public decimal iPorNenhum { get; set; }

    }
}