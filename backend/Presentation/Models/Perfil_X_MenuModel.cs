﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class Perfil_X_MenuModel
    {
        public int idPerfilMenu { get; set; }
        public int idMenu { get; set; }
        public int idPerfil { get; set; }
        public bool FlagAtivo { get; set; }
        public int idUsuarioCadastro { get; set; }
        public int idUsuarioAlteracao { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataAlteracao { get; set; }

        public MenuModel menu { get; set; }

    }
}