﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models.EYE
{

    public class EYE_QuiosquesModel
    { 
        public int QUIOSQUE_ID { get; set; }
        public string QUIOSQUE_NOME { get; set; }
        public string QUIOSQUE_FLAGDELETADO { get; set; }


    }
}