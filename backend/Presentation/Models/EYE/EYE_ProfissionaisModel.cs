﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models.EYE
{
    public class EYE_ProfissionaisModel
    {

        public int PROFISSIONAL_ID { get; set; }
        public string PROFISSIONAL_NOME { get; set; }
        public string PROFISSIONAL_CPF { get; set; }
        public string PROFISSIONAL_STATUS { get; set; }
        public string PROFISSIONAL_SIGLACONCELHO { get; set; }
        public string PROFISSIONAL_IDCLINICA { get; set; }

    }
}