﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models
{
    public class DataSourceObjetoModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Acao { get; set; }
        public int Tipo { get; set; }
        public string Comando { get; set; }
        public DateTime DtInclusao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public int IntegracaoDataSourceId { get; set; }
        public string Descricao { get; set; }
        public string CampoConsultaAlerta { get; set; }
        public string CampoBetweenData { get; set; }
        public string UrlConnection { get; set; }
        public int IdTipoRetorno { get; set; }

        public AgendamentoDataSourceModel agendamentoDS { get; set; }
    }

    public class AgendamentoDataSourceModel
    {

        public int Id { get; set; }
        public int IntegracaoDataSourceObjetoId { get; set; }
        public TimeSpan Intervalo { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public TimeSpan HoraInicio { get; set; }
        public TimeSpan HoraFim { get; set; }
        public bool Habilitado { get; set; }
        public DateTime DtInclusao { get; set; }
        public DateTime DtAlteracao { get; set; }
        public DateTime UltimaExecucao { get; set; }

    }
}