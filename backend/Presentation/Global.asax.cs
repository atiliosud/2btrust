﻿using Domain.Entities._2BTrust;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Mappers;
using Presentation.Models._2BTrustModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Presentation
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.RegisterMappings();
        }

        protected void Application_BeginRequest(Object source, EventArgs e)
        {

            Response.AddHeader("Cache-Control", "max-age=0,no-cache,no-store,must-revalidate");
            Response.AddHeader("Pragma", "no-cache");

            AtualizaDados();
        }



        public string urlWebAPI = System.Configuration.ConfigurationManager.AppSettings["urlWebApiApontadores"];
        public async Task<IRestResponse> PostRestAsync<T>(string ComplementoUrl)
        {
            var client = new RestClient(urlWebAPI + ComplementoUrl);

            var request = new RestRequest(Method.POST);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("accept", "application/json");
            request.AddHeader("authorization", "Basic QWNjZXNzSW5kaWNhZG9yZXM6RDRuMTNsNCQkJA==");

            var taskCompletionSource = new TaskCompletionSource<IRestResponse<T>>();
            EventWaitHandle executedCallBack = new AutoResetEvent(false);
            TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();
            IRestResponse res = new RestResponse();

            client.ExecuteAsync<RestResponse>(request, response =>
            {
                res = response;
                tcs.TrySetResult(res);
                executedCallBack.Set();
            });

            return await tcs.Task;

        }


        protected  async Task AtualizaDados()
        {
            if (SessionManager.usuarioLogado!=null)
            {
                IRestResponse response = await PostRestAsync<Pessoas>("/Pessoa/AtualizaDadosPessoa?idPessoa=" + SessionManager.usuarioLogado.Id);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var model = JsonConvert.DeserializeObject<PessoasModel>(response.Content);
                    if (model._MeusPools != null)
                        SessionManager.usuarioLogado._MeusPools = model._MeusPools;
                    if (model._Carteira != null)
                        SessionManager.usuarioLogado._Carteira = model._Carteira;


                }
            }
            
        }
          
    }
}
