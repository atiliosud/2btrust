﻿$(document).ready(function () {

    console.log("bens html js!");

    var tipoBem = $('#ddlTipoBem option:selected').text();
    console.log(tipoBem);

    if (tipoBem == 'Propriedade') {
        $("#divEndereco").show();
        $("#divAcao").hide();
    }
    else {
        $("#divEndereco").hide();
        $("#divAcao").show();
    };

    $('#ddlTipoBem').change(function () {
        var ddlValue = $(this).val();
        if (ddlValue == "0") //propriedade
        {
            $("#divEndereco").show();
            $("#divAcao").hide();
        }
        else if (ddlValue == "1") //acao
        {
            $("#divEndereco").hide();
            $("#divAcao").show();
        }
    });

    $('#capitalInv').maskMoney({ allowNegative: false, thousands: '.', decimal: ',' });

    $('#capitalInv').change(function () {
        console.log("Calcular Valor Investido");
        var valorCotacaoAtual = parseFloat($("#cotacaoAtual").val()) || 0;
        var qtdeCotas = parseInt($('#qtdeCotas').val()) || 0;
        console.log(valorCotacaoAtual * qtdeCotas);
        if ((valorCotacaoAtual > 0) && (qtdeCotas > 0))
            $('#capitalInv').val(parseFloat(valorCotacaoAtual * qtdeCotas).toFixed(2));
    });

    $('#cotacaoAtual').change(function () {
        console.log("Calcular Valor Investido");
        var valorCotacaoAtual = parseFloat($(this).val()) || 0;
        var qtdeCotas = parseInt($('#qtdeCotas').val()) || 0;
        console.log(valorCotacaoAtual * qtdeCotas);
        if ((valorCotacaoAtual > 0) && (qtdeCotas > 0))
            $('#capitalInv').val(parseFloat(valorCotacaoAtual * qtdeCotas).toFixed(2));
    });

    $('#qtdeCotas').change(function () {
        console.log("Calcular Valor Investido");
        var valorCotacaoAtual = parseFloat($("#cotacaoAtual").val()) || 0;
        var qtdeCotas = parseInt($(this).val()) || 0;
        console.log(valorCotacaoAtual * qtdeCotas);
        if ((valorCotacaoAtual > 0) && (qtdeCotas > 0))
            $('#capitalInv').val(parseFloat(valorCotacaoAtual * qtdeCotas).toFixed(2));
    });
});

function fnCalculateInvestedValue(e) {
    var $this = $(this);
    console.log("Calcular Valor Investido");
    $('#capitalInv').maskMoney({ allowNegative: false, thousands: '.', decimal: ',' });
    var valorCotacaoAtual = parseFloat($this.val()) || 0;
    var qtdeCotas = parseInt($('#qtdeCotas').val()) || 0;
    console.log(valorCotacaoAtual * qtdeCotas);
    if ((valorCotacaoAtual > 0) && (qtdeCotas > 0))
        $('#capitalInv').maskMoney('mask', $('#capitalInv').val(parseFloat(valorCotacaoAtual * qtdeCotas).toFixed(2)));
}

function fnRemoveImagem(idImagem) {
    swal({
        title: "Atenção",
        text: "Deseja deletar a imagem do Bem?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sim",
        cancelButtonText: "Não",
        closeOnConfirm: false
    }).then((result) => {

        $.ajax({
            url: '../Bens/RemoveImageUpload',
            type: 'POST',
            data: {
                idImage: idImagem
            },
            beforeSend: function () {

            },
            onsuccess: function () { },
            success: function (result) {

                if (result == true) {
                    swal("Perfeito", "Imagem removida com sucesso!", "success");
                } else { }
            }
        });

    });
}

function loadImage() {
    $('.slick_demo_1').slick({
        dots: true
    });
}

function validateFields() {
    var tipoBem = $('#ddlTipoBem option:selected').text();
    console.log(tipoBem);

    if (tipoBem == 'propriedade') {
        var cep = $('#_Endereco.Cep').val();
        var endereco = $('#_Endereco.Endereco').val();
        var numero = $('#_Endereco.Numero').val();
        var bairro = $('#_Endereco.Bairro').val();
        var uf = $('#_Endereco.UF').val();
        var cidade = $('#_Endereco.Cidade').val();
        if (cep == null || endereco == null || numero == null || bairro == null || uf == null || cidade)
            alert("É necessário definir os valores dos campos obrigatórios");
    }
    else {
        var codigoAcao = $('#codigoAcao').val();
        var cotacaoAtual = parseFloat($('#cotacaoAtual').val());
        var qtdeCotas = parseInt($('#qtdeCotas').val());
        if (codigoAcao === '' || isNaN(cotacaoAtual) || isNaN(qtdeCotas))
            alert("É necessário definir os valores dos campos obrigatórios");
    };
}

function btnSalvar() {

    validateFields();

    var form = $("#fmBem");
    var $tabs = $('#myTab li a');
    $tabs.each(function (key, ele) {
        validateTab(ele);
        //console.log(ele);
    });

    if (form.valid() == false) {

        swal("Atenção", "Verifique a validação dos campos!", "error");
        return false;
    } else {

        showLoading("Salvando Informações");
        $.ajax({
            url: '../Bens/Bem',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {

            },
            onsuccess: function () { },
            success: function (result) {
                hideLoading();
                if (result.statusRetorno == 'success') {

                    swal("Perfeito", "Cadastro realizado com sucesso!", "success");

                    setTimeout(function () {
                        window.location = "../Bens";
                    }, 2500);

                } else {
                    hideLoading();
                    swal("Atenção", result.statusRetorno, "error");
                }
            }
        });
    }
};

var validateTab = function (element) {

    var _element = $(element);
    var validatePane = _element.attr('data-target');

    if (validatePane != "#tab-1_2" && validatePane != "#tab-4") {

        console.log(_element);
        var isValid = $(validatePane + ' :input').valid();
        var _li = _element.parent();

        console.log(validatePane + " - " + isValid);
        if (isValid) {
            _element.removeClass("fa fa-bell");
            _element.css("color", "#A7B1C2;");
        } else {
            _element.addClass("fa fa-bell");
            _element.css("color", "#d45151;");
        }

    }
}; // end function validateTab

var idGastosBem = 0;



window.onload = function (event) {

    $('#fComprovante').on('ifUnchecked', function () {
        $("#dvAddComprovanteGastos").fadeOut(500);
    })
    $('#fComprovante').on('ifChecked', function () {
        $("#dvAddComprovanteGastos").fadeIn(500);
    })

    $('#sMesAno').attr("data-mask", "99/9999")

    $("#btnAddGastos").on("click", function (event) {

        var sDescricao = $("#DescricaoGasto").val();
        var sCustos = $("#Custos").val();
        var sData = $("#DtReferenciaCusto").val();

        if (sDescricao == '') {
            swal("Atenção", "É necessário digitar a descrição!", "error");
        } else
            if (sCustos == '') {
                swal("Atenção", "É necessário digitar o valor!", "error");
            } else
                if (sData == '') {
                    swal("Atenção", "É necessário digitar a data de referência!", "error");
                } else {
                    addNewDocumento(sDescricao, sCustos, sData);
                }
    });
};

function addNewDocumento(sDescricao, sValor, sData) {
    idGastosBem--;
    var idGastosBemNovo = idGastosBem;

    $.ajax({
        /**/
        url: '@Url.Action("SetGastosBens", "Bens")',
        /**/
        contentType: 'application/json',
        data: {
            idBemGastos: idGastosBemNovo,
            sDescricao: sDescricao,
            sValor: sValor,
            sData: sData,
            sUrlDocumento: ""
        },
        success: function (data) {

            var fInsert = data;
            if (fInsert < 0) {

                addRowTable(idGastosBemNovo, sDescricao, sValor, sData)
                $("#DescricaoGasto").val("");
                $("#Custos").val("");
                $("#DtReferenciaCusto").val("");
            } else {
                swal("Ops!", "Ocorreu um erro.", "error")
            }

        }

    });
};

function addRowTable(idGastosBemNovo, sDescricao, sValor, sData) {
    $("#table-documentos > tbody").
        append("<tr id='tr-" + idGastosBemNovo + "'><td>" + sData + "</td><td>" + sDescricao + "</td><td>" + sValor + "</td><td>Processando</td><td><a  onclick='dellBemGastos(" + idGastosBemNovo + ")'   idgastosbem='" + idGastosBemNovo + "'   href='javascript:;' class='btn btn-danger btn-xs dellTelefone' style='color:white'> Deletar <i class='fa  fa-trash-o'></i></a></td></tr>");
};


function dellBemGastos(idBemGastos) {
    alert("#tr-" + idBemGastos)
    $.ajax({
        url: '@Url.Action("deleteGastos", "Bens")',
        contentType: 'application/json',
        data: {
            idBemGastos: idBemGastos
        },
        success: function (data) {

            if (data == false) {
                swal("Ops!", "Ocorreu um erro.", "error");
            } else {
                swal("Sucesso!", "Documento apagado com Sucesso.", "success");
                $("#tr-" + idBemGastos).remove();
                idGastosBem++;
            }

        }

    });


};

var uploadergastos = new qq.FineUploader({
    debug: true,
    thumbnails: {
        customResizer: !qq.ios() && function (resizeInfo) {
            return new Promise(function (resolve, reject) {
                pica.resizeCanvas(resizeInfo.sourceCanvas, resizeInfo.targetCanvas, {}, resolve)
            })
        }
    },
    validation: {
        allowedExtensions: ['jpg', 'jpeg', 'png', 'pdf']
    },
    template: document.getElementById('qq-template-gastos'),
    element: document.getElementById('uploader-gastos'),
    request: {
        /**/
        endpoint: '../Bens/UploadFileGastos?idBem=@Model.Id&idGastos=' + idGastosBem
        /**/
    },
    deleteFile: {
        enabled: false,
        endpoint: '../Bens/UploadFile'
    },
    retry: {
        enableAuto: true
    }
})


var uploader = new qq.FineUploader({
    debug: true,
    thumbnails: {
        customResizer: !qq.ios() && function (resizeInfo) {
            return new Promise(function (resolve, reject) {
                pica.resizeCanvas(resizeInfo.sourceCanvas, resizeInfo.targetCanvas, {}, resolve)
            })
        }
    },
    validation: {
        allowedExtensions: ['jpg', 'jpeg', 'png']
    },
    template: document.getElementById('qq-template'),
    element: document.getElementById('uploader'),
    request: {
        /**/
        endpoint: '../Bens/UploadFile?idBem=@Model.Id'
        /**/
    },
    deleteFile: {
        enabled: false,
        endpoint: '../Bens/UploadFile'
    },
    retry: {
        enableAuto: true
    }
})

function onBeforeSend(formData, file) {
    // Cancel request
    if (file.name.indexOf(".jpg") < 0) {
        return false;
    }

    // Modify and return form data
    formdata.append("input_name", "input_value");

    return formData;
}


function getEndereco() {


    showLoading("One Moment Please", 'Loading...');
    $.ajax({
        timeout: 1500,
        dataType: "json",
        url: 'http://api.zippopotam.us/us/' + $("[Name='_Endereco.Cep']").val(),
        success: function (dados) {

            $("[Name='_Endereco.Estado']").val(dados.places[0]["state abbreviation"])
            $("[Name='_Endereco.Estado']").attr("readonly", true);
            $("[Name='_Endereco.Cidade']").val(dados.places[0]["place name"])
            $("[Name='_Endereco.Cidade']").attr("readonly", true);

            hideLoading();
        },
        error: function (jqXHR, exception) {
            hideLoading();
            swal("Atenção", "Nenhum endereço encontrado!", "error");
            $("[Name='_Endereco.Estado']").val('')
            $("[Name='_Endereco.Estado']").attr("readonly", false);
            $("[Name='_Endereco.Cidade']").val('')
            $("[Name='_Endereco.Cidade']").attr("readonly", false);
        }

    });



};