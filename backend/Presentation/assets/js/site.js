﻿window.history.pushState({}, document.title, "/" + "");
var fOpen = false;


var spinnerVisible = false;

$(".int").bind("keyup blur focus", function (e) {
    e.preventDefault();
    var expre = /[^\d]/g;
    $(this).val($(this).val().replace(expre, ''));
}); 
function showLoading(texto, sub='') {
    if (!spinnerVisible) {
        $("#spnLoading").html(texto);
        if (sub != '') {

            $("#spnLoadingSub").html(sub);
        }
        $("#fundoSpinner").fadeIn("fast");
        spinnerVisible = true;
    }
};

function hideLoading() {
    if (spinnerVisible) {
        var spinner = $("#fundoSpinner");
        spinner.stop();
        spinner.fadeOut("fast");
        spinnerVisible = false;
    }
};
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var strRegisterNotFound = "<div class='col-md-12' style='text-align:center; padding:50px'>" +
                                       "<img src='../../Content/img/iconInformation_64.png' />" +
                                       "<h2> Nenhum Registro Encontrado!</h2>" +
                                       "</div>";



function checkMenuPrincipal(idMenu) {
    $("#" + idMenu).trigger("click");
}

function checkSubMenu(idSubMenu) {
    $("#" + idSubMenu).addClass("active");
}


function verificaColor(hex) {

    if (hex != "") {
        hex = hex.replace("#", "");

        var r = parseInt(hex.substr(0, 2), 16);
        var g = parseInt(hex.substr(2, 2), 16);
        var b = parseInt(hex.substr(4, 2), 16);
        var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
        return (yiq >= 128) ? 'black' : 'white';


    }
}

$.validator.addMethod("email",
  function (value, element) {
      return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
  },
  "Email inválido!"
);

$.validator.addMethod('date',
        function (value, element, params) {
            if (this.optional(element)) {
                return true;
            }

            var result = true;
            try {
                $.datepicker.parseDate('dd/mm/yy', value);
            } catch (e) {
                result = false;
            }

            return result;
        }
)