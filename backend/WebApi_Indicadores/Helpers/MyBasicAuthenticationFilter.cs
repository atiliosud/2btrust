﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;

namespace WebApi_Indicadores.Helpers
{
    public class MyBasicAuthenticationFilter : BasicAuthenticationFilter
    {

        public MyBasicAuthenticationFilter()
        { }

        public MyBasicAuthenticationFilter(bool active)
            : base(active)
        { }


        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            //Authorization: Basic QWNjZXNzMkJ0cnVzdEFQSTpAMkJUcnVzdDI=
            if (username == "Access2BtrustAPI" && password == "@2BTrust2")
                return true;

            return false;
        }
    }
}