﻿using Data.Repository;
using Domain.Entities;
using Domain.Entities._2BTrust;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Web;

namespace WebApi_Indicadores.Helpers
{
    public static class EmailSend
    {

        public static bool SendMailCadastroInicial(Pessoas pessoa, bool fResidente)
        {

            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];


                string urlValidaEmailCadastro = System.Configuration.ConfigurationManager.AppSettings["urlValidaEmailCadastro"];


                string Assunto = "Account Confirmation";

                var mail = new MailMessage();
                mail.To.Add(pessoa._Usuario.Email);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item, "[2bTrust] - Investments", System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", "[2bTrust] - Investments", System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);
                string complementoMail = "";
                if (!fResidente)
                    complementoMail = "Follow in attachment the Certificate of Foreign Status of Beneficial Owner for United  States Tax Withholding and Reporting (W - 8) to be filled.";
                else
                    complementoMail = "Follow attached the request for Taxpayer identification number(w - 9) to be filled.";


                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocation"]));

                html.GetElementbyId("spnBoasVindas").InnerHtml = string.Format(@"Thank you for your pre-registration. We have received your information and are in the process of reviewing. Within 24-48 hours we will be notifying you or your acceptance or if we need any further information to process your application. " );

                html.GetElementbyId("btnConfirmacao").InnerHtml = string.Format(" <a href=" + urlValidaEmailCadastro + "/CadastreAqui/ValidaEmail?hash=" + pessoa._Usuario.Saltkey + " class='btn-primary'>Verify Account</a>");

                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;






                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                //ADICIONAR ARQUIVO A SER ENVIADO COMO ANEXO
                //if (!fResidente)
                //    mail.Attachments.Add(new Attachment(System.Web.Hosting.HostingEnvironment.MapPath("~/Documentos/fw8ben.pdf")));
                //else
                //    mail.Attachments.Add(new Attachment(System.Web.Hosting.HostingEnvironment.MapPath("~/Documentos/W-9.pdf")));

                try
                {
                    client.Send(mail);

                    LogEmailApp app = new LogEmailApp()
                    {
                        DataEnvio = DateTime.Now,
                        FlagAtivo = true,
                        IdPessoa = pessoa.Id,
                        Titulo = "[Home] . 2bTrust - Investments",
                        Mensagem = "It is an honor to have you as an Investor. <br /> We are here to make sure take we will meet your needs and your complete satisfaction is our first priority. " + complementoMail,

                    };
                     
                    PessoasRepository _pessoaRepository = new PessoasRepository();
                    _pessoaRepository.InsertLogEmailApp(app);

                    return true;
                }
                catch (SmtpException ex)
                {
                    return false;
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                throw;
            }


        }

        public static void SendMailRecoveryPassword(RecoveryPassword obj, string sEmail)
        {
            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];


                string urlSistemaLogin = System.Configuration.ConfigurationManager.AppSettings["urlSistemaLogin"];


                string Assunto = "2bTrust - Investments";

                var mail = new MailMessage();
                mail.To.Add(sEmail);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item, "[2bTrust] - Investments", System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", "[2bTrust] - Investments", System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);

                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocationRecovery"]));

                html.GetElementbyId("spnBoasVindas").InnerHtml = string.Format(@"Hello, you just requested to change your password. Click the link below and you will be redirected to a password recovery screen where you can enter your new password...");

                html.GetElementbyId("spnDown").InnerHtml = string.Format("");


                html.GetElementbyId("btnConfirmacao").InnerHtml = string.Format(" <a href='" + urlSistemaLogin + "/Auth/Recovery?token=" + obj.Token + "' class='btn-primary'>RECOVERY</a>");

                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;


                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                try
                {
                    client.Send(mail);

                }
                catch (SmtpException ex)
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static void SendMailCadastroEntradaPool(Pessoas pessoa, PoolBensPessoas poolBens, PoolBens pool)
        {
            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];


                string urlSistemaLogin = System.Configuration.ConfigurationManager.AppSettings["urlSistemaLogin"];


                string Assunto = "2bTrust - Investments";

                var mail = new MailMessage();
                mail.To.Add(pessoa._Usuario.Email);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item, "[2bTrust] - Investments", System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", "[2bTrust] - Investments", System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);

                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocationEntrada"]));
                //html.GetElementbyId("spnBoasVindas").InnerHtml = string.Format(@"Parabéns <b>" + pessoa.Nome + @"</b>.<br />
                //                                <b> 2bTrust - Investments</b> lhe agradece por escolher nossos serviços.<br />
                //                                Você acaba de entrar no <b>" + pool.Nome + @"</b>, comprando <b>" + poolBens.Cota + @"</b> cotas no valor de <b>" + poolBens.mValorCotas.ToString("c") + @"</b>.
                //                                A partir de agora, seu dinheiro começará a render todo o mês referente ao(s) seu(s) Pool(s). ");
                html.GetElementbyId("spnBoasVindas").InnerHtml = string.Format(@"Congratulations " + pessoa.Nome + ". " +
                    "Thank you for trusting on our company. From now on your investment will work for you.You just purchased " + (int)poolBens.Cota + " units from our portfolio " + pool.Nome + ".");

                html.GetElementbyId("spnDown").InnerHtml = string.Format("");


                html.GetElementbyId("btnConfirmacao").InnerHtml = string.Format(" <a href='" + urlSistemaLogin + "' class='btn-primary'>Access platform</a>");

                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;


                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                try
                {
                    client.Send(mail);

                    LogEmailApp app = new LogEmailApp()
                    {
                        DataEnvio = DateTime.Now,
                        FlagAtivo = true,
                        IdPessoa = pessoa.Id,
                        Titulo = "2bTrust - Investments",
                        Mensagem = "Congratulations " + pessoa.Nome + ". " +
                    "Thank you for trusting on our company. From now on your investment will work for you.You just purchased " + (int)poolBens.Cota + " units from our portfolio " + pool.Nome + ".",

                    };

                    PessoasRepository _pessoaRepository = new PessoasRepository();
                    _pessoaRepository.InsertLogEmailApp(app);
                }
                catch (SmtpException ex)
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public static void SendMailDeposito(ComprovanteDeposito comp)
        {
            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];


                string urlSistemaLogin = System.Configuration.ConfigurationManager.AppSettings["urlSistemaLogin"];


                string Assunto = "2bTrust - Investments";

                var mail = new MailMessage();
                mail.To.Add(comp.EmailCliente);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item, "[2bTrust] - Investments", System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", "[2bTrust] - Investments", System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);

                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocationDeposito"]));

                html.GetElementbyId("spnIdentificacao").InnerHtml = string.Format("Client: " + comp.NomeCliente);
                html.GetElementbyId("spnValor").InnerHtml = string.Format("Amount: " + comp.Valor.ToString("c"));

                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;


                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                try
                {
                    client.Send(mail);

                }
                catch (SmtpException ex)
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public static void SendMailDepositoSystem(ComprovanteDeposito comp)
        {
            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];
                string TO = System.Configuration.ConfigurationManager.AppSettings["TO"];


                string urlSistemaLogin = System.Configuration.ConfigurationManager.AppSettings["urlSistemaLogin"];


                string Assunto = "2bTrust - Investments";

                var mail = new MailMessage();
                mail.To.Add(TO);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item, "[2bTrust] - Investments", System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", "[2bTrust] - Investments", System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);

                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocationDepositoSystem"]));


                html.GetElementbyId("spnIdentificacao").InnerHtml = string.Format("Client: " + comp.NomeCliente);
                html.GetElementbyId("spnValor").InnerHtml = string.Format("Amount: " + comp.Valor.ToString("c"));


                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;
                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                try
                {
                    client.Send(mail);

                    PessoasRepository _pessoaRepository = new PessoasRepository();
                    var carteira = _pessoaRepository.GetCarteiraByIdCarteira(comp.IdCarteira);
                    LogEmailApp app = new LogEmailApp()
                    {
                        DataEnvio = DateTime.Now,
                        FlagAtivo = true,
                        IdPessoa = carteira.IdPessoa,
                        Titulo = "[Deposit] 2bTrust - Investments",
                        Mensagem = "Deposit of "+ comp.Valor.ToString("c") + " made with success",

                    };

                    _pessoaRepository.InsertLogEmailApp(app);
                }
                catch (SmtpException ex)
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public static void SendMailRetirada(ComprovanteRetirada retr)
        {
            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];


                string urlSistemaLogin = System.Configuration.ConfigurationManager.AppSettings["urlSistemaLogin"];


                string Assunto = "2bTrust - Investments";

                var mail = new MailMessage();
                mail.To.Add(retr.EmailCliente);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item, "[2bTrust] - Investments", System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", "[2bTrust] - Investments", System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);

                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocationRetirada"]));


                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;


                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                try
                {
                    client.Send(mail);

                    PessoasRepository _pessoaRepository = new PessoasRepository();
                    var carteira = _pessoaRepository.GetCarteiraByIdCarteira(retr.IdCarteira);
                    LogEmailApp app = new LogEmailApp()
                    {
                        DataEnvio = DateTime.Now,
                        FlagAtivo = true,
                        IdPessoa = carteira.IdPessoa,
                        Titulo = "[Deposit] 2bTrust - Investments",
                        Mensagem = "Withdrawal of "+retr.Valor.ToString("c")+" completed. If you have any questions about our services please let us know. Thank you for confidence.",

                    };

                    _pessoaRepository.InsertLogEmailApp(app);
                }
                catch (SmtpException ex)
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                throw;
            }

        }



        public static void SendMailValidaUsuario(string sEmail, bool isValid)
        {
            try
            {
                string EmailSender = System.Configuration.ConfigurationManager.AppSettings["From"];
                string EmailSenderName = System.Configuration.ConfigurationManager.AppSettings["EmailSenderName"];
                string EmailSenderPass = System.Configuration.ConfigurationManager.AppSettings["Senha"];

                string Host = System.Configuration.ConfigurationManager.AppSettings["SmtpCliente"];
                int iPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Porta"]);
                string CCOS = System.Configuration.ConfigurationManager.AppSettings["CCOS"];


                string urlSistemaLogin = System.Configuration.ConfigurationManager.AppSettings["urlSistemaLogin"];


                string Assunto = "2bTrust - [Accept Process]";

                if (!isValid)
                {
                    Assunto = "2bTrust - [Rejection Process]";
                }


                var mail = new MailMessage();
                mail.To.Add(sEmail);

                mail.From = new MailAddress(EmailSender, EmailSenderName, System.Text.Encoding.UTF8);

                mail.Subject = Assunto;

                string ccoS = CCOS;
                var listaCCOS = ccoS.Split(';');

                foreach (var item in listaCCOS)
                {
                    if (!string.IsNullOrEmpty(item))
                        mail.Bcc.Add(new MailAddress(item,Assunto, System.Text.Encoding.UTF8));
                }

                mail.Bcc.Add(new MailAddress("marcus@2btrust.com", Assunto, System.Text.Encoding.UTF8));

                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                string assemblyFile = (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;
                assemblyFile = Path.GetDirectoryName(assemblyFile);

                var html = new HtmlDocument();
                html.LoadHtml(File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["mailBodyLocationValidation"]));


                if(isValid)
                {
                    html.GetElementbyId("btnConfirmacao").InnerHtml = string.Format(" <a href=" + urlSistemaLogin + " class='btn-primary'>Access Account</a>");

                    html.GetElementbyId("spnTitle").InnerHtml = string.Format(@"Acceptance Email");
                    html.GetElementbyId("spnConteudo").InnerHtml = string.Format("Welcome to 2bTrust, It is an honor to have you as an Investor. Meeting your needs and ensuring your complete satisfaction is our number one priority. We have reviewed your information and are glad to announce that you have been selected to participate in our fund. Please click the link below to continue with your registration: *");
                }
                else
                {

                    html.GetElementbyId("spnTitle").InnerHtml = string.Format(@"Rejection Email");
                    html.GetElementbyId("spnConteudo").InnerHtml = string.Format("Thank you for your interest in 2bTrust. We have reviewed the information you have provided. At this time, we are unable to continue with your application process. If you have any questions, please feel free to reach out to us at contact@2btrust.com");
                }



              
                mail.IsBodyHtml = true;
                mail.Body = html.DocumentNode.InnerHtml;


                //Adicionando as credenciais da conta do remetente
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(EmailSender, EmailSenderPass); // <<<<<<<<<<<< colocar email e senha do gmail

                client.Port = iPort; // Esta porta é a utilizada pelo Gmail para envio

                client.Host = Host; //Definindo o provedor que irá disparar o e-mail

                client.EnableSsl = true; //Gmail trabalha com Server Secured Layer

                // client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;


                try
                {
                    client.Send(mail);

                }
                catch (SmtpException ex)
                {
                    throw ex;
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}