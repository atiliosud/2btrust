﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class PoolsBensController : BaseApiController
    {

        private readonly IPoolBensAppServices _poolBensAPP;
        private readonly ILancamentosAppServices _lancamentosAPP;
        public PoolsBensController(IPoolBensAppServices poolBensAPP, ILancamentosAppServices lancamentosAPP)
        {
            _lancamentosAPP = lancamentosAPP;
            _poolBensAPP = poolBensAPP;
        }

        [Route("api/PoolsBens/GetAllLancamentos")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.Lancamentos> GetAllLancamentos()
        {
            return _lancamentosAPP.GetListLancamentos();
        }

        [Route("api/PoolsBens/GetPessoasPoolsInvestido")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.PessoaPoolsInvestidos> GetPessoasPoolsInvestido()
        {
            return _lancamentosAPP.GetPessoasPoolsInvestido();
        }

        [Route("api/PoolsBens/GetPessoasPoolsInvestidoById")]
        [HttpPost]
        public PessoaPoolsInvestidos GetPessoasPoolsInvestidoById(int id)
        {
            return _lancamentosAPP.GetPessoasPoolsInvestido(id);
        }


        [Route("api/PoolsBens/InsertLancamento")]
        [HttpPut]
        public Domain.Entities._2BTrust.Lancamentos InsertLancamento(Domain.Entities._2BTrust.Lancamentos lancamento)
        {
            return _lancamentosAPP.InsertLancamentos(lancamento);
        }




        [Route("api/PoolsBens/GetListPoolBens")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.PoolBens> GetListPoolBens()
        {
            return _poolBensAPP.GetListPoolBens();
        }


        [Route("api/PoolsBens/GetPoolBensById")]
        [HttpPost]
        public Domain.Entities._2BTrust.PoolBens GetPoolBensById(int idPoolBem)
        {
            return _poolBensAPP.GetPoolBensById(idPoolBem);
        }

        [Route("api/PoolsBens/EditPoolBens")]
        [HttpPut]
        public Domain.Entities._2BTrust.PoolBens EditPoolBens(Domain.Entities._2BTrust.PoolBens pools)
        {
            return _poolBensAPP.UpdatePoolBens(pools);
        }

        [Route("api/PoolsBens/InsertPoolBens")]
        [HttpPut]
        public Domain.Entities._2BTrust.PoolBens InsertContrato(Domain.Entities._2BTrust.PoolBens pools)
        {
            return _poolBensAPP.InsertPoolBens(pools);
        }


        [Route("api/PoolsBens/InsertPoolsBensRentabilidadePerformance")]
        [HttpPut]
        public PoolsBensRentabilidadePerformance InsertPoolsBensRentabilidadePerformance(PoolsBensRentabilidadePerformance pools)
        {
            return _poolBensAPP.InsertPoolsBensRentabilidadePerformance(pools);
        }

        [Route("api/PoolsBens/DesativarAtivarPoolsBensRentabilidadePerformance")]
        [HttpPut]
        public bool DesativarAtivarPoolsBensRentabilidadePerformance(int id, bool flgAtivo)
        {
            return _poolBensAPP.DesativarAtivarPoolsBensRentabilidadePerformance(id, flgAtivo);
        }









    }
}
