﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class Bens_VisaoGeralController : BaseApiController
    {

        private readonly IBensVisaoGeralAppServices _bensVisaoGeralApp;

        public Bens_VisaoGeralController(IBensVisaoGeralAppServices bensVisaoGeralApp)
        {
            _bensVisaoGeralApp = bensVisaoGeralApp;
        }

        [Route("api/Bens_VisaoGeral/GetListBensVisaoGeral")]
        [HttpPost]
        public IEnumerable<BensVisaoGeral> GetListBensVisaoGeral()
        {
            return _bensVisaoGeralApp.GetListBensVisaoGeral();
        }


        [Route("api/Bens_VisaoGeral/GetBensVisaoGeralById")]
        [HttpPost]
        public Domain.Entities._2BTrust.BensVisaoGeral GetBensById(int idBem)
        {
            return _bensVisaoGeralApp.GetBensVisaoGeralById(idBem);
        }

        [Route("api/Bens_VisaoGeral/EditBemVisaoGeral")]
        [HttpPut]
        public Domain.Entities._2BTrust.BensVisaoGeral EditBem(Domain.Entities._2BTrust.BensVisaoGeral bem)
        {
            return _bensVisaoGeralApp.UpdateBensVisaoGeral(bem);
        }

        [Route("api/Bens_VisaoGeral/InsertBemVisaoGeral")]
        [HttpPut]
        public Domain.Entities._2BTrust.BensVisaoGeral InsertBem(Domain.Entities._2BTrust.BensVisaoGeral bem)
        {
            return _bensVisaoGeralApp.InsertBensVisaoGeral(bem);
        }


    }
}
