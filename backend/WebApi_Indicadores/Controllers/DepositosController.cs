﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class DepositosController : BaseApiController
    {
        
        private readonly IPessoasAppServices _pessoaAppServices;

        public DepositosController(IPessoasAppServices pessoaAppServices)
        {
            _pessoaAppServices = pessoaAppServices;
        }

        [Route("api/Depositos/GetListComprovanteDepositoPendentes")]
        [HttpPost]
        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoPendentes()
        {
            return _pessoaAppServices.GetListComprovanteDepositoPendentes();
        }

        [Route("api/Depositos/GetComprovanteDepositoById")]
        [HttpPost]
        public ComprovanteDeposito GetComprovanteDepositoById(int id)
        {
            return _pessoaAppServices.GetComprovanteDepositoById(id);
        }
        


    }
}
