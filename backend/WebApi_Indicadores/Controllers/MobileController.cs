﻿using ApplicationServices.IApplicationServices;
using CriptoODGT.Class;
using Domain.Entities;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class MobileController : BaseApiController
    {
        private readonly IUsuariosAppServices _usuariosApp;
        private readonly IPessoasAppServices _pessoasApp;
        private readonly IPoolBensAppServices _poolsBensApp;

        public MobileController(IPoolBensAppServices poolsBensApp, IUsuariosAppServices usuariosApp, IPessoasAppServices pessoasApp)
        {
            _usuariosApp = usuariosApp;
            _pessoasApp = pessoasApp;
            _poolsBensApp = poolsBensApp;
        }

        [Route("api/Mobile/getLogin")]
        [HttpPost]
        public Pessoas getLogin(string sLogin, string sSenha, string sDevice)
        {
            try
            {
                string noSenha = Cripto.DecryptString("hnmGeNYZ3v7cdoKdCi8+1g==", ReadString("ChaveCripto"));
                if (string.IsNullOrEmpty(noSenha)) { }
                sSenha = Cripto.EncryptString(sSenha, ReadString("ChaveCripto"));



                var pessoa = _pessoasApp.LoginUsuario(sLogin, sSenha);
                if (pessoa != null)
                {
                    pessoa._Usuario._Controles = null;
                    pessoa._ValidToken = _pessoasApp.createTokenAcessMobile(pessoa._Usuario.Id, sDevice);
                    return pessoa;
                }
                else
                {
                    return pessoa;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [Route("api/Mobile/GetDadosPessoaById")]
        [HttpPost]
        public Pessoas GetDadosPessoaById(int idPessoa)
        {
            try
            {
                
                var pessoa = _pessoasApp.GetDadosPessoaById(idPessoa);
                if (pessoa != null)
                {
                    pessoa._Usuario._Controles = null;
                    pessoa._ValidToken = _pessoasApp.createTokenAcessMobile(pessoa._Usuario.Id, "Android");
                    return pessoa;
                }
                else
                {
                    return pessoa;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [Route("api/Mobile/getLoginSimples")]
        [HttpPost]
        public Pessoas getLoginSimples(string sLogin, string sSenha, string sDevice)
        {
            try
            {
                string noSenha = Cripto.DecryptString("hnmGeNYZ3v7cdoKdCi8+1g==", ReadString("ChaveCripto"));
                if (string.IsNullOrEmpty(noSenha)) { }
                sSenha = Cripto.EncryptString(sSenha, ReadString("ChaveCripto"));



                var pessoa = _pessoasApp.LoginUsuarioSimples(sLogin, sSenha);
                if (pessoa != null)
                {
                    pessoa._Usuario._Controles = null;
                    pessoa._ValidToken = _pessoasApp.createTokenAcessMobile(pessoa._Usuario.Id, sDevice);
                    return pessoa;
                }
                else
                {
                    return pessoa;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

      
        [Route("api/Mobile/GetPessoaMeusLancamentos")]
        [HttpPost]
        public Pessoas GetPessoaMeusLancamentos(int idPessoa)
        {
            try
            { 
                var pessoa = _pessoasApp.GetPessoaMeusLancamentos(idPessoa);

                return pessoa; 
            }
            catch (Exception ex)
            {

                throw;
            }
        }



        [Route("api/Mobile/GetRentabilidadeByIdPoolBens")]
        [HttpPost]
        public IEnumerable<PoolsBensRentabilidadePerformance> GetRentabilidadeByIdPoolBens(int idPoolBem)
        {
            try
            {
                var pessoa = _poolsBensApp.GetListPoolsBensRentabilidadePerformance(idPoolBem);

                return pessoa;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [Route("api/Mobile/GetPessoaMeusPools")]
        [HttpPost]
        public Pessoas GetPessoaMeusPools(int idPessoa)
        {
            try
            {
                var pessoa = _pessoasApp.GetPessoaMeusPools(idPessoa);

                return pessoa;
            }
            catch (Exception ex)
            {

                throw;
            }
        }




        [Route("api/Mobile/GetInfoHomeValores")]
        [HttpPost]
        public Pessoas GetInfoHomeValores(int idUsuario, string sHash)
        {
            try
            {

                var pessoa = _pessoasApp.InformacoaHomeValoresMobile(idUsuario, sHash);
                if (pessoa != null)
                {
                    var valorBarraMobile = ReadString("MaxBarProjection");

                    pessoa.MaxBarProjection = valorBarraMobile;
                    return pessoa;
                }
                else
                {
                    return pessoa;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [Route("api/Mobile/ValidHash")]
        [HttpPost]
        public ValidToken ValidHash(int idUsuario, string sHash)
        {
            try
            {

                var valido = _pessoasApp.validaTokenAcessMobile(idUsuario, sHash);
                if (valido == true)
                {

                    return new ValidToken()
                    {
                        FlagAtivo = true,
                        HashValid = sHash
                    };
                }
                else
                {
                    return new ValidToken()
                    {
                        FlagAtivo = false,
                        HashValid = sHash
                    };
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }



        [Route("api/Mobile/ListaEmailLogApp")]
        [HttpPost]
        public IEnumerable<LogEmailApp> ListaEmailLogApp(int idPessoa)
        {
            return _pessoasApp.GetListLogEmailApp(idPessoa);
        }


        [Route("api/Mobile/DeleteLogEmailApp")]
        [HttpPut]
        public bool DeleteLogEmailApp(int idLog)
        {
            return _pessoasApp.DeleteLogEmailApp(idLog);
        }



    }
}
