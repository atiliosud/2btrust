﻿using ApplicationServices.IApplicationServices;
using CriptoODGT.Class;
using Domain.Entities;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Indicadores.Helpers;

namespace WebApi_Indicadores.Controllers
{

    public class AuthController : BaseApiController
    {
        private readonly IUsuariosAppServices _usuariosApp;
        private readonly IPessoasAppServices _pessoasApp;
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof(AuthController));


        public AuthController(IUsuariosAppServices usuariosApp, IPessoasAppServices  pessoasApp)
        {
            _usuariosApp = usuariosApp;
            _pessoasApp = pessoasApp;
        }

        [Route("api/Auth/getPass")]
        [HttpPost]
        public string getPass(string sSenha)
        {
            string noSenha ="";
            noSenha = Cripto.DecryptString("PJ+NEA5xSYu2pxA4CZTaNg==", ReadString("ChaveCripto"));
            noSenha = Cripto.DecryptString("jyZGx24kAZ60LjGfLxR3dw==", ReadString("ChaveCripto"));
            return noSenha;
        }

        [Route("api/Auth/getLogin")]
        public Pessoas getLogin(string sLogin, string sSenha)
        {
            try
            {
                _log.Info("getLogin 1: " + sLogin);
                sSenha = Cripto.EncryptString(sSenha, ReadString("ChaveCripto"));
                return _pessoasApp.LoginUsuario(sLogin, sSenha);
            }
            catch (Exception ex)
            {
                _log.Error("getLogin 2: " + ex.Message + " - " + ex.StackTrace);
                throw;
            }
        }


        [Route("api/Auth/getClientInvests")]
        public IEnumerable<Pessoas> getClientInvests(int idPessoa)
        {
            try
            {
                
                return _pessoasApp.getClientInvests(idPessoa);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [Route("api/Auth/ReceveryPasswordByEmail")]
        [HttpPost]
        public RecoveryPassword ReceveryPasswordByEmail(string sEmail)
        {
             
            var obj = _pessoasApp.ReceveryPasswordByEmail(sEmail);

            if(obj.Id>0)
            {
                EmailSend.SendMailRecoveryPassword(obj, sEmail);
                return obj;
            }
            else
            {
                return obj;
            }

        }

        [Route("api/Auth/RecoveryPasswordValidaToken")]
        [HttpPost]
        public bool RecoveryPasswordValidaToken(string sToken)
        {
            return _pessoasApp.RecoveryPasswordValidaToken(sToken);
        }

        [Route("api/Auth/ReceveryPasswordUpdate")]
        [HttpPost]
        public bool ReceveryPasswordUpdate(int idUsuario, string password, string sToken)
        {
            password = Cripto.EncryptString(password, ReadString("ChaveCripto"));
            return _pessoasApp.ReceveryPasswordUpdate(idUsuario, password, sToken);
        }

    }
}
