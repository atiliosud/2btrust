﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class CidadesController : BaseApiController
    {

        private readonly ICidadesAppServices _cidadesAppServices;

        public CidadesController(ICidadesAppServices  cidadesAppServices)
        {
            _cidadesAppServices = cidadesAppServices;
        }


        [Route("api/Cidades/GetCidadesByEstado")]
        [HttpPost]
        public IEnumerable<Cidades> GetCidadesByEstado(int idEstado)
        {
            return _cidadesAppServices.GetListCidadesByIdEstado(idEstado);
        }

        [Route("api/Cidades/GetCidadesByCidade")]
        [HttpPost]
        public Cidades GetCidadesByCidade(int idCidade)
        {
            return _cidadesAppServices.GetListCidadesByIdCidade(idCidade);
        }

        [Route("api/Cidades/InsertCidade")]
        [HttpPut]
        public Cidades InsertCidade(Cidades cid)
        {
            return _cidadesAppServices.InsertCidade(cid);
        }




    }
}
 