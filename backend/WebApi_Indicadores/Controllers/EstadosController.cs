﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class EstadosController : BaseApiController
    {
        private readonly IEstadosAppServices _estadosAppServices;
        public EstadosController(IEstadosAppServices estadosAppServices)
        {
            _estadosAppServices = estadosAppServices;
        }

        [Route("api/Estados/GetEstados")]
        [HttpPost]
        public IEnumerable<Estados> GetEstados()
        {
            return _estadosAppServices.GetListEstados();
        }

    }
}
