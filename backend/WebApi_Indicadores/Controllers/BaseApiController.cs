﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Indicadores.Helpers;

namespace WebApi_Indicadores.Controllers
{
    [MyBasicAuthenticationFilter]
    public class BaseApiController : ApiController
    {
        public static string ReadString(string key)
        {
            try
            {
                System.Configuration.AppSettingsReader rd = new System.Configuration.AppSettingsReader();
                return rd.GetValue(key, typeof(String)).ToString();
            }
            catch (Exception ex)
            {
                
                return string.Empty;
            }
        }

        public ResponseJson retorno = new ResponseJson();
    }

    public class ResponseJson
    {
        public string statusRetorno { get; set; }

        public bool possuiDados { get; set; }

        public dynamic objetoRetorno { get; set; }
    }


 
}
