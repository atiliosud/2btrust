﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Indicadores.Helpers;

namespace WebApi_Indicadores.Controllers
{
    public class InvestimentoController : BaseApiController
    {
        private readonly IPoolBensAppServices _poolBensAppservices;
        private readonly IPoolBensPessoasAppServices _poolBensPessoasAppservices;
        private readonly IPessoasAppServices _pessoasAppservices;

        public InvestimentoController(IPessoasAppServices pessoasAppservices, IPoolBensAppServices poolBensAppservices, IPoolBensPessoasAppServices poolBensPessoasAppservices)
        {
            _pessoasAppservices = pessoasAppservices;
            _poolBensAppservices = poolBensAppservices;
            _poolBensPessoasAppservices = poolBensPessoasAppservices;
        }



        [Route("api/Investimento/GetListPoolParaInvestimento")]
        [HttpPost]
        public IEnumerable<PoolBens> GetListPoolParaInvestimento()
        {
            var pools = _poolBensAppservices.GetListPoolParaInvestimento();

            return pools;
        }


        [Route("api/Investimento/GetListMeusPoolInvestidos")]
        [HttpPost]
        public IEnumerable<PoolBensPessoas> GetListMeusPoolInvestidos(int idPessoa)
        {
            var pools = _poolBensAppservices.GetListMeusPoolInvestidos(idPessoa);

            return (IEnumerable<PoolBensPessoas>)pools;
        }


        [Route("api/Investimento/GetDetailsPoolParaInvestimentoById")]
        [HttpPost]
        public PoolBens GetDetailsPoolParaInvestimentoById(int id)
        {
            var pools = _poolBensAppservices.GetDetailsPoolParaInvestimentoById(id);

            return pools;
        }

        [Route("api/Investimento/SolicitarEntradaPoolBens")]
        [HttpPut]
        public PoolBensPessoas SolicitarEntradaPoolBens(PoolBensPessoas pes)
        {
            var entrada = _poolBensPessoasAppservices.InsertPoolBensPessoas(pes);
         

            return entrada;
        }


        [Route("api/Investimento/UpdateSolicitarEntradaPoolBens")]
        [HttpPut]
        public PoolBensPessoas UpdateSolicitarEntradaPoolBens(PoolBensPessoas pes)
        {
            var entrada = _poolBensPessoasAppservices.UpdatePoolBensPessoas(pes);

            if (entrada != null && entrada.IdStatus == (int)EnumInvestimento.CompraAprovada)
            {
                var pessoa = _pessoasAppservices.GetPessoasById(pes.IdPessoa);
                var poolBens = _poolBensAppservices.GetPoolBensById(pes.IdPoolBem);
                EmailSend.SendMailCadastroEntradaPool(pessoa, pes, poolBens);
            }


            return entrada;
        }
        [Route("api/Investimento/GetHistoricoByIdPessoa")]
        [HttpPost]
        public IEnumerable<HistoricoCarteira> GetHistoricoByIdPessoa(int idPessoa)
        {
            return _pessoasAppservices.GetHistoricoByIdPessoa(idPessoa);
        }

        [Route("api/Investimento/GetCarteiraLogByIdPessoa")]
        [HttpPost]
        public IEnumerable<CarteiraLog> GetCarteiraLogByIdPessoa(int idPessoa)
        {
            return _pessoasAppservices.GetCarteiraLogByIdPessoa(idPessoa);
        }


        [Route("api/Investimento/SolicitarVendaPoolBens")]
        [HttpPut]
        public PoolBensPessoas SolicitarVendaPoolBens(PoolBensPessoas pes)
        {
            var entrada = _poolBensPessoasAppservices.InsertPoolBensPessoas(pes);
            if (entrada != null)
            {
             //   var pessoa = _pessoasAppservices.GetPessoasById(pes.IdPessoa);
              //  var poolBens = _poolBensAppservices.GetPoolBensById(pes.IdPoolBem);
               // EmailSend.SendMailCadastroEntradaPool(pessoa, pes, poolBens);
            }


            return entrada;
        }



        [Route("api/Investimento/GetPoolBensPessoaById")]
        [HttpPost]
        public PoolBensPessoas GetPoolBensPessoaById(int id)
        {
            var pools = _poolBensPessoasAppservices.GetPoolBensPessoasById(id);

            return pools;
        }

        [Route("api/Investimento/UpdatePoolBensPessoa")]
        [HttpPut]
        public PoolBensPessoas UpdatePoolBensPessoa(PoolBensPessoas pbp)
        {
            var pools = _poolBensPessoasAppservices.UpdatePoolBensPessoas(pbp);

            return pools;
        }





        //--------------------------------VALIDAÇÔES-----------------------------------


        [Route("api/Investimento/GetListValidaVendaCota")]
        [HttpPost]
        public IEnumerable<Pessoas> GetListValidaVendaCota()
        {
            var pools = _poolBensPessoasAppservices.GetListValidaVendaCota();

            return pools;
        }

        [Route("api/Investimento/GetListValidaCompraCota")]
        [HttpPost]
        public IEnumerable<Pessoas> GetListValidaCompraCota()
        {
            var pools = _poolBensPessoasAppservices.GetListValidaCompraCota();

            return pools;
        }


        [Route("api/Investimento/GetListValidaRetirada")]
        [HttpPost]
        public IEnumerable<Pessoas> GetListValidaRetirada()
        {
            var pools = _poolBensPessoasAppservices.GetListValidaRetirada();

            return pools;
        }
    }
}
