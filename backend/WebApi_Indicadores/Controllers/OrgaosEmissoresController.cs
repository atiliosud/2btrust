﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class OrgaosEmissoresController : ApiController
    {

        private readonly IOrgaosEmissoresAppServices _orgaosEmissoresApp;

        public OrgaosEmissoresController(IOrgaosEmissoresAppServices  orgaosEmissoresApp)
        {
            _orgaosEmissoresApp = orgaosEmissoresApp;
        }

        [Route("api/OrgaosEmissores/GetListOrgaosEmissoresByIdEstado")]
        [HttpPost]
        public IEnumerable<OrgaosEmissores> GetListOrgaosEmissoresByIdEstado(int idEstado)
        {
            return _orgaosEmissoresApp.GetListOrgaosEmissoresByIdEstado(idEstado);
        }
    }
}
