﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class DocumentosController : ApiController
    {
        private readonly IDocumentosAppServices _documentosAppServices;

        public DocumentosController(IDocumentosAppServices documentosAppServices)
        {
            _documentosAppServices = documentosAppServices;
        }

        [Route("api/Documentos/GetDocumentos")]
        [HttpPost]
        public IEnumerable<Documentos> GetDocumentos()
        {
            return _documentosAppServices.GetListDocumentos();
        }

    }
}
