﻿using ApplicationServices.IApplicationServices;
using CriptoODGT.Class;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Indicadores.Helpers;

namespace WebApi_Indicadores.Controllers
{
    public class PessoaController : BaseApiController
    {

        private readonly IPessoasAppServices _pessoaAppServices;

        public PessoaController(IPessoasAppServices pessoaAppServices)
        {
            _pessoaAppServices = pessoaAppServices;
        }


        [Route("api/Pessoa/GetPessoasById")]
        [HttpPost]
        public Pessoas GetPessoasById(int idPessoa)
        {
            var pessoa = _pessoaAppServices.GetPessoasById(idPessoa);
            if (pessoa._Usuario != null)
            {
                if (pessoa._Usuario._Login != null)
                    pessoa._Usuario._Login.Senha = Cripto.DecryptString(pessoa._Usuario._Login.Senha, ReadString("ChaveCripto"));
            }

            return pessoa;
        }

        [Route("api/Pessoa/GetListPessoasIndex")]
        [HttpPost]
        public IEnumerable<Pessoas> GetListPessoasIndex(bool fAll)
        {
            return _pessoaAppServices.GetListPessoasIndex(fAll);
        }



        [Route("api/Pessoa/GetListPessoas")]
        [HttpPost]
        public IEnumerable<Pessoas> GetListPessoas(bool fAll)
        {
            return _pessoaAppServices.GetListPessoas(fAll);
        }

        [Route("api/Pessoa/GetListReduzidoPessoas")]
        [HttpPost]
        public IEnumerable<PessoasGenricList> GetListReduzidoPessoas()
        {
            return _pessoaAppServices.GetListReduzidoPessoas();
        }



        [Route("api/Pessoa/AtualizaDadosPessoa")]
        [HttpPost]
        public Pessoas AtualizaDadosPessoa(int idPessoa)
        {


            return _pessoaAppServices.AtualizaDadosPessoa(idPessoa);
        }

        [Route("api/Pessoa/PutCadastroPessoaInicial")]
        [HttpPut]
        public Pessoas PutCadastroPessoaInicial(Pessoas pessoa)
        {
            pessoa._Usuario._Login.Senha = Cripto.EncryptString(pessoa._Usuario._Login.Senha, ReadString("ChaveCripto"));
            pessoa._Usuario.Saltkey = pessoa.Nome.Substring(0, 2) + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
            var fInsert = _pessoaAppServices.CadastroInicial(pessoa);
            if (fInsert)
            {
                EmailSend.SendMailCadastroInicial(pessoa, pessoa.FlagResidenteAmericado);

                return new Pessoas() { Id = 1 };
            }


            return new Pessoas() { Id = 0 };
        }

        [Route("api/Pessoa/PutCadastroPessoaMenu")]
        [HttpPut]
        public Pessoas PutCadastroPessoaMenu(Pessoas pessoa)
        {
            if (!string.IsNullOrEmpty(pessoa._Usuario._Login.Senha))
            {
                pessoa._Usuario._Login.Senha = Cripto.EncryptString(pessoa._Usuario._Login.Senha, ReadString("ChaveCripto"));
                pessoa._Usuario.Saltkey = pessoa.Nome.Substring(0, 2) + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
            }

            var fUpdate = _pessoaAppServices.UpdatePessoaMenu(pessoa);



            return new Pessoas() { Id = 0 };
        }

        [Route("api/Pessoa/PutCadastroPessoaEnderecoMenu")]
        [HttpPut]
        public Pessoas PutCadastroPessoaEnderecoMenu(Pessoas pessoa)
        {

            var fUpdate = _pessoaAppServices.UpdatePessoaMenuEndereco(pessoa);

            return new Pessoas() { Id = 0 };
        }

        [Route("api/Pessoa/PutCadastroPessoaDocumentoMenu")]
        [HttpPut]
        public Pessoas PutCadastroPessoaDocumentoMenu(Pessoas pessoa)
        {

            var fUpdate = _pessoaAppServices.UpdatePessoaMenuDocumento(pessoa);

            return new Pessoas() { Id = 0 };
        }

        [Route("api/Pessoa/PutValidaCadastroInicial")]
        [HttpPost]
        public ReturnGeneric PutValidaCadastroInicial(string saltKey)
        {
            //liberar no pessoa idStatus = 1 e flag Bloqueado em usuario
            try
            {

                if (_pessoaAppServices.LiberacaoViaEmail(saltKey))
                {
                    return new ReturnGeneric()
                    {
                        Descricao = "Liberação realizada com sucesso",
                        isError = false
                    };
                }
                else
                {
                    return new ReturnGeneric()
                    {
                        Descricao = "Erro ao liberar usuário",
                        isError = true
                    };
                }

            }
            catch (Exception ex)
            {
                return new ReturnGeneric()
                {
                    Exception = ex.ToString(),
                    Descricao = "Ocorreu um erro!",
                    isError = true
                };
            }
        }

        [Route("api/Pessoa/PutValidaUsuarioInicial")]
        [HttpPost]
        public Pessoas PutValidaUsuarioInicial(string sUsuario)
        {
            return _pessoaAppServices.PutValidaUsuarioInicial(sUsuario);
        }


        [Route("api/Pessoa/PutValidaEmailInicial")]
        [HttpPost]
        public Pessoas PutValidaEmailInicial(string sEmail)
        {
            return _pessoaAppServices.PutValidaEmailInicial(sEmail);
        }



        [Route("api/Pessoa/PutValidaCPFInicial")]
        [HttpPost]
        public Pessoas PutValidaCPFInicial(string sCPF)
        {
            return _pessoaAppServices.PutValidaCPFInicial(sCPF);
        }

        [Route("api/Pessoa/PutValidaTAXInicial")]
        [HttpPost]
        public Pessoas PutValidaTAXInicial(string sTAX)
        {
            return _pessoaAppServices.PutValidaTAXInicial(sTAX);
        }

        [Route("api/Pessoa/PutValidaSocialInicial")]
        [HttpPost]
        public Pessoas PutValidaSocialInicial(string sSocial)
        {
            return _pessoaAppServices.PutValidaSocialInicial(sSocial);
        }

        [Route("api/Pessoa/GetCarteiraByIdPessoa")]
        [HttpPost]
        public Pessoas GetCarteiraByIdPessoa(int idPessoa)
        {
            var pessoa = _pessoaAppServices.GetPessoasById(idPessoa);
            if (pessoa != null)
            {
                var carteira = _pessoaAppServices.GetCarteiraByIdPessoa(idPessoa);
                pessoa._Carteira = carteira;
            }


            return pessoa;
        }


        /// //////////////////////////////////////Deposito


        [Route("api/Pessoa/GetListComprovanteDepositoByIdCarteira")]
        [HttpPost]
        public IEnumerable<ComprovanteDeposito> GetListComprovanteDepositoByIdCarteira(int idCarteira)
        {
            return _pessoaAppServices.GetListComprovanteDepositoByIdCarteira(idCarteira);
        }


        [Route("api/Pessoa/InsertComprovanteDeposito")]
        [HttpPut]
        public ComprovanteDeposito InsertComprovanteDeposito(ComprovanteDeposito comp)
        {
            var retorno = _pessoaAppServices.InsertComprovanteDeposito(comp);
            if (retorno != null)
            {
                EmailSend.SendMailDeposito(retorno);
                EmailSend.SendMailDepositoSystem(retorno);

                return retorno;
            }
            else
            {
                return retorno;
            }
        }

        [Route("api/Pessoa/UpdateComprovanteDeposito")]
        [HttpPut]
        public ComprovanteDeposito UpdateComprovanteDeposito(ComprovanteDeposito comp)
        {
            return _pessoaAppServices.UpdateComprovanteDeposito(comp);
        }


        [Route("api/Pessoa/GetComprovanteDepositoById")]
        [HttpPost]
        public ComprovanteDeposito GetComprovanteDepositoById(int id)
        {
            return _pessoaAppServices.GetComprovanteDepositoById(id);
        }






        /// //////////////////////////////////////retirada


        [Route("api/Pessoa/GetListComprovanteRetiradaByIdCarteira")]
        [HttpPost]
        public IEnumerable<ComprovanteRetirada> GetListComprovanteRetiradaByIdCarteira(int idCarteira)
        {
            return _pessoaAppServices.GetListComprovanteRetiradaByIdCarteira(idCarteira);
        }


        [Route("api/Pessoa/InsertComprovanteRetirada")]
        [HttpPut]
        public ComprovanteRetirada InsertComprovanteRetirada(ComprovanteRetirada comp)
        {
            var retorno = _pessoaAppServices.InsertComprovanteRetirada(comp);
            if (retorno != null)
            {
                //  EmailSend.SendMailRetirada(retorno);

                return retorno;
            }
            else
            {
                return retorno;
            }
        }

        [Route("api/Pessoa/UpdateComprovanteRetirada")]
        [HttpPut]
        public ComprovanteRetirada UpdateComprovanteRetirada(ComprovanteRetirada comp)
        {
            var retorno = _pessoaAppServices.UpdateComprovanteRetirada(comp);
            if (retorno != null)
            {
                EmailSend.SendMailRetirada(comp);
            }

            return retorno;
        }


        [Route("api/Pessoa/GetComprovanteRetiradaById")]
        [HttpPost]
        public ComprovanteRetirada GetComprovanteRetiradaById(int id)
        {
            return _pessoaAppServices.GetComprovanteRetiradaById(id);
        }

        [Route("api/Pessoa/GetListRetiradaPendentes")]
        [HttpPost]
        public IEnumerable<ComprovanteRetirada> GetListRetiradaPendentes()
        {
            return _pessoaAppServices.GetListComprovanteRetiradaPendentes();
        }





        [Route("api/Pessoa/InsertHelpInterno")]
        [HttpPut]
        public HelpInterno InsertHelpInterno(HelpInterno help)
        {
            return _pessoaAppServices.InsertHelpInterno(help);
        }

        [Route("api/Pessoa/GetListHelpinterno")]
        [HttpPost]
        public IEnumerable<HelpInterno> GetListHelpinterno()
        {
            return _pessoaAppServices.GetListHelpinterno();
        }

        [Route("api/Pessoa/GetHelpinternoById")]
        [HttpPost]
        public HelpInterno GetHelpinternoById(int id)
        {
            return _pessoaAppServices.GetHelpinternoById(id);
        }
    }
}
