﻿using ApplicationServices.IApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class AreasController : BaseApiController
    {
        private readonly IAreasAppServices _areasAppServices;

        public AreasController(IAreasAppServices areasAppServices)
        {
            _areasAppServices = areasAppServices;
        }

        [Route("api/Areas/GetListAreas")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.Areas> GetListAreas()
        {
            return _areasAppServices.GetListAreas();
        }


        [Route("api/Areas/GetAreasById")]
        [HttpPost]
        public Domain.Entities._2BTrust.Areas GetAreasById(int idArea)
        {
            return _areasAppServices.GetAreasById(idArea);
        }

        [Route("api/Areas/EditAreas")]
        [HttpPut]
        public Domain.Entities._2BTrust.Areas EditAreas(Domain.Entities._2BTrust.Areas areas)
        {
            return _areasAppServices.UpdateAreas(areas);
        }

        [Route("api/Areas/InsertAreas")]
        [HttpPut]
        public Domain.Entities._2BTrust.Areas InsertAreas(Domain.Entities._2BTrust.Areas areas)
        {
            return _areasAppServices.InsertAreas(areas);
        }

    }
}
