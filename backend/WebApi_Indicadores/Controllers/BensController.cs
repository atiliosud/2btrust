﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class BensController : BaseApiController
    {

        private readonly IBensAppServices _bensApp;

        public BensController(IBensAppServices  bensApp)
        {
            _bensApp = bensApp;
        }

        [Route("api/Bens/GetListBens")]
        [HttpPost]
        public IEnumerable<Bens> GetListBens()
        {
            return _bensApp.GetListBens();
        }


        [Route("api/Bens/GetBensById")]
        [HttpPost]
        public Domain.Entities._2BTrust.Bens GetBensById(int idBem)
        {
            return _bensApp.GetBensById(idBem);
        }

        [Route("api/Bens/EditBem")]
        [HttpPut]
        public Domain.Entities._2BTrust.Bens EditBem(Domain.Entities._2BTrust.Bens bem)
        {
            return _bensApp.UpdateBens(bem);
        }

        [Route("api/Bens/InsertBem")]
        [HttpPut]
        public Domain.Entities._2BTrust.Bens InsertBem(Domain.Entities._2BTrust.Bens bem)
        {
            return _bensApp.InsertBens(bem);
        }


    }
}
