﻿using Domain.Entities;
using Domain.Entities.EYE;
using Domain.Entities.Vortice;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Indicadores.Helpers;

namespace WebApi_Indicadores.Controllers
{
    [MyBasicAuthenticationFilter]
    public class UsuariosController : ApiController
    {
        //private readonly IUsuarioAppServices _usuarioApp;

        //public UsuariosController(IUsuarioAppServices usuarioApp)
        //{
        //    _usuarioApp = usuarioApp;
        //}

        //[HttpGet]
        //public IEnumerable<Usuario> GetList()
        //{
        //    try
        //    {
        //        return _usuarioApp.GetAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpGet]
        //[Route("api/Usuarios/GetUsuarioId")]
        //public Usuario GetUsuarioId(int id)
        //{
        //    try
        //    {
        //        return _usuarioApp.GetUsuarioId(id);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        //[HttpGet]
        //[Route("api/Usuarios/GetProfissionaisClinica")]
        //public IEnumerable<EYE_Profissionais> GetProfissionaisClinica(int idClinica)
        //{
        //    try
        //    {
        //        return _usuarioApp.GetListProfissionais(idClinica);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        //[HttpGet]
        //[Route("api/Usuarios/GetInfoProfissionalById")]
        //public EYE_Profissionais GetInfoProfissionalById(int idProfissional)
        //{
        //    try
        //    {
        //        return _usuarioApp.GetInfoProfissionalById(idProfissional);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}


        //[HttpPut]
        //[Route("api/Usuarios/PutUsuario")]
        //public Usuario PutUsuarios(Usuario usuario)
        //{
        //    try
        //    {
        //        return _usuarioApp.InsertUpdate(usuario);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}

        //[HttpPut]
        //[Route("api/Usuarios/PutDisableUsuario")]
        //public Usuario PutUsuarios(DeleteUsuario usuario)
        //{
        //    return _usuarioApp.EnableDisable(usuario.idUsuario, usuario.idUsuarioAlterador, ((usuario.isDisable == 1) ? true : false));
        //}

        //[HttpPut]
        //[Route("api/Usuarios/PutLogError")]
        //public void PutLogError(LogException log)
        //{
        //    var Ex = JsonConvert.DeserializeObject<Exception>(log.ex);

        //    _usuarioApp.InsertLogErroControllers(Ex, log.controllerRepository, log.metodo);
        //}


        //[HttpPut]
        //[Route("api/Usuarios/PutLogLogin")]
        //public bool PutLogError(LogLogin log)
        //{ 
        //    _usuarioApp.InsertLogLogin(log);

        //    return true;
        //}


        //[HttpGet]
        //[Route("api/Usuarios/GetInfoLoja")]
        //public Loja GetInfoLoja(int idLoja)
        //{
        //    try
        //    {
        //        return _usuarioApp.GetInfoLoja(idLoja);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

    }

    public class DeleteUsuario
    {
        public int idUsuario { get; set; }
        public int idUsuarioAlterador { get; set; }
        public int isDisable { get; set; }
    }

    public class LogException
    {
        public string ex { get; set; }
        public string controllerRepository { get; set; }
        public string metodo { get; set; }
    }
}
