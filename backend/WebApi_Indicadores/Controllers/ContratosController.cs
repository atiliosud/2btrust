﻿using ApplicationServices.IApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class ContratosController : BaseApiController
    {

        private readonly IContratosAppServices _contratosAPP;
        public ContratosController(IContratosAppServices contratosAPP)
        {
            _contratosAPP = contratosAPP;
        }

        [Route("api/Contratos/GetListContratos")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.Contratos> GetListContratos()
        {
            return _contratosAPP.GetListContratos();
        }

        [Route("api/Contratos/GetContratosById")]
        [HttpPost]
        public Domain.Entities._2BTrust.Contratos GetContratosById(int idContrato)
        {
            return _contratosAPP.GetContratosById(idContrato);
        }

        [Route("api/Contratos/EditContrato")]
        [HttpPut]
        public Domain.Entities._2BTrust.Contratos EditContrato(Domain.Entities._2BTrust.Contratos contrato)
        {
            return _contratosAPP.UpdateContratos(contrato);
        }

        [Route("api/Contratos/InsertContrato")]
        [HttpPut]
        public Domain.Entities._2BTrust.Contratos InsertContrato(Domain.Entities._2BTrust.Contratos contrato)
        {
            return _contratosAPP.InsertContratos(contrato);
        }

    }
}
