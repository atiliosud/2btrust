﻿using ApplicationServices.IApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class ControlesController : BaseApiController
    {

        private readonly IControlesAppServices _controlesAppServices;

        public ControlesController(IControlesAppServices  controlesAppServices)
        {
            _controlesAppServices = controlesAppServices;
        }


        [Route("api/Controles/GetListControles")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.Controles> GetListControles()
        {
            return _controlesAppServices.GetListControles();
        }

        [Route("api/Controles/GetListControlesPai")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.Controles> GetListControlesPai()
        {
            return _controlesAppServices.GetListControlesPai();
        }


        [Route("api/Controles/GetControlesById")]
        [HttpPost]
        public Domain.Entities._2BTrust.Controles GetControlesById(int idControle)
        {
            return _controlesAppServices.GetControlesById(idControle);
        }

        [Route("api/Controles/EditControle")]
        [HttpPut]
        public Domain.Entities._2BTrust.Controles EditControle(Domain.Entities._2BTrust.Controles controle)
        {
            return _controlesAppServices.UpdateControles(controle);
        }

        [Route("api/Controles/InsertControle")]
        [HttpPut]
        public Domain.Entities._2BTrust.Controles InsertControle(Domain.Entities._2BTrust.Controles controle)
        {
            return _controlesAppServices.InsertControles(controle);
        }


    }
}
