﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi_Indicadores.Controllers
{
    public class GrupoAcoesController : BaseApiController
    {

        private readonly IGruposAcoesAppServices _grupoAcaoAppServices;

        public GrupoAcoesController(IGruposAcoesAppServices  grupoAcaoAppServices)
        {
            _grupoAcaoAppServices = grupoAcaoAppServices;
        }


        [Route("api/GrupoAcoes/GetListGrupoXActions")]
        [HttpPost]
        public IEnumerable<Domain.Entities._2BTrust.Grupos> GetListGrupoXActions()
        {
            return _grupoAcaoAppServices.GetListGrupoXActions();
        }


        [Route("api/GrupoAcoes/SetGrupoXActions")]
        [HttpPost]
        public  GruposAcoes SetGrupoXActions(int idGrupo, int idAcao, int idUsuario)
        {
            return _grupoAcaoAppServices.SetGrupoXActions(idGrupo, idAcao, idUsuario);
        }

    }
}
