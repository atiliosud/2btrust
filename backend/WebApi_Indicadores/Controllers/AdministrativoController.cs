﻿using ApplicationServices.IApplicationServices;
using Domain.Entities._2BTrust;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Indicadores.Helpers;

namespace WebApi_Indicadores.Controllers
{
    public class AdministrativoController : ApiController
    {
        private readonly IUsuariosAppServices _usuariosApp;
        private readonly IPessoasAppServices _pessoasApp;

        public AdministrativoController(IUsuariosAppServices usuariosApp, IPessoasAppServices pessoasApp)
        {
            _usuariosApp = usuariosApp;
            _pessoasApp = pessoasApp;
        }

        [Route("api/Administrativo/GetInformacoesAdminHome")]
        [HttpPost]
        public InformacoesAdminHome GetInformacoesAdminHome()
        {
            return _pessoasApp.GetInformacoesAdminHome();
        }

        [Route("api/Administrativo/GetListHomeInvestidores")]
        [HttpPost]
        public IEnumerable<PessoaInvestimentoPoolsValores> GetListHomeInvestidores()
        {
            return _pessoasApp.GetListHomeInvestidores();
        }




        [Route("api/Administrativo/ValidaUsuarioPlataforma")]
        [HttpPost]
        public bool ValidaUsuarioPlataforma(int idPessoa, bool isValid)
        {
            var pessoa = _pessoasApp.GetPessoasById(idPessoa);

            var fAlter = _usuariosApp.ValidaUsuarioPlataforma(pessoa._Usuario.Id, isValid);

            if(fAlter)
            {
                EmailSend.SendMailValidaUsuario(pessoa._Usuario.Email, isValid);
                return true;

            }
            return false;
        }


    }
}
